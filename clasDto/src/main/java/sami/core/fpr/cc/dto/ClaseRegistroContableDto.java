package sami.core.fpr.cc.dto;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Clase de registro Contable
 **/

public class ClaseRegistroContableDto extends AmbitosDto implements Serializable {
  

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String CLASE_REGISTRO = null;
  private String DESCRIPCION = null;
  private String ORIGEN_TRANSACCION = null;
  private String RESTRICTIVA = null;
  private String CLASE_REGISTRO_PAGO = null;
  private Long ID_NATURALEZA = null;
  private Map<String, String> AMBITOS = null;


 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get CLASE_REGISTRO
   * @return CLASE_REGISTRO
  **/
  @JsonProperty("CLASE_REGISTRO")
  public String getCLASEREGISTRO() {
    return CLASE_REGISTRO;
  }

  public void setCLASEREGISTRO(String CLASE_REGISTRO) {
    this.CLASE_REGISTRO = CLASE_REGISTRO;
  }

  public ClaseRegistroContableDto CLASE_REGISTRO(String CLASE_REGISTRO) {
    this.CLASE_REGISTRO = CLASE_REGISTRO;
    return this;
  }

 /**
   * Get DESCRIPCION
   * @return DESCRIPCION
  **/
  @JsonProperty("DESCRIPCION")
  public String getDESCRIPCION() {
    return DESCRIPCION;
  }

  public void setDESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
  }

  public ClaseRegistroContableDto DESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
    return this;
  }

 /**
   * Get ORIGEN_TRANSACCION
   * @return ORIGEN_TRANSACCION
  **/
  @JsonProperty("ORIGEN_TRANSACCION")
  public String getORIGENTRANSACCION() {
    return ORIGEN_TRANSACCION;
  }

  public void setORIGENTRANSACCION(String ORIGEN_TRANSACCION) {
    this.ORIGEN_TRANSACCION = ORIGEN_TRANSACCION;
  }

  public ClaseRegistroContableDto ORIGEN_TRANSACCION(String ORIGEN_TRANSACCION) {
    this.ORIGEN_TRANSACCION = ORIGEN_TRANSACCION;
    return this;
  }

 /**
   * Get RESTRICTIVA
   * @return RESTRICTIVA
  **/
  @JsonProperty("RESTRICTIVA")
  public String getRESTRICTIVA() {
    return RESTRICTIVA;
  }

  public void setRESTRICTIVA(String RESTRICTIVA) {
    this.RESTRICTIVA = RESTRICTIVA;
  }

  public ClaseRegistroContableDto RESTRICTIVA(String RESTRICTIVA) {
    this.RESTRICTIVA = RESTRICTIVA;
    return this;
  }

 /**
   * Get CLASE_REGISTRO_PAGO
   * @return CLASE_REGISTRO_PAGO
  **/
  @JsonProperty("CLASE_REGISTRO_PAGO")
  public String getCLASEREGISTROPAGO() {
    return CLASE_REGISTRO_PAGO;
  }

  public void setCLASEREGISTROPAGO(String CLASE_REGISTRO_PAGO) {
    this.CLASE_REGISTRO_PAGO = CLASE_REGISTRO_PAGO;
  }

  public ClaseRegistroContableDto CLASE_REGISTRO_PAGO(String CLASE_REGISTRO_PAGO) {
    this.CLASE_REGISTRO_PAGO = CLASE_REGISTRO_PAGO;
    return this;
  }

 /**
   * Get ID_NATURALEZA
   * @return ID_NATURALEZA
  **/
  @JsonProperty("ID_NATURALEZA")
  public Long getIDNATURALEZA() {
    return ID_NATURALEZA;
  }

  public void setIDNATURALEZA(Long ID_NATURALEZA) {
    this.ID_NATURALEZA = ID_NATURALEZA;
  }

  public ClaseRegistroContableDto ID_NATURALEZA(Long ID_NATURALEZA) {
    this.ID_NATURALEZA = ID_NATURALEZA;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public ClaseRegistroContableDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public ClaseRegistroContableDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }
  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public ClaseRegistroContableDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ClaseRegistroContableDto {\n");
    
    sb.append("    CLASE_REGISTRO: ").append(toIndentedString(CLASE_REGISTRO)).append("\n");
    sb.append("    DESCRIPCION: ").append(toIndentedString(DESCRIPCION)).append("\n");
    sb.append("    ORIGEN_TRANSACCION: ").append(toIndentedString(ORIGEN_TRANSACCION)).append("\n");
    sb.append("    RESTRICTIVA: ").append(toIndentedString(RESTRICTIVA)).append("\n");
    sb.append("    CLASE_REGISTRO_PAGO: ").append(toIndentedString(CLASE_REGISTRO_PAGO)).append("\n");
    sb.append("    ID_NATURALEZA: ").append(toIndentedString(ID_NATURALEZA)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  
  
  public ClaseRegistroContableDto(String cLASE_REGISTRO, String dESCRIPCION, String oRIGEN_TRANSACCION,
		String rESTRICTIVA, String cLASE_REGISTRO_PAGO, Long iD_NATURALEZA) {
	super();
	initPROPERTY_MAP(); 
	CLASE_REGISTRO = cLASE_REGISTRO;
	DESCRIPCION = dESCRIPCION;
	ORIGEN_TRANSACCION = oRIGEN_TRANSACCION;
	RESTRICTIVA = rESTRICTIVA;
	CLASE_REGISTRO_PAGO = cLASE_REGISTRO_PAGO;
	ID_NATURALEZA = iD_NATURALEZA;
}

public ClaseRegistroContableDto() {
	  initPROPERTY_MAP(); 
  }
  
  public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("CLASE_REGISTRO", "claseRegistro");
		super.PROPERTY_MAP.put("DESCRIPCION", "descripcion");
		super.PROPERTY_MAP.put("ORIGEN_TRANSACCION", "origenTransaccion");
		
		super.PROPERTY_MAP.put("RESTRICTIVA", "restrictiva");
		super.PROPERTY_MAP.put("CLASE_REGISTRO_PAGO", "claseRegistroPago");
		super.PROPERTY_MAP.put("ID_NATURALEZA", "idNaturaleza");
	}

}

