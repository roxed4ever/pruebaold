package sami.ges.dto;


import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
  * Dto para CG Beneficiario.   
 **/

public class CgBeneficiarioDto implements Serializable  {
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

  private long ID_BENEFICIARIO = 0;
  private long ID_ENTIDAD = 0;
  private long ID_UNIDAD_EJECUTORA = 0;
  private String NOMBRE = null;
  private String APELLIDO = null;
  private String DIRECCION = null;
  private String TELEFONO = null;
  private String CORREO_ELECTRONICO = null;
  private String NUMERO_IDENTIDAD = null;
  private String PUESTO = null;
  private String RTN = null;
  private String ESTADO = null;
  private Date FECHA_ESTADO = null;
  private String CUENTA_BANCARIA = null;
  private String CODIGO = null;
  private String IHSS = null;
  private String NO_PLACA = null;
  private Long ID_EMPLEADO_EXTERNO = null;
  private Date FECHA_INGRESO = null;
  private String ES_GENERICO = null;
  private String DEPARTAMENTO_ADMINISTRATIVO = null;
  private String SITUACION = null;
  private Long COD_BANCO = null;
  private Long ID_TIPO_CUENTA = null;
  private Long ID_TIPO_BENEFICIARIO = null;
 /**
   * Get ID_BENEFICIARIO
   * @return ID_BENEFICIARIO
  **/
  @JsonProperty("ID_BENEFICIARIO")
  public long getIDBENEFICIARIO() {
    return ID_BENEFICIARIO;
  }

  public void setIDBENEFICIARIO(long ID_BENEFICIARIO) {
    this.ID_BENEFICIARIO = ID_BENEFICIARIO;
  }

  public CgBeneficiarioDto ID_BENEFICIARIO(Integer ID_BENEFICIARIO) {
    this.ID_BENEFICIARIO = ID_BENEFICIARIO;
    return this;
  }

 /**
   * Get ID_ENTIDAD
   * @return ID_ENTIDAD
  **/
  @JsonProperty("ID_ENTIDAD")
  public long getIDENTIDAD() {
    return ID_ENTIDAD;
  }

  public void setIDENTIDAD(Integer ID_ENTIDAD) {
    this.ID_ENTIDAD = ID_ENTIDAD;
  }

  public CgBeneficiarioDto ID_ENTIDAD(Integer ID_ENTIDAD) {
    this.ID_ENTIDAD = ID_ENTIDAD;
    return this;
  }

 /**
   * Get ID_UNIDAD_EJECUTORA
   * @return ID_UNIDAD_EJECUTORA
  **/
  @JsonProperty("ID_UNIDAD_EJECUTORA")
  public long getIDUNIDADEJECUTORA() {
    return ID_UNIDAD_EJECUTORA;
  }

  public void setIDUNIDADEJECUTORA(Integer ID_UNIDAD_EJECUTORA) {
    this.ID_UNIDAD_EJECUTORA = ID_UNIDAD_EJECUTORA;
  }

  public CgBeneficiarioDto ID_UNIDAD_EJECUTORA(Integer ID_UNIDAD_EJECUTORA) {
    this.ID_UNIDAD_EJECUTORA = ID_UNIDAD_EJECUTORA;
    return this;
  }

 /**
   * Get NOMBRE
   * @return NOMBRE
  **/
  @JsonProperty("NOMBRE")
  public String getNOMBRE() {
    return NOMBRE;
  }

  public void setNOMBRE(String NOMBRE) {
    this.NOMBRE = NOMBRE;
  }

  public CgBeneficiarioDto NOMBRE(String NOMBRE) {
    this.NOMBRE = NOMBRE;
    return this;
  }

 /**
   * Get APELLIDO
   * @return APELLIDO
  **/
  @JsonProperty("APELLIDO")
  public String getAPELLIDO() {
    return APELLIDO;
  }

  public void setAPELLIDO(String APELLIDO) {
    this.APELLIDO = APELLIDO;
  }

  public CgBeneficiarioDto APELLIDO(String APELLIDO) {
    this.APELLIDO = APELLIDO;
    return this;
  }

 /**
   * Get DIRECCION
   * @return DIRECCION
  **/
  @JsonProperty("DIRECCION")
  public String getDIRECCION() {
    return DIRECCION;
  }

  public void setDIRECCION(String DIRECCION) {
    this.DIRECCION = DIRECCION;
  }

  public CgBeneficiarioDto DIRECCION(String DIRECCION) {
    this.DIRECCION = DIRECCION;
    return this;
  }

 /**
   * Get TELEFONO
   * @return TELEFONO
  **/
  @JsonProperty("TELEFONO")
  public String getTELEFONO() {
    return TELEFONO;
  }

  public void setTELEFONO(String TELEFONO) {
    this.TELEFONO = TELEFONO;
  }

  public CgBeneficiarioDto TELEFONO(String TELEFONO) {
    this.TELEFONO = TELEFONO;
    return this;
  }

 /**
   * Get CORREO_ELECTRONICO
   * @return CORREO_ELECTRONICO
  **/
  @JsonProperty("CORREO_ELECTRONICO")
  public String getCORREOELECTRONICO() {
    return CORREO_ELECTRONICO;
  }

  public void setCORREOELECTRONICO(String CORREO_ELECTRONICO) {
    this.CORREO_ELECTRONICO = CORREO_ELECTRONICO;
  }

  public CgBeneficiarioDto CORREO_ELECTRONICO(String CORREO_ELECTRONICO) {
    this.CORREO_ELECTRONICO = CORREO_ELECTRONICO;
    return this;
  }

 /**
   * Get NUMERO_IDENTIDAD
   * @return NUMERO_IDENTIDAD
  **/
  @JsonProperty("NUMERO_IDENTIDAD")
  public String getNUMEROIDENTIDAD() {
    return NUMERO_IDENTIDAD;
  }

  public void setNUMEROIDENTIDAD(String NUMERO_IDENTIDAD) {
    this.NUMERO_IDENTIDAD = NUMERO_IDENTIDAD;
  }

  public CgBeneficiarioDto NUMERO_IDENTIDAD(String NUMERO_IDENTIDAD) {
    this.NUMERO_IDENTIDAD = NUMERO_IDENTIDAD;
    return this;
  }

 /**
   * Get PUESTO
   * @return PUESTO
  **/
  @JsonProperty("PUESTO")
  public String getPUESTO() {
    return PUESTO;
  }

  public void setPUESTO(String PUESTO) {
    this.PUESTO = PUESTO;
  }

  public CgBeneficiarioDto PUESTO(String PUESTO) {
    this.PUESTO = PUESTO;
    return this;
  }

 /**
   * Get RTN
   * @return RTN
  **/
  @JsonProperty("RTN")
  public String getRTN() {
    return RTN;
  }

  public void setRTN(String RTN) {
    this.RTN = RTN;
  }

  public CgBeneficiarioDto RTN(String RTN) {
    this.RTN = RTN;
    return this;
  }

 /**
   * Get ESTADO
   * @return ESTADO
  **/
  @JsonProperty("ESTADO")
  public String getESTADO() {
    return ESTADO;
  }

  public void setESTADO(String ESTADO) {
    this.ESTADO = ESTADO;
  }

  public CgBeneficiarioDto ESTADO(String ESTADO) {
    this.ESTADO = ESTADO;
    return this;
  }

 /**
   * Get FECHA_ESTADO
   * @return FECHA_ESTADO
  **/
  @JsonProperty("FECHA_ESTADO")
  public Date getFECHAESTADO() {
    return FECHA_ESTADO;
  }

  public void setFECHAESTADO(Date FECHA_ESTADO) {
    this.FECHA_ESTADO = FECHA_ESTADO;
  }

  public CgBeneficiarioDto FECHA_ESTADO(Date FECHA_ESTADO) {
    this.FECHA_ESTADO = FECHA_ESTADO;
    return this;
  }

 /**
   * Get CUENTA_BANCARIA
   * @return CUENTA_BANCARIA
  **/
  @JsonProperty("CUENTA_BANCARIA")
  public String getCUENTABANCARIA() {
    return CUENTA_BANCARIA;
  }

  public void setCUENTABANCARIA(String CUENTA_BANCARIA) {
    this.CUENTA_BANCARIA = CUENTA_BANCARIA;
  }

  public CgBeneficiarioDto CUENTA_BANCARIA(String CUENTA_BANCARIA) {
    this.CUENTA_BANCARIA = CUENTA_BANCARIA;
    return this;
  }

 /**
   * Get CODIGO
   * @return CODIGO
  **/
  @JsonProperty("CODIGO")
  public String getCODIGO() {
    return CODIGO;
  }

  public void setCODIGO(String CODIGO) {
    this.CODIGO = CODIGO;
  }

  public CgBeneficiarioDto CODIGO(String CODIGO) {
    this.CODIGO = CODIGO;
    return this;
  }

 /**
   * Get IHSS
   * @return IHSS
  **/
  @JsonProperty("IHSS")
  public String getIHSS() {
    return IHSS;
  }

  public void setIHSS(String IHSS) {
    this.IHSS = IHSS;
  }

  public CgBeneficiarioDto IHSS(String IHSS) {
    this.IHSS = IHSS;
    return this;
  }

 /**
   * Get NO_PLACA
   * @return NO_PLACA
  **/
  @JsonProperty("NO_PLACA")
  public String getNOPLACA() {
    return NO_PLACA;
  }

  public void setNOPLACA(String NO_PLACA) {
    this.NO_PLACA = NO_PLACA;
  }

  public CgBeneficiarioDto NO_PLACA(String NO_PLACA) {
    this.NO_PLACA = NO_PLACA;
    return this;
  }

 /**
   * Get ID_EMPLEADO_EXTERNO
   * @return ID_EMPLEADO_EXTERNO
  **/
  @JsonProperty("ID_EMPLEADO_EXTERNO")
  public Long getIDEMPLEADOEXTERNO() {
    return ID_EMPLEADO_EXTERNO;
  }

  public void setIDEMPLEADOEXTERNO(Long ID_EMPLEADO_EXTERNO) {
    this.ID_EMPLEADO_EXTERNO = ID_EMPLEADO_EXTERNO;
  }

  public CgBeneficiarioDto ID_EMPLEADO_EXTERNO(Long ID_EMPLEADO_EXTERNO) {
    this.ID_EMPLEADO_EXTERNO = ID_EMPLEADO_EXTERNO;
    return this;
  }

 /**
   * Get FECHA_INGRESO
   * @return FECHA_INGRESO
  **/
  @JsonProperty("FECHA_INGRESO")
  public Date getFECHAINGRESO() {
    return FECHA_INGRESO;
  }

  public void setFECHAINGRESO(Date FECHA_INGRESO) {
    this.FECHA_INGRESO = FECHA_INGRESO;
  }

  public CgBeneficiarioDto FECHA_INGRESO(Date FECHA_INGRESO) {
    this.FECHA_INGRESO = FECHA_INGRESO;
    return this;
  }

 /**
   * Get ES_GENERICO
   * @return ES_GENERICO
  **/
  @JsonProperty("ES_GENERICO")
  public String getESGENERICO() {
    return ES_GENERICO;
  }

  public void setESGENERICO(String ES_GENERICO) {
    this.ES_GENERICO = ES_GENERICO;
  }

  public CgBeneficiarioDto ES_GENERICO(String ES_GENERICO) {
    this.ES_GENERICO = ES_GENERICO;
    return this;
  }

 /**
   * Get DEPARTAMENTO_ADMINISTRATIVO
   * @return DEPARTAMENTO_ADMINISTRATIVO
  **/
  @JsonProperty("DEPARTAMENTO_ADMINISTRATIVO")
  public String getDEPARTAMENTOADMINISTRATIVO() {
    return DEPARTAMENTO_ADMINISTRATIVO;
  }

  public void setDEPARTAMENTOADMINISTRATIVO(String DEPARTAMENTO_ADMINISTRATIVO) {
    this.DEPARTAMENTO_ADMINISTRATIVO = DEPARTAMENTO_ADMINISTRATIVO;
  }

  public CgBeneficiarioDto DEPARTAMENTO_ADMINISTRATIVO(String DEPARTAMENTO_ADMINISTRATIVO) {
    this.DEPARTAMENTO_ADMINISTRATIVO = DEPARTAMENTO_ADMINISTRATIVO;
    return this;
  }

 /**
   * Get SITUACION
   * @return SITUACION
  **/
  @JsonProperty("SITUACION")
  public String getSITUACION() {
    return SITUACION;
  }

  public void setSITUACION(String SITUACION) {
    this.SITUACION = SITUACION;
  }

  public CgBeneficiarioDto SITUACION(String SITUACION) {
    this.SITUACION = SITUACION;
    return this;
  }

 /**
   * Get COD_BANCO
   * @return COD_BANCO
  **/
  @JsonProperty("COD_BANCO")
  public Long getCODBANCO() {
    return COD_BANCO;
  }

  public void setCODBANCO(Long COD_BANCO) {
    this.COD_BANCO = COD_BANCO;
  }

  public CgBeneficiarioDto COD_BANCO(Long COD_BANCO) {
    this.COD_BANCO = COD_BANCO;
    return this;
  }

 /**
   * Get ID_TIPO_CUENTA
   * @return ID_TIPO_CUENTA
  **/
  @JsonProperty("ID_TIPO_CUENTA")
  public Long getIDTIPOCUENTA() {
    return ID_TIPO_CUENTA;
  }

  public void setIDTIPOCUENTA(Long ID_TIPO_CUENTA) {
    this.ID_TIPO_CUENTA = ID_TIPO_CUENTA;
  }

  public CgBeneficiarioDto ID_TIPO_CUENTA(Long ID_TIPO_CUENTA) {
    this.ID_TIPO_CUENTA = ID_TIPO_CUENTA;
    return this;
  }

 /**
   * Get ID_TIPO_BENEFICIARIO
   * @return ID_TIPO_BENEFICIARIO
  **/
  @JsonProperty("ID_TIPO_BENEFICIARIO")
  public Long getIDTIPOBENEFICIARIO() {
    return ID_TIPO_BENEFICIARIO;
  }

  public void setIDTIPOBENEFICIARIO(Long ID_TIPO_BENEFICIARIO) {
    this.ID_TIPO_BENEFICIARIO = ID_TIPO_BENEFICIARIO;
  }

  public CgBeneficiarioDto ID_TIPO_BENEFICIARIO(Long ID_TIPO_BENEFICIARIO) {
    this.ID_TIPO_BENEFICIARIO = ID_TIPO_BENEFICIARIO;
    return this;
  }
  
  
 
  public CgBeneficiarioDto() {
  }

public CgBeneficiarioDto(long iD_BENEFICIARIO, long iD_ENTIDAD, long iD_UNIDAD_EJECUTORA, String nOMBRE,
		String aPELLIDO, String dIRECCION, String tELEFONO, String cORREO_ELECTRONICO, String nUMERO_IDENTIDAD,
		String pUESTO, String rTN, String eSTADO, Date fECHA_ESTADO, String cUENTA_BANCARIA, String cODIGO,
		String iHSS, String nO_PLACA, Long iD_EMPLEADO_EXTERNO, Date fECHA_INGRESO, String eS_GENERICO,
		String dEPARTAMENTO_ADMINISTRATIVO, String sITUACION, Long cOD_BANCO, Long iD_TIPO_CUENTA,
		Long iD_TIPO_BENEFICIARIO) {
	super();
	ID_BENEFICIARIO = iD_BENEFICIARIO;
	ID_ENTIDAD = iD_ENTIDAD;
	ID_UNIDAD_EJECUTORA = iD_UNIDAD_EJECUTORA;
	NOMBRE = nOMBRE;
	APELLIDO = aPELLIDO;
	DIRECCION = dIRECCION;
	TELEFONO = tELEFONO;
	CORREO_ELECTRONICO = cORREO_ELECTRONICO;
	NUMERO_IDENTIDAD = nUMERO_IDENTIDAD;
	PUESTO = pUESTO;
	RTN = rTN;
	ESTADO = eSTADO;
	FECHA_ESTADO = fECHA_ESTADO;
	CUENTA_BANCARIA = cUENTA_BANCARIA;
	CODIGO = cODIGO;
	IHSS = iHSS;
	NO_PLACA = nO_PLACA;
	ID_EMPLEADO_EXTERNO = iD_EMPLEADO_EXTERNO;
	FECHA_INGRESO = fECHA_INGRESO;
	ES_GENERICO = eS_GENERICO;
	DEPARTAMENTO_ADMINISTRATIVO = dEPARTAMENTO_ADMINISTRATIVO;
	SITUACION = sITUACION;
	COD_BANCO = cOD_BANCO;
	ID_TIPO_CUENTA = iD_TIPO_CUENTA;
	ID_TIPO_BENEFICIARIO = iD_TIPO_BENEFICIARIO;
}

@Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CgBeneficiarioDto {\n");
    
    sb.append("    ID_BENEFICIARIO: ").append(toIndentedString(ID_BENEFICIARIO)).append("\n");
    sb.append("    ID_ENTIDAD: ").append(toIndentedString(ID_ENTIDAD)).append("\n");
    sb.append("    ID_UNIDAD_EJECUTORA: ").append(toIndentedString(ID_UNIDAD_EJECUTORA)).append("\n");
    sb.append("    NOMBRE: ").append(toIndentedString(NOMBRE)).append("\n");
    sb.append("    APELLIDO: ").append(toIndentedString(APELLIDO)).append("\n");
    sb.append("    DIRECCION: ").append(toIndentedString(DIRECCION)).append("\n");
    sb.append("    TELEFONO: ").append(toIndentedString(TELEFONO)).append("\n");
    sb.append("    CORREO_ELECTRONICO: ").append(toIndentedString(CORREO_ELECTRONICO)).append("\n");
    sb.append("    NUMERO_IDENTIDAD: ").append(toIndentedString(NUMERO_IDENTIDAD)).append("\n");
    sb.append("    PUESTO: ").append(toIndentedString(PUESTO)).append("\n");
    sb.append("    RTN: ").append(toIndentedString(RTN)).append("\n");
    sb.append("    ESTADO: ").append(toIndentedString(ESTADO)).append("\n");
    sb.append("    FECHA_ESTADO: ").append(toIndentedString(FECHA_ESTADO)).append("\n");
    sb.append("    CUENTA_BANCARIA: ").append(toIndentedString(CUENTA_BANCARIA)).append("\n");
    sb.append("    CODIGO: ").append(toIndentedString(CODIGO)).append("\n");
    sb.append("    IHSS: ").append(toIndentedString(IHSS)).append("\n");
    sb.append("    NO_PLACA: ").append(toIndentedString(NO_PLACA)).append("\n");
    sb.append("    ID_EMPLEADO_EXTERNO: ").append(toIndentedString(ID_EMPLEADO_EXTERNO)).append("\n");
    sb.append("    FECHA_INGRESO: ").append(toIndentedString(FECHA_INGRESO)).append("\n");
    sb.append("    ES_GENERICO: ").append(toIndentedString(ES_GENERICO)).append("\n");
    sb.append("    DEPARTAMENTO_ADMINISTRATIVO: ").append(toIndentedString(DEPARTAMENTO_ADMINISTRATIVO)).append("\n");
    sb.append("    SITUACION: ").append(toIndentedString(SITUACION)).append("\n");
    sb.append("    COD_BANCO: ").append(toIndentedString(COD_BANCO)).append("\n");
    sb.append("    ID_TIPO_CUENTA: ").append(toIndentedString(ID_TIPO_CUENTA)).append("\n");
    sb.append("    ID_TIPO_BENEFICIARIO: ").append(toIndentedString(ID_TIPO_BENEFICIARIO)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

