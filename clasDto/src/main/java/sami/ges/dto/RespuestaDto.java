package sami.ges.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * mensaje
 **/

public class RespuestaDto implements Serializable  {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String mensaje = "false";
 /**
   * Get mensaje
   * @return mensaje
  **/
  @JsonProperty("mensaje")
  public String getMensaje() {
    return mensaje;
  }

  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }

  public RespuestaDto mensaje(String mensaje) {
    this.mensaje = mensaje;
    return this;
  }
  
  public RespuestaDto(String mensaje) {
	super();
	this.mensaje = mensaje;
}

@Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RespuestaDto {\n");
    
    sb.append("    mensaje: ").append(toIndentedString(mensaje)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

