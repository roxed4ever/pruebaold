package clas.core.cont.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/

public class ClasesDeGastoOpcDetDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer CLASE_DE_GASTO = null;
	private String CUENTA = null;
	private String ESTADO = null;

	public ClasesDeGastoOpcDetDto() {
		initPROPERTY_MAP();
	}

	public ClasesDeGastoOpcDetDto(Integer gESTION, Integer cLASE_DE_GASTO, String cUENTA, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		CLASE_DE_GASTO = cLASE_DE_GASTO;
		CUENTA = cUENTA;
		ESTADO = eSTADO;
	}

	/**
	 **/
	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/
	@JsonProperty("CLASE_DE_GASTO")
	public Integer getCLASEDEGASTO() {
		return CLASE_DE_GASTO;
	}

	public void setCLASEDEGASTO(Integer CLASE_DE_GASTO) {
		this.CLASE_DE_GASTO = CLASE_DE_GASTO;
	}

	/**
	 **/
	@JsonProperty("CUENTA")
	public String getCUENTA() {
		return CUENTA;
	}

	public void setCUENTA(String CUENTA) {
		this.CUENTA = CUENTA;
	}

	/**
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ClasesDeGastoOpcDetDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    CLASE_DE_GASTO: ").append(StringUtil.toIndentedString(CLASE_DE_GASTO)).append("\n");
		sb.append("    CUENTA: ").append(StringUtil.toIndentedString(CUENTA)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "GESTION");
		super.PROPERTY_MAP.put("CLASE_DE_GASTO", "CLASE_DE_GASTO");
		super.PROPERTY_MAP.put("CUENTA", "CUENTA");
		super.PROPERTY_MAP.put("ESTADO", "ESTADO");
	}
	
}
