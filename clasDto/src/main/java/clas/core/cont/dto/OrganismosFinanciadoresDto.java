package clas.core.cont.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class OrganismosFinanciadoresDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer ORGANISMO = null;
	private String DESC_ORGANISMO = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	public OrganismosFinanciadoresDto() {
		initPROPERTY_MAP();
	}

	public OrganismosFinanciadoresDto(boolean pAGINAR, boolean fILTRAR_PAGINA, boolean oRDENAR_PAGINA,
			PaginacionDto pAGINACION, String pAGINACION_FILTRO, Map<String, Boolean> pAGINACION_ORDEN,
			Map<String, String> aMBITOS) {
		super(pAGINAR, fILTRAR_PAGINA, oRDENAR_PAGINA, pAGINACION, pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
		// TODO Auto-generated constructor stub
	}

	public OrganismosFinanciadoresDto(Integer oRGANISMO, String dESC_ORGANISMO, String vIGENTE, String eSTADO) {
		super();
		ORGANISMO = oRGANISMO;
		DESC_ORGANISMO = dESC_ORGANISMO;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	/**
	   **/
	@JsonProperty("ORGANISMO")
	public Integer getORGANISMO() {
		return ORGANISMO;
	}

	public void setORGANISMO(Integer ORGANISMO) {
		this.ORGANISMO = ORGANISMO;
	}

	/**
	 **/
	@JsonProperty("DESC_ORGANISMO")
	public String getDESCORGANISMO() {
		return DESC_ORGANISMO;
	}

	public void setDESCORGANISMO(String DESC_ORGANISMO) {
		this.DESC_ORGANISMO = DESC_ORGANISMO;
	}

	/**
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OrganismosFinanciadoresDto {\n");

		sb.append("    ORGANISMO: ").append(StringUtil.toIndentedString(ORGANISMO)).append("\n");
		sb.append("    DESC_ORGANISMO: ").append(StringUtil.toIndentedString(DESC_ORGANISMO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    PAGINAR: ").append(StringUtil.toIndentedString(super.isPAGINAR())).append("\n");
		sb.append("    FILTRAR_PAGINA: ").append(StringUtil.toIndentedString(super.isFILTRAR_PAGINA())).append("\n");
		sb.append("    ORDENAR_PAGINA: ").append(StringUtil.toIndentedString(super.isORDENAR_PAGINA())).append("\n");
		sb.append("    PAGINACION_FILTRO: ").append(StringUtil.toIndentedString(super.getPAGINACION_FILTRO()))
				.append("\n");
		sb.append("    PAGINACION_ORDEN: ").append(StringUtil.toIndentedString(super.getPAGINACION_ORDEN()))
				.append("\n");
		sb.append("    AMBITOS: ").append(StringUtil.toIndentedString(super.getAMBITOS())).append("\n");
		sb.append("    PAGINACION: ").append(StringUtil.toIndentedString(super.getPAGINACION())).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.setPROPERTY_MAP(new HashMap<>());
		super.PROPERTY_MAP.put("ORGANISMO", "ORGANISMO");
		super.PROPERTY_MAP.put("DESC_ORGANISMO", "DESC_ORGANISMO");
		super.PROPERTY_MAP.put("VIGENTE", "VIGENTE");
		super.PROPERTY_MAP.put("ESTADO", "ESTADO");
	}
}
