package clas.core.cont.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class FdeEntidadesDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer ENTIDAD = null;
	private String DESC_ENTIDAD = null;
	private String NATURALEZA = null;
	private Float SECTOR = null;
	private String ASIGNABLE = null;
	private String VIGENTE = null;
	private String NEW_DESC_ENTIDAD = null;
	private String NEW_SIGLA_ENTIDAD = null;
	private String NEW_NATURALEZA = null;
	private Float NEW_SECTOR = null;
	private String NEW_ASIGNABLE = null;
	private String NEW_VIGENTE = null;
	private String API_ESTADO = null;
	private String API_TRANSACCION = null;
	private String USU_CRE = null;
	private String FEC_CRE = null;
	private String USU_MOD = null;
	private String FEC_MOD = null;

	public FdeEntidadesDto() {
		initPROPERTY_MAP();
	}

	public FdeEntidadesDto(boolean pAGINAR, boolean fILTRAR_PAGINA, boolean oRDENAR_PAGINA, PaginacionDto pAGINACION,
			String pAGINACION_FILTRO, Map<String, Boolean> pAGINACION_ORDEN, Map<String, String> aMBITOS) {
		super(pAGINAR, fILTRAR_PAGINA, oRDENAR_PAGINA, pAGINACION, pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
		// TODO Auto-generated constructor stub
	}

	public FdeEntidadesDto(Integer eNTIDAD, String dESC_ENTIDAD, String nATURALEZA, Float sECTOR, String aSIGNABLE,
			String vIGENTE, String nEW_DESC_ENTIDAD, String nEW_SIGLA_ENTIDAD, String nEW_NATURALEZA, Float nEW_SECTOR,
			String nEW_ASIGNABLE, String nEW_VIGENTE, String aPI_ESTADO, String aPI_TRANSACCION, String uSU_CRE,
			String fEC_CRE, String uSU_MOD, String fEC_MOD) {
		super();
		ENTIDAD = eNTIDAD;
		DESC_ENTIDAD = dESC_ENTIDAD;
		NATURALEZA = nATURALEZA;
		SECTOR = sECTOR;
		ASIGNABLE = aSIGNABLE;
		VIGENTE = vIGENTE;
		NEW_DESC_ENTIDAD = nEW_DESC_ENTIDAD;
		NEW_SIGLA_ENTIDAD = nEW_SIGLA_ENTIDAD;
		NEW_NATURALEZA = nEW_NATURALEZA;
		NEW_SECTOR = nEW_SECTOR;
		NEW_ASIGNABLE = nEW_ASIGNABLE;
		NEW_VIGENTE = nEW_VIGENTE;
		API_ESTADO = aPI_ESTADO;
		API_TRANSACCION = aPI_TRANSACCION;
		USU_CRE = uSU_CRE;
		FEC_CRE = fEC_CRE;
		USU_MOD = uSU_MOD;
		FEC_MOD = fEC_MOD;
	}

	/**
	   **/
	@JsonProperty("ENTIDAD")
	public Integer getENTIDAD() {
		return ENTIDAD;
	}

	public void setENTIDAD(Integer ENTIDAD) {
		this.ENTIDAD = ENTIDAD;
	}

	/**
	 **/
	@JsonProperty("DESC_ENTIDAD")
	public String getDESCENTIDAD() {
		return DESC_ENTIDAD;
	}

	public void setDESCENTIDAD(String DESC_ENTIDAD) {
		this.DESC_ENTIDAD = DESC_ENTIDAD;
	}

	/**
	 **/
	@JsonProperty("NATURALEZA")
	public String getNATURALEZA() {
		return NATURALEZA;
	}

	public void setNATURALEZA(String NATURALEZA) {
		this.NATURALEZA = NATURALEZA;
	}

	/**
	 **/
	@JsonProperty("SECTOR")
	public Float getSECTOR() {
		return SECTOR;
	}

	public void setSECTOR(Float SECTOR) {
		this.SECTOR = SECTOR;
	}

	/**
	 **/
	@JsonProperty("ASIGNABLE")
	public String getASIGNABLE() {
		return ASIGNABLE;
	}

	public void setASIGNABLE(String ASIGNABLE) {
		this.ASIGNABLE = ASIGNABLE;
	}

	/**
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/
	@JsonProperty("NEW_DESC_ENTIDAD")
	public String getNEWDESCENTIDAD() {
		return NEW_DESC_ENTIDAD;
	}

	public void setNEWDESCENTIDAD(String NEW_DESC_ENTIDAD) {
		this.NEW_DESC_ENTIDAD = NEW_DESC_ENTIDAD;
	}

	/**
	 **/
	@JsonProperty("NEW_SIGLA_ENTIDAD")
	public String getNEWSIGLAENTIDAD() {
		return NEW_SIGLA_ENTIDAD;
	}

	public void setNEWSIGLAENTIDAD(String NEW_SIGLA_ENTIDAD) {
		this.NEW_SIGLA_ENTIDAD = NEW_SIGLA_ENTIDAD;
	}

	/**
	 **/
	@JsonProperty("NEW_NATURALEZA")
	public String getNEWNATURALEZA() {
		return NEW_NATURALEZA;
	}

	public void setNEWNATURALEZA(String NEW_NATURALEZA) {
		this.NEW_NATURALEZA = NEW_NATURALEZA;
	}

	/**
	 **/
	@JsonProperty("NEW_SECTOR")
	public Float getNEWSECTOR() {
		return NEW_SECTOR;
	}

	public void setNEWSECTOR(Float NEW_SECTOR) {
		this.NEW_SECTOR = NEW_SECTOR;
	}

	/**
	 **/
	@JsonProperty("NEW_ASIGNABLE")
	public String getNEWASIGNABLE() {
		return NEW_ASIGNABLE;
	}

	public void setNEWASIGNABLE(String NEW_ASIGNABLE) {
		this.NEW_ASIGNABLE = NEW_ASIGNABLE;
	}

	/**
	 **/
	@JsonProperty("NEW_VIGENTE")
	public String getNEWVIGENTE() {
		return NEW_VIGENTE;
	}

	public void setNEWVIGENTE(String NEW_VIGENTE) {
		this.NEW_VIGENTE = NEW_VIGENTE;
	}

	/**
	 **/
	@JsonProperty("API_ESTADO")
	public String getAPIESTADO() {
		return API_ESTADO;
	}

	public void setAPIESTADO(String API_ESTADO) {
		this.API_ESTADO = API_ESTADO;
	}

	/**
	 **/
	@JsonProperty("API_TRANSACCION")
	public String getAPITRANSACCION() {
		return API_TRANSACCION;
	}

	public void setAPITRANSACCION(String API_TRANSACCION) {
		this.API_TRANSACCION = API_TRANSACCION;
	}

	/**
	 **/
	@JsonProperty("USU_CRE")
	public String getUSUCRE() {
		return USU_CRE;
	}

	public void setUSUCRE(String USU_CRE) {
		this.USU_CRE = USU_CRE;
	}

	/**
	 **/
	@JsonProperty("FEC_CRE")
	public String getFECCRE() {
		return FEC_CRE;
	}

	public void setFECCRE(String FEC_CRE) {
		this.FEC_CRE = FEC_CRE;
	}

	/**
	 **/
	@JsonProperty("USU_MOD")
	public String getUSUMOD() {
		return USU_MOD;
	}

	public void setUSUMOD(String USU_MOD) {
		this.USU_MOD = USU_MOD;
	}

	/**
	 **/
	@JsonProperty("FEC_MOD")
	public String getFECMOD() {
		return FEC_MOD;
	}

	public void setFECMOD(String FEC_MOD) {
		this.FEC_MOD = FEC_MOD;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FdeEntidadesDto {\n");

		sb.append("    ENTIDAD: ").append(StringUtil.toIndentedString(ENTIDAD)).append("\n");
		sb.append("    DESC_ENTIDAD: ").append(StringUtil.toIndentedString(DESC_ENTIDAD)).append("\n");
		sb.append("    NATURALEZA: ").append(StringUtil.toIndentedString(NATURALEZA)).append("\n");
		sb.append("    SECTOR: ").append(StringUtil.toIndentedString(SECTOR)).append("\n");
		sb.append("    ASIGNABLE: ").append(StringUtil.toIndentedString(ASIGNABLE)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    NEW_DESC_ENTIDAD: ").append(StringUtil.toIndentedString(NEW_DESC_ENTIDAD)).append("\n");
		sb.append("    NEW_SIGLA_ENTIDAD: ").append(StringUtil.toIndentedString(NEW_SIGLA_ENTIDAD)).append("\n");
		sb.append("    NEW_NATURALEZA: ").append(StringUtil.toIndentedString(NEW_NATURALEZA)).append("\n");
		sb.append("    NEW_SECTOR: ").append(StringUtil.toIndentedString(NEW_SECTOR)).append("\n");
		sb.append("    NEW_ASIGNABLE: ").append(StringUtil.toIndentedString(NEW_ASIGNABLE)).append("\n");
		sb.append("    NEW_VIGENTE: ").append(StringUtil.toIndentedString(NEW_VIGENTE)).append("\n");
		sb.append("    API_ESTADO: ").append(StringUtil.toIndentedString(API_ESTADO)).append("\n");
		sb.append("    API_TRANSACCION: ").append(StringUtil.toIndentedString(API_TRANSACCION)).append("\n");
		sb.append("    USU_CRE: ").append(StringUtil.toIndentedString(USU_CRE)).append("\n");
		sb.append("    FEC_CRE: ").append(StringUtil.toIndentedString(FEC_CRE)).append("\n");
		sb.append("    USU_MOD: ").append(StringUtil.toIndentedString(USU_MOD)).append("\n");
		sb.append("    FEC_MOD: ").append(StringUtil.toIndentedString(FEC_MOD)).append("\n");
		sb.append("    PAGINAR: ").append(StringUtil.toIndentedString(super.isPAGINAR())).append("\n");
		sb.append("    FILTRAR_PAGINA: ").append(StringUtil.toIndentedString(super.isFILTRAR_PAGINA())).append("\n");
		sb.append("    ORDENAR_PAGINA: ").append(StringUtil.toIndentedString(super.isORDENAR_PAGINA())).append("\n");
		sb.append("    PAGINACION_FILTRO: ").append(StringUtil.toIndentedString(super.getPAGINACION_FILTRO()))
				.append("\n");
		sb.append("    PAGINACION_ORDEN: ").append(StringUtil.toIndentedString(super.getPAGINACION_ORDEN()))
				.append("\n");
		sb.append("    AMBITOS: ").append(StringUtil.toIndentedString(super.getAMBITOS())).append("\n");
		sb.append("    PAGINACION: ").append(StringUtil.toIndentedString(super.getPAGINACION())).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.setPROPERTY_MAP(new HashMap<>());
		super.PROPERTY_MAP.put("ENTIDAD", "ENTIDAD");
		super.PROPERTY_MAP.put("DESC_ENTIDAD", "DESC_ENTIDAD");
		super.PROPERTY_MAP.put("NATURALEZA", "NATURALEZA");
		super.PROPERTY_MAP.put("SECTOR", "SECTOR");
		super.PROPERTY_MAP.put("ASIGNABLE", "ASIGNABLE");
		super.PROPERTY_MAP.put("VIGENTE", "VIGENTE");
		super.PROPERTY_MAP.put("NEW_DESC_ENTIDAD", "NEW_DESC_ENTIDAD");
		super.PROPERTY_MAP.put("NEW_SIGLA_ENTIDAD", "NEW_SIGLA_ENTIDAD");
		super.PROPERTY_MAP.put("NEW_NATURALEZA", "NEW_NATURALEZA");
		super.PROPERTY_MAP.put("NEW_SECTOR", "NEW_SECTOR");
		super.PROPERTY_MAP.put("NEW_ASIGNABLE", "NEW_ASIGNABLE");
		super.PROPERTY_MAP.put("NEW_VIGENTE", "NEW_VIGENTE");
		super.PROPERTY_MAP.put("API_ESTADO", "API_ESTADO");
		super.PROPERTY_MAP.put("API_TRANSACCION", "API_TRANSACCION");
		super.PROPERTY_MAP.put("USU_CRE", "USU_CRE");
		super.PROPERTY_MAP.put("FEC_CRE", "FEC_CRE");
		super.PROPERTY_MAP.put("USU_MOD", "USU_MOD");
		super.PROPERTY_MAP.put("ESTADO", "ESTADO");
	}
}
