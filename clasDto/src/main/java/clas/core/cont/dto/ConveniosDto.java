package clas.core.cont.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class ConveniosDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String NRO_CONVENIO = null;
	private String DESC_CONVENIO = null;
	private String NOTA_PRIORIDAD = null;
	private String FONDOS_NACIONALES = null;
	private String ESTADO = null;

	public ConveniosDto() {
		initPROPERTY_MAP();
	}

	public ConveniosDto(boolean pAGINAR, boolean fILTRAR_PAGINA, boolean oRDENAR_PAGINA, PaginacionDto pAGINACION,
			String pAGINACION_FILTRO, Map<String, Boolean> pAGINACION_ORDEN, Map<String, String> aMBITOS) {
		super(pAGINAR, fILTRAR_PAGINA, oRDENAR_PAGINA, pAGINACION, pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
		// TODO Auto-generated constructor stub
	}

	public ConveniosDto(String nRO_CONVENIO, String dESC_CONVENIO, String nOTA_PRIORIDAD, String fONDOS_NACIONALES,
			String eSTADO) {
		super();
		NRO_CONVENIO = nRO_CONVENIO;
		DESC_CONVENIO = dESC_CONVENIO;
		NOTA_PRIORIDAD = nOTA_PRIORIDAD;
		FONDOS_NACIONALES = fONDOS_NACIONALES;
		ESTADO = eSTADO;
	}

	/**
	   **/
	@JsonProperty("NRO_CONVENIO")
	public String getNROCONVENIO() {
		return NRO_CONVENIO;
	}

	public void setNROCONVENIO(String NRO_CONVENIO) {
		this.NRO_CONVENIO = NRO_CONVENIO;
	}

	/**
	 **/
	@JsonProperty("DESC_CONVENIO")
	public String getDESCCONVENIO() {
		return DESC_CONVENIO;
	}

	public void setDESCCONVENIO(String DESC_CONVENIO) {
		this.DESC_CONVENIO = DESC_CONVENIO;
	}

	/**
	 **/
	@JsonProperty("NOTA_PRIORIDAD")
	public String getNOTAPRIORIDAD() {
		return NOTA_PRIORIDAD;
	}

	public void setNOTAPRIORIDAD(String NOTA_PRIORIDAD) {
		this.NOTA_PRIORIDAD = NOTA_PRIORIDAD;
	}

	/**
	 **/
	@JsonProperty("FONDOS_NACIONALES")
	public String getFONDOSNACIONALES() {
		return FONDOS_NACIONALES;
	}

	public void setFONDOSNACIONALES(String FONDOS_NACIONALES) {
		this.FONDOS_NACIONALES = FONDOS_NACIONALES;
	}

	/**
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ConveniosDto {\n");

		sb.append("    NRO_CONVENIO: ").append(StringUtil.toIndentedString(NRO_CONVENIO)).append("\n");
		sb.append("    DESC_CONVENIO: ").append(StringUtil.toIndentedString(DESC_CONVENIO)).append("\n");
		sb.append("    NOTA_PRIORIDAD: ").append(StringUtil.toIndentedString(NOTA_PRIORIDAD)).append("\n");
		sb.append("    FONDOS_NACIONALES: ").append(StringUtil.toIndentedString(FONDOS_NACIONALES)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    PAGINAR: ").append(StringUtil.toIndentedString(super.isPAGINAR())).append("\n");
		sb.append("    FILTRAR_PAGINA: ").append(StringUtil.toIndentedString(super.isFILTRAR_PAGINA())).append("\n");
		sb.append("    ORDENAR_PAGINA: ").append(StringUtil.toIndentedString(super.isORDENAR_PAGINA())).append("\n");
		sb.append("    PAGINACION_FILTRO: ").append(StringUtil.toIndentedString(super.getPAGINACION_FILTRO()))
				.append("\n");
		sb.append("    PAGINACION_ORDEN: ").append(StringUtil.toIndentedString(super.getPAGINACION_ORDEN()))
				.append("\n");
		sb.append("    AMBITOS: ").append(StringUtil.toIndentedString(super.getAMBITOS())).append("\n");
		sb.append("    PAGINACION: ").append(StringUtil.toIndentedString(super.getPAGINACION())).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.setPROPERTY_MAP(new HashMap<>());
		super.PROPERTY_MAP.put("GESTION", "GESTION");
		super.PROPERTY_MAP.put("CLASE_DE_GASTO", "CLASE_DE_GASTO");
		super.PROPERTY_MAP.put("DESC_CLASE_DE_GASTO", "DESC_CLASE_DE_GASTO");
		super.PROPERTY_MAP.put("ESTADO", "ESTADO");
	}
}
