package clas.core.cont.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

public class ClasesDeGastoCipDetDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer CLASE_DE_GASTO = null;
	private String OBJETO = null;
	private String ESTADO = null;

	public ClasesDeGastoCipDetDto() {
		initPROPERTY_MAP();
	}

	public ClasesDeGastoCipDetDto(Integer gESTION, Integer cLASE_DE_GASTO, String oBJETO, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		CLASE_DE_GASTO = cLASE_DE_GASTO;
		OBJETO = oBJETO;
		ESTADO = eSTADO;
	}

	/**
	 **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/
	@JsonProperty("CLASE_DE_GASTO")
	public Integer getCLASEDEGASTO() {
		return CLASE_DE_GASTO;
	}

	public void setCLASEDEGASTO(Integer CLASE_DE_GASTO) {
		this.CLASE_DE_GASTO = CLASE_DE_GASTO;
	}

	/**
	 **/
	@JsonProperty("OBJETO")
	public String getOBJETO() {
		return OBJETO;
	}

	public void setOBJETO(String OBJETO) {
		this.OBJETO = OBJETO;
	}

	/**
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ClasesDeGastoCipDetDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    CLASE_DE_GASTO: ").append(StringUtil.toIndentedString(CLASE_DE_GASTO)).append("\n");
		sb.append("    OBJETO: ").append(StringUtil.toIndentedString(OBJETO)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");

		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "GESTION");
		super.PROPERTY_MAP.put("CLASE_DE_GASTO", "CLASE_DE_GASTO");
		super.PROPERTY_MAP.put("OBJETO", "OBJETO");
		super.PROPERTY_MAP.put("ETAPA_BIP", "ETAPA_BIP");//
		super.PROPERTY_MAP.put("ESTADO", "ESTADO");
	}
}
