package clas.core.cont.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class FuentesFinanciamientoDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer ORGANISMO = null;
	private Integer GRUPO_FUENTE = null;
	private Integer SUB_GRUPO_FUENTE = null;
	private String DESC_FUENTE = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	public FuentesFinanciamientoDto() {
		initPROPERTY_MAP();
	}

	public FuentesFinanciamientoDto(boolean pAGINAR, boolean fILTRAR_PAGINA, boolean oRDENAR_PAGINA,
			PaginacionDto pAGINACION, String pAGINACION_FILTRO, Map<String, Boolean> pAGINACION_ORDEN,
			Map<String, String> aMBITOS) {
		super(pAGINAR, fILTRAR_PAGINA, oRDENAR_PAGINA, pAGINACION, pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
		// TODO Auto-generated constructor stub
	}

	public FuentesFinanciamientoDto(Integer oRGANISMO, Integer gRUPO_FUENTE, Integer sUB_GRUPO_FUENTE,
			String dESC_FUENTE, String vIGENTE, String eSTADO) {
		super();
		ORGANISMO = oRGANISMO;
		GRUPO_FUENTE = gRUPO_FUENTE;
		SUB_GRUPO_FUENTE = sUB_GRUPO_FUENTE;
		DESC_FUENTE = dESC_FUENTE;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	/**
	   **/
	@JsonProperty("ORGANISMO")
	public Integer getORGANISMO() {
		return ORGANISMO;
	}

	public void setORGANISMO(Integer ORGANISMO) {
		this.ORGANISMO = ORGANISMO;
	}

	/**
	 **/
	@JsonProperty("GRUPO_FUENTE")
	public Integer getGRUPOFUENTE() {
		return GRUPO_FUENTE;
	}

	public void setGRUPOFUENTE(Integer GRUPO_FUENTE) {
		this.GRUPO_FUENTE = GRUPO_FUENTE;
	}

	/**
	 **/
	@JsonProperty("SUB_GRUPO_FUENTE")
	public Integer getSUBGRUPOFUENTE() {
		return SUB_GRUPO_FUENTE;
	}

	public void setSUBGRUPOFUENTE(Integer SUB_GRUPO_FUENTE) {
		this.SUB_GRUPO_FUENTE = SUB_GRUPO_FUENTE;
	}

	/**
	 **/
	@JsonProperty("DESC_FUENTE")
	public String getDESCFUENTE() {
		return DESC_FUENTE;
	}

	public void setDESCFUENTE(String DESC_FUENTE) {
		this.DESC_FUENTE = DESC_FUENTE;
	}

	/**
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FuentesFinanciamientoDto {\n");

		sb.append("    ORGANISMO: ").append(StringUtil.toIndentedString(ORGANISMO)).append("\n");
		sb.append("    GRUPO_FUENTE: ").append(StringUtil.toIndentedString(GRUPO_FUENTE)).append("\n");
		sb.append("    SUB_GRUPO_FUENTE: ").append(StringUtil.toIndentedString(SUB_GRUPO_FUENTE)).append("\n");
		sb.append("    DESC_FUENTE: ").append(StringUtil.toIndentedString(DESC_FUENTE)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    PAGINAR: ").append(StringUtil.toIndentedString(super.isPAGINAR())).append("\n");
		sb.append("    FILTRAR_PAGINA: ").append(StringUtil.toIndentedString(super.isFILTRAR_PAGINA())).append("\n");
		sb.append("    ORDENAR_PAGINA: ").append(StringUtil.toIndentedString(super.isORDENAR_PAGINA())).append("\n");
		sb.append("    PAGINACION_FILTRO: ").append(StringUtil.toIndentedString(super.getPAGINACION_FILTRO()))
				.append("\n");
		sb.append("    PAGINACION_ORDEN: ").append(StringUtil.toIndentedString(super.getPAGINACION_ORDEN()))
				.append("\n");
		sb.append("    AMBITOS: ").append(StringUtil.toIndentedString(super.getAMBITOS())).append("\n");
		sb.append("    PAGINACION: ").append(StringUtil.toIndentedString(super.getPAGINACION())).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.setPROPERTY_MAP(new HashMap<>());
		super.PROPERTY_MAP.put("ORGANISMO", "ORGANISMO");
		super.PROPERTY_MAP.put("GRUPO_FUENTE", "GRUPO_FUENTE");
		super.PROPERTY_MAP.put("SUB_GRUPO_FUENTE", "SUB_GRUPO_FUENTE");
		super.PROPERTY_MAP.put("DESC_FUENTE", "DESC_FUENTE");
		super.PROPERTY_MAP.put("VIGENTE", "VIGENTE");
		super.PROPERTY_MAP.put("ESTADO", "ESTADO");
	}
}
