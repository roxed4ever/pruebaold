package clas.core.cont.dto;

import clas.util.StringUtil;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class BancoIntegradoProyectosDto extends AmbitosDto implements Serializable {

	/**
	 * 	
	 */
	private static final long serialVersionUID = 1L;
	private String BIP = null;
	private String DESC_BIP = null;
	private String TIPO_BIP = null;
	private String ETAPA_BIP = null;
	private Integer SECTOR_IP = null;
	private Integer SUB_SECTOR_IP = null;
	private Integer AREA_SECTORIAL_IP = null;
	private String NRO_CONVENIO = null;
	private String PRINCIPAL = null;
	private String VIGENTE = null;
	private String ESTADO = null;
	private Float BIP_SNIPH = null;

	public BancoIntegradoProyectosDto() {
		super();
		initPROPERTY_MAP();
	}

	public BancoIntegradoProyectosDto(String bIP, String dESC_BIP, String tIPO_BIP, String eTAPA_BIP, Integer sECTOR_IP,
			Integer sUB_SECTOR_IP, Integer aREA_SECTORIAL_IP, String nRO_CONVENIO, String pRINCIPAL, String vIGENTE,
			String eSTADO, Float bIP_SNIPH) {
		super();
		initPROPERTY_MAP();
		BIP = bIP;
		DESC_BIP = dESC_BIP;
		TIPO_BIP = tIPO_BIP;
		ETAPA_BIP = eTAPA_BIP;
		SECTOR_IP = sECTOR_IP;
		SUB_SECTOR_IP = sUB_SECTOR_IP;
		AREA_SECTORIAL_IP = aREA_SECTORIAL_IP;
		NRO_CONVENIO = nRO_CONVENIO;
		PRINCIPAL = pRINCIPAL;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
		BIP_SNIPH = bIP_SNIPH;
	}

	public BancoIntegradoProyectosDto(boolean pAGINAR, boolean fILTRAR_PAGINA, boolean oRDENAR_PAGINA,
			PaginacionDto pAGINACION, String pAGINACION_FILTRO, Map<String, Boolean> pAGINACION_ORDEN,
			Map<String, String> aMBITOS) {
		super(pAGINAR, fILTRAR_PAGINA, oRDENAR_PAGINA, pAGINACION, pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
		// TODO Auto-generated constructor stub
	}

	/**
	   **/
	@JsonProperty("BIP")
	public String getBIP() {
		return BIP;
	}

	public void setBIP(String BIP) {
		this.BIP = BIP;
	}

	/**
	 **/
	@JsonProperty("DESC_BIP")
	public String getDESCBIP() {
		return DESC_BIP;
	}

	public void setDESCBIP(String DESC_BIP) {
		this.DESC_BIP = DESC_BIP;
	}

	/**
	 **/
	@JsonProperty("TIPO_BIP")
	public String getTIPOBIP() {
		return TIPO_BIP;
	}

	public void setTIPOBIP(String TIPO_BIP) {
		this.TIPO_BIP = TIPO_BIP;
	}

	/**
	 **/
	@JsonProperty("ETAPA_BIP")
	public String getETAPABIP() {
		return ETAPA_BIP;
	}

	public void setETAPABIP(String ETAPA_BIP) {
		this.ETAPA_BIP = ETAPA_BIP;
	}

	/**
	 **/
	@JsonProperty("SECTOR_IP")
	public Integer getSECTORIP() {
		return SECTOR_IP;
	}

	public void setSECTORIP(Integer SECTOR_IP) {
		this.SECTOR_IP = SECTOR_IP;
	}

	/**
	 **/
	@JsonProperty("SUB_SECTOR_IP")
	public Integer getSUBSECTORIP() {
		return SUB_SECTOR_IP;
	}

	public void setSUBSECTORIP(Integer SUB_SECTOR_IP) {
		this.SUB_SECTOR_IP = SUB_SECTOR_IP;
	}

	/**
	 **/
	@JsonProperty("AREA_SECTORIAL_IP")
	public Integer getAREASECTORIALIP() {
		return AREA_SECTORIAL_IP;
	}

	public void setAREASECTORIALIP(Integer AREA_SECTORIAL_IP) {
		this.AREA_SECTORIAL_IP = AREA_SECTORIAL_IP;
	}

	/**
	 **/
	@JsonProperty("NRO_CONVENIO")
	public String getNROCONVENIO() {
		return NRO_CONVENIO;
	}

	public void setNROCONVENIO(String NRO_CONVENIO) {
		this.NRO_CONVENIO = NRO_CONVENIO;
	}

	/**
	 **/
	@JsonProperty("PRINCIPAL")
	public String getPRINCIPAL() {
		return PRINCIPAL;
	}

	public void setPRINCIPAL(String PRINCIPAL) {
		this.PRINCIPAL = PRINCIPAL;
	}

	/**
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	/**
	 **/
	@JsonProperty("BIP_SNIPH")
	public Float getBIPSNIPH() {
		return BIP_SNIPH;
	}

	public void setBIPSNIPH(Float BIP_SNIPH) {
		this.BIP_SNIPH = BIP_SNIPH;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class BancoIntegradoProyectosDto {\n");

		sb.append("    BIP: ").append(StringUtil.toIndentedString(BIP)).append("\n");
		sb.append("    DESC_BIP: ").append(StringUtil.toIndentedString(DESC_BIP)).append("\n");
		sb.append("    TIPO_BIP: ").append(StringUtil.toIndentedString(TIPO_BIP)).append("\n");
		sb.append("    ETAPA_BIP: ").append(StringUtil.toIndentedString(ETAPA_BIP)).append("\n");
		sb.append("    SECTOR_IP: ").append(StringUtil.toIndentedString(SECTOR_IP)).append("\n");
		sb.append("    SUB_SECTOR_IP: ").append(StringUtil.toIndentedString(SUB_SECTOR_IP)).append("\n");
		sb.append("    AREA_SECTORIAL_IP: ").append(StringUtil.toIndentedString(AREA_SECTORIAL_IP)).append("\n");
		sb.append("    NRO_CONVENIO: ").append(StringUtil.toIndentedString(NRO_CONVENIO)).append("\n");
		sb.append("    PRINCIPAL: ").append(StringUtil.toIndentedString(PRINCIPAL)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    BIP_SNIPH: ").append(StringUtil.toIndentedString(BIP_SNIPH)).append("\n");
		sb.append("    PAGINAR: ").append(StringUtil.toIndentedString(super.isPAGINAR())).append("\n");
		sb.append("    FILTRAR_PAGINA: ").append(StringUtil.toIndentedString(super.isFILTRAR_PAGINA())).append("\n");
		sb.append("    ORDENAR_PAGINA: ").append(StringUtil.toIndentedString(super.isORDENAR_PAGINA())).append("\n");
		sb.append("    PAGINACION_FILTRO: ").append(StringUtil.toIndentedString(super.getPAGINACION_FILTRO())).append("\n");
		sb.append("    PAGINACION_ORDEN: ").append(StringUtil.toIndentedString(super.getPAGINACION_ORDEN())).append("\n");
		sb.append("    AMBITOS: ").append(StringUtil.toIndentedString(super.getAMBITOS())).append("\n");
		sb.append("    PAGINACION: ").append(StringUtil.toIndentedString(super.getPAGINACION())).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	
	public void initPROPERTY_MAP(){
		super.setPROPERTY_MAP(new HashMap<>());
		super.PROPERTY_MAP.put("BIP","BIP");
		super.PROPERTY_MAP.put("DESC_BIP","DESC_BIP");
		super.PROPERTY_MAP.put("TIPO_BIP","TIPO_BIP");
		super.PROPERTY_MAP.put("ETAPA_BIP","ETAPA_BIP");//
		super.PROPERTY_MAP.put("SECTOR_IP","SECTOR_IP");
		super.PROPERTY_MAP.put("SUB_SECTOR_IP","SUB_SECTOR_IP");
		super.PROPERTY_MAP.put("AREA_SECTORIAL_IP","AREA_SECTORIAL_IP");
		super.PROPERTY_MAP.put("NRO_CONVENIO","NRO_CONVENIO");
		super.PROPERTY_MAP.put("PRINCIPAL","PRINCIPAL");
		super.PROPERTY_MAP.put("VIGENTE","VIGENTE");//
		super.PROPERTY_MAP.put("ESTADO","ESTADO");
		super.PROPERTY_MAP.put("BIP_SNIPH","BIP_SNIPH");
	}
}
