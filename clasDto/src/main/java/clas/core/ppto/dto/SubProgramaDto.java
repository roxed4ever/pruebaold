package clas.core.ppto.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class SubProgramaDto extends AmbitosDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer INSTITUCION = null;
	private Integer PROGRAMA = null;
	private Integer SUB_PROGRAMA = null;
	private String DESC_SUB_PROGRAMA = null;
	private Integer ETAPA_DOCUMENTO = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	
	public SubProgramaDto() {
		initPROPERTY_MAP();
	}
	
	
	public SubProgramaDto(Integer gESTION, Integer iNSTITUCION, Integer pROGRAMA, Integer sUB_PROGRAMA,
			String dESC_SUB_PROGRAMA, Integer eTAPA_DOCUMENTO, String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		INSTITUCION = iNSTITUCION;
		PROGRAMA = pROGRAMA;
		SUB_PROGRAMA = sUB_PROGRAMA;
		DESC_SUB_PROGRAMA = dESC_SUB_PROGRAMA;
		ETAPA_DOCUMENTO = eTAPA_DOCUMENTO;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}


	/**
	 * 
	 **/
	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 * 
	 **/
	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 * 
	 **/
	@JsonProperty("PROGRAMA")
	public Integer getPROGRAMA() {
		return PROGRAMA;
	}

	public void setPROGRAMA(Integer PROGRAMA) {
		this.PROGRAMA = PROGRAMA;
	}

	/**
	 * 
	 **/
	@JsonProperty("SUB_PROGRAMA")
	public Integer getSUBPROGRAMA() {
		return SUB_PROGRAMA;
	}

	public void setSUBPROGRAMA(Integer SUB_PROGRAMA) {
		this.SUB_PROGRAMA = SUB_PROGRAMA;
	}

	/**
	 * 
	 **/
	@JsonProperty("DESC_SUB_PROGRAMA")
	public String getDESCSUBPROGRAMA() {
		return DESC_SUB_PROGRAMA;
	}

	public void setDESCSUBPROGRAMA(String DESC_SUB_PROGRAMA) {
		this.DESC_SUB_PROGRAMA = DESC_SUB_PROGRAMA;
	}

	/**
	 * 
	 **/
	@JsonProperty("ETAPA_DOCUMENTO")
	public Integer getETAPADOCUMENTO() {
		return ETAPA_DOCUMENTO;
	}

	public void setETAPADOCUMENTO(Integer ETAPA_DOCUMENTO) {
		this.ETAPA_DOCUMENTO = ETAPA_DOCUMENTO;
	}

	/**
	 * 
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 * 
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class SubProgramaDto {\n");
		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    PROGRAMA: ").append(StringUtil.toIndentedString(PROGRAMA)).append("\n");
		sb.append("    SUB_PROGRAMA: ").append(StringUtil.toIndentedString(SUB_PROGRAMA)).append("\n");
		sb.append("    DESC_SUB_PROGRAMA: ").append(StringUtil.toIndentedString(DESC_SUB_PROGRAMA)).append("\n");
		sb.append("    ETAPA_DOCUMENTO: ").append(StringUtil.toIndentedString(ETAPA_DOCUMENTO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "id.institucion");
		super.PROPERTY_MAP.put("PROGRAMA", "id.programa");
		super.PROPERTY_MAP.put("SUB_PROGRAMA", "id.subPrograma");
		super.PROPERTY_MAP.put("DESC_SUB_PROGRAMA", "descSubPrograma");
		super.PROPERTY_MAP.put("ETAPA_DOCUMENTO", "etapaDocumento");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
