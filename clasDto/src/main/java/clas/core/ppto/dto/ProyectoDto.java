package clas.core.ppto.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class ProyectoDto extends AmbitosDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer INSTITUCION = null;
	private Integer PROGRAMA = null;
	private Integer SUB_PROGRAMA = null;
	private Integer PROYECTO = null;
	private String DESC_PROYECTO = null;
	private Integer ETAPA_DOCUMENTO = null;
	private String BIP = null;
	private Integer UBIGEO_DEP = null;
	private Integer UBIGEO_MUN = null;
	private Integer FINALIDAD = null;
	private Integer FUNCION = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	public ProyectoDto() {
		initPROPERTY_MAP();
	}
	
	
	public ProyectoDto(Integer gESTION, Integer iNSTITUCION, Integer pROGRAMA, Integer sUB_PROGRAMA, Integer pROYECTO,
			String dESC_PROYECTO, Integer eTAPA_DOCUMENTO, String bIP, Integer uBIGEO_DEP, Integer uBIGEO_MUN,
			Integer fINALIDAD, Integer fUNCION, String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		INSTITUCION = iNSTITUCION;
		PROGRAMA = pROGRAMA;
		SUB_PROGRAMA = sUB_PROGRAMA;
		PROYECTO = pROYECTO;
		DESC_PROYECTO = dESC_PROYECTO;
		ETAPA_DOCUMENTO = eTAPA_DOCUMENTO;
		BIP = bIP;
		UBIGEO_DEP = uBIGEO_DEP;
		UBIGEO_MUN = uBIGEO_MUN;
		FINALIDAD = fINALIDAD;
		FUNCION = fUNCION;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}


	/**
	 * 
	 **/
	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 * 
	 **/
	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 * 
	 **/
	@JsonProperty("PROGRAMA")
	public Integer getPROGRAMA() {
		return PROGRAMA;
	}

	public void setPROGRAMA(Integer PROGRAMA) {
		this.PROGRAMA = PROGRAMA;
	}

	/**
	 * 
	 **/
	@JsonProperty("SUB_PROGRAMA")
	public Integer getSUBPROGRAMA() {
		return SUB_PROGRAMA;
	}

	public void setSUBPROGRAMA(Integer SUB_PROGRAMA) {
		this.SUB_PROGRAMA = SUB_PROGRAMA;
	}

	/**
	 * 
	 **/
	@JsonProperty("PROYECTO")
	public Integer getPROYECTO() {
		return PROYECTO;
	}

	public void setPROYECTO(Integer PROYECTO) {
		this.PROYECTO = PROYECTO;
	}

	/**
	 * 
	 **/
	@JsonProperty("DESC_PROYECTO")
	public String getDESCPROYECTO() {
		return DESC_PROYECTO;
	}

	public void setDESCPROYECTO(String DESC_PROYECTO) {
		this.DESC_PROYECTO = DESC_PROYECTO;
	}

	/**
	 * 
	 **/
	@JsonProperty("ETAPA_DOCUMENTO")
	public Integer getETAPADOCUMENTO() {
		return ETAPA_DOCUMENTO;
	}

	public void setETAPADOCUMENTO(Integer ETAPA_DOCUMENTO) {
		this.ETAPA_DOCUMENTO = ETAPA_DOCUMENTO;
	}

	/**
	 * 
	 **/
	@JsonProperty("BIP")
	public String getBIP() {
		return BIP;
	}

	public void setBIP(String BIP) {
		this.BIP = BIP;
	}

	/**
	 * 
	 **/
	@JsonProperty("UBIGEO_DEP")
	public Integer getUBIGEODEP() {
		return UBIGEO_DEP;
	}

	public void setUBIGEODEP(Integer UBIGEO_DEP) {
		this.UBIGEO_DEP = UBIGEO_DEP;
	}

	/**
	 * 
	 **/
	@JsonProperty("UBIGEO_MUN")
	public Integer getUBIGEOMUN() {
		return UBIGEO_MUN;
	}

	public void setUBIGEOMUN(Integer UBIGEO_MUN) {
		this.UBIGEO_MUN = UBIGEO_MUN;
	}

	/**
	 * 
	 **/
	@JsonProperty("FINALIDAD")
	public Integer getFINALIDAD() {
		return FINALIDAD;
	}

	public void setFINALIDAD(Integer FINALIDAD) {
		this.FINALIDAD = FINALIDAD;
	}

	/**
	 * 
	 **/
	@JsonProperty("FUNCION")
	public Integer getFUNCION() {
		return FUNCION;
	}

	public void setFUNCION(Integer FUNCION) {
		this.FUNCION = FUNCION;
	}

	/**
	 * 
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 * 
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ProyectoDto {\n");
		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    PROGRAMA: ").append(StringUtil.toIndentedString(PROGRAMA)).append("\n");
		sb.append("    SUB_PROGRAMA: ").append(StringUtil.toIndentedString(SUB_PROGRAMA)).append("\n");
		sb.append("    PROYECTO: ").append(StringUtil.toIndentedString(PROYECTO)).append("\n");
		sb.append("    DESC_PROYECTO: ").append(StringUtil.toIndentedString(DESC_PROYECTO)).append("\n");
		sb.append("    ETAPA_DOCUMENTO: ").append(StringUtil.toIndentedString(ETAPA_DOCUMENTO)).append("\n");
		sb.append("    BIP: ").append(StringUtil.toIndentedString(BIP)).append("\n");
		sb.append("    UBIGEO_DEP: ").append(StringUtil.toIndentedString(UBIGEO_DEP)).append("\n");
		sb.append("    UBIGEO_MUN: ").append(StringUtil.toIndentedString(UBIGEO_MUN)).append("\n");
		sb.append("    FINALIDAD: ").append(StringUtil.toIndentedString(FINALIDAD)).append("\n");
		sb.append("    FUNCION: ").append(StringUtil.toIndentedString(FUNCION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "id.institucion");
		super.PROPERTY_MAP.put("PROGRAMA", "id.programa");
		super.PROPERTY_MAP.put("SUB_PROGRAMA", "id.subPrograma");
		super.PROPERTY_MAP.put("PROYECTO", "id.proyecto");
		super.PROPERTY_MAP.put("DESC_PROYECTO", "descProyecto");
		super.PROPERTY_MAP.put("ETAPA_DOCUMENTO", "etapaDocumento");
		super.PROPERTY_MAP.put("BIP", "bip");
		super.PROPERTY_MAP.put("UBIGEO_DEP", "ubigeoDep");
		super.PROPERTY_MAP.put("UBIGEO_MUN", "ubigeoMun");
		super.PROPERTY_MAP.put("FINALIDAD", "finalidad");
		super.PROPERTY_MAP.put("FUNCION", "funcion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");

	}
}
