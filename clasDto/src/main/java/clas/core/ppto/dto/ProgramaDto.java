package clas.core.ppto.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class ProgramaDto extends AmbitosDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer INSTITUCION = null;
	private Integer PROGRAMA = null;
	private String DESC_PROGRAMA = null;
	private Integer ETAPA_DOCUMENTO = null;
	private Integer FINALIDAD = null;
	private Integer FUNCION = null;
	private String VIGENTE = null;
	private String ESTADO = null;
	private String SIN_PRODUCCION = null;

	public ProgramaDto() {
		initPROPERTY_MAP();
	}
	
	
	
	public ProgramaDto(Integer gESTION, Integer iNSTITUCION, Integer pROGRAMA, String dESC_PROGRAMA,
			Integer eTAPA_DOCUMENTO, Integer fINALIDAD, Integer fUNCION, String vIGENTE, String eSTADO,
			String sIN_PRODUCCION) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		INSTITUCION = iNSTITUCION;
		PROGRAMA = pROGRAMA;
		DESC_PROGRAMA = dESC_PROGRAMA;
		ETAPA_DOCUMENTO = eTAPA_DOCUMENTO;
		FINALIDAD = fINALIDAD;
		FUNCION = fUNCION;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
		SIN_PRODUCCION = sIN_PRODUCCION;
	}



	/**
	 * 
	 **/
	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 * 
	 **/
	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 * 
	 **/
	@JsonProperty("PROGRAMA")
	public Integer getPROGRAMA() {
		return PROGRAMA;
	}

	public void setPROGRAMA(Integer PROGRAMA) {
		this.PROGRAMA = PROGRAMA;
	}

	/**
	 * 
	 **/
	@JsonProperty("DESC_PROGRAMA")
	public String getDESCPROGRAMA() {
		return DESC_PROGRAMA;
	}

	public void setDESCPROGRAMA(String DESC_PROGRAMA) {
		this.DESC_PROGRAMA = DESC_PROGRAMA;
	}

	/**
	 * 
	 **/
	@JsonProperty("ETAPA_DOCUMENTO")
	public Integer getETAPADOCUMENTO() {
		return ETAPA_DOCUMENTO;
	}

	public void setETAPADOCUMENTO(Integer ETAPA_DOCUMENTO) {
		this.ETAPA_DOCUMENTO = ETAPA_DOCUMENTO;
	}

	/**
	 * 
	 **/
	@JsonProperty("FINALIDAD")
	public Integer getFINALIDAD() {
		return FINALIDAD;
	}

	public void setFINALIDAD(Integer FINALIDAD) {
		this.FINALIDAD = FINALIDAD;
	}

	/**
	 * 
	 **/
	@JsonProperty("FUNCION")
	public Integer getFUNCION() {
		return FUNCION;
	}

	public void setFUNCION(Integer FUNCION) {
		this.FUNCION = FUNCION;
	}

	/**
	 * 
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 * 
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("SIN_PRODUCCION")
	public String getSINPRODUCCION() {
		return SIN_PRODUCCION;
	}

	public void setSINPRODUCCION(String SIN_PRODUCCION) {
		this.SIN_PRODUCCION = SIN_PRODUCCION;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ProgramaDto {\n");
		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    PROGRAMA: ").append(StringUtil.toIndentedString(PROGRAMA)).append("\n");
		sb.append("    DESC_PROGRAMA: ").append(StringUtil.toIndentedString(DESC_PROGRAMA)).append("\n");
		sb.append("    ETAPA_DOCUMENTO: ").append(StringUtil.toIndentedString(ETAPA_DOCUMENTO)).append("\n");
		sb.append("    FINALIDAD: ").append(StringUtil.toIndentedString(FINALIDAD)).append("\n");
		sb.append("    FUNCION: ").append(StringUtil.toIndentedString(FUNCION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    SIN_PRODUCCION: ").append(StringUtil.toIndentedString(SIN_PRODUCCION)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "id.institucion");
		super.PROPERTY_MAP.put("PROGRAMA", "id.programa");
		super.PROPERTY_MAP.put("DESC_PROGRAMA", "descPrograma");
		super.PROPERTY_MAP.put("ETAPA_DOCUMENTO", "etapaDocumento");
		super.PROPERTY_MAP.put("FINALIDAD", "finalidad");
		super.PROPERTY_MAP.put("FUNCION", "funcion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
		super.PROPERTY_MAP.put("SIN_PRODUCCION", "sinProduccion");
	}

}
