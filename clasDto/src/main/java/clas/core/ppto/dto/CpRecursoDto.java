package clas.core.ppto.dto;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * CP Recurso DTO
 **/

public class CpRecursoDto extends AmbitosDto implements Serializable  {
  

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private Long ID_RECURSO = null;
  private String RECURSO = null;
  private Long NIVEL = null;
  private String DESCRIPCION = null;
  private String RESTRICTIVA = null;
  private Long POR_FUNCIONAMIENTO = null;
  private Long POR_INVERSION = null;
  private Long TIPO_SERVICIO = null;
  private Long ID_RECURSO_PADRE = null;
  private Long EJERCICIO = null;
  private String IMPUTABLE = null;

    private Map<String, String> AMBITOS = null;


 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get ID_RECURSO
   * @return ID_RECURSO
  **/
  @JsonProperty("ID_RECURSO")
  public Long getIDRECURSO() {
    return ID_RECURSO;
  }

  public void setIDRECURSO(Long ID_RECURSO) {
    this.ID_RECURSO = ID_RECURSO;
  }

  public CpRecursoDto ID_RECURSO(Long ID_RECURSO) {
    this.ID_RECURSO = ID_RECURSO;
    return this;
  }

 /**
   * Get RECURSO
   * @return RECURSO
  **/
  @JsonProperty("RECURSO")
  public String getRECURSO() {
    return RECURSO;
  }

  public void setRECURSO(String RECURSO) {
    this.RECURSO = RECURSO;
  }

  public CpRecursoDto RECURSO(String RECURSO) {
    this.RECURSO = RECURSO;
    return this;
  }

 /**
   * Get NIVEL
   * @return NIVEL
  **/
  @JsonProperty("NIVEL")
  public Long getNIVEL() {
    return NIVEL;
  }

  public void setNIVEL(Long NIVEL) {
    this.NIVEL = NIVEL;
  }

  public CpRecursoDto NIVEL(Long NIVEL) {
    this.NIVEL = NIVEL;
    return this;
  }

 /**
   * Get DESCRIPCION
   * @return DESCRIPCION
  **/
  @JsonProperty("DESCRIPCION")
  public String getDESCRIPCION() {
    return DESCRIPCION;
  }

  public void setDESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
  }

  public CpRecursoDto DESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
    return this;
  }

 /**
   * Get RESTRICTIVA
   * @return RESTRICTIVA
  **/
  @JsonProperty("RESTRICTIVA")
  public String getRESTRICTIVA() {
    return RESTRICTIVA;
  }

  public void setRESTRICTIVA(String RESTRICTIVA) {
    this.RESTRICTIVA = RESTRICTIVA;
  }

  public CpRecursoDto RESTRICTIVA(String RESTRICTIVA) {
    this.RESTRICTIVA = RESTRICTIVA;
    return this;
  }

 /**
   * Get POR_FUNCIONAMIENTO
   * @return POR_FUNCIONAMIENTO
  **/
  @JsonProperty("POR_FUNCIONAMIENTO")
  public Long getPORFUNCIONAMIENTO() {
    return POR_FUNCIONAMIENTO;
  }

  public void setPORFUNCIONAMIENTO(Long POR_FUNCIONAMIENTO) {
    this.POR_FUNCIONAMIENTO = POR_FUNCIONAMIENTO;
  }

  public CpRecursoDto POR_FUNCIONAMIENTO(Long POR_FUNCIONAMIENTO) {
    this.POR_FUNCIONAMIENTO = POR_FUNCIONAMIENTO;
    return this;
  }

 /**
   * Get POR_INVERSION
   * @return POR_INVERSION
  **/
  @JsonProperty("POR_INVERSION")
  public Long getPORINVERSION() {
    return POR_INVERSION;
  }

  public void setPORINVERSION(Long POR_INVERSION) {
    this.POR_INVERSION = POR_INVERSION;
  }

  public CpRecursoDto POR_INVERSION(Long POR_INVERSION) {
    this.POR_INVERSION = POR_INVERSION;
    return this;
  }

 /**
   * Get TIPO_SERVICIO
   * @return TIPO_SERVICIO
  **/
  @JsonProperty("TIPO_SERVICIO")
  public Long getTIPOSERVICIO() {
    return TIPO_SERVICIO;
  }

  public void setTIPOSERVICIO(Long TIPO_SERVICIO) {
    this.TIPO_SERVICIO = TIPO_SERVICIO;
  }

  public CpRecursoDto TIPO_SERVICIO(Long TIPO_SERVICIO) {
    this.TIPO_SERVICIO = TIPO_SERVICIO;
    return this;
  }

 /**
   * Get ID_RECURSO_PADRE
   * @return ID_RECURSO_PADRE
  **/
  @JsonProperty("ID_RECURSO_PADRE")
  public Long getIDRECURSOPADRE() {
    return ID_RECURSO_PADRE;
  }

  public void setIDRECURSOPADRE(Long ID_RECURSO_PADRE) {
    this.ID_RECURSO_PADRE = ID_RECURSO_PADRE;
  }

  public CpRecursoDto ID_RECURSO_PADRE(Long ID_RECURSO_PADRE) {
    this.ID_RECURSO_PADRE = ID_RECURSO_PADRE;
    return this;
  }

 /**
   * Get EJERCICIO
   * @return EJERCICIO
  **/
  @JsonProperty("EJERCICIO")
  public Long getEJERCICIO() {
    return EJERCICIO;
  }

  public void setEJERCICIO(Long EJERCICIO) {
    this.EJERCICIO = EJERCICIO;
  }

  public CpRecursoDto EJERCICIO(Long EJERCICIO) {
    this.EJERCICIO = EJERCICIO;
    return this;
  }

 /**
   * Get IMPUTABLE
   * @return IMPUTABLE
  **/
  @JsonProperty("IMPUTABLE")
  public String getIMPUTABLE() {
    return IMPUTABLE;
  }

  public void setIMPUTABLE(String IMPUTABLE) {
    this.IMPUTABLE = IMPUTABLE;
  }

  public CpRecursoDto IMPUTABLE(String IMPUTABLE) {
    this.IMPUTABLE = IMPUTABLE;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public CpRecursoDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public CpRecursoDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }

  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public CpRecursoDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CpRecursoDto {\n");
    
    sb.append("    ID_RECURSO: ").append(toIndentedString(ID_RECURSO)).append("\n");
    sb.append("    RECURSO: ").append(toIndentedString(RECURSO)).append("\n");
    sb.append("    NIVEL: ").append(toIndentedString(NIVEL)).append("\n");
    sb.append("    DESCRIPCION: ").append(toIndentedString(DESCRIPCION)).append("\n");
    sb.append("    RESTRICTIVA: ").append(toIndentedString(RESTRICTIVA)).append("\n");
    sb.append("    POR_FUNCIONAMIENTO: ").append(toIndentedString(POR_FUNCIONAMIENTO)).append("\n");
    sb.append("    POR_INVERSION: ").append(toIndentedString(POR_INVERSION)).append("\n");
    sb.append("    TIPO_SERVICIO: ").append(toIndentedString(TIPO_SERVICIO)).append("\n");
    sb.append("    ID_RECURSO_PADRE: ").append(toIndentedString(ID_RECURSO_PADRE)).append("\n");
    sb.append("    EJERCICIO: ").append(toIndentedString(EJERCICIO)).append("\n");
    sb.append("    IMPUTABLE: ").append(toIndentedString(IMPUTABLE)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public CpRecursoDto() {
	  initPROPERTY_MAP();
  }
  
    
  public CpRecursoDto(Long iD_RECURSO, String rECURSO, Long nIVEL, String dESCRIPCION, String rESTRICTIVA,
		Long pOR_FUNCIONAMIENTO, Long pOR_INVERSION, Long tIPO_SERVICIO,  Long eJERCICIO, String iMPUTABLE) {
	super();
	initPROPERTY_MAP();
	ID_RECURSO = iD_RECURSO;
	RECURSO = rECURSO;
	NIVEL = nIVEL;
	DESCRIPCION = dESCRIPCION;
	RESTRICTIVA = rESTRICTIVA;
	POR_FUNCIONAMIENTO = pOR_FUNCIONAMIENTO;
	POR_INVERSION = pOR_INVERSION;
	TIPO_SERVICIO = tIPO_SERVICIO;
	EJERCICIO = eJERCICIO;
	IMPUTABLE = iMPUTABLE;
}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("ID_RECURSO", "idRecurso");
		super.PROPERTY_MAP.put("RECURSO", "recurso");
		super.PROPERTY_MAP.put("NIVEL", "nivel");
		
		super.PROPERTY_MAP.put("DESCRIPCION", "descripcion");
		super.PROPERTY_MAP.put("RESTRICTIVA", "restrictiva");
		super.PROPERTY_MAP.put("POR_FUNCIONAMIENTO", "porFuncionamiento");
		
		super.PROPERTY_MAP.put("POR_INVERSION", "porInversion");
		super.PROPERTY_MAP.put("TIPO_SERVICIO", "tipoServicio");
//		super.PROPERTY_MAP.put("ID_RECURSO_PADRE", "CpRecurso.idRecurso");
		
		super.PROPERTY_MAP.put("EJERCICIO", "ejercicio");
		super.PROPERTY_MAP.put("IMPUTABLE", "imputable");
		
	}
}



