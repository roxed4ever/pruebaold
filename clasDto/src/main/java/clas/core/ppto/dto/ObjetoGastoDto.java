package clas.core.ppto.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class ObjetoGastoDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private String OBJETO = null;
	private Integer GRUPO_OBJETO = null;
	private Integer SUB_GRUPO_OBJETO = null;
	private Integer PARTIDA_OBJETO = null;
	private Integer SUB_PARTIDA_OBJETO = null;
	private String DESC_OBJETO = null;
	private Integer CLASE_DE_GASTO = null;
	private String DESC_CLASE_DE_GASTO = null;
	private String IMPUTABLE = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	
	public ObjetoGastoDto() {
		initPROPERTY_MAP();
	}
	
	public ObjetoGastoDto(Integer gESTION, String oBJETO, Integer gRUPO_OBJETO, Integer sUB_GRUPO_OBJETO,
			Integer pARTIDA_OBJETO, Integer sUB_PARTIDA_OBJETO, String dESC_OBJETO, Integer cLASE_DE_GASTO,
			String dESC_CLASE_DE_GASTO, String iMPUTABLE, String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		OBJETO = oBJETO;
		GRUPO_OBJETO = gRUPO_OBJETO;
		SUB_GRUPO_OBJETO = sUB_GRUPO_OBJETO;
		PARTIDA_OBJETO = pARTIDA_OBJETO;
		SUB_PARTIDA_OBJETO = sUB_PARTIDA_OBJETO;
		DESC_OBJETO = dESC_OBJETO;
		CLASE_DE_GASTO = cLASE_DE_GASTO;
		DESC_CLASE_DE_GASTO = dESC_CLASE_DE_GASTO;
		IMPUTABLE = iMPUTABLE;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 * 
	 **/
	@JsonProperty("OBJETO")
	public String getOBJETO() {
		return OBJETO;
	}

	public void setOBJETO(String OBJETO) {
		this.OBJETO = OBJETO;
	}

	/**
	 * 
	 **/
	@JsonProperty("GRUPO_OBJETO")
	public Integer getGRUPOOBJETO() {
		return GRUPO_OBJETO;
	}

	public void setGRUPOOBJETO(Integer GRUPO_OBJETO) {
		this.GRUPO_OBJETO = GRUPO_OBJETO;
	}

	/**
	 * 
	 **/
	@JsonProperty("SUB_GRUPO_OBJETO")
	public Integer getSUBGRUPOOBJETO() {
		return SUB_GRUPO_OBJETO;
	}

	public void setSUBGRUPOOBJETO(Integer SUB_GRUPO_OBJETO) {
		this.SUB_GRUPO_OBJETO = SUB_GRUPO_OBJETO;
	}

	/**
	 * 
	 **/
	@JsonProperty("PARTIDA_OBJETO")
	public Integer getPARTIDAOBJETO() {
		return PARTIDA_OBJETO;
	}

	public void setPARTIDAOBJETO(Integer PARTIDA_OBJETO) {
		this.PARTIDA_OBJETO = PARTIDA_OBJETO;
	}

	/**
	 * 
	 **/
	@JsonProperty("SUB_PARTIDA_OBJETO")
	public Integer getSUBPARTIDAOBJETO() {
		return SUB_PARTIDA_OBJETO;
	}

	public void setSUBPARTIDAOBJETO(Integer SUB_PARTIDA_OBJETO) {
		this.SUB_PARTIDA_OBJETO = SUB_PARTIDA_OBJETO;
	}

	/**
	 * 
	 **/
	@JsonProperty("DESC_OBJETO")
	public String getDESCOBJETO() {
		return DESC_OBJETO;
	}

	public void setDESCOBJETO(String DESC_OBJETO) {
		this.DESC_OBJETO = DESC_OBJETO;
	}

	/**
	 * 
	 **/
	@JsonProperty("CLASE_DE_GASTO")
	public Integer getCLASEDEGASTO() {
		return CLASE_DE_GASTO;
	}

	public void setCLASEDEGASTO(Integer CLASE_DE_GASTO) {
		this.CLASE_DE_GASTO = CLASE_DE_GASTO;
	}

	/**
	 * 
	 **/
	@JsonProperty("DESC_CLASE_DE_GASTO")
	public String getDESCCLASEDEGASTO() {
		return DESC_CLASE_DE_GASTO;
	}

	public void setDESCCLASEDEGASTO(String DESC_CLASE_DE_GASTO) {
		this.DESC_CLASE_DE_GASTO = DESC_CLASE_DE_GASTO;
	}

	/**
	 * 
	 **/
	@JsonProperty("IMPUTABLE")
	public String getIMPUTABLE() {
		return IMPUTABLE;
	}

	public void setIMPUTABLE(String IMPUTABLE) {
		this.IMPUTABLE = IMPUTABLE;
	}

	/**
	 * 
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 * 
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ObjetoGastoDto {\n");
		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    OBJETO: ").append(StringUtil.toIndentedString(OBJETO)).append("\n");
		sb.append("    GRUPO_OBJETO: ").append(StringUtil.toIndentedString(GRUPO_OBJETO)).append("\n");
		sb.append("    SUB_GRUPO_OBJETO: ").append(StringUtil.toIndentedString(SUB_GRUPO_OBJETO)).append("\n");
		sb.append("    PARTIDA_OBJETO: ").append(StringUtil.toIndentedString(PARTIDA_OBJETO)).append("\n");
		sb.append("    SUB_PARTIDA_OBJETO: ").append(StringUtil.toIndentedString(SUB_PARTIDA_OBJETO)).append("\n");
		sb.append("    DESC_OBJETO: ").append(StringUtil.toIndentedString(DESC_OBJETO)).append("\n");
		sb.append("    CLASE_DE_GASTO: ").append(StringUtil.toIndentedString(CLASE_DE_GASTO)).append("\n");
		sb.append("    DESC_CLASE_DE_GASTO: ").append(StringUtil.toIndentedString(DESC_CLASE_DE_GASTO)).append("\n");
		sb.append("    IMPUTABLE: ").append(StringUtil.toIndentedString(IMPUTABLE)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("OBJETO", "id.objeto");
		super.PROPERTY_MAP.put("GRUPO_OBJETO", "grupoObjeto");
		super.PROPERTY_MAP.put("SUB_GRUPO_OBJETO", "subGrupoObjeto");
		super.PROPERTY_MAP.put("PARTIDA_OBJETO", "partidaObjeto");
		super.PROPERTY_MAP.put("SUB_PARTIDA_OBJETO", "subPartidaObjeto");
		super.PROPERTY_MAP.put("DESC_OBJETO", "descObjeto");
		super.PROPERTY_MAP.put("CLASE_DE_GASTO", "");
		super.PROPERTY_MAP.put("DESC_CLASE_DE_GASTO", "");
		super.PROPERTY_MAP.put("IMPUTABLE", "imputable");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");

	}
}
