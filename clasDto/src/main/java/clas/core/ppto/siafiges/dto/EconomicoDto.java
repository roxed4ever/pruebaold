package clas.core.ppto.siafiges.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-11T11:55:58.558-06:00")
public class EconomicoDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer ECONOMICO = null;
	private String NOMBRE = null;
	private String GASTO_INGRESO = null;
	private String IMPUTABLE = null;
	private String VIGENTE = null;

	public EconomicoDto() {
		initPROPERTY_MAP();
	}

	public EconomicoDto(Integer gESTION, Integer eCONOMICO, String nOMBRE, String gASTO_INGRESO, String iMPUTABLE,
			String vIGENTE) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		ECONOMICO = eCONOMICO;
		NOMBRE = nOMBRE;
		GASTO_INGRESO = gASTO_INGRESO;
		IMPUTABLE = iMPUTABLE;
		VIGENTE = vIGENTE;
	}

	/**
	 **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("ECONOMICO")
	public Integer getECONOMICO() {
		return ECONOMICO;
	}

	public void setECONOMICO(Integer ECONOMICO) {
		this.ECONOMICO = ECONOMICO;
	}

	/**
	 **/

	@JsonProperty("NOMBRE")
	public String getNOMBRE() {
		return NOMBRE;
	}

	public void setNOMBRE(String NOMBRE) {
		this.NOMBRE = NOMBRE;
	}

	/**
	 **/

	@JsonProperty("GASTO_INGRESO")
	public String getGASTOINGRESO() {
		return GASTO_INGRESO;
	}

	public void setGASTOINGRESO(String GASTO_INGRESO) {
		this.GASTO_INGRESO = GASTO_INGRESO;
	}

	/**
	 **/

	@JsonProperty("IMPUTABLE")
	public String getIMPUTABLE() {
		return IMPUTABLE;
	}

	public void setIMPUTABLE(String IMPUTABLE) {
		this.IMPUTABLE = IMPUTABLE;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class EconomicoDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    ECONOMICO: ").append(StringUtil.toIndentedString(ECONOMICO)).append("\n");
		sb.append("    NOMBRE: ").append(StringUtil.toIndentedString(NOMBRE)).append("\n");
		sb.append("    GASTO_INGRESO: ").append(StringUtil.toIndentedString(GASTO_INGRESO)).append("\n");
		sb.append("    IMPUTABLE: ").append(StringUtil.toIndentedString(IMPUTABLE)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("ECONOMICO", "id.economico");
		super.PROPERTY_MAP.put("NOMBRE", "nombre");
		super.PROPERTY_MAP.put("GASTO_INGRESO", "gastoIngreso");
		super.PROPERTY_MAP.put("IMPUTABLE", "imputable");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");

	}

}
