package clas.core.ppto.siafiges.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-11T11:55:58.558-06:00")
public class PoliticaDto extends AmbitosDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer POLITICA = null;
	private String DESCRIPCION = null;
	private String IMPUTABLE = null;
	private Integer NIVEL = null;
	private String VIGENTE = null;

	public PoliticaDto() {
		initPROPERTY_MAP();
	}

	public PoliticaDto(Integer gESTION, Integer pOLITICA, String dESCRIPCION, String iMPUTABLE, Integer nIVEL,
			String vIGENTE) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		POLITICA = pOLITICA;
		DESCRIPCION = dESCRIPCION;
		IMPUTABLE = iMPUTABLE;
		NIVEL = nIVEL;
		VIGENTE = vIGENTE;
	}

	/**
	 **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("POLITICA")
	public Integer getPOLITICA() {
		return POLITICA;
	}

	public void setPOLITICA(Integer POLITICA) {
		this.POLITICA = POLITICA;
	}

	/**
	 **/

	@JsonProperty("DESCRIPCION")
	public String getDESCRIPCION() {
		return DESCRIPCION;
	}

	public void setDESCRIPCION(String DESCRIPCION) {
		this.DESCRIPCION = DESCRIPCION;
	}

	/**
	 **/

	@JsonProperty("IMPUTABLE")
	public String getIMPUTABLE() {
		return IMPUTABLE;
	}

	public void setIMPUTABLE(String IMPUTABLE) {
		this.IMPUTABLE = IMPUTABLE;
	}

	/**
	 **/

	@JsonProperty("NIVEL")
	public Integer getNIVEL() {
		return NIVEL;
	}

	public void setNIVEL(Integer NIVEL) {
		this.NIVEL = NIVEL;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PoliticaDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    POLITICA: ").append(StringUtil.toIndentedString(POLITICA)).append("\n");
		sb.append("    DESCRIPCION: ").append(StringUtil.toIndentedString(DESCRIPCION)).append("\n");
		sb.append("    IMPUTABLE: ").append(StringUtil.toIndentedString(IMPUTABLE)).append("\n");
		sb.append("    NIVEL: ").append(StringUtil.toIndentedString(NIVEL)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");

		sb.append("}");
		return sb.toString();
	}
	
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("POLITICA", "id.politica");
		super.PROPERTY_MAP.put("DESCRIPCION", "descripcion");
		super.PROPERTY_MAP.put("IMPUTABLE", "imputable");
		super.PROPERTY_MAP.put("NIVEL", "nivel");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");


	}
}
