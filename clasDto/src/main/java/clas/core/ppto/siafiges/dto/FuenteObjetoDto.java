package clas.core.ppto.siafiges.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-11T11:55:58.558-06:00")
public class FuenteObjetoDto extends AmbitosDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer FUENTE = null;
	private Integer OBJETO_GASTO = null;
	private Integer SECTOR_PUBLICO = null;
	private String EXCEPCION = null;
	private String VIGENTE = null;

	public FuenteObjetoDto() {
		initPROPERTY_MAP();
	}

	public FuenteObjetoDto(Integer gESTION, Integer fUENTE, Integer oBJETO_GASTO, Integer sECTOR_PUBLICO,
			String eXCEPCION, String vIGENTE) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		FUENTE = fUENTE;
		OBJETO_GASTO = oBJETO_GASTO;
		SECTOR_PUBLICO = sECTOR_PUBLICO;
		EXCEPCION = eXCEPCION;
		VIGENTE = vIGENTE;
	}

	/**
	 **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("FUENTE")
	public Integer getFUENTE() {
		return FUENTE;
	}

	public void setFUENTE(Integer FUENTE) {
		this.FUENTE = FUENTE;
	}

	/**
	 **/

	@JsonProperty("OBJETO_GASTO")
	public Integer getOBJETOGASTO() {
		return OBJETO_GASTO;
	}

	public void setOBJETOGASTO(Integer OBJETO_GASTO) {
		this.OBJETO_GASTO = OBJETO_GASTO;
	}

	/**
	 **/

	@JsonProperty("SECTOR_PUBLICO")
	public Integer getSECTORPUBLICO() {
		return SECTOR_PUBLICO;
	}

	public void setSECTORPUBLICO(Integer SECTOR_PUBLICO) {
		this.SECTOR_PUBLICO = SECTOR_PUBLICO;
	}

	/**
	 **/

	@JsonProperty("EXCEPCION")
	public String getEXCEPCION() {
		return EXCEPCION;
	}

	public void setEXCEPCION(String EXCEPCION) {
		this.EXCEPCION = EXCEPCION;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class FuenteObjetoDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    FUENTE: ").append(StringUtil.toIndentedString(FUENTE)).append("\n");
		sb.append("    OBJETO_GASTO: ").append(StringUtil.toIndentedString(OBJETO_GASTO)).append("\n");
		sb.append("    SECTOR_PUBLICO: ").append(StringUtil.toIndentedString(SECTOR_PUBLICO)).append("\n");
		sb.append("    EXCEPCION: ").append(StringUtil.toIndentedString(EXCEPCION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("FUENTE", "id.fuente");
		super.PROPERTY_MAP.put("OBJETO_GASTO", "id.objetoGasto");
		super.PROPERTY_MAP.put("SECTOR_PUBLICO", "id.sectorPublico");
		super.PROPERTY_MAP.put("EXCEPCION", "excepcion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");

	}
}
