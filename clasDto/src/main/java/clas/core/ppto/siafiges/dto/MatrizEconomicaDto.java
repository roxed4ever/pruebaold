package clas.core.ppto.siafiges.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-11T11:55:58.558-06:00")
public class MatrizEconomicaDto extends AmbitosDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private String GASTO_INGRESO = null;
	private Integer SECUENCIA = null;
	private Integer OBJETO_GASTO_INI = null;
	private Integer OBJETO_GASTO_FIN = null;
	private Integer RUBRO_INI = null;
	private Integer RUBRO_FIN = null;
	private Integer SECTOR_PUBLICO = null;
	private String TIENE_PROYECTO = null;
	private Integer TIPO_PROYECTO = null;
	private Integer ECONOMICO = null;
	private String TIPO_ADMINISTRACION = null;
	private String VIGENTE = null;

	public MatrizEconomicaDto() {
		initPROPERTY_MAP();
	}

	public MatrizEconomicaDto(Integer gESTION, String gASTO_INGRESO, Integer sECUENCIA, Integer oBJETO_GASTO_INI,
			Integer oBJETO_GASTO_FIN, Integer rUBRO_INI, Integer rUBRO_FIN, Integer sECTOR_PUBLICO,
			String tIENE_PROYECTO, Integer tIPO_PROYECTO, Integer eCONOMICO, String tIPO_ADMINISTRACION,
			String vIGENTE) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		GASTO_INGRESO = gASTO_INGRESO;
		SECUENCIA = sECUENCIA;
		OBJETO_GASTO_INI = oBJETO_GASTO_INI;
		OBJETO_GASTO_FIN = oBJETO_GASTO_FIN;
		RUBRO_INI = rUBRO_INI;
		RUBRO_FIN = rUBRO_FIN;
		SECTOR_PUBLICO = sECTOR_PUBLICO;
		TIENE_PROYECTO = tIENE_PROYECTO;
		TIPO_PROYECTO = tIPO_PROYECTO;
		ECONOMICO = eCONOMICO;
		TIPO_ADMINISTRACION = tIPO_ADMINISTRACION;
		VIGENTE = vIGENTE;
	}

	/**
	 **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("GASTO_INGRESO")
	public String getGASTOINGRESO() {
		return GASTO_INGRESO;
	}

	public void setGASTOINGRESO(String GASTO_INGRESO) {
		this.GASTO_INGRESO = GASTO_INGRESO;
	}

	/**
	 **/

	@JsonProperty("SECUENCIA")
	public Integer getSECUENCIA() {
		return SECUENCIA;
	}

	public void setSECUENCIA(Integer SECUENCIA) {
		this.SECUENCIA = SECUENCIA;
	}

	/**
	 **/

	@JsonProperty("OBJETO_GASTO_INI")
	public Integer getOBJETOGASTOINI() {
		return OBJETO_GASTO_INI;
	}

	public void setOBJETOGASTOINI(Integer OBJETO_GASTO_INI) {
		this.OBJETO_GASTO_INI = OBJETO_GASTO_INI;
	}

	/**
	 **/

	@JsonProperty("OBJETO_GASTO_FIN")
	public Integer getOBJETOGASTOFIN() {
		return OBJETO_GASTO_FIN;
	}

	public void setOBJETOGASTOFIN(Integer OBJETO_GASTO_FIN) {
		this.OBJETO_GASTO_FIN = OBJETO_GASTO_FIN;
	}

	/**
	 **/

	@JsonProperty("RUBRO_INI")
	public Integer getRUBROINI() {
		return RUBRO_INI;
	}

	public void setRUBROINI(Integer RUBRO_INI) {
		this.RUBRO_INI = RUBRO_INI;
	}

	/**
	 **/

	@JsonProperty("RUBRO_FIN")
	public Integer getRUBROFIN() {
		return RUBRO_FIN;
	}

	public void setRUBROFIN(Integer RUBRO_FIN) {
		this.RUBRO_FIN = RUBRO_FIN;
	}

	/**
	 **/

	@JsonProperty("SECTOR_PUBLICO")
	public Integer getSECTORPUBLICO() {
		return SECTOR_PUBLICO;
	}

	public void setSECTORPUBLICO(Integer SECTOR_PUBLICO) {
		this.SECTOR_PUBLICO = SECTOR_PUBLICO;
	}

	/**
	 **/

	@JsonProperty("TIENE_PROYECTO")
	public String getTIENEPROYECTO() {
		return TIENE_PROYECTO;
	}

	public void setTIENEPROYECTO(String TIENE_PROYECTO) {
		this.TIENE_PROYECTO = TIENE_PROYECTO;
	}

	/**
	 **/

	@JsonProperty("TIPO_PROYECTO")
	public Integer getTIPOPROYECTO() {
		return TIPO_PROYECTO;
	}

	public void setTIPOPROYECTO(Integer TIPO_PROYECTO) {
		this.TIPO_PROYECTO = TIPO_PROYECTO;
	}

	/**
	 **/

	@JsonProperty("ECONOMICO")
	public Integer getECONOMICO() {
		return ECONOMICO;
	}

	public void setECONOMICO(Integer ECONOMICO) {
		this.ECONOMICO = ECONOMICO;
	}

	/**
	 **/

	@JsonProperty("TIPO_ADMINISTRACION")
	public String getTIPOADMINISTRACION() {
		return TIPO_ADMINISTRACION;
	}

	public void setTIPOADMINISTRACION(String TIPO_ADMINISTRACION) {
		this.TIPO_ADMINISTRACION = TIPO_ADMINISTRACION;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MatrizEconomicaDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    GASTO_INGRESO: ").append(StringUtil.toIndentedString(GASTO_INGRESO)).append("\n");
		sb.append("    SECUENCIA: ").append(StringUtil.toIndentedString(SECUENCIA)).append("\n");
		sb.append("    OBJETO_GASTO_INI: ").append(StringUtil.toIndentedString(OBJETO_GASTO_INI)).append("\n");
		sb.append("    OBJETO_GASTO_FIN: ").append(StringUtil.toIndentedString(OBJETO_GASTO_FIN)).append("\n");
		sb.append("    RUBRO_INI: ").append(StringUtil.toIndentedString(RUBRO_INI)).append("\n");
		sb.append("    RUBRO_FIN: ").append(StringUtil.toIndentedString(RUBRO_FIN)).append("\n");
		sb.append("    SECTOR_PUBLICO: ").append(StringUtil.toIndentedString(SECTOR_PUBLICO)).append("\n");
		sb.append("    TIENE_PROYECTO: ").append(StringUtil.toIndentedString(TIENE_PROYECTO)).append("\n");
		sb.append("    TIPO_PROYECTO: ").append(StringUtil.toIndentedString(TIPO_PROYECTO)).append("\n");
		sb.append("    ECONOMICO: ").append(StringUtil.toIndentedString(ECONOMICO)).append("\n");
		sb.append("    TIPO_ADMINISTRACION: ").append(StringUtil.toIndentedString(TIPO_ADMINISTRACION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("GASTO_INGRESO", "id.gastoIngreso");
		super.PROPERTY_MAP.put("SECUENCIA", "id.secuencia");
		super.PROPERTY_MAP.put("OBJETO_GASTO_INI", "objetoGastoIni");
		super.PROPERTY_MAP.put("OBJETO_GASTO_FIN", "objetoGastoFin");
		super.PROPERTY_MAP.put("RUBRO_INI", "rubroIni");
		super.PROPERTY_MAP.put("RUBRO_FIN", "rubroFin");
		super.PROPERTY_MAP.put("SECTOR_PUBLICO", "sectorPublico");
		super.PROPERTY_MAP.put("TIENE_PROYECTO", "tieneProyecto");
		super.PROPERTY_MAP.put("TIPO_PROYECTO", "tipoProyecto");
		super.PROPERTY_MAP.put("ECONOMICO", "economico");
		super.PROPERTY_MAP.put("TIPO_ADMINISTRACION", "tipoAdministracion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");

	}
}
