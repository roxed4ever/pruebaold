 package clas.core.tes.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

public class MonedasDto extends AmbitosDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String MONEDA;
	private String DESC_MONEDA;
	private String ESTADO;
	
	public MonedasDto() {
		super();
	}	

	public MonedasDto(boolean pAGINAR, boolean fILTRAR_PAGINA, boolean oRDENAR_PAGINA, PaginacionDto pAGINACION,
			String pAGINACION_FILTRO, Map<String, Boolean> pAGINACION_ORDEN, Map<String, String> aMBITOS, String MONEDA, String DESC_MONEDA, String ESTADO) {
		super(pAGINAR,fILTRAR_PAGINA,oRDENAR_PAGINA,pAGINACION,pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
		this.MONEDA = MONEDA;
		this.DESC_MONEDA = DESC_MONEDA;
		this.ESTADO = ESTADO;
	}


	public MonedasDto(String MONEDA, String DESC_MONEDA, String ESTADO) {
		super();
		this.MONEDA = MONEDA;
		this.DESC_MONEDA = DESC_MONEDA;
		this.ESTADO = ESTADO;
	}
	
	@JsonProperty("MONEDA")
	public String getMONEDA() {
		return MONEDA;
	}
	public void setMONEDA(String mONEDA) {
		MONEDA = mONEDA;
	}
	
	@JsonProperty("DESC_MONEDA")
	public String getDESC_MONEDA() {
		return DESC_MONEDA;
	}
	public void setDESC_MONEDA(String dESC_MONEDA) {
		DESC_MONEDA = dESC_MONEDA;
	}
	
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}
	public void setESTADO(String eSTADO) {
		ESTADO = eSTADO;
	}
	
	@Override
	public Map<String, String> getPROPERTY_MAP(){
		super.PROPERTY_MAP = new HashMap<String,String>();
		super.PROPERTY_MAP.put("MONEDA","moneda");
		super.PROPERTY_MAP.put("DESC_MONEDA","descMoneda");
		super.PROPERTY_MAP.put("ESTADO","apiEstado");
		return super.PROPERTY_MAP;
	}
	
	
}
