package clas.ges.clc.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class CbaCodigosRefInternoDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String CODIGO_REFERENCIA = null;
	private String DESC_CODIGO_REFERENCIA = null;
	private String SIGLA_CODIGO_REFERENCIA = null;
	private String TIPO_REGISTRO = null;
	private String CONTRA_CODIGO = null;
	private Integer TIPO_CONCILIACION = null;
	private Integer GRUPO_CONCILIACION = null;
	private String REGISTRA_SOLO_LIBRETAS = null;
	private String TIPO_CODIGO = null;
	private String ESTADO = null;

	public CbaCodigosRefInternoDto() {
		initPROPERTY_MAP();
	}

	public CbaCodigosRefInternoDto(String cODIGO_REFERENCIA, String dESC_CODIGO_REFERENCIA,
			String sIGLA_CODIGO_REFERENCIA, String tIPO_REGISTRO, String cONTRA_CODIGO, Integer tIPO_CONCILIACION,
			Integer gRUPO_CONCILIACION, String rEGISTRA_SOLO_LIBRETAS, String tIPO_CODIGO, String eSTADO) {
		super();
		initPROPERTY_MAP();
		CODIGO_REFERENCIA = cODIGO_REFERENCIA;
		DESC_CODIGO_REFERENCIA = dESC_CODIGO_REFERENCIA;
		SIGLA_CODIGO_REFERENCIA = sIGLA_CODIGO_REFERENCIA;
		TIPO_REGISTRO = tIPO_REGISTRO;
		CONTRA_CODIGO = cONTRA_CODIGO;
		TIPO_CONCILIACION = tIPO_CONCILIACION;
		GRUPO_CONCILIACION = gRUPO_CONCILIACION;
		REGISTRA_SOLO_LIBRETAS = rEGISTRA_SOLO_LIBRETAS;
		TIPO_CODIGO = tIPO_CODIGO;
		ESTADO = eSTADO;
	}

	@JsonProperty("CODIGO_REFERENCIA")
	public String getCODIGOREFERENCIA() {
		return CODIGO_REFERENCIA;
	}

	public void setCODIGOREFERENCIA(String CODIGO_REFERENCIA) {
		this.CODIGO_REFERENCIA = CODIGO_REFERENCIA;
	}

	@JsonProperty("DESC_CODIGO_REFERENCIA")
	public String getDESCCODIGOREFERENCIA() {
		return DESC_CODIGO_REFERENCIA;
	}

	public void setDESCCODIGOREFERENCIA(String DESC_CODIGO_REFERENCIA) {
		this.DESC_CODIGO_REFERENCIA = DESC_CODIGO_REFERENCIA;
	}

	@JsonProperty("SIGLA_CODIGO_REFERENCIA")
	public String getSIGLACODIGOREFERENCIA() {
		return SIGLA_CODIGO_REFERENCIA;
	}

	public void setSIGLACODIGOREFERENCIA(String SIGLA_CODIGO_REFERENCIA) {
		this.SIGLA_CODIGO_REFERENCIA = SIGLA_CODIGO_REFERENCIA;
	}

	@JsonProperty("TIPO_REGISTRO")
	public String getTIPOREGISTRO() {
		return TIPO_REGISTRO;
	}

	public void setTIPOREGISTRO(String TIPO_REGISTRO) {
		this.TIPO_REGISTRO = TIPO_REGISTRO;
	}

	@JsonProperty("CONTRA_CODIGO")
	public String getCONTRACODIGO() {
		return CONTRA_CODIGO;
	}

	public void setCONTRACODIGO(String CONTRA_CODIGO) {
		this.CONTRA_CODIGO = CONTRA_CODIGO;
	}

	@JsonProperty("TIPO_CONCILIACION")
	public Integer getTIPOCONCILIACION() {
		return TIPO_CONCILIACION;
	}

	public void setTIPOCONCILIACION(Integer TIPO_CONCILIACION) {
		this.TIPO_CONCILIACION = TIPO_CONCILIACION;
	}

	@JsonProperty("GRUPO_CONCILIACION")
	public Integer getGRUPOCONCILIACION() {
		return GRUPO_CONCILIACION;
	}

	public void setGRUPOCONCILIACION(Integer GRUPO_CONCILIACION) {
		this.GRUPO_CONCILIACION = GRUPO_CONCILIACION;
	}

	@JsonProperty("REGISTRA_SOLO_LIBRETAS")
	public String getREGISTRASOLOLIBRETAS() {
		return REGISTRA_SOLO_LIBRETAS;
	}

	public void setREGISTRASOLOLIBRETAS(String REGISTRA_SOLO_LIBRETAS) {
		this.REGISTRA_SOLO_LIBRETAS = REGISTRA_SOLO_LIBRETAS;
	}

	@JsonProperty("TIPO_CODIGO")
	public String getTIPOCODIGO() {
		return TIPO_CODIGO;
	}

	public void setTIPOCODIGO(String TIPO_CODIGO) {
		this.TIPO_CODIGO = TIPO_CODIGO;
	}

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CbaCodigosRefInternoDto {\n");

		sb.append("    CODIGO_REFERENCIA: ").append(StringUtil.toIndentedString(CODIGO_REFERENCIA)).append("\n");
		sb.append("    DESC_CODIGO_REFERENCIA: ").append(StringUtil.toIndentedString(DESC_CODIGO_REFERENCIA))
				.append("\n");
		sb.append("    SIGLA_CODIGO_REFERENCIA: ").append(StringUtil.toIndentedString(SIGLA_CODIGO_REFERENCIA))
				.append("\n");
		sb.append("    TIPO_REGISTRO: ").append(StringUtil.toIndentedString(TIPO_REGISTRO)).append("\n");
		sb.append("    CONTRA_CODIGO: ").append(StringUtil.toIndentedString(CONTRA_CODIGO)).append("\n");
		sb.append("    TIPO_CONCILIACION: ").append(StringUtil.toIndentedString(TIPO_CONCILIACION)).append("\n");
		sb.append("    GRUPO_CONCILIACION: ").append(StringUtil.toIndentedString(GRUPO_CONCILIACION)).append("\n");
		sb.append("    REGISTRA_SOLO_LIBRETAS: ").append(StringUtil.toIndentedString(REGISTRA_SOLO_LIBRETAS))
				.append("\n");
		sb.append("    TIPO_CODIGO: ").append(StringUtil.toIndentedString(TIPO_CODIGO)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("CODIGO_REFERENCIA", "departamento");
		super.PROPERTY_MAP.put("DESC_CODIGO_REFERENCIA", "municipio");
		super.PROPERTY_MAP.put("SIGLA_CODIGO_REFERENCIA", "aldea");
		super.PROPERTY_MAP.put("TIPO_REGISTRO", "cacerio");
		super.PROPERTY_MAP.put("CONTRA_CODIGO", "descCacerio");
		super.PROPERTY_MAP.put("TIPO_CONCILIACION", "vigente");
		super.PROPERTY_MAP.put("GRUPO_CONCILIACION", "apiEstado");
		super.PROPERTY_MAP.put("REGISTRA_SOLO_LIBRETAS", "apiEstado");
		super.PROPERTY_MAP.put("TIPO_CODIGO", "apiEstado");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
