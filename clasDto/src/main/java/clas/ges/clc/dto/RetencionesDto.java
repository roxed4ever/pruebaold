package clas.ges.clc.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class RetencionesDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String BIP = null;
	private String NRO_CONVENIO = null;
	private String API_ESTADO = null;
	private String CUENTA_CONTABLE = null;
	private Integer TRF_BENEF = null;
	private String RUBRO = null;
	private Integer ORGANISMO = null;
	private String CODIGO_SIGADE = null;
	private Integer FUENTE = null;
	private Integer INSTITUCION = null;
	private String TIPO_FORMULARIO = null;
	private String DESCRIPCION = null;
	private Integer SUB_TIPO = null;
	private Integer TIPO = null;
	private Integer RETENCION = null;
	private Integer GESTION = null;
	private Integer GA = null;
	private Integer TRAMO = null;

	public RetencionesDto() {
		initPROPERTY_MAP();
	}

	public RetencionesDto(String bIP, String nRO_CONVENIO, String aPI_ESTADO, String cUENTA_CONTABLE, Integer tRF_BENEF,
			String rUBRO, Integer oRGANISMO, String cODIGO_SIGADE, Integer fUENTE, Integer iNSTITUCION,
			String tIPO_FORMULARIO, String dESCRIPCION, Integer sUB_TIPO, Integer tIPO, Integer rETENCION,
			Integer gESTION, Integer gA, Integer tRAMO) {
		super();
		initPROPERTY_MAP();
		BIP = bIP;
		NRO_CONVENIO = nRO_CONVENIO;
		API_ESTADO = aPI_ESTADO;
		CUENTA_CONTABLE = cUENTA_CONTABLE;
		TRF_BENEF = tRF_BENEF;
		RUBRO = rUBRO;
		ORGANISMO = oRGANISMO;
		CODIGO_SIGADE = cODIGO_SIGADE;
		FUENTE = fUENTE;
		INSTITUCION = iNSTITUCION;
		TIPO_FORMULARIO = tIPO_FORMULARIO;
		DESCRIPCION = dESCRIPCION;
		SUB_TIPO = sUB_TIPO;
		TIPO = tIPO;
		RETENCION = rETENCION;
		GESTION = gESTION;
		GA = gA;
		TRAMO = tRAMO;
	}

	@JsonProperty("BIP")
	public String getBIP() {
		return BIP;
	}

	public void setBIP(String BIP) {
		this.BIP = BIP;
	}

	@JsonProperty("NRO_CONVENIO")
	public String getNROCONVENIO() {
		return NRO_CONVENIO;
	}

	public void setNROCONVENIO(String NRO_CONVENIO) {
		this.NRO_CONVENIO = NRO_CONVENIO;
	}

	@JsonProperty("API_ESTADO")
	public String getAPIESTADO() {
		return API_ESTADO;
	}

	public void setAPIESTADO(String API_ESTADO) {
		this.API_ESTADO = API_ESTADO;
	}

	@JsonProperty("CUENTA_CONTABLE")
	public String getCUENTACONTABLE() {
		return CUENTA_CONTABLE;
	}

	public void setCUENTACONTABLE(String CUENTA_CONTABLE) {
		this.CUENTA_CONTABLE = CUENTA_CONTABLE;
	}

	@JsonProperty("TRF_BENEF")
	public Integer getTRFBENEF() {
		return TRF_BENEF;
	}

	public void setTRFBENEF(Integer TRF_BENEF) {
		this.TRF_BENEF = TRF_BENEF;
	}

	@JsonProperty("RUBRO")
	public String getRUBRO() {
		return RUBRO;
	}

	public void setRUBRO(String RUBRO) {
		this.RUBRO = RUBRO;
	}

	@JsonProperty("ORGANISMO")
	public Integer getORGANISMO() {
		return ORGANISMO;
	}

	public void setORGANISMO(Integer ORGANISMO) {
		this.ORGANISMO = ORGANISMO;
	}

	@JsonProperty("CODIGO_SIGADE")
	public String getCODIGOSIGADE() {
		return CODIGO_SIGADE;
	}

	public void setCODIGOSIGADE(String CODIGO_SIGADE) {
		this.CODIGO_SIGADE = CODIGO_SIGADE;
	}

	@JsonProperty("FUENTE")
	public Integer getFUENTE() {
		return FUENTE;
	}

	public void setFUENTE(Integer FUENTE) {
		this.FUENTE = FUENTE;
	}

	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	@JsonProperty("TIPO_FORMULARIO")
	public String getTIPOFORMULARIO() {
		return TIPO_FORMULARIO;
	}

	public void setTIPOFORMULARIO(String TIPO_FORMULARIO) {
		this.TIPO_FORMULARIO = TIPO_FORMULARIO;
	}

	@JsonProperty("DESCRIPCION")
	public String getDESCRIPCION() {
		return DESCRIPCION;
	}

	public void setDESCRIPCION(String DESCRIPCION) {
		this.DESCRIPCION = DESCRIPCION;
	}

	@JsonProperty("SUB_TIPO")
	public Integer getSUBTIPO() {
		return SUB_TIPO;
	}

	public void setSUBTIPO(Integer SUB_TIPO) {
		this.SUB_TIPO = SUB_TIPO;
	}

	@JsonProperty("TIPO")
	public Integer getTIPO() {
		return TIPO;
	}

	public void setTIPO(Integer TIPO) {
		this.TIPO = TIPO;
	}

	@JsonProperty("RETENCION")
	public Integer getRETENCION() {
		return RETENCION;
	}

	public void setRETENCION(Integer RETENCION) {
		this.RETENCION = RETENCION;
	}

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	@JsonProperty("GA")
	public Integer getGA() {
		return GA;
	}

	public void setGA(Integer GA) {
		this.GA = GA;
	}

	@JsonProperty("TRAMO")
	public Integer getTRAMO() {
		return TRAMO;
	}

	public void setTRAMO(Integer TRAMO) {
		this.TRAMO = TRAMO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class RetencionesDto {\n");

		sb.append("    BIP: ").append(StringUtil.toIndentedString(BIP)).append("\n");
		sb.append("    NRO_CONVENIO: ").append(StringUtil.toIndentedString(NRO_CONVENIO)).append("\n");
		sb.append("    API_ESTADO: ").append(StringUtil.toIndentedString(API_ESTADO)).append("\n");
		sb.append("    CUENTA_CONTABLE: ").append(StringUtil.toIndentedString(CUENTA_CONTABLE)).append("\n");
		sb.append("    TRF_BENEF: ").append(StringUtil.toIndentedString(TRF_BENEF)).append("\n");
		sb.append("    RUBRO: ").append(StringUtil.toIndentedString(RUBRO)).append("\n");
		sb.append("    ORGANISMO: ").append(StringUtil.toIndentedString(ORGANISMO)).append("\n");
		sb.append("    CODIGO_SIGADE: ").append(StringUtil.toIndentedString(CODIGO_SIGADE)).append("\n");
		sb.append("    FUENTE: ").append(StringUtil.toIndentedString(FUENTE)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    TIPO_FORMULARIO: ").append(StringUtil.toIndentedString(TIPO_FORMULARIO)).append("\n");
		sb.append("    DESCRIPCION: ").append(StringUtil.toIndentedString(DESCRIPCION)).append("\n");
		sb.append("    SUB_TIPO: ").append(StringUtil.toIndentedString(SUB_TIPO)).append("\n");
		sb.append("    TIPO: ").append(StringUtil.toIndentedString(TIPO)).append("\n");
		sb.append("    RETENCION: ").append(StringUtil.toIndentedString(RETENCION)).append("\n");
		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    GA: ").append(StringUtil.toIndentedString(GA)).append("\n");
		sb.append("    TRAMO: ").append(StringUtil.toIndentedString(TRAMO)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("BIP", "departamento");
		super.PROPERTY_MAP.put("NRO_CONVENIO", "municipio");
		super.PROPERTY_MAP.put("API_ESTADO", "aldea");
		super.PROPERTY_MAP.put("CUENTA_CONTABLE", "cacerio");
		super.PROPERTY_MAP.put("TRF_BENEF", "descCacerio");
		super.PROPERTY_MAP.put("RUBRO", "departamento");
		super.PROPERTY_MAP.put("ORGANISMO", "municipio");
		super.PROPERTY_MAP.put("CODIGO_SIGADE", "aldea");
		super.PROPERTY_MAP.put("FUENTE", "cacerio");
		super.PROPERTY_MAP.put("INSTITUCION", "descCacerio");
		super.PROPERTY_MAP.put("TIPO_FORMULARIO", "departamento");
		super.PROPERTY_MAP.put("DESCRIPCION", "municipio");
		super.PROPERTY_MAP.put("SUB_TIPO", "aldea");
		super.PROPERTY_MAP.put("TIPO", "departamento");
		super.PROPERTY_MAP.put("RETENCION", "municipio");
		super.PROPERTY_MAP.put("GESTION", "aldea");
		super.PROPERTY_MAP.put("GA", "cacerio");
		super.PROPERTY_MAP.put("TRAMO", "descCacerio");

	}
}
