package clas.ges.clc.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CbaCodigosRefExternoDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer BANCO = null;
	private Integer CODIGO_REFERENCIA = null;
	private String DESC_CODIGO_REFERENCIA = null;
	private String SIGLA_CODIGO_REFERENCIA = null;
	private String TIPO_REGISTRO = null;
	private Integer CONTRA_CODIGO = null;
	private String CODIGO_REF_INTERNO = null;
	private String ESTADO = null;

	public CbaCodigosRefExternoDto() {
		initPROPERTY_MAP();
	}
	
	
	
	public CbaCodigosRefExternoDto(Integer bANCO, Integer cODIGO_REFERENCIA, String dESC_CODIGO_REFERENCIA,
			String sIGLA_CODIGO_REFERENCIA, String tIPO_REGISTRO, Integer cONTRA_CODIGO, String cODIGO_REF_INTERNO,
			String eSTADO) {
		super();
		initPROPERTY_MAP();
		BANCO = bANCO;
		CODIGO_REFERENCIA = cODIGO_REFERENCIA;
		DESC_CODIGO_REFERENCIA = dESC_CODIGO_REFERENCIA;
		SIGLA_CODIGO_REFERENCIA = sIGLA_CODIGO_REFERENCIA;
		TIPO_REGISTRO = tIPO_REGISTRO;
		CONTRA_CODIGO = cONTRA_CODIGO;
		CODIGO_REF_INTERNO = cODIGO_REF_INTERNO;
		ESTADO = eSTADO;
	}



	@JsonProperty("BANCO")
	public Integer getBANCO() {
		return BANCO;
	}

	public void setBANCO(Integer BANCO) {
		this.BANCO = BANCO;
	}

	@JsonProperty("CODIGO_REFERENCIA")
	public Integer getCODIGOREFERENCIA() {
		return CODIGO_REFERENCIA;
	}

	public void setCODIGOREFERENCIA(Integer CODIGO_REFERENCIA) {
		this.CODIGO_REFERENCIA = CODIGO_REFERENCIA;
	}

	@JsonProperty("DESC_CODIGO_REFERENCIA")
	public String getDESCCODIGOREFERENCIA() {
		return DESC_CODIGO_REFERENCIA;
	}

	public void setDESCCODIGOREFERENCIA(String DESC_CODIGO_REFERENCIA) {
		this.DESC_CODIGO_REFERENCIA = DESC_CODIGO_REFERENCIA;
	}

	@JsonProperty("SIGLA_CODIGO_REFERENCIA")
	public String getSIGLACODIGOREFERENCIA() {
		return SIGLA_CODIGO_REFERENCIA;
	}

	public void setSIGLACODIGOREFERENCIA(String SIGLA_CODIGO_REFERENCIA) {
		this.SIGLA_CODIGO_REFERENCIA = SIGLA_CODIGO_REFERENCIA;
	}

	@JsonProperty("TIPO_REGISTRO")
	public String getTIPOREGISTRO() {
		return TIPO_REGISTRO;
	}

	public void setTIPOREGISTRO(String TIPO_REGISTRO) {
		this.TIPO_REGISTRO = TIPO_REGISTRO;
	}

	@JsonProperty("CONTRA_CODIGO")
	public Integer getCONTRACODIGO() {
		return CONTRA_CODIGO;
	}

	public void setCONTRACODIGO(Integer CONTRA_CODIGO) {
		this.CONTRA_CODIGO = CONTRA_CODIGO;
	}

	@JsonProperty("CODIGO_REF_INTERNO")
	public String getCODIGOREFINTERNO() {
		return CODIGO_REF_INTERNO;
	}

	public void setCODIGOREFINTERNO(String CODIGO_REF_INTERNO) {
		this.CODIGO_REF_INTERNO = CODIGO_REF_INTERNO;
	}

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CbaCodigosRefExternoDto {\n");

		sb.append("    BANCO: ").append(StringUtil.toIndentedString(BANCO)).append("\n");
		sb.append("    CODIGO_REFERENCIA: ").append(StringUtil.toIndentedString(CODIGO_REFERENCIA)).append("\n");
		sb.append("    DESC_CODIGO_REFERENCIA: ").append(StringUtil.toIndentedString(DESC_CODIGO_REFERENCIA))
				.append("\n");
		sb.append("    SIGLA_CODIGO_REFERENCIA: ").append(StringUtil.toIndentedString(SIGLA_CODIGO_REFERENCIA))
				.append("\n");
		sb.append("    TIPO_REGISTRO: ").append(StringUtil.toIndentedString(TIPO_REGISTRO)).append("\n");
		sb.append("    CONTRA_CODIGO: ").append(StringUtil.toIndentedString(CONTRA_CODIGO)).append("\n");
		sb.append("    CODIGO_REF_INTERNO: ").append(StringUtil.toIndentedString(CODIGO_REF_INTERNO)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("BANCO", "departamento");
		super.PROPERTY_MAP.put("CODIGO_REFERENCIA", "municipio");
		super.PROPERTY_MAP.put("DESC_CODIGO_REFERENCIA", "aldea");
		super.PROPERTY_MAP.put("SIGLA_CODIGO_REFERENCIA", "cacerio");
		super.PROPERTY_MAP.put("TIPO_REGISTRO", "descCacerio");
		super.PROPERTY_MAP.put("CONTRA_CODIGO", "vigente");
		super.PROPERTY_MAP.put("CODIGO_REF_INTERNO", "apiEstado");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
