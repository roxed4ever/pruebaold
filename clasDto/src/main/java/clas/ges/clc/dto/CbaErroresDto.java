package clas.ges.clc.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class CbaErroresDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer ERROR = null;
	private String DESC_ERROR = null;
	private String SIGLA_ERROR = null;
	private String ESTADO = null;
	private String TIPO_ERROR = null;

	public CbaErroresDto() {
		initPROPERTY_MAP();
	}

	public CbaErroresDto(Integer eRROR, String dESC_ERROR, String sIGLA_ERROR, String eSTADO, String tIPO_ERROR) {
		super();
		initPROPERTY_MAP();
		ERROR = eRROR;
		DESC_ERROR = dESC_ERROR;
		SIGLA_ERROR = sIGLA_ERROR;
		ESTADO = eSTADO;
		TIPO_ERROR = tIPO_ERROR;
	}

	@JsonProperty("ERROR")
	public Integer getERROR() {
		return ERROR;
	}

	public void setERROR(Integer ERROR) {
		this.ERROR = ERROR;
	}

	@JsonProperty("DESC_ERROR")
	public String getDESCERROR() {
		return DESC_ERROR;
	}

	public void setDESCERROR(String DESC_ERROR) {
		this.DESC_ERROR = DESC_ERROR;
	}

	@JsonProperty("SIGLA_ERROR")
	public String getSIGLAERROR() {
		return SIGLA_ERROR;
	}

	public void setSIGLAERROR(String SIGLA_ERROR) {
		this.SIGLA_ERROR = SIGLA_ERROR;
	}

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@JsonProperty("TIPO_ERROR")
	public String getTIPOERROR() {
		return TIPO_ERROR;
	}

	public void setTIPOERROR(String TIPO_ERROR) {
		this.TIPO_ERROR = TIPO_ERROR;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CbaErroresDto {\n");

		sb.append("    ERROR: ").append(StringUtil.toIndentedString(ERROR)).append("\n");
		sb.append("    DESC_ERROR: ").append(StringUtil.toIndentedString(DESC_ERROR)).append("\n");
		sb.append("    SIGLA_ERROR: ").append(StringUtil.toIndentedString(SIGLA_ERROR)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    TIPO_ERROR: ").append(StringUtil.toIndentedString(TIPO_ERROR)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("ERROR", "departamento");
		super.PROPERTY_MAP.put("DESC_ERROR", "municipio");
		super.PROPERTY_MAP.put("SIGLA_ERROR", "aldea");
		super.PROPERTY_MAP.put("ESTADO", "cacerio");
		super.PROPERTY_MAP.put("TIPO_ERROR", "descCacerio");
	}
}
