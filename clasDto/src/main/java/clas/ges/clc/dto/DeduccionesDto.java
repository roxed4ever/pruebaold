package clas.ges.clc.dto;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 **/
public class DeduccionesDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer DEDUCCION = null;
	private Integer TIPO = null;
	private Integer SUB_TIPO = null;
	private String DESCRIPCION = null;
	private String BASE_LEY = null;
	private String PRIORIDAD = null;
	private String BASE_CALCULO = null;
	private String MODO_CALCULO = null;
	private Double MONTO_PORC = null;
	private Double MONTO_FIJO = null;
	private String NETEABLE = null;
	private String API_ESTADO = null;
	private String VIGENTE = null;

	public DeduccionesDto() {
		initPROPERTY_MAP();
	}

	public DeduccionesDto(Integer dEDUCCION, Integer tIPO, Integer sUB_TIPO, String dESCRIPCION, String bASE_LEY,
			String pRIORIDAD, String bASE_CALCULO, String mODO_CALCULO, Double mONTO_PORC, Double mONTO_FIJO,
			String nETEABLE, String aPI_ESTADO, String vIGENTE) {
		super();
		initPROPERTY_MAP();
		DEDUCCION = dEDUCCION;
		TIPO = tIPO;
		SUB_TIPO = sUB_TIPO;
		DESCRIPCION = dESCRIPCION;
		BASE_LEY = bASE_LEY;
		PRIORIDAD = pRIORIDAD;
		BASE_CALCULO = bASE_CALCULO;
		MODO_CALCULO = mODO_CALCULO;
		MONTO_PORC = mONTO_PORC;
		MONTO_FIJO = mONTO_FIJO;
		NETEABLE = nETEABLE;
		API_ESTADO = aPI_ESTADO;
		VIGENTE = vIGENTE;
	}

	@JsonProperty("DEDUCCION")
	public Integer getDEDUCCION() {
		return DEDUCCION;
	}

	public void setDEDUCCION(Integer DEDUCCION) {
		this.DEDUCCION = DEDUCCION;
	}

	@JsonProperty("TIPO")
	public Integer getTIPO() {
		return TIPO;
	}

	public void setTIPO(Integer TIPO) {
		this.TIPO = TIPO;
	}

	@JsonProperty("SUB_TIPO")
	public Integer getSUBTIPO() {
		return SUB_TIPO;
	}

	public void setSUBTIPO(Integer SUB_TIPO) {
		this.SUB_TIPO = SUB_TIPO;
	}

	@JsonProperty("DESCRIPCION")
	public String getDESCRIPCION() {
		return DESCRIPCION;
	}

	public void setDESCRIPCION(String DESCRIPCION) {
		this.DESCRIPCION = DESCRIPCION;
	}

	@JsonProperty("BASE_LEY")
	public String getBASELEY() {
		return BASE_LEY;
	}

	public void setBASELEY(String BASE_LEY) {
		this.BASE_LEY = BASE_LEY;
	}

	@JsonProperty("PRIORIDAD")
	public String getPRIORIDAD() {
		return PRIORIDAD;
	}

	public void setPRIORIDAD(String PRIORIDAD) {
		this.PRIORIDAD = PRIORIDAD;
	}

	@JsonProperty("BASE_CALCULO")
	public String getBASECALCULO() {
		return BASE_CALCULO;
	}

	public void setBASECALCULO(String BASE_CALCULO) {
		this.BASE_CALCULO = BASE_CALCULO;
	}

	@JsonProperty("MODO_CALCULO")
	public String getMODOCALCULO() {
		return MODO_CALCULO;
	}

	public void setMODOCALCULO(String MODO_CALCULO) {
		this.MODO_CALCULO = MODO_CALCULO;
	}

	@JsonProperty("MONTO_PORC")
	public Double getMONTOPORC() {
		return MONTO_PORC;
	}

	public void setMONTOPORC(Double MONTO_PORC) {
		this.MONTO_PORC = MONTO_PORC;
	}

	@JsonProperty("MONTO_FIJO")
	public Double getMONTOFIJO() {
		return MONTO_FIJO;
	}

	public void setMONTOFIJO(Double MONTO_FIJO) {
		this.MONTO_FIJO = MONTO_FIJO;
	}

	@JsonProperty("NETEABLE")
	public String getNETEABLE() {
		return NETEABLE;
	}

	public void setNETEABLE(String NETEABLE) {
		this.NETEABLE = NETEABLE;
	}

	@JsonProperty("API_ESTADO")
	public String getAPIESTADO() {
		return API_ESTADO;
	}

	public void setAPIESTADO(String API_ESTADO) {
		this.API_ESTADO = API_ESTADO;
	}

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class DeduccionesDto {\n");

		sb.append("    DEDUCCION: ").append(StringUtil.toIndentedString(DEDUCCION)).append("\n");
		sb.append("    TIPO: ").append(StringUtil.toIndentedString(TIPO)).append("\n");
		sb.append("    SUB_TIPO: ").append(StringUtil.toIndentedString(SUB_TIPO)).append("\n");
		sb.append("    DESCRIPCION: ").append(StringUtil.toIndentedString(DESCRIPCION)).append("\n");
		sb.append("    BASE_LEY: ").append(StringUtil.toIndentedString(BASE_LEY)).append("\n");
		sb.append("    PRIORIDAD: ").append(StringUtil.toIndentedString(PRIORIDAD)).append("\n");
		sb.append("    BASE_CALCULO: ").append(StringUtil.toIndentedString(BASE_CALCULO)).append("\n");
		sb.append("    MODO_CALCULO: ").append(StringUtil.toIndentedString(MODO_CALCULO)).append("\n");
		sb.append("    MONTO_PORC: ").append(StringUtil.toIndentedString(MONTO_PORC)).append("\n");
		sb.append("    MONTO_FIJO: ").append(StringUtil.toIndentedString(MONTO_FIJO)).append("\n");
		sb.append("    NETEABLE: ").append(StringUtil.toIndentedString(NETEABLE)).append("\n");
		sb.append("    API_ESTADO: ").append(StringUtil.toIndentedString(API_ESTADO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("DEDUCCION", "departamento");
		super.PROPERTY_MAP.put("TIPO", "municipio");
		super.PROPERTY_MAP.put("SUB_TIPO", "aldea");
		super.PROPERTY_MAP.put("DESCRIPCION", "cacerio");
		super.PROPERTY_MAP.put("BASE_LEY", "descCacerio");
		super.PROPERTY_MAP.put("PRIORIDAD", "departamento");
		super.PROPERTY_MAP.put("BASE_CALCULO", "municipio");
		super.PROPERTY_MAP.put("MODO_CALCULO", "aldea");
		super.PROPERTY_MAP.put("MONTO_PORC", "cacerio");
		super.PROPERTY_MAP.put("MONTO_FIJO", "descCacerio");
		super.PROPERTY_MAP.put("NETEABLE", "departamento");
		super.PROPERTY_MAP.put("API_ESTADO", "municipio");
		super.PROPERTY_MAP.put("VIGENTE", "aldea");

	}

}
