package clas.ges.clg.siafiges.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-12T11:23:23.196-06:00")
public class TipoUnidadMedidaDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer UNIDAD_MEDIDA = null;
	private Integer TIPO_UNIDAD = null;
	private String NOMBRE = null;
	private String SIMBOLO = null;
	private String VIGENTE = null;

	public TipoUnidadMedidaDto() {
		initPROPERTY_MAP();
	}

	public TipoUnidadMedidaDto(Integer gESTION, Integer uNIDAD_MEDIDA, Integer tIPO_UNIDAD, String nOMBRE,
			String sIMBOLO, String vIGENTE) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		UNIDAD_MEDIDA = uNIDAD_MEDIDA;
		TIPO_UNIDAD = tIPO_UNIDAD;
		NOMBRE = nOMBRE;
		SIMBOLO = sIMBOLO;
		VIGENTE = vIGENTE;
	}

	/**
	   **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("UNIDAD_MEDIDA")
	public Integer getUNIDADMEDIDA() {
		return UNIDAD_MEDIDA;
	}

	public void setUNIDADMEDIDA(Integer UNIDAD_MEDIDA) {
		this.UNIDAD_MEDIDA = UNIDAD_MEDIDA;
	}

	/**
	 **/

	@JsonProperty("TIPO_UNIDAD")
	public Integer getTIPOUNIDAD() {
		return TIPO_UNIDAD;
	}

	public void setTIPOUNIDAD(Integer TIPO_UNIDAD) {
		this.TIPO_UNIDAD = TIPO_UNIDAD;
	}

	/**
	 **/

	@JsonProperty("NOMBRE")
	public String getNOMBRE() {
		return NOMBRE;
	}

	public void setNOMBRE(String NOMBRE) {
		this.NOMBRE = NOMBRE;
	}

	/**
	 **/

	@JsonProperty("SIMBOLO")
	public String getSIMBOLO() {
		return SIMBOLO;
	}

	public void setSIMBOLO(String SIMBOLO) {
		this.SIMBOLO = SIMBOLO;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class TipoUnidadMedidaDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    UNIDAD_MEDIDA: ").append(StringUtil.toIndentedString(UNIDAD_MEDIDA)).append("\n");
		sb.append("    TIPO_UNIDAD: ").append(StringUtil.toIndentedString(TIPO_UNIDAD)).append("\n");
		sb.append("    NOMBRE: ").append(StringUtil.toIndentedString(NOMBRE)).append("\n");
		sb.append("    SIMBOLO: ").append(StringUtil.toIndentedString(SIMBOLO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");

		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("UNIDAD_MEDIDA", "id.unidadMedida");
		super.PROPERTY_MAP.put("TIPO_UNIDAD", "tipoUnidad");
		super.PROPERTY_MAP.put("NOMBRE", "nombre");
		super.PROPERTY_MAP.put("SIMBOLO", "simbolo");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");

	}
}
