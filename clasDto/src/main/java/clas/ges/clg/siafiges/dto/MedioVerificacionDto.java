package clas.ges.clg.siafiges.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-12T11:23:23.196-06:00")
public class MedioVerificacionDto extends AmbitosDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer MEDIO_VERIFICACION = null;
	private String NOMBRE = null;
	private String DESCRIPCION = null;
	private String VIGENTE = null;

	public MedioVerificacionDto() {
		initPROPERTY_MAP();
	}

	public MedioVerificacionDto(Integer gESTION, Integer mEDIO_VERIFICACION, String nOMBRE, String dESCRIPCION,
			String vIGENTE) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		MEDIO_VERIFICACION = mEDIO_VERIFICACION;
		NOMBRE = nOMBRE;
		DESCRIPCION = dESCRIPCION;
		VIGENTE = vIGENTE;
	}

	/**
	   **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("MEDIO_VERIFICACION")
	public Integer getMEDIOVERIFICACION() {
		return MEDIO_VERIFICACION;
	}

	public void setMEDIOVERIFICACION(Integer MEDIO_VERIFICACION) {
		this.MEDIO_VERIFICACION = MEDIO_VERIFICACION;
	}

	/**
	 **/

	@JsonProperty("NOMBRE")
	public String getNOMBRE() {
		return NOMBRE;
	}

	public void setNOMBRE(String NOMBRE) {
		this.NOMBRE = NOMBRE;
	}

	/**
	 **/

	@JsonProperty("DESCRIPCION")
	public String getDESCRIPCION() {
		return DESCRIPCION;
	}

	public void setDESCRIPCION(String DESCRIPCION) {
		this.DESCRIPCION = DESCRIPCION;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MedioVerificacionDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    MEDIO_VERIFICACION: ").append(StringUtil.toIndentedString(MEDIO_VERIFICACION)).append("\n");
		sb.append("    NOMBRE: ").append(StringUtil.toIndentedString(NOMBRE)).append("\n");
		sb.append("    DESCRIPCION: ").append(StringUtil.toIndentedString(DESCRIPCION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("MEDIO_VERIFICACION", "id.medioVerificacion");
		super.PROPERTY_MAP.put("NOMBRE", "nombre");
		super.PROPERTY_MAP.put("DESCRIPCION", "descripcion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");

	}
}
