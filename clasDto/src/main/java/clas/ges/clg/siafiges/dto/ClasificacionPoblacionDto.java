package clas.ges.clg.siafiges.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-12T11:23:23.196-06:00")
public class ClasificacionPoblacionDto extends AmbitosDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer CLASIFICACION = null;
	private String NOMBRE = null;
	private String DESCRIPCION = null;
	private String VIGENTE = null;

	public ClasificacionPoblacionDto() {
		initPROPERTY_MAP();
	}

	public ClasificacionPoblacionDto(Integer gESTION, Integer cLASIFICACION, String nOMBRE, String dESCRIPCION,
			String vIGENTE) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		CLASIFICACION = cLASIFICACION;
		NOMBRE = nOMBRE;
		DESCRIPCION = dESCRIPCION;
		VIGENTE = vIGENTE;
	}

	/**
	   **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("CLASIFICACION")
	public Integer getCLASIFICACION() {
		return CLASIFICACION;
	}

	public void setCLASIFICACION(Integer CLASIFICACION) {
		this.CLASIFICACION = CLASIFICACION;
	}

	/**
	 **/

	@JsonProperty("NOMBRE")
	public String getNOMBRE() {
		return NOMBRE;
	}

	public void setNOMBRE(String NOMBRE) {
		this.NOMBRE = NOMBRE;
	}

	/**
	 **/

	@JsonProperty("DESCRIPCION")
	public String getDESCRIPCION() {
		return DESCRIPCION;
	}

	public void setDESCRIPCION(String DESCRIPCION) {
		this.DESCRIPCION = DESCRIPCION;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ClasificacionPoblacionDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    CLASIFICACION: ").append(StringUtil.toIndentedString(CLASIFICACION)).append("\n");
		sb.append("    NOMBRE: ").append(StringUtil.toIndentedString(NOMBRE)).append("\n");
		sb.append("    DESCRIPCION: ").append(StringUtil.toIndentedString(DESCRIPCION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("CLASIFICACION", "id.clasificacion");
		super.PROPERTY_MAP.put("NOMBRE", "nombre");
		super.PROPERTY_MAP.put("DESCRIPCION", "descripcion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");

	}
}
