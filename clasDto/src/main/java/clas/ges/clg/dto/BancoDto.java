package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;


/**
 * 
 **/
public class BancoDto extends AmbitosDto implements Serializable{
  
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
	
  private Integer BANCO = null;
  private String DESC_BANCO = null;
  private String SIGLA_BANCO = null;
  private String TIPO_BANCO = null;
  private String CUENTA_ENCAJE = null;
  private String RECIPIENTE_ID = null;
  private String ESTADO = null;
  private String CONECTADO_SIAFI = null;
  private String HABILITADO_ENVIOS = null;
  private String BEN_PAIS_ID = null;
  private String BEN_TIPO_ID = null;
  private String BEN_NRO_ID = null;
  private Integer BEN_BANCO = null;
  private String BEN_CUENTA = null;

  public BancoDto() {
	super();
  }
  
  public BancoDto(Integer bANCO, String dESC_BANCO, String sIGLA_BANCO, String tIPO_BANCO, String cUENTA_ENCAJE,
		String rECIPIENTE_ID, String eSTADO, String cONECTADO_SIAFI, String hABILITADO_ENVIOS, String bEN_PAIS_ID,
		String bEN_TIPO_ID, String bEN_NRO_ID, Integer bEN_BANCO, String bEN_CUENTA) {
	BANCO = bANCO;
	DESC_BANCO = dESC_BANCO;
	SIGLA_BANCO = sIGLA_BANCO;
	TIPO_BANCO = tIPO_BANCO;
	CUENTA_ENCAJE = cUENTA_ENCAJE;
	RECIPIENTE_ID = rECIPIENTE_ID;
	ESTADO = eSTADO;
	CONECTADO_SIAFI = cONECTADO_SIAFI;
	HABILITADO_ENVIOS = hABILITADO_ENVIOS;
	BEN_PAIS_ID = bEN_PAIS_ID;
	BEN_TIPO_ID = bEN_TIPO_ID;
	BEN_NRO_ID = bEN_NRO_ID;
	BEN_BANCO = bEN_BANCO;
	BEN_CUENTA = bEN_CUENTA;
}
  
  public BancoDto(Integer bANCO, String dESC_BANCO, String sIGLA_BANCO, String tIPO_BANCO, String cUENTA_ENCAJE,
		String rECIPIENTE_ID, String eSTADO, String cONECTADO_SIAFI, String hABILITADO_ENVIOS, String bEN_PAIS_ID,
		String bEN_TIPO_ID, String bEN_NRO_ID, Integer bEN_BANCO, String bEN_CUENTA, PaginacionDto pAGINACION,
		Boolean pAGINAR, Boolean fILTRAR_PAGINA, Boolean oRDENAR_PAGINA, String pAGINACION_FILTRO,
		Map<String, Boolean> pAGINACION_ORDEN, Map<String, String> aMBITOS) {
	super(pAGINAR,fILTRAR_PAGINA,oRDENAR_PAGINA,pAGINACION,pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
	BANCO = bANCO;
	DESC_BANCO = dESC_BANCO;
	SIGLA_BANCO = sIGLA_BANCO;
	TIPO_BANCO = tIPO_BANCO;
	CUENTA_ENCAJE = cUENTA_ENCAJE;
	RECIPIENTE_ID = rECIPIENTE_ID;
	ESTADO = eSTADO;
	CONECTADO_SIAFI = cONECTADO_SIAFI;
	HABILITADO_ENVIOS = hABILITADO_ENVIOS;
	BEN_PAIS_ID = bEN_PAIS_ID;
	BEN_TIPO_ID = bEN_TIPO_ID;
	BEN_NRO_ID = bEN_NRO_ID;
	BEN_BANCO = bEN_BANCO;
	BEN_CUENTA = bEN_CUENTA;
}
/**
   **/

  @JsonProperty("BANCO")
  public Integer getBANCO() {
    return BANCO;
  }
  public void setBANCO(Integer BANCO) {
    this.BANCO = BANCO;
  }

  
  /**
   **/
  
  @JsonProperty("DESC_BANCO")
  public String getDESCBANCO() {
    return DESC_BANCO;
  }
  public void setDESCBANCO(String DESC_BANCO) {
    this.DESC_BANCO = DESC_BANCO;
  }

  
  /**
   **/
  @JsonProperty("SIGLA_BANCO")
  public String getSIGLABANCO() {
    return SIGLA_BANCO;
  }
  public void setSIGLABANCO(String SIGLA_BANCO) {
    this.SIGLA_BANCO = SIGLA_BANCO;
  }

  
  /**
   **/
  @JsonProperty("TIPO_BANCO")
  public String getTIPOBANCO() {
    return TIPO_BANCO;
  }
  public void setTIPOBANCO(String TIPO_BANCO) {
    this.TIPO_BANCO = TIPO_BANCO;
  }

  
  /**
   **/
  
  @JsonProperty("CUENTA_ENCAJE")
  public String getCUENTAENCAJE() {
    return CUENTA_ENCAJE;
  }
  public void setCUENTAENCAJE(String CUENTA_ENCAJE) {
    this.CUENTA_ENCAJE = CUENTA_ENCAJE;
  }

  
  /**
   **/
  
  @JsonProperty("RECIPIENTE_ID")
  public String getRECIPIENTEID() {
    return RECIPIENTE_ID;
  }
  public void setRECIPIENTEID(String RECIPIENTE_ID) {
    this.RECIPIENTE_ID = RECIPIENTE_ID;
  }

  
  /**
   **/
  
  @JsonProperty("ESTADO")
  public String getESTADO() {
    return ESTADO;
  }
  public void setESTADO(String ESTADO) {
    this.ESTADO = ESTADO;
  }

  
  /**
   **/
  
  @JsonProperty("CONECTADO_SIAFI")
  public String getCONECTADOSIAFI() {
    return CONECTADO_SIAFI;
  }
  public void setCONECTADOSIAFI(String CONECTADO_SIAFI) {
    this.CONECTADO_SIAFI = CONECTADO_SIAFI;
  }

  
  /**
   **/
  
  @JsonProperty("HABILITADO_ENVIOS")
  public String getHABILITADOENVIOS() {
    return HABILITADO_ENVIOS;
  }
  public void setHABILITADOENVIOS(String HABILITADO_ENVIOS) {
    this.HABILITADO_ENVIOS = HABILITADO_ENVIOS;
  }

  
  /**
   **/
  
  @JsonProperty("BEN_PAIS_ID")
  public String getBENPAISID() {
    return BEN_PAIS_ID;
  }
  public void setBENPAISID(String BEN_PAIS_ID) {
    this.BEN_PAIS_ID = BEN_PAIS_ID;
  }

  
  /**
   **/
  
  @JsonProperty("BEN_TIPO_ID")
  public String getBENTIPOID() {
    return BEN_TIPO_ID;
  }
  public void setBENTIPOID(String BEN_TIPO_ID) {
    this.BEN_TIPO_ID = BEN_TIPO_ID;
  }

  
  /**
   **/
  
  @JsonProperty("BEN_NRO_ID")
  public String getBENNROID() {
    return BEN_NRO_ID;
  }
  public void setBENNROID(String BEN_NRO_ID) {
    this.BEN_NRO_ID = BEN_NRO_ID;
  }

  
  /**
   **/
  
  @JsonProperty("BEN_BANCO")
  public Integer getBENBANCO() {
    return BEN_BANCO;
  }
  public void setBENBANCO(Integer BEN_BANCO) {
    this.BEN_BANCO = BEN_BANCO;
  }

  
  /**
   **/
  
  @JsonProperty("BEN_CUENTA")
  public String getBENCUENTA() {
    return BEN_CUENTA;
  }
  public void setBENCUENTA(String BEN_CUENTA) {
    this.BEN_CUENTA = BEN_CUENTA;
  }


  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class BancoDto {\n");
    
    sb.append("    BANCO: ").append(StringUtil.toIndentedString(BANCO)).append("\n");
    sb.append("    DESC_BANCO: ").append(StringUtil.toIndentedString(DESC_BANCO)).append("\n");
    sb.append("    SIGLA_BANCO: ").append(StringUtil.toIndentedString(SIGLA_BANCO)).append("\n");
    sb.append("    TIPO_BANCO: ").append(StringUtil.toIndentedString(TIPO_BANCO)).append("\n");
    sb.append("    CUENTA_ENCAJE: ").append(StringUtil.toIndentedString(CUENTA_ENCAJE)).append("\n");
    sb.append("    RECIPIENTE_ID: ").append(StringUtil.toIndentedString(RECIPIENTE_ID)).append("\n");
    sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
    sb.append("    CONECTADO_SIAFI: ").append(StringUtil.toIndentedString(CONECTADO_SIAFI)).append("\n");
    sb.append("    HABILITADO_ENVIOS: ").append(StringUtil.toIndentedString(HABILITADO_ENVIOS)).append("\n");
    sb.append("    BEN_PAIS_ID: ").append(StringUtil.toIndentedString(BEN_PAIS_ID)).append("\n");
    sb.append("    BEN_TIPO_ID: ").append(StringUtil.toIndentedString(BEN_TIPO_ID)).append("\n");
    sb.append("    BEN_NRO_ID: ").append(StringUtil.toIndentedString(BEN_NRO_ID)).append("\n");
    sb.append("    BEN_BANCO: ").append(StringUtil.toIndentedString(BEN_BANCO)).append("\n");
    sb.append("    BEN_CUENTA: ").append(StringUtil.toIndentedString(BEN_CUENTA)).append("\n");
    sb.append("}");
    return sb.toString();
  }
}
