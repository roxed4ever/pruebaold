package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
 * Objeto de transferencia de datos para clasificador de Aldeas
 **/
public class AldeaDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer DEPARTAMENTO = null;
	private Integer MUNICIPIO = null;
	private Integer ALDEA = null;
	private String DESC_ALDEA = null;
	private String VIGENTE = null;
	private String ESTADO = null;
	

	public AldeaDto() {
		initPROPERTY_MAP();
	}
	
	public AldeaDto(Integer dEPARTAMENTO, Integer mUNICIPIO, Integer aLDEA, String dESC_ALDEA, String vIGENTE,
			String eSTADO) {
		super();
		initPROPERTY_MAP();
		DEPARTAMENTO = dEPARTAMENTO;
		MUNICIPIO = mUNICIPIO;
		ALDEA = aLDEA;
		DESC_ALDEA = dESC_ALDEA;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}
		
	public AldeaDto(Integer dEPARTAMENTO, Integer mUNICIPIO, Integer aLDEA, String dESC_ALDEA, String vIGENTE,
			String eSTADO, Boolean pAGINAR, Boolean fILTRAR_PAGINA, Boolean oRDENAR_PAGINA, String pAGINACION_FILTRO,
			Map<String, Boolean> pAGINACION_ORDEN, Map<String, String> aMBITOS, PaginacionDto pAGINACION) {
		super(pAGINAR,fILTRAR_PAGINA,oRDENAR_PAGINA,pAGINACION,pAGINACION_FILTRO, pAGINACION_ORDEN, aMBITOS);
		DEPARTAMENTO = dEPARTAMENTO;
		MUNICIPIO = mUNICIPIO;
		ALDEA = aLDEA;
		DESC_ALDEA = dESC_ALDEA;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}



	/**
	 **/
	@JsonProperty("DEPARTAMENTO")
	public Integer getDEPARTAMENTO() {
		return DEPARTAMENTO;
	}

	public void setDEPARTAMENTO(Integer DEPARTAMENTO) {
		this.DEPARTAMENTO = DEPARTAMENTO;
	}

	/**
	 **/
	@JsonProperty("MUNICIPIO")
	public Integer getMUNICIPIO() {
		return MUNICIPIO;
	}

	public void setMUNICIPIO(Integer MUNICIPIO) {
		this.MUNICIPIO = MUNICIPIO;
	}

	/**
	 **/
	@JsonProperty("ALDEA")
	public Integer getALDEA() {
		return ALDEA;
	}

	public void setALDEA(Integer ALDEA) {
		this.ALDEA = ALDEA;
	}

	/**
	 **/
	@JsonProperty("DESC_ALDEA")
	public String getDESCALDEA() {
		return DESC_ALDEA;
	}

	public void setDESCALDEA(String DESC_ALDEA) {
		this.DESC_ALDEA = DESC_ALDEA;
	}

	/**
	 **/
	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/
	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class AldeaDto {\n");

		sb.append("    DEPARTAMENTO: ").append(StringUtil.toIndentedString(DEPARTAMENTO)).append("\n");
		sb.append("    MUNICIPIO: ").append(StringUtil.toIndentedString(MUNICIPIO)).append("\n");
		sb.append("    ALDEA: ").append(StringUtil.toIndentedString(ALDEA)).append("\n");
		sb.append("    DESC_ALDEA: ").append(StringUtil.toIndentedString(DESC_ALDEA)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    PAGINAR: ").append(StringUtil.toIndentedString(super.isPAGINAR())).append("\n");
		sb.append("    FILTRAR_PAGINA: ").append(StringUtil.toIndentedString(super.isFILTRAR_PAGINA())).append("\n");
		sb.append("    ORDENAR_PAGINA: ").append(StringUtil.toIndentedString(super.isORDENAR_PAGINA())).append("\n");
		sb.append("    PAGINACION_FILTRO: ").append(StringUtil.toIndentedString(super.getPAGINACION_FILTRO())).append("\n");
		sb.append("    PAGINACION_ORDEN: ").append(StringUtil.toIndentedString(super.getPAGINACION_ORDEN())).append("\n");
		sb.append("    AMBITOS: ").append(StringUtil.toIndentedString(super.getAMBITOS())).append("\n");
		sb.append("    PAGINACION: ").append(StringUtil.toIndentedString(super.getPAGINACION())).append("\n");
		sb.append("}");
		return sb.toString();
	}
	

	public void initPROPERTY_MAP(){
		super.PROPERTY_MAP = new HashMap<String,String>();
		super.PROPERTY_MAP.put("DEPARTAMENTO","id.departamento");
		super.PROPERTY_MAP.put("MUNICIPIO","id.municipio");
		super.PROPERTY_MAP.put("ALDEA","id.aldea");
		super.PROPERTY_MAP.put("DESC_ALDEA","descAldea");//
		super.PROPERTY_MAP.put("VIGENTE","vigente");
		super.PROPERTY_MAP.put("ESTADO","apiEstado");
	}
}
