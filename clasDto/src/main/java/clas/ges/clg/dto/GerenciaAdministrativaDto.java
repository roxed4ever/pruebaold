package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class GerenciaAdministrativaDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer INSTITUCION = null;
	private Integer GA = null;
	private String DESC_GA = null;
	private String TIPO_GA = null;
	private Integer ETAPA_DOCUMENTO = null;
	private String VIGENTE = null;

	public GerenciaAdministrativaDto() {
		 initPROPERTY_MAP();
	}

	public GerenciaAdministrativaDto(Integer gESTION, Integer iNSTITUCION, Integer gA, String dESC_GA, String tIPO_GA,
			Integer eTAPA_DOCUMENTO, String vIGENTE) {
		super();
		 initPROPERTY_MAP();
		GESTION = gESTION;
		INSTITUCION = iNSTITUCION;
		GA = gA;
		DESC_GA = dESC_GA;
		TIPO_GA = tIPO_GA;
		ETAPA_DOCUMENTO = eTAPA_DOCUMENTO;
		VIGENTE = vIGENTE;
	}

	/**
	   **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 **/

	@JsonProperty("GA")
	public Integer getGA() {
		return GA;
	}

	public void setGA(Integer GA) {
		this.GA = GA;
	}

	/**
	 **/

	@JsonProperty("DESC_GA")
	public String getDESCGA() {
		return DESC_GA;
	}

	public void setDESCGA(String DESC_GA) {
		this.DESC_GA = DESC_GA;
	}

	/**
	 **/

	@JsonProperty("TIPO_GA")
	public String getTIPOGA() {
		return TIPO_GA;
	}

	public void setTIPOGA(String TIPO_GA) {
		this.TIPO_GA = TIPO_GA;
	}

	/**
	 **/

	@JsonProperty("ETAPA_DOCUMENTO")
	public Integer getETAPADOCUMENTO() {
		return ETAPA_DOCUMENTO;
	}

	public void setETAPADOCUMENTO(Integer ETAPA_DOCUMENTO) {
		this.ETAPA_DOCUMENTO = ETAPA_DOCUMENTO;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class GerenciaAdministrativaDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    GA: ").append(StringUtil.toIndentedString(GA)).append("\n");
		sb.append("    DESC_GA: ").append(StringUtil.toIndentedString(DESC_GA)).append("\n");
		sb.append("    TIPO_GA: ").append(StringUtil.toIndentedString(TIPO_GA)).append("\n");
		sb.append("    ETAPA_DOCUMENTO: ").append(StringUtil.toIndentedString(ETAPA_DOCUMENTO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "institucion");
		super.PROPERTY_MAP.put("GA", "ga");
		super.PROPERTY_MAP.put("DESC_GA", "descGa");
		super.PROPERTY_MAP.put("TIPO_GA", "tipoGa");
		super.PROPERTY_MAP.put("ETAPA_DOCUMENTO", "etapaDocumento");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
	}
}
