package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class RegionDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String REGION = null;
	private String DESC_REGION = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	/**
	 **/

	public RegionDto() {
		initPROPERTY_MAP();
	}

	public RegionDto(String rEGION, String dESC_REGION, String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		REGION = rEGION;
		DESC_REGION = dESC_REGION;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	@JsonProperty("REGION")
	public String getREGION() {
		return REGION;
	}

	public void setREGION(String REGION) {
		this.REGION = REGION;
	}

	/**
	 **/

	@JsonProperty("DESC_REGION")
	public String getDESCREGION() {
		return DESC_REGION;
	}

	public void setDESCREGION(String DESC_REGION) {
		this.DESC_REGION = DESC_REGION;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class RegionDto {\n");

		sb.append("    REGION: ").append(StringUtil.toIndentedString(REGION)).append("\n");
		sb.append("    DESC_REGION: ").append(StringUtil.toIndentedString(DESC_REGION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("REGION", "region");
		super.PROPERTY_MAP.put("DESC_REGION", "descRegion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
