package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class CuencaDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String CUENCA = null;
	private String DESC_CUENCA = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	/**
	 **/

	public CuencaDto() {
		super();
		initPROPERTY_MAP();
	}
	
	
	public CuencaDto(String cUENCA, String dESC_CUENCA, String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		CUENCA = cUENCA;
		DESC_CUENCA = dESC_CUENCA;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}


	@JsonProperty("CUENCA")
	public String getCUENCA() {
		return CUENCA;
	}

	public void setCUENCA(String CUENCA) {
		this.CUENCA = CUENCA;
	}

	/**
	 **/

	@JsonProperty("DESC_CUENCA")
	public String getDESCCUENCA() {
		return DESC_CUENCA;
	}

	public void setDESCCUENCA(String DESC_CUENCA) {
		this.DESC_CUENCA = DESC_CUENCA;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CuencaDto {\n");

		sb.append("    CUENCA: ").append(StringUtil.toIndentedString(CUENCA)).append("\n");
		sb.append("    DESC_CUENCA: ").append(StringUtil.toIndentedString(DESC_CUENCA)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("CUENCA", "cuenca");
		super.PROPERTY_MAP.put("DESC_CUENCA", "descCuenca");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
