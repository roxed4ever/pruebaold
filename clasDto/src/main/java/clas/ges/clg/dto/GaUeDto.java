package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class GaUeDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer INSTITUCION = null;
	private Integer GA = null;
	private Integer UE = null;
	private Integer ETAPA_DOCUMENTO = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	public GaUeDto() {
		initPROPERTY_MAP();
	}

	public GaUeDto(Integer gESTION, Integer iNSTITUCION, Integer gA, Integer uE, Integer eTAPA_DOCUMENTO,
			String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		INSTITUCION = iNSTITUCION;
		GA = gA;
		UE = uE;
		ETAPA_DOCUMENTO = eTAPA_DOCUMENTO;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	/**
	   **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 **/

	@JsonProperty("GA")
	public Integer getGA() {
		return GA;
	}

	public void setGA(Integer GA) {
		this.GA = GA;
	}

	/**
	 **/

	@JsonProperty("UE")
	public Integer getUE() {
		return UE;
	}

	public void setUE(Integer UE) {
		this.UE = UE;
	}

	/**
	 **/

	@JsonProperty("ETAPA_DOCUMENTO")
	public Integer getETAPADOCUMENTO() {
		return ETAPA_DOCUMENTO;
	}

	public void setETAPADOCUMENTO(Integer ETAPA_DOCUMENTO) {
		this.ETAPA_DOCUMENTO = ETAPA_DOCUMENTO;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class GaUeDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    GA: ").append(StringUtil.toIndentedString(GA)).append("\n");
		sb.append("    UE: ").append(StringUtil.toIndentedString(UE)).append("\n");
		sb.append("    ETAPA_DOCUMENTO: ").append(StringUtil.toIndentedString(ETAPA_DOCUMENTO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "institucion");
		super.PROPERTY_MAP.put("GA", "ga");
		super.PROPERTY_MAP.put("UE", "ue");
		super.PROPERTY_MAP.put("ETAPA_DOCUMENTO", "etapaDocumento");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");

	}
}
