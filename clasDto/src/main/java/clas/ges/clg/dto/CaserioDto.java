package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class CaserioDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer DEPARTAMENTO = null;
	private Integer MUNICIPIO = null;
	private Integer ALDEA = null;
	private Integer CACERIO = null;
	private String DESC_CACERIO = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	
	public CaserioDto() {
		initPROPERTY_MAP();
	}
	
	
	public CaserioDto(Integer dEPARTAMENTO, Integer mUNICIPIO, Integer aLDEA, Integer cACERIO, String dESC_CACERIO,
			String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		DEPARTAMENTO = dEPARTAMENTO;
		MUNICIPIO = mUNICIPIO;
		ALDEA = aLDEA;
		CACERIO = cACERIO;
		DESC_CACERIO = dESC_CACERIO;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}


	/**
	 **/

	@JsonProperty("DEPARTAMENTO")
	public Integer getDEPARTAMENTO() {
		return DEPARTAMENTO;
	}

	public void setDEPARTAMENTO(Integer DEPARTAMENTO) {
		this.DEPARTAMENTO = DEPARTAMENTO;
	}

	/**
	 **/

	@JsonProperty("MUNICIPIO")
	public Integer getMUNICIPIO() {
		return MUNICIPIO;
	}

	public void setMUNICIPIO(Integer MUNICIPIO) {
		this.MUNICIPIO = MUNICIPIO;
	}

	/**
	 **/

	@JsonProperty("ALDEA")
	public Integer getALDEA() {
		return ALDEA;
	}

	public void setALDEA(Integer ALDEA) {
		this.ALDEA = ALDEA;
	}

	/**
	 **/

	@JsonProperty("CACERIO")
	public Integer getCACERIO() {
		return CACERIO;
	}

	public void setCACERIO(Integer CACERIO) {
		this.CACERIO = CACERIO;
	}

	/**
	 **/

	@JsonProperty("DESC_CACERIO")
	public String getDESCCACERIO() {
		return DESC_CACERIO;
	}

	public void setDESCCACERIO(String DESC_CACERIO) {
		this.DESC_CACERIO = DESC_CACERIO;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CaserioDto {\n");

		sb.append("    DEPARTAMENTO: ").append(StringUtil.toIndentedString(DEPARTAMENTO)).append("\n");
		sb.append("    MUNICIPIO: ").append(StringUtil.toIndentedString(MUNICIPIO)).append("\n");
		sb.append("    ALDEA: ").append(StringUtil.toIndentedString(ALDEA)).append("\n");
		sb.append("    CACERIO: ").append(StringUtil.toIndentedString(CACERIO)).append("\n");
		sb.append("    DESC_CACERIO: ").append(StringUtil.toIndentedString(DESC_CACERIO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("DEPARTAMENTO", "id.departamento");
		super.PROPERTY_MAP.put("MUNICIPIO", "id.municipio");
		super.PROPERTY_MAP.put("ALDEA", "id.aldea");
		super.PROPERTY_MAP.put("CACERIO", "id.cacerio");
		super.PROPERTY_MAP.put("DESC_CACERIO", "descCacerio");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}

}
