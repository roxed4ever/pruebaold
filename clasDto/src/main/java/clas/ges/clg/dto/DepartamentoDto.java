package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class DepartamentoDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer DEPARTAMENTO = null;
	private String DESC_DEPARTAMENTO = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	public DepartamentoDto() {
		initPROPERTY_MAP();
	}
	
	
	
	
	public DepartamentoDto(Integer dEPARTAMENTO, String dESC_DEPARTAMENTO, String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		DEPARTAMENTO = dEPARTAMENTO;
		DESC_DEPARTAMENTO = dESC_DEPARTAMENTO;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}




	/**
	 **/

	@JsonProperty("DEPARTAMENTO")
	public Integer getDEPARTAMENTO() {
		return DEPARTAMENTO;
	}

	public void setDEPARTAMENTO(Integer DEPARTAMENTO) {
		this.DEPARTAMENTO = DEPARTAMENTO;
	}

	/**
	 **/

	@JsonProperty("DESC_DEPARTAMENTO")
	public String getDESCDEPARTAMENTO() {
		return DESC_DEPARTAMENTO;
	}

	public void setDESCDEPARTAMENTO(String DESC_DEPARTAMENTO) {
		this.DESC_DEPARTAMENTO = DESC_DEPARTAMENTO;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class DepartamentoDto {\n");

		sb.append("    DEPARTAMENTO: ").append(StringUtil.toIndentedString(DEPARTAMENTO)).append("\n");
		sb.append("    DESC_DEPARTAMENTO: ").append(StringUtil.toIndentedString(DESC_DEPARTAMENTO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("DEPARTAMENTO", "departamento");
		super.PROPERTY_MAP.put("DESC_DEPARTAMENTO", "descDepartamento");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
