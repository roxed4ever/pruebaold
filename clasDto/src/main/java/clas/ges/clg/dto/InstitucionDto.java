package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class InstitucionDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer INSTITUCION = null;
	private String DESC_INSTITUCION = null;
	private String SIGLA_INSTITUCION = null;
	private Integer CODIGO_ANTERIOR = null;
	private String GABINETE = null;
	private String VIGENTE = null;
	private String ESTADO = null;
	private String EJECUTA_INGRESOS = null;
	private String EJECUTA_EGRESOS = null;
	private Integer RTN = null;
	private String BASE_LEGAL = null;
	private String FORMULA_PPTO = null;
	private String DETALLA_PPTO = null;
	private String OPERA_CUT = null;

	public InstitucionDto() {
		initPROPERTY_MAP();
	}
	
	
	public InstitucionDto(Integer iNSTITUCION, String dESC_INSTITUCION, String sIGLA_INSTITUCION,
			Integer cODIGO_ANTERIOR, String gABINETE, String vIGENTE, String eSTADO, String eJECUTA_INGRESOS,
			String eJECUTA_EGRESOS, Integer rTN, String bASE_LEGAL, String fORMULA_PPTO, String dETALLA_PPTO,
			String oPERA_CUT) {
		super();
		initPROPERTY_MAP();
		INSTITUCION = iNSTITUCION;
		DESC_INSTITUCION = dESC_INSTITUCION;
		SIGLA_INSTITUCION = sIGLA_INSTITUCION;
		CODIGO_ANTERIOR = cODIGO_ANTERIOR;
		GABINETE = gABINETE;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
		EJECUTA_INGRESOS = eJECUTA_INGRESOS;
		EJECUTA_EGRESOS = eJECUTA_EGRESOS;
		RTN = rTN;
		BASE_LEGAL = bASE_LEGAL;
		FORMULA_PPTO = fORMULA_PPTO;
		DETALLA_PPTO = dETALLA_PPTO;
		OPERA_CUT = oPERA_CUT;
	}


	/**
	 **/

	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 **/

	@JsonProperty("DESC_INSTITUCION")
	public String getDESCINSTITUCION() {
		return DESC_INSTITUCION;
	}

	public void setDESCINSTITUCION(String DESC_INSTITUCION) {
		this.DESC_INSTITUCION = DESC_INSTITUCION;
	}

	/**
	 **/

	@JsonProperty("SIGLA_INSTITUCION")
	public String getSIGLAINSTITUCION() {
		return SIGLA_INSTITUCION;
	}

	public void setSIGLAINSTITUCION(String SIGLA_INSTITUCION) {
		this.SIGLA_INSTITUCION = SIGLA_INSTITUCION;
	}

	/**
	 **/

	@JsonProperty("CODIGO_ANTERIOR")
	public Integer getCODIGOANTERIOR() {
		return CODIGO_ANTERIOR;
	}

	public void setCODIGOANTERIOR(Integer CODIGO_ANTERIOR) {
		this.CODIGO_ANTERIOR = CODIGO_ANTERIOR;
	}

	/**
	 **/

	@JsonProperty("GABINETE")
	public String getGABINETE() {
		return GABINETE;
	}

	public void setGABINETE(String GABINETE) {
		this.GABINETE = GABINETE;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	/**
	 **/

	@JsonProperty("EJECUTA_INGRESOS")
	public String getEJECUTAINGRESOS() {
		return EJECUTA_INGRESOS;
	}

	public void setEJECUTAINGRESOS(String EJECUTA_INGRESOS) {
		this.EJECUTA_INGRESOS = EJECUTA_INGRESOS;
	}

	/**
	 **/

	@JsonProperty("EJECUTA_EGRESOS")
	public String getEJECUTAEGRESOS() {
		return EJECUTA_EGRESOS;
	}

	public void setEJECUTAEGRESOS(String EJECUTA_EGRESOS) {
		this.EJECUTA_EGRESOS = EJECUTA_EGRESOS;
	}

	/**
	 **/

	@JsonProperty("RTN")
	public Integer getRTN() {
		return RTN;
	}

	public void setRTN(Integer RTN) {
		this.RTN = RTN;
	}

	/**
	 **/

	@JsonProperty("BASE_LEGAL")
	public String getBASELEGAL() {
		return BASE_LEGAL;
	}

	public void setBASELEGAL(String BASE_LEGAL) {
		this.BASE_LEGAL = BASE_LEGAL;
	}

	/**
	 **/

	@JsonProperty("FORMULA_PPTO")
	public String getFORMULAPPTO() {
		return FORMULA_PPTO;
	}

	public void setFORMULAPPTO(String FORMULA_PPTO) {
		this.FORMULA_PPTO = FORMULA_PPTO;
	}

	/**
	 **/

	@JsonProperty("DETALLA_PPTO")
	public String getDETALLAPPTO() {
		return DETALLA_PPTO;
	}

	public void setDETALLAPPTO(String DETALLA_PPTO) {
		this.DETALLA_PPTO = DETALLA_PPTO;
	}

	/**
	 **/

	@JsonProperty("OPERA_CUT")
	public String getOPERACUT() {
		return OPERA_CUT;
	}

	public void setOPERACUT(String OPERA_CUT) {
		this.OPERA_CUT = OPERA_CUT;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class InstitucionDto {\n");

		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    DESC_INSTITUCION: ").append(StringUtil.toIndentedString(DESC_INSTITUCION)).append("\n");
		sb.append("    SIGLA_INSTITUCION: ").append(StringUtil.toIndentedString(SIGLA_INSTITUCION)).append("\n");
		sb.append("    CODIGO_ANTERIOR: ").append(StringUtil.toIndentedString(CODIGO_ANTERIOR)).append("\n");
		sb.append("    GABINETE: ").append(StringUtil.toIndentedString(GABINETE)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("    EJECUTA_INGRESOS: ").append(StringUtil.toIndentedString(EJECUTA_INGRESOS)).append("\n");
		sb.append("    EJECUTA_EGRESOS: ").append(StringUtil.toIndentedString(EJECUTA_EGRESOS)).append("\n");
		sb.append("    RTN: ").append(StringUtil.toIndentedString(RTN)).append("\n");
		sb.append("    BASE_LEGAL: ").append(StringUtil.toIndentedString(BASE_LEGAL)).append("\n");
		sb.append("    FORMULA_PPTO: ").append(StringUtil.toIndentedString(FORMULA_PPTO)).append("\n");
		sb.append("    DETALLA_PPTO: ").append(StringUtil.toIndentedString(DETALLA_PPTO)).append("\n");
		sb.append("    OPERA_CUT: ").append(StringUtil.toIndentedString(OPERA_CUT)).append("\n");
		sb.append("}");
		return sb.toString();
	}
	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("INSTITUCION", "gestion");
		super.PROPERTY_MAP.put("DESC_INSTITUCION", "gabinete");
		super.PROPERTY_MAP.put("SIGLA_INSTITUCION", "descGabinete");
		super.PROPERTY_MAP.put("CODIGO_ANTERIOR", "institucion");
		super.PROPERTY_MAP.put("GABINETE", "vigente");
		super.PROPERTY_MAP.put("VIGENTE", "apiEstado");
		super.PROPERTY_MAP.put("ESTADO", "gestion");
		super.PROPERTY_MAP.put("EJECUTA_INGRESOS", "gabinete");
		super.PROPERTY_MAP.put("EJECUTA_EGRESOS", "descGabinete");
		super.PROPERTY_MAP.put("RTN", "institucion");
		super.PROPERTY_MAP.put("BASE_LEGAL", "vigente");
		super.PROPERTY_MAP.put("FORMULA_PPTO", "apiEstado");
		super.PROPERTY_MAP.put("DETALLA_PPTO", "gestion");
		super.PROPERTY_MAP.put("OPERA_CUT", "gabinete");
	}
}
