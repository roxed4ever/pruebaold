package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class MunicipioDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer DEPARTAMENTO = null;
	private Integer MUNICIPIO = null;
	private String DESC_MUNICIPIO = null;
	private String SIGLA_MUNICIPIO = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	/**
	 **/

	public MunicipioDto() {
		initPROPERTY_MAP();
	}

	public MunicipioDto(Integer dEPARTAMENTO, Integer mUNICIPIO, String dESC_MUNICIPIO, String sIGLA_MUNICIPIO,
			String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		DEPARTAMENTO = dEPARTAMENTO;
		MUNICIPIO = mUNICIPIO;
		DESC_MUNICIPIO = dESC_MUNICIPIO;
		SIGLA_MUNICIPIO = sIGLA_MUNICIPIO;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	@JsonProperty("DEPARTAMENTO")
	public Integer getDEPARTAMENTO() {
		return DEPARTAMENTO;
	}

	public void setDEPARTAMENTO(Integer DEPARTAMENTO) {
		this.DEPARTAMENTO = DEPARTAMENTO;
	}

	/**
	 **/

	@JsonProperty("MUNICIPIO")
	public Integer getMUNICIPIO() {
		return MUNICIPIO;
	}

	public void setMUNICIPIO(Integer MUNICIPIO) {
		this.MUNICIPIO = MUNICIPIO;
	}

	/**
	 **/

	@JsonProperty("DESC_MUNICIPIO")
	public String getDESCMUNICIPIO() {
		return DESC_MUNICIPIO;
	}

	public void setDESCMUNICIPIO(String DESC_MUNICIPIO) {
		this.DESC_MUNICIPIO = DESC_MUNICIPIO;
	}

	/**
	 **/

	@JsonProperty("SIGLA_MUNICIPIO")
	public String getSIGLAMUNICIPIO() {
		return SIGLA_MUNICIPIO;
	}

	public void setSIGLAMUNICIPIO(String SIGLA_MUNICIPIO) {
		this.SIGLA_MUNICIPIO = SIGLA_MUNICIPIO;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MunicipioDto {\n");

		sb.append("    DEPARTAMENTO: ").append(StringUtil.toIndentedString(DEPARTAMENTO)).append("\n");
		sb.append("    MUNICIPIO: ").append(StringUtil.toIndentedString(MUNICIPIO)).append("\n");
		sb.append("    DESC_MUNICIPIO: ").append(StringUtil.toIndentedString(DESC_MUNICIPIO)).append("\n");
		sb.append("    SIGLA_MUNICIPIO: ").append(StringUtil.toIndentedString(SIGLA_MUNICIPIO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("DEPARTAMENTO", "cuenca");
		super.PROPERTY_MAP.put("MUNICIPIO", "descCuenca");
		super.PROPERTY_MAP.put("DESC_MUNICIPIO", "vigente");
		super.PROPERTY_MAP.put("SIGLA_MUNICIPIO", "apiEstado");
		super.PROPERTY_MAP.put("VIGENTE", "apiEstado");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
