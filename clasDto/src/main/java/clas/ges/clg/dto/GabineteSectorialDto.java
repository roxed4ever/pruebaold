package clas.ges.clg.dto;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/
public class GabineteSectorialDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer GABINETE = null;
	private String DESC_GABINETE = null;
	private Integer INSTITUCION = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	public GabineteSectorialDto() {
		initPROPERTY_MAP();
	}

	public GabineteSectorialDto(Integer gESTION, Integer gABINETE, String dESC_GABINETE, Integer iNSTITUCION,
			String vIGENTE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		GABINETE = gABINETE;
		DESC_GABINETE = dESC_GABINETE;
		INSTITUCION = iNSTITUCION;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	/**
	 **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("GABINETE")
	public Integer getGABINETE() {
		return GABINETE;
	}

	public void setGABINETE(Integer GABINETE) {
		this.GABINETE = GABINETE;
	}

	/**
	 **/

	@JsonProperty("DESC_GABINETE")
	public String getDESCGABINETE() {
		return DESC_GABINETE;
	}

	public void setDESCGABINETE(String DESC_GABINETE) {
		this.DESC_GABINETE = DESC_GABINETE;
	}

	/**
	 **/

	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class GabineteSectorialDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    GABINETE: ").append(StringUtil.toIndentedString(GABINETE)).append("\n");
		sb.append("    DESC_GABINETE: ").append(StringUtil.toIndentedString(DESC_GABINETE)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "gestion");
		super.PROPERTY_MAP.put("GABINETE", "gabinete");
		super.PROPERTY_MAP.put("DESC_GABINETE", "descGabinete");
		super.PROPERTY_MAP.put("INSTITUCION", "institucion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");
	}
}
