package clas.example.dto;


import fwk.core.clas.dto.AmbitosDto;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import clas.util.StringUtil;


/**
 * 
 **/

public class ActividadObraDto extends AmbitosDto implements Serializable  {
	
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String ESTADO = null;
  private String VIGENTE = null;
  private Integer INDICADOR = null;
  private Integer META = null;
  private Integer OBJETIVO = null;
  private Integer UBIGEO_MUN = null;
  private Integer UBIGEO_DEP = null;
  private String TIPO_ACTIVIDAD = null;
  private String SIN_PRODUCCION = null;
  private String TIPO_ADMINISTRACION = null;
  private Integer ETAPA_DOCUMENTO = null;
  private String DESC_ACTIVIDAD_OBRA = null;
  private Integer ACTIVIDAD_OBRA = null;
  private Integer PROYECTO = null;
  private Integer SUB_PROGRAMA = null;
  private Integer PROGRAMA = null;
  private Integer INSTITUCION = null;
  private Integer GESTION = null;
  private String TIPO_AOB = null;
  private Double COMPONENTE = null;
 
  public ActividadObraDto() {
	  getPROPERTY_MAP();
}
  
  
  
  public ActividadObraDto(String eSTADO, String vIGENTE, Integer iNDICADOR, Integer mETA, Integer oBJETIVO,
		Integer uBIGEO_MUN, Integer uBIGEO_DEP, String tIPO_ACTIVIDAD, String sIN_PRODUCCION,
		String tIPO_ADMINISTRACION, Integer eTAPA_DOCUMENTO, String dESC_ACTIVIDAD_OBRA, Integer aCTIVIDAD_OBRA,
		Integer pROYECTO, Integer sUB_PROGRAMA, Integer pROGRAMA, Integer iNSTITUCION, Integer gESTION, String tIPO_AOB,
		Double cOMPONENTE) {
	super();
	getPROPERTY_MAP();
	ESTADO = eSTADO;
	VIGENTE = vIGENTE;
	INDICADOR = iNDICADOR;
	META = mETA;
	OBJETIVO = oBJETIVO;
	UBIGEO_MUN = uBIGEO_MUN;
	UBIGEO_DEP = uBIGEO_DEP;
	TIPO_ACTIVIDAD = tIPO_ACTIVIDAD;
	SIN_PRODUCCION = sIN_PRODUCCION;
	TIPO_ADMINISTRACION = tIPO_ADMINISTRACION;
	ETAPA_DOCUMENTO = eTAPA_DOCUMENTO;
	DESC_ACTIVIDAD_OBRA = dESC_ACTIVIDAD_OBRA;
	ACTIVIDAD_OBRA = aCTIVIDAD_OBRA;
	PROYECTO = pROYECTO;
	SUB_PROGRAMA = sUB_PROGRAMA;
	PROGRAMA = pROGRAMA;
	INSTITUCION = iNSTITUCION;
	GESTION = gESTION;
	TIPO_AOB = tIPO_AOB;
	COMPONENTE = cOMPONENTE;
}



@JsonProperty("ESTADO")
  public String getESTADO() {
    return ESTADO;
  }
  public void setESTADO(String ESTADO) {
    this.ESTADO = ESTADO;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("VIGENTE")
  public String getVIGENTE() {
    return VIGENTE;
  }
  public void setVIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("INDICADOR")
  public Integer getINDICADOR() {
    return INDICADOR;
  }
  public void setINDICADOR(Integer INDICADOR) {
    this.INDICADOR = INDICADOR;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("META")
  public Integer getMETA() {
    return META;
  }
  public void setMETA(Integer META) {
    this.META = META;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("OBJETIVO")
  public Integer getOBJETIVO() {
    return OBJETIVO;
  }
  public void setOBJETIVO(Integer OBJETIVO) {
    this.OBJETIVO = OBJETIVO;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("UBIGEO_MUN")
  public Integer getUBIGEOMUN() {
    return UBIGEO_MUN;
  }
  public void setUBIGEOMUN(Integer UBIGEO_MUN) {
    this.UBIGEO_MUN = UBIGEO_MUN;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("UBIGEO_DEP")
  public Integer getUBIGEODEP() {
    return UBIGEO_DEP;
  }
  public void setUBIGEODEP(Integer UBIGEO_DEP) {
    this.UBIGEO_DEP = UBIGEO_DEP;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("TIPO_ACTIVIDAD")
  public String getTIPOACTIVIDAD() {
    return TIPO_ACTIVIDAD;
  }
  public void setTIPOACTIVIDAD(String TIPO_ACTIVIDAD) {
    this.TIPO_ACTIVIDAD = TIPO_ACTIVIDAD;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("SIN_PRODUCCION")
  public String getSINPRODUCCION() {
    return SIN_PRODUCCION;
  }
  public void setSINPRODUCCION(String SIN_PRODUCCION) {
    this.SIN_PRODUCCION = SIN_PRODUCCION;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("TIPO_ADMINISTRACION")
  public String getTIPOADMINISTRACION() {
    return TIPO_ADMINISTRACION;
  }
  public void setTIPOADMINISTRACION(String TIPO_ADMINISTRACION) {
    this.TIPO_ADMINISTRACION = TIPO_ADMINISTRACION;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("ETAPA_DOCUMENTO")
  public Integer getETAPADOCUMENTO() {
    return ETAPA_DOCUMENTO;
  }
  public void setETAPADOCUMENTO(Integer ETAPA_DOCUMENTO) {
    this.ETAPA_DOCUMENTO = ETAPA_DOCUMENTO;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("DESC_ACTIVIDAD_OBRA")
  public String getDESCACTIVIDADOBRA() {
    return DESC_ACTIVIDAD_OBRA;
  }
  public void setDESCACTIVIDADOBRA(String DESC_ACTIVIDAD_OBRA) {
    this.DESC_ACTIVIDAD_OBRA = DESC_ACTIVIDAD_OBRA;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("ACTIVIDAD_OBRA")
  public Integer getACTIVIDADOBRA() {
    return ACTIVIDAD_OBRA;
  }
  public void setACTIVIDADOBRA(Integer ACTIVIDAD_OBRA) {
    this.ACTIVIDAD_OBRA = ACTIVIDAD_OBRA;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("PROYECTO")
  public Integer getPROYECTO() {
    return PROYECTO;
  }
  public void setPROYECTO(Integer PROYECTO) {
    this.PROYECTO = PROYECTO;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("SUB_PROGRAMA")
  public Integer getSUBPROGRAMA() {
    return SUB_PROGRAMA;
  }
  public void setSUBPROGRAMA(Integer SUB_PROGRAMA) {
    this.SUB_PROGRAMA = SUB_PROGRAMA;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("PROGRAMA")
  public Integer getPROGRAMA() {
    return PROGRAMA;
  }
  public void setPROGRAMA(Integer PROGRAMA) {
    this.PROGRAMA = PROGRAMA;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("INSTITUCION")
  public Integer getINSTITUCION() {
    return INSTITUCION;
  }
  public void setINSTITUCION(Integer INSTITUCION) {
    this.INSTITUCION = INSTITUCION;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("GESTION")
  public Integer getGESTION() {
    return GESTION;
  }
  public void setGESTION(Integer GESTION) {
    this.GESTION = GESTION;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("TIPO_AOB")
  public String getTIPOAOB() {
    return TIPO_AOB;
  }
  public void setTIPOAOB(String TIPO_AOB) {
    this.TIPO_AOB = TIPO_AOB;
  }

  
  /**
   * 
   **/
  
  @JsonProperty("COMPONENTE")
  public Double getCOMPONENTE() {
    return COMPONENTE;
  }
  public void setCOMPONENTE(Double COMPONENTE) {
    this.COMPONENTE = COMPONENTE;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class ActividadObraDto {\n");
    sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
    sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
    sb.append("    INDICADOR: ").append(StringUtil.toIndentedString(INDICADOR)).append("\n");
    sb.append("    META: ").append(StringUtil.toIndentedString(META)).append("\n");
    sb.append("    OBJETIVO: ").append(StringUtil.toIndentedString(OBJETIVO)).append("\n");
    sb.append("    UBIGEO_MUN: ").append(StringUtil.toIndentedString(UBIGEO_MUN)).append("\n");
    sb.append("    UBIGEO_DEP: ").append(StringUtil.toIndentedString(UBIGEO_DEP)).append("\n");
    sb.append("    TIPO_ACTIVIDAD: ").append(StringUtil.toIndentedString(TIPO_ACTIVIDAD)).append("\n");
    sb.append("    SIN_PRODUCCION: ").append(StringUtil.toIndentedString(SIN_PRODUCCION)).append("\n");
    sb.append("    TIPO_ADMINISTRACION: ").append(StringUtil.toIndentedString(TIPO_ADMINISTRACION)).append("\n");
    sb.append("    ETAPA_DOCUMENTO: ").append(StringUtil.toIndentedString(ETAPA_DOCUMENTO)).append("\n");
    sb.append("    DESC_ACTIVIDAD_OBRA: ").append(StringUtil.toIndentedString(DESC_ACTIVIDAD_OBRA)).append("\n");
    sb.append("    ACTIVIDAD_OBRA: ").append(StringUtil.toIndentedString(ACTIVIDAD_OBRA)).append("\n");
    sb.append("    PROYECTO: ").append(StringUtil.toIndentedString(PROYECTO)).append("\n");
    sb.append("    SUB_PROGRAMA: ").append(StringUtil.toIndentedString(SUB_PROGRAMA)).append("\n");
    sb.append("    PROGRAMA: ").append(StringUtil.toIndentedString(PROGRAMA)).append("\n");
    sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
    sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
    sb.append("    TIPO_AOB: ").append(StringUtil.toIndentedString(TIPO_AOB)).append("\n");
    sb.append("    COMPONENTE: ").append(StringUtil.toIndentedString(COMPONENTE)).append("\n");
    sb.append("}");
    return sb.toString();
  }
  
  
	@Override
	public Map<String, String> getPROPERTY_MAP(){
		super.PROPERTY_MAP = new HashMap<String,String>();
		super.PROPERTY_MAP.put("ESTADO","");
		super.PROPERTY_MAP.put("ESTADO","");
		return super.PROPERTY_MAP;
	}
}
