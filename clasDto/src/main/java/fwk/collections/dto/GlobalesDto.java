package fwk.collections.dto;

public enum GlobalesDto {
	WA_IDGESTION,
    WA_IDPERFIL,
    WA_IDFLUJO,
    WA_IDOPERACION,
    WA_IDETAPAGESTION,
    WA_IDPAGINACION
}
