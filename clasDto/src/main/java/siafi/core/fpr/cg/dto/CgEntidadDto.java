package siafi.core.fpr.cg.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * CG_ENTIDAD 
 **/

public class CgEntidadDto extends AmbitosDto implements Serializable {
	
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
  private Long ID_ENTIDAD = null;
  private Long ID_UNIDAD_EJECUTORA = null;
  private Long CODIGO_UE = null;
  private String DESCRIPCION = null;
  private String CUENTADANCIA = null;
  private String RESTRICTIVA = null;
  private String IDENT_TRIBUTARIA = null;
  private String GEOGRAFICO = null;
  private String CODIGO_INSTITUCION = null;
  private Long ID_MUNICIPIO = null;
  private Long ID_DEPARTAMENTO = null;
  private String SECTOR = null;
  private Map<String, String> AMBITOS = null;

 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get ID_ENTIDAD
   * @return ID_ENTIDAD
  **/
  @JsonProperty("ID_ENTIDAD")
  public Long getIDENTIDAD() {
    return ID_ENTIDAD;
  }

  public void setIDENTIDAD(Long ID_ENTIDAD) {
    this.ID_ENTIDAD = ID_ENTIDAD;
  }

  public CgEntidadDto ID_ENTIDAD(Long ID_ENTIDAD) {
    this.ID_ENTIDAD = ID_ENTIDAD;
    return this;
  }

 /**
   * Get ID_UNIDAD_EJECUTORA
   * @return ID_UNIDAD_EJECUTORA
  **/
  @JsonProperty("ID_UNIDAD_EJECUTORA")
  public Long getIDUNIDADEJECUTORA() {
    return ID_UNIDAD_EJECUTORA;
  }

  public void setIDUNIDADEJECUTORA(Long ID_UNIDAD_EJECUTORA) {
    this.ID_UNIDAD_EJECUTORA = ID_UNIDAD_EJECUTORA;
  }

  public CgEntidadDto ID_UNIDAD_EJECUTORA(Long ID_UNIDAD_EJECUTORA) {
    this.ID_UNIDAD_EJECUTORA = ID_UNIDAD_EJECUTORA;
    return this;
  }

 /**
   * Get CODIGO_UE
   * @return CODIGO_UE
  **/
  @JsonProperty("CODIGO_UE")
  public Long getCODIGOUE() {
    return CODIGO_UE;
  }

  public void setCODIGOUE(Long CODIGO_UE) {
    this.CODIGO_UE = CODIGO_UE;
  }

  public CgEntidadDto CODIGO_UE(Long CODIGO_UE) {
    this.CODIGO_UE = CODIGO_UE;
    return this;
  }

 /**
   * Get DESCRIPCION
   * @return DESCRIPCION
  **/
  @JsonProperty("DESCRIPCION")
  public String getDESCRIPCION() {
    return DESCRIPCION;
  }

  public void setDESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
  }

  public CgEntidadDto DESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
    return this;
  }

 /**
   * Get CUENTADANCIA
   * @return CUENTADANCIA
  **/
  @JsonProperty("CUENTADANCIA")
  public String getCUENTADANCIA() {
    return CUENTADANCIA;
  }

  public void setCUENTADANCIA(String CUENTADANCIA) {
    this.CUENTADANCIA = CUENTADANCIA;
  }

  public CgEntidadDto CUENTADANCIA(String CUENTADANCIA) {
    this.CUENTADANCIA = CUENTADANCIA;
    return this;
  }

 /**
   * Get RESTRICTIVA
   * @return RESTRICTIVA
  **/
  @JsonProperty("RESTRICTIVA")
  public String getRESTRICTIVA() {
    return RESTRICTIVA;
  }

  public void setRESTRICTIVA(String RESTRICTIVA) {
    this.RESTRICTIVA = RESTRICTIVA;
  }

  public CgEntidadDto RESTRICTIVA(String RESTRICTIVA) {
    this.RESTRICTIVA = RESTRICTIVA;
    return this;
  }

 /**
   * Get IDENT_TRIBUTARIA
   * @return IDENT_TRIBUTARIA
  **/
  @JsonProperty("IDENT_TRIBUTARIA")
  public String getIDENTTRIBUTARIA() {
    return IDENT_TRIBUTARIA;
  }

  public void setIDENTTRIBUTARIA(String IDENT_TRIBUTARIA) {
    this.IDENT_TRIBUTARIA = IDENT_TRIBUTARIA;
  }

  public CgEntidadDto IDENT_TRIBUTARIA(String IDENT_TRIBUTARIA) {
    this.IDENT_TRIBUTARIA = IDENT_TRIBUTARIA;
    return this;
  }

 /**
   * Get GEOGRAFICO
   * @return GEOGRAFICO
  **/
  @JsonProperty("GEOGRAFICO")
  public String getGEOGRAFICO() {
    return GEOGRAFICO;
  }

  public void setGEOGRAFICO(String GEOGRAFICO) {
    this.GEOGRAFICO = GEOGRAFICO;
  }

  public CgEntidadDto GEOGRAFICO(String GEOGRAFICO) {
    this.GEOGRAFICO = GEOGRAFICO;
    return this;
  }

 /**
   * Get CODIGO_INSTITUCION
   * @return CODIGO_INSTITUCION
  **/
  @JsonProperty("CODIGO_INSTITUCION")
  public String getCODIGOINSTITUCION() {
    return CODIGO_INSTITUCION;
  }

  public void setCODIGOINSTITUCION(String CODIGO_INSTITUCION) {
    this.CODIGO_INSTITUCION = CODIGO_INSTITUCION;
  }

  public CgEntidadDto CODIGO_INSTITUCION(String CODIGO_INSTITUCION) {
    this.CODIGO_INSTITUCION = CODIGO_INSTITUCION;
    return this;
  }

 /**
   * Get ID_MUNICIPIO
   * @return ID_MUNICIPIO
  **/
  @JsonProperty("ID_MUNICIPIO")
  public Long getIDMUNICIPIO() {
    return ID_MUNICIPIO;
  }

  public void setIDMUNICIPIO(Long ID_MUNICIPIO) {
    this.ID_MUNICIPIO = ID_MUNICIPIO;
  }

  public CgEntidadDto ID_MUNICIPIO(Long ID_MUNICIPIO) {
    this.ID_MUNICIPIO = ID_MUNICIPIO;
    return this;
  }

 /**
   * Get ID_DEPARTAMENTO
   * @return ID_DEPARTAMENTO
  **/
  @JsonProperty("ID_DEPARTAMENTO")
  public Long getIDDEPARTAMENTO() {
    return ID_DEPARTAMENTO;
  }

  public void setIDDEPARTAMENTO(Long ID_DEPARTAMENTO) {
    this.ID_DEPARTAMENTO = ID_DEPARTAMENTO;
  }

  public CgEntidadDto ID_DEPARTAMENTO(Long ID_DEPARTAMENTO) {
    this.ID_DEPARTAMENTO = ID_DEPARTAMENTO;
    return this;
  }

 /**
   * Get SECTOR
   * @return SECTOR
  **/
  @JsonProperty("SECTOR")
  public String getSECTOR() {
    return SECTOR;
  }

  public void setSECTOR(String SECTOR) {
    this.SECTOR = SECTOR;
  }

  public CgEntidadDto SECTOR(String SECTOR) {
    this.SECTOR = SECTOR;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public CgEntidadDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public CgEntidadDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }


  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public CgEntidadDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CgEntidadDto {\n");
    
    sb.append("    ID_ENTIDAD: ").append(toIndentedString(ID_ENTIDAD)).append("\n");
    sb.append("    ID_UNIDAD_EJECUTORA: ").append(toIndentedString(ID_UNIDAD_EJECUTORA)).append("\n");
    sb.append("    CODIGO_UE: ").append(toIndentedString(CODIGO_UE)).append("\n");
    sb.append("    DESCRIPCION: ").append(toIndentedString(DESCRIPCION)).append("\n");
    sb.append("    CUENTADANCIA: ").append(toIndentedString(CUENTADANCIA)).append("\n");
    sb.append("    RESTRICTIVA: ").append(toIndentedString(RESTRICTIVA)).append("\n");
    sb.append("    IDENT_TRIBUTARIA: ").append(toIndentedString(IDENT_TRIBUTARIA)).append("\n");
    sb.append("    GEOGRAFICO: ").append(toIndentedString(GEOGRAFICO)).append("\n");
    sb.append("    CODIGO_INSTITUCION: ").append(toIndentedString(CODIGO_INSTITUCION)).append("\n");
    sb.append("    ID_MUNICIPIO: ").append(toIndentedString(ID_MUNICIPIO)).append("\n");
    sb.append("    ID_DEPARTAMENTO: ").append(toIndentedString(ID_DEPARTAMENTO)).append("\n");
    sb.append("    SECTOR: ").append(toIndentedString(SECTOR)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }
  
  public CgEntidadDto() {
		initPROPERTY_MAP();
  }
  
  public CgEntidadDto(Long iD_ENTIDAD, Long iD_UNIDAD_EJECUTORA, Long cODIGO_UE, String dESCRIPCION, String cUENTADANCIA,
		String rESTRICTIVA, String iDENT_TRIBUTARIA, String gEOGRAFICO, String cODIGO_INSTITUCION, Long iD_MUNICIPIO,
		Long iD_DEPARTAMENTO, String sECTOR) {
	super();
	initPROPERTY_MAP();
	ID_ENTIDAD = iD_ENTIDAD;
	ID_UNIDAD_EJECUTORA = iD_UNIDAD_EJECUTORA;
	CODIGO_UE = cODIGO_UE;
	DESCRIPCION = dESCRIPCION;
	CUENTADANCIA = cUENTADANCIA;
	RESTRICTIVA = rESTRICTIVA;
	IDENT_TRIBUTARIA = iDENT_TRIBUTARIA;
	GEOGRAFICO = gEOGRAFICO;
	CODIGO_INSTITUCION = cODIGO_INSTITUCION;
	ID_MUNICIPIO = iD_MUNICIPIO;
	ID_DEPARTAMENTO = iD_DEPARTAMENTO;
	SECTOR = sECTOR;
}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("ID_ENTIDAD", "id.idEntidad");
		super.PROPERTY_MAP.put("ID_UNIDAD_EJECUTORA", "id.idUnidadEjecutora");
		super.PROPERTY_MAP.put("CODIGO_UE", "codigoUe");
		super.PROPERTY_MAP.put("DESCRIPCION", "descripcion");
		super.PROPERTY_MAP.put("CUENTADANCIA", "cuentadancia");
		
		super.PROPERTY_MAP.put("RESTRICTIVA", "restrictiva");
		super.PROPERTY_MAP.put("IDENT_TRIBUTARIA", "identTributaria");
		super.PROPERTY_MAP.put("GEOGRAFICO", "geografico");
		super.PROPERTY_MAP.put("CODIGO_INSTITUCION", "codigoInstitucion");
		super.PROPERTY_MAP.put("ID_MUNICIPIO", "idMunicipio");
		
		super.PROPERTY_MAP.put("ID_DEPARTAMENTO", "idDepartamento");
		super.PROPERTY_MAP.put("SECTOR", "sector");
	}
  

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

