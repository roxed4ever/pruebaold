package siafi.core.cont.cpd.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Dto para la tablas de tipos de Asientos
 **/

public class TiposAsientosDto  extends AmbitosDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
  
  private String TIPO_ASIENTO = null;
  private String DESC_TIPO_ASIENTO = null;
  private String CODIGO_ANTERIOR = null;
  private String APROPIABLE = null;
  private String VIGENTE = null;
  private String CONTABILIZACION = null;
  private String FLUJO_EFECTIVO = null;
  private String EVOLUCION_PATRIMONIO = null;
  private String NIC_OPERACION = null;
  private String NIC_ANTECESOR = null;
  private Long NIC_SECUENCIA = (long) 0; 
  
  private Map<String, String> AMBITOS = null;

 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get TIPO_ASIENTO
   * @return TIPO_ASIENTO
  **/
  @JsonProperty("TIPO_ASIENTO")
  public String getTIPOASIENTO() {
    return TIPO_ASIENTO;
  }

  public void setTIPOASIENTO(String TIPO_ASIENTO) {
    this.TIPO_ASIENTO = TIPO_ASIENTO;
  }

  public TiposAsientosDto TIPO_ASIENTO(String TIPO_ASIENTO) {
    this.TIPO_ASIENTO = TIPO_ASIENTO;
    return this;
  }

 /**
   * Get DESC_TIPO_ASIENTO
   * @return DESC_TIPO_ASIENTO
  **/
  @JsonProperty("DESC_TIPO_ASIENTO")
  public String getDESCTIPOASIENTO() {
    return DESC_TIPO_ASIENTO;
  }

  public void setDESCTIPOASIENTO(String DESC_TIPO_ASIENTO) {
    this.DESC_TIPO_ASIENTO = DESC_TIPO_ASIENTO;
  }

  public TiposAsientosDto DESC_TIPO_ASIENTO(String DESC_TIPO_ASIENTO) {
    this.DESC_TIPO_ASIENTO = DESC_TIPO_ASIENTO;
    return this;
  }

 /**
   * Get CODIGO_ANTERIOR
   * @return CODIGO_ANTERIOR
  **/
  @JsonProperty("CODIGO_ANTERIOR")
  public String getCODIGOANTERIOR() {
    return CODIGO_ANTERIOR;
  }

  public void setCODIGOANTERIOR(String CODIGO_ANTERIOR) {
    this.CODIGO_ANTERIOR = CODIGO_ANTERIOR;
  }

  public TiposAsientosDto CODIGO_ANTERIOR(String CODIGO_ANTERIOR) {
    this.CODIGO_ANTERIOR = CODIGO_ANTERIOR;
    return this;
  }

 /**
   * Get APROPIABLE
   * @return APROPIABLE
  **/
  @JsonProperty("APROPIABLE")
  public String getAPROPIABLE() {
    return APROPIABLE;
  }

  public void setAPROPIABLE(String APROPIABLE) {
    this.APROPIABLE = APROPIABLE;
  }

  public TiposAsientosDto APROPIABLE(String APROPIABLE) {
    this.APROPIABLE = APROPIABLE;
    return this;
  }

 /**
   * Get VIGENTE
   * @return VIGENTE
  **/
  @JsonProperty("VIGENTE")
  public String getVIGENTE() {
    return VIGENTE;
  }

  public void setVIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
  }

  public TiposAsientosDto VIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
    return this;
  }

 /**
   * Get CONTABILIZACION
   * @return CONTABILIZACION
  **/
  @JsonProperty("CONTABILIZACION")
  public String getCONTABILIZACION() {
    return CONTABILIZACION;
  }

  public void setCONTABILIZACION(String CONTABILIZACION) {
    this.CONTABILIZACION = CONTABILIZACION;
  }

  public TiposAsientosDto CONTABILIZACION(String CONTABILIZACION) {
    this.CONTABILIZACION = CONTABILIZACION;
    return this;
  }

 /**
   * Get FLUJO_EFECTIVO
   * @return FLUJO_EFECTIVO
  **/
  @JsonProperty("FLUJO_EFECTIVO")
  public String getFLUJOEFECTIVO() {
    return FLUJO_EFECTIVO;
  }

  public void setFLUJOEFECTIVO(String FLUJO_EFECTIVO) {
    this.FLUJO_EFECTIVO = FLUJO_EFECTIVO;
  }

  public TiposAsientosDto FLUJO_EFECTIVO(String FLUJO_EFECTIVO) {
    this.FLUJO_EFECTIVO = FLUJO_EFECTIVO;
    return this;
  }

 /**
   * Get EVOLUCION_PATRIMONIO
   * @return EVOLUCION_PATRIMONIO
  **/
  @JsonProperty("EVOLUCION_PATRIMONIO")
  public String getEVOLUCIONPATRIMONIO() {
    return EVOLUCION_PATRIMONIO;
  }

  public void setEVOLUCIONPATRIMONIO(String EVOLUCION_PATRIMONIO) {
    this.EVOLUCION_PATRIMONIO = EVOLUCION_PATRIMONIO;
  }

  public TiposAsientosDto EVOLUCION_PATRIMONIO(String EVOLUCION_PATRIMONIO) {
    this.EVOLUCION_PATRIMONIO = EVOLUCION_PATRIMONIO;
    return this;
  }

 /**
   * Get NIC_OPERACION
   * @return NIC_OPERACION
  **/
  @JsonProperty("NIC_OPERACION")
  public String getNICOPERACION() {
    return NIC_OPERACION;
  }

  public void setNICOPERACION(String NIC_OPERACION) {
    this.NIC_OPERACION = NIC_OPERACION;
  }

  public TiposAsientosDto NIC_OPERACION(String NIC_OPERACION) {
    this.NIC_OPERACION = NIC_OPERACION;
    return this;
  }

 /**
   * Get NIC_ANTECESOR
   * @return NIC_ANTECESOR
  **/
  @JsonProperty("NIC_ANTECESOR")
  public String getNICANTECESOR() {
    return NIC_ANTECESOR;
  }

  public void setNICANTECESOR(String NIC_ANTECESOR) {
    this.NIC_ANTECESOR = NIC_ANTECESOR;
  }

  public TiposAsientosDto NIC_ANTECESOR(String NIC_ANTECESOR) {
    this.NIC_ANTECESOR = NIC_ANTECESOR;
    return this;
  }

 /**
   * Get NIC_SECUENCIA
   * @return NIC_SECUENCIA
  **/
  @JsonProperty("NIC_SECUENCIA")
  public Long getNICSECUENCIA() {
    return NIC_SECUENCIA;
  }

  public void setNICSECUENCIA(Long NIC_SECUENCIA) {
    this.NIC_SECUENCIA = NIC_SECUENCIA;
  }

  public TiposAsientosDto NIC_SECUENCIA(Long NIC_SECUENCIA) {
    this.NIC_SECUENCIA = NIC_SECUENCIA;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public TiposAsientosDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public TiposAsientosDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }


  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public TiposAsientosDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TiposAsientosDto {\n");
    
    sb.append("    TIPO_ASIENTO: ").append(toIndentedString(TIPO_ASIENTO)).append("\n");
    sb.append("    DESC_TIPO_ASIENTO: ").append(toIndentedString(DESC_TIPO_ASIENTO)).append("\n");
    sb.append("    CODIGO_ANTERIOR: ").append(toIndentedString(CODIGO_ANTERIOR)).append("\n");
    sb.append("    APROPIABLE: ").append(toIndentedString(APROPIABLE)).append("\n");
    sb.append("    VIGENTE: ").append(toIndentedString(VIGENTE)).append("\n");
    sb.append("    CONTABILIZACION: ").append(toIndentedString(CONTABILIZACION)).append("\n");
    sb.append("    FLUJO_EFECTIVO: ").append(toIndentedString(FLUJO_EFECTIVO)).append("\n");
    sb.append("    EVOLUCION_PATRIMONIO: ").append(toIndentedString(EVOLUCION_PATRIMONIO)).append("\n");
    sb.append("    NIC_OPERACION: ").append(toIndentedString(NIC_OPERACION)).append("\n");
    sb.append("    NIC_ANTECESOR: ").append(toIndentedString(NIC_ANTECESOR)).append("\n");
    sb.append("    NIC_SECUENCIA: ").append(toIndentedString(NIC_SECUENCIA)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public TiposAsientosDto() {
	  initPROPERTY_MAP(); 
  }
  
  public TiposAsientosDto(String tIPO_ASIENTO, String dESC_TIPO_ASIENTO, String cODIGO_ANTERIOR, String aPROPIABLE,
		String vIGENTE, String cONTABILIZACION, String fLUJO_EFECTIVO, String eVOLUCION_PATRIMONIO,
		String nIC_OPERACION, String nIC_ANTECESOR, Long nIC_SECUENCIA) {
	super();
	initPROPERTY_MAP(); 
	TIPO_ASIENTO = tIPO_ASIENTO;
	DESC_TIPO_ASIENTO = dESC_TIPO_ASIENTO;
	CODIGO_ANTERIOR = cODIGO_ANTERIOR;
	APROPIABLE = aPROPIABLE;
	VIGENTE = vIGENTE;
	CONTABILIZACION = cONTABILIZACION;
	FLUJO_EFECTIVO = fLUJO_EFECTIVO;
	EVOLUCION_PATRIMONIO = eVOLUCION_PATRIMONIO;
	NIC_OPERACION = nIC_OPERACION;
	NIC_ANTECESOR = nIC_ANTECESOR;
	NIC_SECUENCIA = nIC_SECUENCIA;

}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("TIPO_ASIENTO", "tipoAsiento");
		super.PROPERTY_MAP.put("DESC_TIPO_ASIENTO", "descTipoAsiento");
		super.PROPERTY_MAP.put("CODIGO_ANTERIOR", "codigoAnterior");
		super.PROPERTY_MAP.put("APROPIABLE", "apropiable");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("CONTABILIZACION", "contabilizacion");
		super.PROPERTY_MAP.put("FLUJO_EFECTIVO", "flujoEfectivo");
		super.PROPERTY_MAP.put("EVOLUCION_PATRIMONIO", "evolucionPatrimonio");
		super.PROPERTY_MAP.put("NIC_OPERACION", "nicOperacion");
		super.PROPERTY_MAP.put("NIC_ANTECESOR", "nicAntecesor");
		super.PROPERTY_MAP.put("NIC_SECUENCIA", "nicSecuencia");
	}
 }

