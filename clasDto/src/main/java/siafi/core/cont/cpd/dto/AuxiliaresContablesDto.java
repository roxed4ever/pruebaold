package siafi.core.cont.cpd.dto;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Matriz de auxiliares contables Dto
 **/

public class AuxiliaresContablesDto extends AmbitosDto implements Serializable  {
  

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  
  private long GESTION = 0;
  private long INSTITUCION = 0;
  private String CUENTA_CONTABLE = null;
  private String AUXILIAR = null;
  private String AUX_PRINCIPAL = null;
  private Map<String, String> AMBITOS = null;

 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get GESTION
   * @return GESTION
  **/
  @JsonProperty("GESTION")
  public long getGESTION() {
    return GESTION;
  }

  public void setGESTION(long GESTION) {
    this.GESTION = GESTION;
  }

  public AuxiliaresContablesDto GESTION(long GESTION) {
    this.GESTION = GESTION;
    return this;
  }

 /**
   * Get INSTITUCION
   * @return INSTITUCION
  **/
  @JsonProperty("INSTITUCION")
  public long getINSTITUCION() {
    return INSTITUCION;
  }

  public void setINSTITUCION(long INSTITUCION) {
    this.INSTITUCION = INSTITUCION;
  }

  public AuxiliaresContablesDto INSTITUCION(long INSTITUCION) {
    this.INSTITUCION = INSTITUCION;
    return this;
  }

 /**
   * Get CUENTA_CONTABLE
   * @return CUENTA_CONTABLE
  **/
  @JsonProperty("CUENTA_CONTABLE")
  public String getCUENTACONTABLE() {
    return CUENTA_CONTABLE;
  }

  public void setCUENTACONTABLE(String CUENTA_CONTABLE) {
    this.CUENTA_CONTABLE = CUENTA_CONTABLE;
  }

  public AuxiliaresContablesDto CUENTA_CONTABLE(String CUENTA_CONTABLE) {
    this.CUENTA_CONTABLE = CUENTA_CONTABLE;
    return this;
  }

 /**
   * Get AUXILIAR
   * @return AUXILIAR
  **/
  @JsonProperty("AUXILIAR")
  public String getAUXILIAR() {
    return AUXILIAR;
  }

  public void setAUXILIAR(String AUXILIAR) {
    this.AUXILIAR = AUXILIAR;
  }

  public AuxiliaresContablesDto AUXILIAR(String AUXILIAR) {
    this.AUXILIAR = AUXILIAR;
    return this;
  }

 /**
   * Get AUX_PRINCIPAL
   * @return AUX_PRINCIPAL
  **/
  @JsonProperty("AUX_PRINCIPAL")
  public String getAUXPRINCIPAL() {
    return AUX_PRINCIPAL;
  }

  public void setAUXPRINCIPAL(String AUX_PRINCIPAL) {
    this.AUX_PRINCIPAL = AUX_PRINCIPAL;
  }

  public AuxiliaresContablesDto AUX_PRINCIPAL(String AUX_PRINCIPAL) {
    this.AUX_PRINCIPAL = AUX_PRINCIPAL;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public AuxiliaresContablesDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public AuxiliaresContablesDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }

  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public AuxiliaresContablesDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AuxiliaresContablesDto {\n");
    
    sb.append("    GESTION: ").append(toIndentedString(GESTION)).append("\n");
    sb.append("    INSTITUCION: ").append(toIndentedString(INSTITUCION)).append("\n");
    sb.append("    CUENTA_CONTABLE: ").append(toIndentedString(CUENTA_CONTABLE)).append("\n");
    sb.append("    AUXILIAR: ").append(toIndentedString(AUXILIAR)).append("\n");
    sb.append("    AUX_PRINCIPAL: ").append(toIndentedString(AUX_PRINCIPAL)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public AuxiliaresContablesDto () {
	  initPROPERTY_MAP();
  }
  
  
  	public AuxiliaresContablesDto(long gESTION, long iNSTITUCION, String cUENTA_CONTABLE, String aUXILIAR,
		String aUX_PRINCIPAL) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		INSTITUCION = iNSTITUCION;
		CUENTA_CONTABLE = cUENTA_CONTABLE;
		AUXILIAR = aUXILIAR;
		AUX_PRINCIPAL = aUX_PRINCIPAL;
  	}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "id.institucion");
		super.PROPERTY_MAP.put("CUENTA_CONTABLE", "cuentaContable");
		super.PROPERTY_MAP.put("AUXILIAR", "apropiable");
		super.PROPERTY_MAP.put("AUX_PRINCIPAL", "vigente");
	}

}

