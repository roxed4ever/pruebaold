package siafi.core.cont.cpd.dto;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * NCE Gestiones Dto, Instituciones que generan Contabilidad   
 **/

public class NceGestionesDto extends AmbitosDto implements Serializable  {
  

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
  private long GESTION = 0;
  private long ENTECONTABLE = 0;
  private Date FECHA_DESDE = null;
  private Date FECHA_HASTA = null;
  private Date FECHA_CIERRE = null;
  private String CONTABILIZA = null;
  private String API_TABLA = null;
  private Map<String, String> AMBITOS = null;

 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get GESTION
   * @return GESTION
  **/
  @JsonProperty("GESTION")
  public long getGESTION() {
    return GESTION;
  }

  public void setGESTION(long GESTION) {
    this.GESTION = GESTION;
  }

  public NceGestionesDto GESTION(long GESTION) {
    this.GESTION = GESTION;
    return this;
  }

 /**
   * Get ENTECONTABLE
   * @return ENTECONTABLE
  **/
  @JsonProperty("ENTECONTABLE")
  public long getENTECONTABLE() {
    return ENTECONTABLE;
  }

  public void setENTECONTABLE(long ENTECONTABLE) {
    this.ENTECONTABLE = ENTECONTABLE;
  }

  public NceGestionesDto ENTECONTABLE(long ENTECONTABLE) {
    this.ENTECONTABLE = ENTECONTABLE;
    return this;
  }

 /**
   * Get FECHA_DESDE
   * @return FECHA_DESDE
  **/
  @JsonProperty("FECHA_DESDE")
  public Date getFECHADESDE() {
    return FECHA_DESDE;
  }

  public void setFECHADESDE(Date FECHA_DESDE) {
    this.FECHA_DESDE = FECHA_DESDE;
  }

  public NceGestionesDto FECHA_DESDE(Date FECHA_DESDE) {
    this.FECHA_DESDE = FECHA_DESDE;
    return this;
  }

 /**
   * Get FECHA_HASTA
   * @return FECHA_HASTA
  **/
  @JsonProperty("FECHA_HASTA")
  public Date getFECHAHASTA() {
    return FECHA_HASTA;
  }

  public void setFECHAHASTA(Date FECHA_HASTA) {
    this.FECHA_HASTA = FECHA_HASTA;
  }

  public NceGestionesDto FECHA_HASTA(Date FECHA_HASTA) {
    this.FECHA_HASTA = FECHA_HASTA;
    return this;
  }

 /**
   * Get FECHA_CIERRE
   * @return FECHA_CIERRE
  **/
  @JsonProperty("FECHA_CIERRE")
  public Date getFECHACIERRE() {
    return FECHA_CIERRE;
  }

  public void setFECHACIERRE(Date FECHA_CIERRE) {
    this.FECHA_CIERRE = FECHA_CIERRE;
  }

  public NceGestionesDto FECHA_CIERRE(Date FECHA_CIERRE) {
    this.FECHA_CIERRE = FECHA_CIERRE;
    return this;
  }

 /**
   * Get CONTABILIZA
   * @return CONTABILIZA
  **/
  @JsonProperty("CONTABILIZA")
  public String getCONTABILIZA() {
    return CONTABILIZA;
  }

  public void setCONTABILIZA(String CONTABILIZA) {
    this.CONTABILIZA = CONTABILIZA;
  }

  public NceGestionesDto CONTABILIZA(String CONTABILIZA) {
    this.CONTABILIZA = CONTABILIZA;
    return this;
  }

 /**
   * Get API_TABLA
   * @return API_TABLA
  **/
  @JsonProperty("API_TABLA")
  public String getAPITABLA() {
    return API_TABLA;
  }

  public void setAPITABLA(String API_TABLA) {
    this.API_TABLA = API_TABLA;
  }

  public NceGestionesDto API_TABLA(String API_TABLA) {
    this.API_TABLA = API_TABLA;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public NceGestionesDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public NceGestionesDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }


  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public NceGestionesDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NceGestionesDto {\n");
    
    sb.append("    GESTION: ").append(toIndentedString(GESTION)).append("\n");
    sb.append("    ENTECONTABLE: ").append(toIndentedString(ENTECONTABLE)).append("\n");
    sb.append("    FECHA_DESDE: ").append(toIndentedString(FECHA_DESDE)).append("\n");
    sb.append("    FECHA_HASTA: ").append(toIndentedString(FECHA_HASTA)).append("\n");
    sb.append("    FECHA_CIERRE: ").append(toIndentedString(FECHA_CIERRE)).append("\n");
    sb.append("    CONTABILIZA: ").append(toIndentedString(CONTABILIZA)).append("\n");
    sb.append("    API_TABLA: ").append(toIndentedString(API_TABLA)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public NceGestionesDto() {
	  initPROPERTY_MAP();
  }
  
  
  
  public NceGestionesDto(long gESTION, long eNTECONTABLE, Date fECHA_DESDE, Date fECHA_HASTA, Date fECHA_CIERRE,
		String cONTABILIZA, String aPI_TABLA) {
	super();
    initPROPERTY_MAP();
	GESTION = gESTION;
	ENTECONTABLE = eNTECONTABLE;
	FECHA_DESDE = fECHA_DESDE;
	FECHA_HASTA = fECHA_HASTA;
	FECHA_CIERRE = fECHA_CIERRE;
	CONTABILIZA = cONTABILIZA;
	API_TABLA = aPI_TABLA;
}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "gestion");
		super.PROPERTY_MAP.put("ENTECONTABLE", "entecontable");
		super.PROPERTY_MAP.put("FECHA_DESDE", "fechaDesde");
		super.PROPERTY_MAP.put("FECHA_HASTA", "fechaHasta");
		super.PROPERTY_MAP.put("CONTABILIZA", "contabiliza");
		super.PROPERTY_MAP.put("API_TABLA", "apiTabla");
	}
}


