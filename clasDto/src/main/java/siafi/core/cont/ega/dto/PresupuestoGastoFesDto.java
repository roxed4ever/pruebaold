package siafi.core.cont.ega.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

public class PresupuestoGastoFesDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	  private Double TOTAL_COMPROMISO_APROBADO = null;
	  private Double TOTAL_PRECOMPROMISO_APROBADO = null;
	  private Double TOTAL_DEVENGADO_VERIFICADO = null;
	  private Double TOTAL_COMPROMISO_VERIFICADO = null;
	  private Double TOTAL_PRECOMPROMISO_VERIFICADO = null;
	  private Double TOTAL_DEVENGADO_ELABORADO = null;
	  private Double TOTAL_COMPROMISO_ELABORADO = null;
	  private Double TOTAL_PRECOMPROMISO_ELABORADO = null;
	  private Double PPTO_CREDITO_DISPONIBLE = null;
	  private Double PPTO_DISPONIBLE = null;
	  private Double PPTO_VIGENTE = null;
	  private Double PPTO_INICIAL = null;
	  private Integer SECUENCIAL_FTE_ESPEC = null;
	  private Integer TRF_BENEFICIARIO = null;
	  private String OBJETO = null;
	  private Integer ORGANISMO = null;
	  private Integer FUENTE = null;
	  private Integer ACTIVIDAD_OBRA = null;
	  private String CONVENIO = null;
	  private String BIP = null;
	  private Integer PROYECTO = null;
	  private Integer SUB_PROGRAMA = null;
	  private Integer PROGRAMA = null;
	  private Integer UE = null;
	  private Integer GA = null;
	  private Integer INSTITUCION = null;
	  private Integer GESTION = null;
	  private Double TOTAL_DEVENGADO_APROBADO = null;
	  private Double TOTAL_PAGO_APROBADO = null;

	  
	  public PresupuestoGastoFesDto() {
		// TODO Auto-generated constructor stub
	}
	  
	  
	public PresupuestoGastoFesDto(Double tOTAL_COMPROMISO_APROBADO, Double tOTAL_PRECOMPROMISO_APROBADO,
			Double tOTAL_DEVENGADO_VERIFICADO, Double tOTAL_COMPROMISO_VERIFICADO,
			Double tOTAL_PRECOMPROMISO_VERIFICADO, Double tOTAL_DEVENGADO_ELABORADO, Double tOTAL_COMPROMISO_ELABORADO,
			Double tOTAL_PRECOMPROMISO_ELABORADO, Double pPTO_CREDITO_DISPONIBLE, Double pPTO_DISPONIBLE,
			Double pPTO_VIGENTE, Double pPTO_INICIAL, Integer sECUENCIAL_FTE_ESPEC, Integer tRF_BENEFICIARIO,
			String oBJETO, Integer oRGANISMO, Integer fUENTE, Integer aCTIVIDAD_OBRA, String cONVENIO, String bIP,
			Integer pROYECTO, Integer sUB_PROGRAMA, Integer pROGRAMA, Integer uE, Integer gA, Integer iNSTITUCION,
			Integer gESTION, Double tOTAL_DEVENGADO_APROBADO, Double tOTAL_PAGO_APROBADO) {
		super();
		TOTAL_COMPROMISO_APROBADO = tOTAL_COMPROMISO_APROBADO;
		TOTAL_PRECOMPROMISO_APROBADO = tOTAL_PRECOMPROMISO_APROBADO;
		TOTAL_DEVENGADO_VERIFICADO = tOTAL_DEVENGADO_VERIFICADO;
		TOTAL_COMPROMISO_VERIFICADO = tOTAL_COMPROMISO_VERIFICADO;
		TOTAL_PRECOMPROMISO_VERIFICADO = tOTAL_PRECOMPROMISO_VERIFICADO;
		TOTAL_DEVENGADO_ELABORADO = tOTAL_DEVENGADO_ELABORADO;
		TOTAL_COMPROMISO_ELABORADO = tOTAL_COMPROMISO_ELABORADO;
		TOTAL_PRECOMPROMISO_ELABORADO = tOTAL_PRECOMPROMISO_ELABORADO;
		PPTO_CREDITO_DISPONIBLE = pPTO_CREDITO_DISPONIBLE;
		PPTO_DISPONIBLE = pPTO_DISPONIBLE;
		PPTO_VIGENTE = pPTO_VIGENTE;
		PPTO_INICIAL = pPTO_INICIAL;
		SECUENCIAL_FTE_ESPEC = sECUENCIAL_FTE_ESPEC;
		TRF_BENEFICIARIO = tRF_BENEFICIARIO;
		OBJETO = oBJETO;
		ORGANISMO = oRGANISMO;
		FUENTE = fUENTE;
		ACTIVIDAD_OBRA = aCTIVIDAD_OBRA;
		CONVENIO = cONVENIO;
		BIP = bIP;
		PROYECTO = pROYECTO;
		SUB_PROGRAMA = sUB_PROGRAMA;
		PROGRAMA = pROGRAMA;
		UE = uE;
		GA = gA;
		INSTITUCION = iNSTITUCION;
		GESTION = gESTION;
		TOTAL_DEVENGADO_APROBADO = tOTAL_DEVENGADO_APROBADO;
		TOTAL_PAGO_APROBADO = tOTAL_PAGO_APROBADO;
	}


	/**
	 * 
	 **/
	@JsonProperty("TOTAL_COMPROMISO_APROBADO")
	public Double getTOTALCOMPROMISOAPROBADO() {
		return TOTAL_COMPROMISO_APROBADO;
	}

	public void setTOTALCOMPROMISOAPROBADO(Double TOTAL_COMPROMISO_APROBADO) {
		this.TOTAL_COMPROMISO_APROBADO = TOTAL_COMPROMISO_APROBADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("TOTAL_PRECOMPROMISO_APROBADO")
	public Double getTOTALPRECOMPROMISOAPROBADO() {
		return TOTAL_PRECOMPROMISO_APROBADO;
	}

	public void setTOTALPRECOMPROMISOAPROBADO(Double TOTAL_PRECOMPROMISO_APROBADO) {
		this.TOTAL_PRECOMPROMISO_APROBADO = TOTAL_PRECOMPROMISO_APROBADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("TOTAL_DEVENGADO_VERIFICADO")
	public Double getTOTALDEVENGADOVERIFICADO() {
		return TOTAL_DEVENGADO_VERIFICADO;
	}

	public void setTOTALDEVENGADOVERIFICADO(Double TOTAL_DEVENGADO_VERIFICADO) {
		this.TOTAL_DEVENGADO_VERIFICADO = TOTAL_DEVENGADO_VERIFICADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("TOTAL_COMPROMISO_VERIFICADO")
	public Double getTOTALCOMPROMISOVERIFICADO() {
		return TOTAL_COMPROMISO_VERIFICADO;
	}

	public void setTOTALCOMPROMISOVERIFICADO(Double TOTAL_COMPROMISO_VERIFICADO) {
		this.TOTAL_COMPROMISO_VERIFICADO = TOTAL_COMPROMISO_VERIFICADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("TOTAL_PRECOMPROMISO_VERIFICADO")
	public Double getTOTALPRECOMPROMISOVERIFICADO() {
		return TOTAL_PRECOMPROMISO_VERIFICADO;
	}

	public void setTOTALPRECOMPROMISOVERIFICADO(Double TOTAL_PRECOMPROMISO_VERIFICADO) {
		this.TOTAL_PRECOMPROMISO_VERIFICADO = TOTAL_PRECOMPROMISO_VERIFICADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("TOTAL_DEVENGADO_ELABORADO")
	public Double getTOTALDEVENGADOELABORADO() {
		return TOTAL_DEVENGADO_ELABORADO;
	}

	public void setTOTALDEVENGADOELABORADO(Double TOTAL_DEVENGADO_ELABORADO) {
		this.TOTAL_DEVENGADO_ELABORADO = TOTAL_DEVENGADO_ELABORADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("TOTAL_COMPROMISO_ELABORADO")
	public Double getTOTALCOMPROMISOELABORADO() {
		return TOTAL_COMPROMISO_ELABORADO;
	}

	public void setTOTALCOMPROMISOELABORADO(Double TOTAL_COMPROMISO_ELABORADO) {
		this.TOTAL_COMPROMISO_ELABORADO = TOTAL_COMPROMISO_ELABORADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("TOTAL_PRECOMPROMISO_ELABORADO")
	public Double getTOTALPRECOMPROMISOELABORADO() {
		return TOTAL_PRECOMPROMISO_ELABORADO;
	}

	public void setTOTALPRECOMPROMISOELABORADO(Double TOTAL_PRECOMPROMISO_ELABORADO) {
		this.TOTAL_PRECOMPROMISO_ELABORADO = TOTAL_PRECOMPROMISO_ELABORADO;
	}

	/**
	 * 
	 **/
	@JsonProperty("PPTO_CREDITO_DISPONIBLE")
	public Double getPPTOCREDITODISPONIBLE() {
		return PPTO_CREDITO_DISPONIBLE;
	}

	public void setPPTOCREDITODISPONIBLE(Double PPTO_CREDITO_DISPONIBLE) {
		this.PPTO_CREDITO_DISPONIBLE = PPTO_CREDITO_DISPONIBLE;
	}

	/**
	 * 
	 **/
	@JsonProperty("PPTO_DISPONIBLE")
	public Double getPPTODISPONIBLE() {
		return PPTO_DISPONIBLE;
	}

	public void setPPTODISPONIBLE(Double PPTO_DISPONIBLE) {
		this.PPTO_DISPONIBLE = PPTO_DISPONIBLE;
	}

	/**
	 * 
	 **/
	@JsonProperty("PPTO_VIGENTE")
	public Double getPPTOVIGENTE() {
		return PPTO_VIGENTE;
	}

	public void setPPTOVIGENTE(Double PPTO_VIGENTE) {
		this.PPTO_VIGENTE = PPTO_VIGENTE;
	}

	/**
	 * 
	 **/
	@JsonProperty("PPTO_INICIAL")
	public Double getPPTOINICIAL() {
		return PPTO_INICIAL;
	}

	public void setPPTOINICIAL(Double PPTO_INICIAL) {
		this.PPTO_INICIAL = PPTO_INICIAL;
	}
	
	
	/**
	   * 
	   **/
	  @JsonProperty("SECUENCIAL_FTE_ESPEC")
	  public Integer getSECUENCIALFTEESPEC() {
	    return SECUENCIAL_FTE_ESPEC;
	  }
	  public void setSECUENCIALFTEESPEC(Integer SECUENCIAL_FTE_ESPEC) {
	    this.SECUENCIAL_FTE_ESPEC = SECUENCIAL_FTE_ESPEC;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("TRF_BENEFICIARIO")
	  public Integer getTRFBENEFICIARIO() {
	    return TRF_BENEFICIARIO;
	  }
	  public void setTRFBENEFICIARIO(Integer TRF_BENEFICIARIO) {
	    this.TRF_BENEFICIARIO = TRF_BENEFICIARIO;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("OBJETO")
	  public String getOBJETO() {
	    return OBJETO;
	  }
	  public void setOBJETO(String OBJETO) {
	    this.OBJETO = OBJETO;
	  }

	  
	  /**
	   * 
	   **/

	  @JsonProperty("ORGANISMO")
	  public Integer getORGANISMO() {
	    return ORGANISMO;
	  }
	  public void setORGANISMO(Integer ORGANISMO) {
	    this.ORGANISMO = ORGANISMO;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("FUENTE")
	  public Integer getFUENTE() {
	    return FUENTE;
	  }
	  public void setFUENTE(Integer FUENTE) {
	    this.FUENTE = FUENTE;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("ACTIVIDAD_OBRA")
	  public Integer getACTIVIDADOBRA() {
	    return ACTIVIDAD_OBRA;
	  }
	  public void setACTIVIDADOBRA(Integer ACTIVIDAD_OBRA) {
	    this.ACTIVIDAD_OBRA = ACTIVIDAD_OBRA;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("CONVENIO")
	  public String getCONVENIO() {
	    return CONVENIO;
	  }
	  public void setCONVENIO(String CONVENIO) {
	    this.CONVENIO = CONVENIO;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("BIP")
	  public String getBIP() {
	    return BIP;
	  }
	  public void setBIP(String BIP) {
	    this.BIP = BIP;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("PROYECTO")
	  public Integer getPROYECTO() {
	    return PROYECTO;
	  }
	  public void setPROYECTO(Integer PROYECTO) {
	    this.PROYECTO = PROYECTO;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("SUB_PROGRAMA")
	  public Integer getSUBPROGRAMA() {
	    return SUB_PROGRAMA;
	  }
	  public void setSUBPROGRAMA(Integer SUB_PROGRAMA) {
	    this.SUB_PROGRAMA = SUB_PROGRAMA;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("PROGRAMA")
	  public Integer getPROGRAMA() {
	    return PROGRAMA;
	  }
	  public void setPROGRAMA(Integer PROGRAMA) {
	    this.PROGRAMA = PROGRAMA;
	  }

	  
	  /**
	   * 
	   **/

	  @JsonProperty("UE")
	  public Integer getUE() {
	    return UE;
	  }
	  public void setUE(Integer UE) {
	    this.UE = UE;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("GA")
	  public Integer getGA() {
	    return GA;
	  }
	  public void setGA(Integer GA) {
	    this.GA = GA;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("INSTITUCION")
	  public Integer getINSTITUCION() {
	    return INSTITUCION;
	  }
	  public void setINSTITUCION(Integer INSTITUCION) {
	    this.INSTITUCION = INSTITUCION;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("GESTION")
	  public Integer getGESTION() {
	    return GESTION;
	  }
	  public void setGESTION(Integer GESTION) {
	    this.GESTION = GESTION;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("TOTAL_DEVENGADO_APROBADO")
	  public Double getTOTALDEVENGADOAPROBADO() {
	    return TOTAL_DEVENGADO_APROBADO;
	  }
	  public void setTOTALDEVENGADOAPROBADO(Double TOTAL_DEVENGADO_APROBADO) {
	    this.TOTAL_DEVENGADO_APROBADO = TOTAL_DEVENGADO_APROBADO;
	  }

	  
	  /**
	   * 
	   **/
	  @JsonProperty("TOTAL_PAGO_APROBADO")
	  public Double getTOTALPAGOAPROBADO() {
	    return TOTAL_PAGO_APROBADO;
	  }
	  public void setTOTALPAGOAPROBADO(Double TOTAL_PAGO_APROBADO) {
	    this.TOTAL_PAGO_APROBADO = TOTAL_PAGO_APROBADO;
	  }


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PresupuestoGastoFesDto {\n");

		sb.append("    TOTAL_COMPROMISO_APROBADO: ").append(StringUtil.toIndentedString(TOTAL_COMPROMISO_APROBADO))
				.append("\n");
		sb.append("    TOTAL_PRECOMPROMISO_APROBADO: ")
				.append(StringUtil.toIndentedString(TOTAL_PRECOMPROMISO_APROBADO)).append("\n");
		sb.append("    TOTAL_DEVENGADO_VERIFICADO: ").append(StringUtil.toIndentedString(TOTAL_DEVENGADO_VERIFICADO))
				.append("\n");
		sb.append("    TOTAL_COMPROMISO_VERIFICADO: ").append(StringUtil.toIndentedString(TOTAL_COMPROMISO_VERIFICADO))
				.append("\n");
		sb.append("    TOTAL_PRECOMPROMISO_VERIFICADO: ")
				.append(StringUtil.toIndentedString(TOTAL_PRECOMPROMISO_VERIFICADO)).append("\n");
		sb.append("    TOTAL_DEVENGADO_ELABORADO: ").append(StringUtil.toIndentedString(TOTAL_DEVENGADO_ELABORADO))
				.append("\n");
		sb.append("    TOTAL_COMPROMISO_ELABORADO: ").append(StringUtil.toIndentedString(TOTAL_COMPROMISO_ELABORADO))
				.append("\n");
		sb.append("    TOTAL_PRECOMPROMISO_ELABORADO: ")
				.append(StringUtil.toIndentedString(TOTAL_PRECOMPROMISO_ELABORADO)).append("\n");
		sb.append("    PPTO_CREDITO_DISPONIBLE: ").append(StringUtil.toIndentedString(PPTO_CREDITO_DISPONIBLE))
				.append("\n");
		sb.append("    PPTO_DISPONIBLE: ").append(StringUtil.toIndentedString(PPTO_DISPONIBLE)).append("\n");
		sb.append("    PPTO_VIGENTE: ").append(StringUtil.toIndentedString(PPTO_VIGENTE)).append("\n");
		sb.append("    PPTO_INICIAL: ").append(StringUtil.toIndentedString(PPTO_INICIAL)).append("\n");
		sb.append("    SECUENCIAL_FTE_ESPEC: ").append(StringUtil.toIndentedString(SECUENCIAL_FTE_ESPEC)).append("\n");
	    sb.append("    TRF_BENEFICIARIO: ").append(StringUtil.toIndentedString(TRF_BENEFICIARIO)).append("\n");
	    sb.append("    OBJETO: ").append(StringUtil.toIndentedString(OBJETO)).append("\n");
	    sb.append("    ORGANISMO: ").append(StringUtil.toIndentedString(ORGANISMO)).append("\n");
	    sb.append("    FUENTE: ").append(StringUtil.toIndentedString(FUENTE)).append("\n");
	    sb.append("    ACTIVIDAD_OBRA: ").append(StringUtil.toIndentedString(ACTIVIDAD_OBRA)).append("\n");
	    sb.append("    CONVENIO: ").append(StringUtil.toIndentedString(CONVENIO)).append("\n");
	    sb.append("    BIP: ").append(StringUtil.toIndentedString(BIP)).append("\n");
	    sb.append("    PROYECTO: ").append(StringUtil.toIndentedString(PROYECTO)).append("\n");
	    sb.append("    SUB_PROGRAMA: ").append(StringUtil.toIndentedString(SUB_PROGRAMA)).append("\n");
	    sb.append("    PROGRAMA: ").append(StringUtil.toIndentedString(PROGRAMA)).append("\n");
	    sb.append("    UE: ").append(StringUtil.toIndentedString(UE)).append("\n");
	    sb.append("    GA: ").append(StringUtil.toIndentedString(GA)).append("\n");
	    sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
	    sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
	    sb.append("    TOTAL_DEVENGADO_APROBADO: ").append(StringUtil.toIndentedString(TOTAL_DEVENGADO_APROBADO)).append("\n");
	    sb.append("    TOTAL_PAGO_APROBADO: ").append(StringUtil.toIndentedString(TOTAL_PAGO_APROBADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
