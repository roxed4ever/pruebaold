package siafi.core.cont.ing.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * mensaje
 **/

public class RespuestaDto  {

  private boolean mensaje = false;
 /**
   * Get mensaje
   * @return mensaje
  **/
  @JsonProperty("mensaje")
  public boolean getMensaje() {
    return mensaje;
  }

  public void setMensaje(boolean mensaje) {
    this.mensaje = mensaje;
  }

  public RespuestaDto mensaje(boolean mensaje) {
    this.mensaje = mensaje;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RespuestaDto {\n");
    
    sb.append("    mensaje: ").append(toIndentedString(mensaje)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

