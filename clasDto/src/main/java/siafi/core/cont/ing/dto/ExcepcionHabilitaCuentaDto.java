package siafi.core.cont.ing.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Excepcion habilita cuenta
 **/

public class ExcepcionHabilitaCuentaDto implements Serializable{
  

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
	
	
  private String nombreExcepcion = null;
  private String gestion = null;
  private String gestionIngreso = null;
  private String institucion = null;
  private String ga = null;
  private String banco = null;
  private String numeroCuenta = null;
  private String vigencia = null;
  private String observaciones = null;
  private String comentario = null;
  private String moneda = null;
  private String apiTransaccion = null;
 /**
   * Get nombreExcepcion
   * @return nombreExcepcion
  **/
  @JsonProperty("nombreExcepcion")
  public String getNombreExcepcion() {
    return nombreExcepcion;
  }

  public void setNombreExcepcion(String nombreExcepcion) {
    this.nombreExcepcion = nombreExcepcion;
  }

  public ExcepcionHabilitaCuentaDto nombreExcepcion(String nombreExcepcion) {
    this.nombreExcepcion = nombreExcepcion;
    return this;
  }

 /**
   * Get gestion
   * @return gestion
  **/
  @JsonProperty("gestion")
  public String getGestion() {
    return gestion;
  }

  public void setGestion(String gestion) {
    this.gestion = gestion;
  }

  public ExcepcionHabilitaCuentaDto gestion(String gestion) {
    this.gestion = gestion;
    return this;
  }

 /**
   * Get gestionIngreso
   * @return gestionIngreso
  **/
  @JsonProperty("gestionIngreso")
  public String getGestionIngreso() {
    return gestionIngreso;
  }

  public void setGestionIngreso(String gestionIngreso) {
    this.gestionIngreso = gestionIngreso;
  }

  public ExcepcionHabilitaCuentaDto gestionIngreso(String gestionIngreso) {
    this.gestionIngreso = gestionIngreso;
    return this;
  }

 /**
   * Get institucion
   * @return institucion
  **/
  @JsonProperty("institucion")
  public String getInstitucion() {
    return institucion;
  }

  public void setInstitucion(String institucion) {
    this.institucion = institucion;
  }

  public ExcepcionHabilitaCuentaDto institucion(String institucion) {
    this.institucion = institucion;
    return this;
  }

 /**
   * Get ga
   * @return ga
  **/
  @JsonProperty("ga")
  public String getGa() {
    return ga;
  }

  public void setGa(String ga) {
    this.ga = ga;
  }

  public ExcepcionHabilitaCuentaDto ga(String ga) {
    this.ga = ga;
    return this;
  }

 /**
   * Get banco
   * @return banco
  **/
  @JsonProperty("banco")
  public String getBanco() {
    return banco;
  }

  public void setBanco(String banco) {
    this.banco = banco;
  }

  public ExcepcionHabilitaCuentaDto banco(String banco) {
    this.banco = banco;
    return this;
  }

 /**
   * Get numeroCuenta
   * @return numeroCuenta
  **/
  @JsonProperty("numeroCuenta")
  public String getNumeroCuenta() {
    return numeroCuenta;
  }

  public void setNumeroCuenta(String numeroCuenta) {
    this.numeroCuenta = numeroCuenta;
  }

  public ExcepcionHabilitaCuentaDto numeroCuenta(String numeroCuenta) {
    this.numeroCuenta = numeroCuenta;
    return this;
  }

 /**
   * Get vigencia
   * @return vigencia
  **/
  @JsonProperty("vigencia")
  public String getVigencia() {
    return vigencia;
  }

  public void setVigencia(String vigencia) {
    this.vigencia = vigencia;
  }

  public ExcepcionHabilitaCuentaDto vigencia(String vigencia) {
    this.vigencia = vigencia;
    return this;
  }

 /**
   * Get observaciones
   * @return observaciones
  **/
  @JsonProperty("observaciones")
  public String getObservaciones() {
    return observaciones;
  }

  public void setObservaciones(String observaciones) {
    this.observaciones = observaciones;
  }

  public ExcepcionHabilitaCuentaDto observaciones(String observaciones) {
    this.observaciones = observaciones;
    return this;
  }

 /**
   * Get comentario
   * @return comentario
  **/
  @JsonProperty("comentario")
  public String getComentario() {
    return comentario;
  }

  public void setComentario(String comentario) {
    this.comentario = comentario;
  }

  public ExcepcionHabilitaCuentaDto comentario(String comentario) {
    this.comentario = comentario;
    return this;
  }

 /**
   * Get moneda
   * @return moneda
  **/
  @JsonProperty("moneda")
  public String getMoneda() {
    return moneda;
  }

  public void setMoneda(String moneda) {
    this.moneda = moneda;
  }

  public ExcepcionHabilitaCuentaDto moneda(String moneda) {
    this.moneda = moneda;
    return this;
  }

 /**
   * Get apiTransaccion
   * @return apiTransaccion
  **/
  @JsonProperty("apiTransaccion")
  public String getApiTransaccion() {
    return apiTransaccion;
  }

  public void setApiTransaccion(String apiTransaccion) {
    this.apiTransaccion = apiTransaccion;
  }

  public ExcepcionHabilitaCuentaDto apiTransaccion(String apiTransaccion) {
    this.apiTransaccion = apiTransaccion;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExcepcionHabilitaCuentaDto {\n");
    
    sb.append("    nombreExcepcion: ").append(toIndentedString(nombreExcepcion)).append("\n");
    sb.append("    gestion: ").append(toIndentedString(gestion)).append("\n");
    sb.append("    gestionIngreso: ").append(toIndentedString(gestionIngreso)).append("\n");
    sb.append("    institucion: ").append(toIndentedString(institucion)).append("\n");
    sb.append("    ga: ").append(toIndentedString(ga)).append("\n");
    sb.append("    banco: ").append(toIndentedString(banco)).append("\n");
    sb.append("    numeroCuenta: ").append(toIndentedString(numeroCuenta)).append("\n");
    sb.append("    vigencia: ").append(toIndentedString(vigencia)).append("\n");
    sb.append("    observaciones: ").append(toIndentedString(observaciones)).append("\n");
    sb.append("    comentario: ").append(toIndentedString(comentario)).append("\n");
    sb.append("    moneda: ").append(toIndentedString(moneda)).append("\n");
    sb.append("    apiTransaccion: ").append(toIndentedString(apiTransaccion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

