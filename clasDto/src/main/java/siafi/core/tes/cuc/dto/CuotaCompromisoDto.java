package siafi.core.tes.cuc.dto;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-12T18:33:44.630-06:00")
public class CuotaCompromisoDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private Integer INSTITUCION = null;
	private Integer GA = null;
	private Integer UE = null;
	private Integer TRIMESTRE = null;
	private Integer FUENTE = null;
	private Integer CLASE_DE_GASTO = null;
	private Double CUOTA_SOLICITADA = null;
	private Double CUOTA_ASIGNADA = null;
	private Double CUOTA_REDISTRIBUIDA = null;
	private Double CUOTA_EJECUTADA = null;
	private Double EJECUCION_REVERTIDA = null;
	private Double AJUSTES_POR_DISMINUCIONES = null;
	private String API_ESTADO = null;
	private Double CUOTA_RESERVADA = null;

	public CuotaCompromisoDto() {
		initPROPERTY_MAP();
	}

	public CuotaCompromisoDto(Integer gESTION, Integer iNSTITUCION, Integer gA, Integer uE, Integer tRIMESTRE,
			Integer fUENTE, Integer cLASE_DE_GASTO, Double cUOTA_SOLICITADA, Double cUOTA_ASIGNADA,
			Double cUOTA_REDISTRIBUIDA, Double cUOTA_EJECUTADA, Double eJECUCION_REVERTIDA,
			Double aJUSTES_POR_DISMINUCIONES, String aPI_ESTADO, Double cUOTA_RESERVADA) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		INSTITUCION = iNSTITUCION;
		GA = gA;
		UE = uE;
		TRIMESTRE = tRIMESTRE;
		FUENTE = fUENTE;
		CLASE_DE_GASTO = cLASE_DE_GASTO;
		CUOTA_SOLICITADA = cUOTA_SOLICITADA;
		CUOTA_ASIGNADA = cUOTA_ASIGNADA;
		CUOTA_REDISTRIBUIDA = cUOTA_REDISTRIBUIDA;
		CUOTA_EJECUTADA = cUOTA_EJECUTADA;
		EJECUCION_REVERTIDA = eJECUCION_REVERTIDA;
		AJUSTES_POR_DISMINUCIONES = aJUSTES_POR_DISMINUCIONES;
		API_ESTADO = aPI_ESTADO;
		CUOTA_RESERVADA = cUOTA_RESERVADA;
	}

	/**
	 **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("INSTITUCION")
	public Integer getINSTITUCION() {
		return INSTITUCION;
	}

	public void setINSTITUCION(Integer INSTITUCION) {
		this.INSTITUCION = INSTITUCION;
	}

	/**
	 **/

	@JsonProperty("GA")
	public Integer getGA() {
		return GA;
	}

	public void setGA(Integer GA) {
		this.GA = GA;
	}

	/**
	 **/

	@JsonProperty("UE")
	public Integer getUE() {
		return UE;
	}

	public void setUE(Integer UE) {
		this.UE = UE;
	}

	/**
	 **/

	@JsonProperty("TRIMESTRE")
	public Integer getTRIMESTRE() {
		return TRIMESTRE;
	}

	public void setTRIMESTRE(Integer TRIMESTRE) {
		this.TRIMESTRE = TRIMESTRE;
	}

	/**
	 **/

	@JsonProperty("FUENTE")
	public Integer getFUENTE() {
		return FUENTE;
	}

	public void setFUENTE(Integer FUENTE) {
		this.FUENTE = FUENTE;
	}

	/**
	 **/

	@JsonProperty("CLASE_DE_GASTO")
	public Integer getCLASEDEGASTO() {
		return CLASE_DE_GASTO;
	}

	public void setCLASEDEGASTO(Integer CLASE_DE_GASTO) {
		this.CLASE_DE_GASTO = CLASE_DE_GASTO;
	}

	/**
	 **/

	@JsonProperty("CUOTA_SOLICITADA")
	public Double getCUOTASOLICITADA() {
		return CUOTA_SOLICITADA;
	}

	public void setCUOTASOLICITADA(Double CUOTA_SOLICITADA) {
		this.CUOTA_SOLICITADA = CUOTA_SOLICITADA;
	}

	/**
	 **/

	@JsonProperty("CUOTA_ASIGNADA")
	public Double getCUOTAASIGNADA() {
		return CUOTA_ASIGNADA;
	}

	public void setCUOTAASIGNADA(Double CUOTA_ASIGNADA) {
		this.CUOTA_ASIGNADA = CUOTA_ASIGNADA;
	}

	/**
	 **/

	@JsonProperty("CUOTA_REDISTRIBUIDA")
	public Double getCUOTAREDISTRIBUIDA() {
		return CUOTA_REDISTRIBUIDA;
	}

	public void setCUOTAREDISTRIBUIDA(Double CUOTA_REDISTRIBUIDA) {
		this.CUOTA_REDISTRIBUIDA = CUOTA_REDISTRIBUIDA;
	}

	/**
	 **/

	@JsonProperty("CUOTA_EJECUTADA")
	public Double getCUOTAEJECUTADA() {
		return CUOTA_EJECUTADA;
	}

	public void setCUOTAEJECUTADA(Double CUOTA_EJECUTADA) {
		this.CUOTA_EJECUTADA = CUOTA_EJECUTADA;
	}

	/**
	 **/

	@JsonProperty("EJECUCION_REVERTIDA")
	public Double getEJECUCIONREVERTIDA() {
		return EJECUCION_REVERTIDA;
	}

	public void setEJECUCIONREVERTIDA(Double EJECUCION_REVERTIDA) {
		this.EJECUCION_REVERTIDA = EJECUCION_REVERTIDA;
	}

	/**
	 **/

	@JsonProperty("AJUSTES_POR_DISMINUCIONES")
	public Double getAJUSTESPORDISMINUCIONES() {
		return AJUSTES_POR_DISMINUCIONES;
	}

	public void setAJUSTESPORDISMINUCIONES(Double AJUSTES_POR_DISMINUCIONES) {
		this.AJUSTES_POR_DISMINUCIONES = AJUSTES_POR_DISMINUCIONES;
	}

	/**
	 **/

	@JsonProperty("API_ESTADO")
	public String getAPIESTADO() {
		return API_ESTADO;
	}

	public void setAPIESTADO(String API_ESTADO) {
		this.API_ESTADO = API_ESTADO;
	}

	/**
	 **/

	@JsonProperty("CUOTA_RESERVADA")
	public Double getCUOTARESERVADA() {
		return CUOTA_RESERVADA;
	}

	public void setCUOTARESERVADA(Double CUOTA_RESERVADA) {
		this.CUOTA_RESERVADA = CUOTA_RESERVADA;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CuotaCompromisoDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    INSTITUCION: ").append(StringUtil.toIndentedString(INSTITUCION)).append("\n");
		sb.append("    GA: ").append(StringUtil.toIndentedString(GA)).append("\n");
		sb.append("    UE: ").append(StringUtil.toIndentedString(UE)).append("\n");
		sb.append("    TRIMESTRE: ").append(StringUtil.toIndentedString(TRIMESTRE)).append("\n");
		sb.append("    FUENTE: ").append(StringUtil.toIndentedString(FUENTE)).append("\n");
		sb.append("    CLASE_DE_GASTO: ").append(StringUtil.toIndentedString(CLASE_DE_GASTO)).append("\n");
		sb.append("    CUOTA_SOLICITADA: ").append(StringUtil.toIndentedString(CUOTA_SOLICITADA)).append("\n");
		sb.append("    CUOTA_ASIGNADA: ").append(StringUtil.toIndentedString(CUOTA_ASIGNADA)).append("\n");
		sb.append("    CUOTA_REDISTRIBUIDA: ").append(StringUtil.toIndentedString(CUOTA_REDISTRIBUIDA)).append("\n");
		sb.append("    CUOTA_EJECUTADA: ").append(StringUtil.toIndentedString(CUOTA_EJECUTADA)).append("\n");
		sb.append("    EJECUCION_REVERTIDA: ").append(StringUtil.toIndentedString(EJECUCION_REVERTIDA)).append("\n");
		sb.append("    AJUSTES_POR_DISMINUCIONES: ").append(StringUtil.toIndentedString(AJUSTES_POR_DISMINUCIONES))
				.append("\n");
		sb.append("    API_ESTADO: ").append(StringUtil.toIndentedString(API_ESTADO)).append("\n");
		sb.append("    CUOTA_RESERVADA: ").append(StringUtil.toIndentedString(CUOTA_RESERVADA)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "id.institucion");
		super.PROPERTY_MAP.put("GA", "id.ga");
		super.PROPERTY_MAP.put("UE", "id.ue");//
		super.PROPERTY_MAP.put("TRIMESTRE", "id.trimestre");
		super.PROPERTY_MAP.put("FUENTE", "id.fuente");
		super.PROPERTY_MAP.put("CLASE_DE_GASTO", "id.claseDeGasto");
		super.PROPERTY_MAP.put("CUOTA_SOLICITADA", "cuotaSolicitada");
		super.PROPERTY_MAP.put("CUOTA_ASIGNADA", "cuotaAsignada");
		super.PROPERTY_MAP.put("CUOTA_REDISTRIBUIDA", "cuotaRedistribuida");//
		super.PROPERTY_MAP.put("CUOTA_EJECUTADA", "cuotaEjecutada");
		super.PROPERTY_MAP.put("EJECUCION_REVERTIDA", "ejecucionRevertida");
		super.PROPERTY_MAP.put("AJUSTES_POR_DISMINUCIONES", "ajustesPorDisminuciones");
		super.PROPERTY_MAP.put("API_ESTADO", "apiEstado");
	}
}
