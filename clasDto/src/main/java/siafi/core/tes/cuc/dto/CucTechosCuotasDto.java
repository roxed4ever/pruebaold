package siafi.core.tes.cuc.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Tabla de Cuc_Techos_cuotas
 **/
public class CucTechosCuotasDto extends AmbitosDto implements Serializable  {
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

  private long GESTION = 0;
  private long INSTITUCION = 0;
  private long GA = 0;
  private long UE = 0; 
  private long FUENTE = 0;
  private long CLASE_DE_GASTO = 0;
  private long TRIMESTRE = 0;
  private long MES = 0;


  private String VIGENTE = null;
  private Double IMPORTE_TECHO = null;
  private Double IMPORTE_CONGELADO = null;
  private Double IMPORTE_EJECUTADO = null;
  private Double IMPORTE_REVERTIDO = null;
  private String API_ESTADO = null;
  private String API_TRANSACCION = null;
  private Boolean PAGINAR = null;
  private Boolean FILTRAR_PAGINA = null;
  private Boolean ORDENAR_PAGINA = null;
  private String PAGINACION_FILTRO = null;
  private Map<String, Boolean> PAGINACION_ORDEN = null;
  private Map<String, String> AMBITOS = null;
  
  public CucTechosCuotasDto() {
	  initPROPERTY_MAP();
  }
  
    
  public CucTechosCuotasDto(long gESTION, long iNSTITUCION, long gA, long uE, long fUENTE,
		long cLASE_DE_GASTO, long tRIMESTRE, long mES, String vIGENTE, Double iMPORTE_TECHO,
		Double iMPORTE_CONGELADO, Double iMPORTE_EJECUTADO, Double iMPORTE_REVERTIDO, String aPI_ESTADO,
		String aPI_TRANSACCION, Boolean pAGINAR, Boolean fILTRAR_PAGINA, Boolean oRDENAR_PAGINA,
		String pAGINACION_FILTRO, Map<String, Boolean> pAGINACION_ORDEN, Map<String, String> aMBITOS,
		PaginacionDto pAGINACION) {
	super();
	GESTION = gESTION;
	INSTITUCION = iNSTITUCION;
	GA = gA;
	UE = uE;
	FUENTE = fUENTE;
	CLASE_DE_GASTO = cLASE_DE_GASTO;
	TRIMESTRE = tRIMESTRE;
	MES = mES;
	VIGENTE = vIGENTE;
	IMPORTE_TECHO = iMPORTE_TECHO;
	IMPORTE_CONGELADO = iMPORTE_CONGELADO;
	IMPORTE_EJECUTADO = iMPORTE_EJECUTADO;
	IMPORTE_REVERTIDO = iMPORTE_REVERTIDO;
	API_ESTADO = aPI_ESTADO;
	API_TRANSACCION = aPI_TRANSACCION;
	PAGINAR = pAGINAR;
	FILTRAR_PAGINA = fILTRAR_PAGINA;
	ORDENAR_PAGINA = oRDENAR_PAGINA;
	PAGINACION_FILTRO = pAGINACION_FILTRO;
	PAGINACION_ORDEN = pAGINACION_ORDEN;
	AMBITOS = aMBITOS;
	PAGINACION = pAGINACION;
	initPROPERTY_MAP();
}


/**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get GESTION
   * @return GESTION
  **/
  @JsonProperty("GESTION")
  public long getGESTION() {
    return GESTION;
  }

  public void setGESTION(long GESTION) {
    this.GESTION = GESTION;
  }

  public CucTechosCuotasDto GESTION(long GESTION) {
    this.GESTION = GESTION;
    return this;
  }

 /**
   * Get INSTITUCION
   * @return INSTITUCION
  **/
  @JsonProperty("INSTITUCION")
  public long getINSTITUCION() {
    return INSTITUCION;
  }

  public void setINSTITUCION(long INSTITUCION) {
    this.INSTITUCION = INSTITUCION;
  }

  public CucTechosCuotasDto INSTITUCION(long INSTITUCION) {
    this.INSTITUCION = INSTITUCION;
    return this;
  }

 /**
   * Get GA
   * @return GA
  **/
  @JsonProperty("GA")
  public long getGA() {
    return GA;
  }

  public void setGA(long GA) {
    this.GA = GA;
  }

  public CucTechosCuotasDto GA(long GA) {
    this.GA = GA;
    return this;
  }

 /**
   * Get UE
   * @return UE
  **/
  @JsonProperty("UE")
  public long getUE() {
    return UE;
  }

  public void setUE(long UE) {
    this.UE = UE;
  }

  public CucTechosCuotasDto UE(long UE) {
    this.UE = UE;
    return this;
  }

 /**
   * Get FUENTE
   * @return FUENTE
  **/
  @JsonProperty("FUENTE")
  public long getFUENTE() {
    return FUENTE;
  }

  public void setFUENTE(long FUENTE) {
    this.FUENTE = FUENTE;
  }

  public CucTechosCuotasDto FUENTE(long FUENTE) {
    this.FUENTE = FUENTE;
    return this;
  }

 /**
   * Get CLASE_DE_GASTO
   * @return CLASE_DE_GASTO
  **/
  @JsonProperty("CLASE_DE_GASTO")
  public long getCLASEDEGASTO() {
    return CLASE_DE_GASTO;
  }

  public void setCLASEDEGASTO(long CLASE_DE_GASTO) {
    this.CLASE_DE_GASTO = CLASE_DE_GASTO;
  }

  public CucTechosCuotasDto CLASE_DE_GASTO(long CLASE_DE_GASTO) {
    this.CLASE_DE_GASTO = CLASE_DE_GASTO;
    return this;
  }

 /**
   * Get TRIMESTRE
   * @return TRIMESTRE
  **/
  @JsonProperty("TRIMESTRE")
  public long getTRIMESTRE() {
    return TRIMESTRE;
  }

  public void setTRIMESTRE(long TRIMESTRE) {
    this.TRIMESTRE = TRIMESTRE;
  }

  public CucTechosCuotasDto TRIMESTRE(long TRIMESTRE) {
    this.TRIMESTRE = TRIMESTRE;
    return this;
  }

 /**
   * Get MES
   * @return MES
  **/
  @JsonProperty("MES")
  public long getMES() {
    return MES;
  }

  public void setMES(long MES) {
    this.MES = MES;
  }

  public CucTechosCuotasDto MES(long MES) {
    this.MES = MES;
    return this;
  }

 /**
   * Get VIGENTE
   * @return VIGENTE
  **/
  @JsonProperty("VIGENTE")
  public String getVIGENTE() {
    return VIGENTE;
  }

  public void setVIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
  }

  public CucTechosCuotasDto VIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
    return this;
  }

 /**
   * Get IMPORTE_TECHO
   * @return IMPORTE_TECHO
  **/
  @JsonProperty("IMPORTE_TECHO")
  public Double getIMPORTETECHO() {
    return IMPORTE_TECHO;
  }

  public void setIMPORTETECHO(Double IMPORTE_TECHO) {
    this.IMPORTE_TECHO = IMPORTE_TECHO;
  }

  public CucTechosCuotasDto IMPORTE_TECHO(Double IMPORTE_TECHO) {
    this.IMPORTE_TECHO = IMPORTE_TECHO;
    return this;
  }

 /**
   * Get IMPORTE_CONGELADO
   * @return IMPORTE_CONGELADO
  **/
  @JsonProperty("IMPORTE_CONGELADO")
  public Double getIMPORTECONGELADO() {
    return IMPORTE_CONGELADO;
  }

  public void setIMPORTECONGELADO(Double IMPORTE_CONGELADO) {
    this.IMPORTE_CONGELADO = IMPORTE_CONGELADO;
  }

  public CucTechosCuotasDto IMPORTE_CONGELADO(Double IMPORTE_CONGELADO) {
    this.IMPORTE_CONGELADO = IMPORTE_CONGELADO;
    return this;
  }

 /**
   * Get IMPORTE_EJECUTADO
   * @return IMPORTE_EJECUTADO
  **/
  @JsonProperty("IMPORTE_EJECUTADO")
  public Double getIMPORTEEJECUTADO() {
    return IMPORTE_EJECUTADO;
  }

  public void setIMPORTEEJECUTADO(Double IMPORTE_EJECUTADO) {
    this.IMPORTE_EJECUTADO = IMPORTE_EJECUTADO;
  }

  public CucTechosCuotasDto IMPORTE_EJECUTADO(Double IMPORTE_EJECUTADO) {
    this.IMPORTE_EJECUTADO = IMPORTE_EJECUTADO;
    return this;
  }

 /**
   * Get IMPORTE_REVERTIDO
   * @return IMPORTE_REVERTIDO
  **/
  @JsonProperty("IMPORTE_REVERTIDO")
  public Double getIMPORTEREVERTIDO() {
    return IMPORTE_REVERTIDO;
  }

  public void setIMPORTEREVERTIDO(Double IMPORTE_REVERTIDO) {
    this.IMPORTE_REVERTIDO = IMPORTE_REVERTIDO;
  }

  public CucTechosCuotasDto IMPORTE_REVERTIDO(Double IMPORTE_REVERTIDO) {
    this.IMPORTE_REVERTIDO = IMPORTE_REVERTIDO;
    return this;
  }

 /**
   * Get API_ESTADO
   * @return API_ESTADO
  **/
  @JsonProperty("API_ESTADO")
  public String getAPIESTADO() {
    return API_ESTADO;
  }

  public void setAPIESTADO(String API_ESTADO) {
    this.API_ESTADO = API_ESTADO;
  }

  public CucTechosCuotasDto API_ESTADO(String API_ESTADO) {
    this.API_ESTADO = API_ESTADO;
    return this;
  }

 /**
   * Get API_TRANSACCION
   * @return API_TRANSACCION
  **/
  @JsonProperty("API_TRANSACCION")
  public String getAPITRANSACCION() {
    return API_TRANSACCION;
  }

  public void setAPITRANSACCION(String API_TRANSACCION) {
    this.API_TRANSACCION = API_TRANSACCION;
  }

  public CucTechosCuotasDto API_TRANSACCION(String API_TRANSACCION) {
    this.API_TRANSACCION = API_TRANSACCION;
    return this;
  }

 /**
   * Get PAGINAR
   * @return PAGINAR
  **/
  @JsonProperty("PAGINAR")
  public boolean isPAGINAR() {
    return PAGINAR;
  }

  public void setPAGINAR(Boolean PAGINAR) {
    this.PAGINAR = PAGINAR;
  }

  public CucTechosCuotasDto PAGINAR(Boolean PAGINAR) {
    this.PAGINAR = PAGINAR;
    return this;
  }

 /**
   * Get FILTRAR_PAGINA
   * @return FILTRAR_PAGINA
  **/
  @JsonProperty("FILTRAR_PAGINA")
  public Boolean isFILTRARPAGINA() {
    return FILTRAR_PAGINA;
  }

  public void setFILTRARPAGINA(Boolean FILTRAR_PAGINA) {
    this.FILTRAR_PAGINA = FILTRAR_PAGINA;
  }

  public CucTechosCuotasDto FILTRAR_PAGINA(Boolean FILTRAR_PAGINA) {
    this.FILTRAR_PAGINA = FILTRAR_PAGINA;
    return this;
  }

 /**
   * Get ORDENAR_PAGINA
   * @return ORDENAR_PAGINA
  **/
  @JsonProperty("ORDENAR_PAGINA")
  public Boolean isORDENARPAGINA() {
    return ORDENAR_PAGINA;
  }

  public void setORDENARPAGINA(Boolean ORDENAR_PAGINA) {
    this.ORDENAR_PAGINA = ORDENAR_PAGINA;
  }

  public CucTechosCuotasDto ORDENAR_PAGINA(Boolean ORDENAR_PAGINA) {
    this.ORDENAR_PAGINA = ORDENAR_PAGINA;
    return this;
  }

 /**
   * Get PAGINACION_FILTRO
   * @return PAGINACION_FILTRO
  **/
  @JsonProperty("PAGINACION_FILTRO")
  public String getPAGINACIONFILTRO() {
    return PAGINACION_FILTRO;
  }

  public void setPAGINACIONFILTRO(String PAGINACION_FILTRO) {
    this.PAGINACION_FILTRO = PAGINACION_FILTRO;
  }

  public CucTechosCuotasDto PAGINACION_FILTRO(String PAGINACION_FILTRO) {
    this.PAGINACION_FILTRO = PAGINACION_FILTRO;
    return this;
  }

 /**
   * Get PAGINACION_ORDEN
   * @return PAGINACION_ORDEN
  **/
  @JsonProperty("PAGINACION_ORDEN")
  public Map<String, Boolean> getPAGINACIONORDEN() {
    return PAGINACION_ORDEN;
  }

  public void setPAGINACIONORDEN(Map<String, Boolean> PAGINACION_ORDEN) {
    this.PAGINACION_ORDEN = PAGINACION_ORDEN;
  }

  public CucTechosCuotasDto PAGINACION_ORDEN(Map<String, Boolean> PAGINACION_ORDEN) {
    this.PAGINACION_ORDEN = PAGINACION_ORDEN;
    return this;
  }

  public CucTechosCuotasDto putPAGINACIONORDENItem(String key, Boolean PAGINACION_ORDENItem) {
    this.PAGINACION_ORDEN.put(key, PAGINACION_ORDENItem);
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public CucTechosCuotasDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public CucTechosCuotasDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
    return PAGINACION;
  }

  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public CucTechosCuotasDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CucTechosCuotasDto {\n");
    
    sb.append("    GESTION: ").append(toIndentedString(GESTION)).append("\n");
    sb.append("    INSTITUCION: ").append(toIndentedString(INSTITUCION)).append("\n");
    sb.append("    GA: ").append(toIndentedString(GA)).append("\n");
    sb.append("    UE: ").append(toIndentedString(UE)).append("\n");
    sb.append("    FUENTE: ").append(toIndentedString(FUENTE)).append("\n");
    sb.append("    CLASE_DE_GASTO: ").append(toIndentedString(CLASE_DE_GASTO)).append("\n");
    sb.append("    TRIMESTRE: ").append(toIndentedString(TRIMESTRE)).append("\n");
    sb.append("    MES: ").append(toIndentedString(MES)).append("\n");
    sb.append("    VIGENTE: ").append(toIndentedString(VIGENTE)).append("\n");
    sb.append("    IMPORTE_TECHO: ").append(toIndentedString(IMPORTE_TECHO)).append("\n");
    sb.append("    IMPORTE_CONGELADO: ").append(toIndentedString(IMPORTE_CONGELADO)).append("\n");
    sb.append("    IMPORTE_EJECUTADO: ").append(toIndentedString(IMPORTE_EJECUTADO)).append("\n");
    sb.append("    IMPORTE_REVERTIDO: ").append(toIndentedString(IMPORTE_REVERTIDO)).append("\n");
    sb.append("    API_ESTADO: ").append(toIndentedString(API_ESTADO)).append("\n");
    sb.append("    API_TRANSACCION: ").append(toIndentedString(API_TRANSACCION)).append("\n");
    sb.append("    PAGINAR: ").append(toIndentedString(PAGINAR)).append("\n");
    sb.append("    FILTRAR_PAGINA: ").append(toIndentedString(FILTRAR_PAGINA)).append("\n");
    sb.append("    ORDENAR_PAGINA: ").append(toIndentedString(ORDENAR_PAGINA)).append("\n");
    sb.append("    PAGINACION_FILTRO: ").append(toIndentedString(PAGINACION_FILTRO)).append("\n");
    sb.append("    PAGINACION_ORDEN: ").append(toIndentedString(PAGINACION_ORDEN)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("INSTITUCION", "id.institucion");
		super.PROPERTY_MAP.put("GA", "id.ga");
		super.PROPERTY_MAP.put("UE", "id.ue");
		super.PROPERTY_MAP.put("FUENTE", "id.fuente");
		super.PROPERTY_MAP.put("CLASE_DE_GASTO", "id.claseDeGasto");
		super.PROPERTY_MAP.put("TRIMESTRE", "id.trimestre");
		super.PROPERTY_MAP.put("MES", "id.mes");
		
		super.PROPERTY_MAP.put("VIGENTE", "id.gestion");
		super.PROPERTY_MAP.put("IMPORTE_TECHO", "id.institucion");
		super.PROPERTY_MAP.put("IMPORTE_CONGELADO", "id.ga");
		super.PROPERTY_MAP.put("IMPORTE_EJECUTADO", "id.ue");
		super.PROPERTY_MAP.put("IMPORTE_REVERTIDO", "id.ga");
		super.PROPERTY_MAP.put("API_ESTADO", "apiEstado");
		super.PROPERTY_MAP.put("API_TRANSACCION", "apiTransaccion");
	}
}
  