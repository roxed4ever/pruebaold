package siafi.core.ppto.fpr.dto;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Finalidades 
 **/

public class FinalidadesDto extends AmbitosDto implements Serializable  {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  private long FINALIDAD = 0;
  private String DESC_FINALIDAD = null;
  private String VIGENTE = null;
  private Map<String, String> AMBITOS = null;

 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
  
  private List<FuncionesDto> funciones = null;
 /**
   * Get FINALIDAD
   * @return FINALIDAD
  **/
  @JsonProperty("FINALIDAD")
  public long getFINALIDAD() {
    return FINALIDAD;
  }

  public void setFINALIDAD(long FINALIDAD) {
    this.FINALIDAD = FINALIDAD;
  }

  public FinalidadesDto FINALIDAD(long FINALIDAD) {
    this.FINALIDAD = FINALIDAD;
    return this;
  }

 /**
   * Get DESC_FINALIDAD
   * @return DESC_FINALIDAD
  **/
  @JsonProperty("DESC_FINALIDAD")
  public String getDESCFINALIDAD() {
    return DESC_FINALIDAD;
  }

  public void setDESCFINALIDAD(String DESC_FINALIDAD) {
    this.DESC_FINALIDAD = DESC_FINALIDAD;
  }

  public FinalidadesDto DESC_FINALIDAD(String DESC_FINALIDAD) {
    this.DESC_FINALIDAD = DESC_FINALIDAD;
    return this;
  }

 /**
   * Get VIGENTE
   * @return VIGENTE
  **/
  @JsonProperty("VIGENTE")
  public String getVIGENTE() {
    return VIGENTE;
  }

  public void setVIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
  }

  public FinalidadesDto VIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public FinalidadesDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public FinalidadesDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/

  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }


  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public FinalidadesDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }

  /**
   * Funciones   
   * @return funciones
  **/
  @JsonProperty("funciones")
  public List<FuncionesDto> getFunciones() {
    return funciones;
  }

  public void setFunciones(List<FuncionesDto> funciones) {
    this.funciones = funciones;
  }

  public FinalidadesDto funciones(List<FuncionesDto> funciones) {
    this.funciones = funciones;
    return this;
  }

  public FinalidadesDto addFuncionesItem(FuncionesDto funcionesItem) {
    this.funciones.add(funcionesItem);
    return this;
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FinalidadesDto {\n");
    
    sb.append("    FINALIDAD: ").append(toIndentedString(FINALIDAD)).append("\n");
    sb.append("    DESC_FINALIDAD: ").append(toIndentedString(DESC_FINALIDAD)).append("\n");
    sb.append("    VIGENTE: ").append(toIndentedString(VIGENTE)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  
  public FinalidadesDto() {
	initPROPERTY_MAP();
  }
  
  public FinalidadesDto(long fINALIDAD, String dESC_FINALIDAD, String vIGENTE, Map<String, String> aMBITOS,
		PaginacionDto pAGINACION, List<FuncionesDto> funciones) {
	super();
	initPROPERTY_MAP();
	FINALIDAD = fINALIDAD;
	DESC_FINALIDAD = dESC_FINALIDAD;
	VIGENTE = vIGENTE;
	AMBITOS = aMBITOS;
	PAGINACION = pAGINACION;

}
  
  

public FinalidadesDto(long fINALIDAD, String dESC_FINALIDAD, String vIGENTE) {
	super();
	FINALIDAD = fINALIDAD;
	DESC_FINALIDAD = dESC_FINALIDAD;
	VIGENTE = vIGENTE;
}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("FINALIDAD", "finalidad");
		super.PROPERTY_MAP.put("FUNCION", "descFinalidad");
		super.PROPERTY_MAP.put("SUB_FUNCION", "vigente");
	}
}

