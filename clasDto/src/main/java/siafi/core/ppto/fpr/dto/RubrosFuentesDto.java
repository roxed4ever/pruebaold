package siafi.core.ppto.fpr.dto;


import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Informacion sobre RubrosFuentes (Dto)
 **/

public class RubrosFuentesDto extends AmbitosDto implements Serializable {
  
 
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  
  private long GESTION = 0;
  private long FUENTE = 0;
  private String RUBRO = null;
  private String ADM_CENTRAL = null;
  private String INS_DESC = null;
  private String SEG_SOCIAL = null;
  private String UNIVERSIDADES = null;
  private String GOB_LOCALES = null;
  private String EMP_NACIONALES = null;
  private String EMP_LOCALES = null;
  private String FIN_BANCARIAS = null;
  private String FIN_NO_BANCARIAS = null;
  private String EXCEPCION = null;
  private String VIGENTE = null;
  private Map<String, String> AMBITOS = null;
 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get GESTION
   * @return GESTION
  **/
  @JsonProperty("GESTION")
  public long getGESTION() {
    return GESTION;
  }

  public void setGESTION(long GESTION) {
    this.GESTION = GESTION;
  }

  public RubrosFuentesDto GESTION(long GESTION) {
    this.GESTION = GESTION;
    return this;
  }

 /**
   * Get FUENTE
   * @return FUENTE
  **/
  @JsonProperty("FUENTE")
  public long getFUENTE() {
    return FUENTE;
  }

  public void setFUENTE(long FUENTE) {
    this.FUENTE = FUENTE;
  }

  public RubrosFuentesDto FUENTE(long FUENTE) {
    this.FUENTE = FUENTE;
    return this;
  }

 /**
   * Get RUBRO
   * @return RUBRO
  **/
  @JsonProperty("RUBRO")
  public String getRUBRO() {
    return RUBRO;
  }

  public void setRUBRO(String RUBRO) {
    this.RUBRO = RUBRO;
  }

  public RubrosFuentesDto RUBRO(String RUBRO) {
    this.RUBRO = RUBRO;
    return this;
  }

 /**
   * Get ADM_CENTRAL
   * @return ADM_CENTRAL
  **/
  @JsonProperty("ADM_CENTRAL")
  public String getADMCENTRAL() {
    return ADM_CENTRAL;
  }

  public void setADMCENTRAL(String ADM_CENTRAL) {
    this.ADM_CENTRAL = ADM_CENTRAL;
  }

  public RubrosFuentesDto ADM_CENTRAL(String ADM_CENTRAL) {
    this.ADM_CENTRAL = ADM_CENTRAL;
    return this;
  }

 /**
   * Get INS_DESC
   * @return INS_DESC
  **/
  @JsonProperty("INS_DESC")
  public String getINSDESC() {
    return INS_DESC;
  }

  public void setINSDESC(String INS_DESC) {
    this.INS_DESC = INS_DESC;
  }

  public RubrosFuentesDto INS_DESC(String INS_DESC) {
    this.INS_DESC = INS_DESC;
    return this;
  }

 /**
   * Get SEG_SOCIAL
   * @return SEG_SOCIAL
  **/
  @JsonProperty("SEG_SOCIAL")
  public String getSEGSOCIAL() {
    return SEG_SOCIAL;
  }

  public void setSEGSOCIAL(String SEG_SOCIAL) {
    this.SEG_SOCIAL = SEG_SOCIAL;
  }

  public RubrosFuentesDto SEG_SOCIAL(String SEG_SOCIAL) {
    this.SEG_SOCIAL = SEG_SOCIAL;
    return this;
  }

 /**
   * Get UNIVERSIDADES
   * @return UNIVERSIDADES
  **/
  @JsonProperty("UNIVERSIDADES")
  public String getUNIVERSIDADES() {
    return UNIVERSIDADES;
  }

  public void setUNIVERSIDADES(String UNIVERSIDADES) {
    this.UNIVERSIDADES = UNIVERSIDADES;
  }

  public RubrosFuentesDto UNIVERSIDADES(String UNIVERSIDADES) {
    this.UNIVERSIDADES = UNIVERSIDADES;
    return this;
  }

 /**
   * Get GOB_LOCALES
   * @return GOB_LOCALES
  **/
  @JsonProperty("GOB_LOCALES")
  public String getGOBLOCALES() {
    return GOB_LOCALES;
  }

  public void setGOBLOCALES(String GOB_LOCALES) {
    this.GOB_LOCALES = GOB_LOCALES;
  }

  public RubrosFuentesDto GOB_LOCALES(String GOB_LOCALES) {
    this.GOB_LOCALES = GOB_LOCALES;
    return this;
  }

 /**
   * Get EMP_NACIONALES
   * @return EMP_NACIONALES
  **/
  @JsonProperty("EMP_NACIONALES")
  public String getEMPNACIONALES() {
    return EMP_NACIONALES;
  }

  public void setEMPNACIONALES(String EMP_NACIONALES) {
    this.EMP_NACIONALES = EMP_NACIONALES;
  }

  public RubrosFuentesDto EMP_NACIONALES(String EMP_NACIONALES) {
    this.EMP_NACIONALES = EMP_NACIONALES;
    return this;
  }

 /**
   * Get EMP_LOCALES
   * @return EMP_LOCALES
  **/
  @JsonProperty("EMP_LOCALES")
  public String getEMPLOCALES() {
    return EMP_LOCALES;
  }

  public void setEMPLOCALES(String EMP_LOCALES) {
    this.EMP_LOCALES = EMP_LOCALES;
  }

  public RubrosFuentesDto EMP_LOCALES(String EMP_LOCALES) {
    this.EMP_LOCALES = EMP_LOCALES;
    return this;
  }

 /**
   * Get FIN_BANCARIAS
   * @return FIN_BANCARIAS
  **/
  @JsonProperty("FIN_BANCARIAS")
  public String getFINBANCARIAS() {
    return FIN_BANCARIAS;
  }

  public void setFINBANCARIAS(String FIN_BANCARIAS) {
    this.FIN_BANCARIAS = FIN_BANCARIAS;
  }

  public RubrosFuentesDto FIN_BANCARIAS(String FIN_BANCARIAS) {
    this.FIN_BANCARIAS = FIN_BANCARIAS;
    return this;
  }

 /**
   * Get FIN_NO_BANCARIAS
   * @return FIN_NO_BANCARIAS
  **/
  @JsonProperty("FIN_NO_BANCARIAS")
  public String getFINNOBANCARIAS() {
    return FIN_NO_BANCARIAS;
  }

  public void setFINNOBANCARIAS(String FIN_NO_BANCARIAS) {
    this.FIN_NO_BANCARIAS = FIN_NO_BANCARIAS;
  }

  public RubrosFuentesDto FIN_NO_BANCARIAS(String FIN_NO_BANCARIAS) {
    this.FIN_NO_BANCARIAS = FIN_NO_BANCARIAS;
    return this;
  }

 /**
   * Get EXCEPCION
   * @return EXCEPCION
  **/
  @JsonProperty("EXCEPCION")
  public String getEXCEPCION() {
    return EXCEPCION;
  }

  public void setEXCEPCION(String EXCEPCION) {
    this.EXCEPCION = EXCEPCION;
  }

  public RubrosFuentesDto EXCEPCION(String EXCEPCION) {
    this.EXCEPCION = EXCEPCION;
    return this;
  }

 /**
   * Get VIGENTE
   * @return VIGENTE
  **/
  @JsonProperty("VIGENTE")
  public String getVIGENTE() {
    return VIGENTE;
  }

  public void setVIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
  }

  public RubrosFuentesDto VIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public RubrosFuentesDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public RubrosFuentesDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }


  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public RubrosFuentesDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RubrosFuentesDto {\n");
    
    sb.append("    GESTION: ").append(toIndentedString(GESTION)).append("\n");
    sb.append("    FUENTE: ").append(toIndentedString(FUENTE)).append("\n");
    sb.append("    RUBRO: ").append(toIndentedString(RUBRO)).append("\n");
    sb.append("    ADM_CENTRAL: ").append(toIndentedString(ADM_CENTRAL)).append("\n");
    sb.append("    INS_DESC: ").append(toIndentedString(INS_DESC)).append("\n");
    sb.append("    SEG_SOCIAL: ").append(toIndentedString(SEG_SOCIAL)).append("\n");
    sb.append("    UNIVERSIDADES: ").append(toIndentedString(UNIVERSIDADES)).append("\n");
    sb.append("    GOB_LOCALES: ").append(toIndentedString(GOB_LOCALES)).append("\n");
    sb.append("    EMP_NACIONALES: ").append(toIndentedString(EMP_NACIONALES)).append("\n");
    sb.append("    EMP_LOCALES: ").append(toIndentedString(EMP_LOCALES)).append("\n");
    sb.append("    FIN_BANCARIAS: ").append(toIndentedString(FIN_BANCARIAS)).append("\n");
    sb.append("    FIN_NO_BANCARIAS: ").append(toIndentedString(FIN_NO_BANCARIAS)).append("\n");
    sb.append("    EXCEPCION: ").append(toIndentedString(EXCEPCION)).append("\n");
    sb.append("    VIGENTE: ").append(toIndentedString(VIGENTE)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  public RubrosFuentesDto() {
		initPROPERTY_MAP();
  }
  
  public RubrosFuentesDto(long gESTION, long fUENTE, String rUBRO, String aDM_CENTRAL, String iNS_DESC, String sEG_SOCIAL,
		String uNIVERSIDADES, String gOB_LOCALES, String eMP_NACIONALES, String eMP_LOCALES, String fIN_BANCARIAS,
		String fIN_NO_BANCARIAS, String eXCEPCION, String vIGENTE, Map<String, String> aMBITOS,
		PaginacionDto pAGINACION) {
	super();
	GESTION = gESTION;
	FUENTE = fUENTE;
	RUBRO = rUBRO;
	ADM_CENTRAL = aDM_CENTRAL;
	INS_DESC = iNS_DESC;
	SEG_SOCIAL = sEG_SOCIAL;
	UNIVERSIDADES = uNIVERSIDADES;
	GOB_LOCALES = gOB_LOCALES;
	EMP_NACIONALES = eMP_NACIONALES;
	EMP_LOCALES = eMP_LOCALES;
	FIN_BANCARIAS = fIN_BANCARIAS;
	FIN_NO_BANCARIAS = fIN_NO_BANCARIAS;
	EXCEPCION = eXCEPCION;
	VIGENTE = vIGENTE;
	AMBITOS = aMBITOS;
	PAGINACION = pAGINACION;
	initPROPERTY_MAP();
}
  

/**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");
		super.PROPERTY_MAP.put("FUENTE", "id.fuente");
		super.PROPERTY_MAP.put("RUBRO", "id.rubro");		
		super.PROPERTY_MAP.put("ADM_CENTRAL", "admCentral");
		super.PROPERTY_MAP.put("INS_DESC", "insDesc");
		super.PROPERTY_MAP.put("SEG_SOCIAL", "segSocial");
		super.PROPERTY_MAP.put("UNIVERSIDADES", "universidades");
		super.PROPERTY_MAP.put("GOB_LOCALES", "gobLocales");
		super.PROPERTY_MAP.put("EMP_NACIONALES", "empNacionales");
		super.PROPERTY_MAP.put("EMP_LOCALES", "empLocales");		
		super.PROPERTY_MAP.put("FIN_BANCARIAS", "finBancarias");
		super.PROPERTY_MAP.put("FIN_NO_BANCARIAS", "finNoBancarias");
		super.PROPERTY_MAP.put("EXCEPCION", "excepcion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
	}
}

