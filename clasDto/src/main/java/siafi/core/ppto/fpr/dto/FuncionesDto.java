package siafi.core.ppto.fpr.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.io.Serializable;
import java.util.HashMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;

/**
  * Funciones referentes a una finalidad
 **/

public class FuncionesDto extends AmbitosDto implements Serializable  {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String FINALIDAD = null;
  private String FUNCION = null;
  private String CODIGO_FUNCION = null;
  private String SUB_FUNCION = null;
  private String DESC_FUNCION = null;
  private String VIGENTE = null;
  private String IMPUTABLE = null;
 /**
   * Get FINALIDAD
   * @return FINALIDAD
  **/
  @JsonProperty("FINALIDAD")
  public String getFINALIDAD() {
    return FINALIDAD;
  }

  public void setFINALIDAD(String FINALIDAD) {
    this.FINALIDAD = FINALIDAD;
  }

  public FuncionesDto FINALIDAD(String FINALIDAD) {
    this.FINALIDAD = FINALIDAD;
    return this;
  }

 /**
   * Get FUNCION
   * @return FUNCION
  **/
  @JsonProperty("FUNCION")
  public String getFUNCION() {
    return FUNCION;
  }

  public void setFUNCION(String FUNCION) {
    this.FUNCION = FUNCION;
  }

  public FuncionesDto FUNCION(String FUNCION) {
    this.FUNCION = FUNCION;
    return this;
  }

 /**
   * Get CODIGO_FUNCION
   * @return CODIGO_FUNCION
  **/
  @JsonProperty("CODIGO_FUNCION")
  public String getCODIGOFUNCION() {
    return CODIGO_FUNCION;
  }

  public void setCODIGOFUNCION(String CODIGO_FUNCION) {
    this.CODIGO_FUNCION = CODIGO_FUNCION;
  }

  public FuncionesDto CODIGO_FUNCION(String CODIGO_FUNCION) {
    this.CODIGO_FUNCION = CODIGO_FUNCION;
    return this;
  }

 /**
   * Get SUB_FUNCION
   * @return SUB_FUNCION
  **/
  @JsonProperty("SUB_FUNCION")
  public String getSUBFUNCION() {
    return SUB_FUNCION;
  }

  public void setSUBFUNCION(String SUB_FUNCION) {
    this.SUB_FUNCION = SUB_FUNCION;
  }

  public FuncionesDto SUB_FUNCION(String SUB_FUNCION) {
    this.SUB_FUNCION = SUB_FUNCION;
    return this;
  }

 /**
   * Get DESC_FUNCION
   * @return DESC_FUNCION
  **/
  @JsonProperty("DESC_FUNCION")
  public String getDESCFUNCION() {
    return DESC_FUNCION;
  }

  public void setDESCFUNCION(String DESC_FUNCION) {
    this.DESC_FUNCION = DESC_FUNCION;
  }

  public FuncionesDto DESC_FUNCION(String DESC_FUNCION) {
    this.DESC_FUNCION = DESC_FUNCION;
    return this;
  }

 /**
   * Get VIGENTE
   * @return VIGENTE
  **/
  @JsonProperty("VIGENTE")
  public String getVIGENTE() {
    return VIGENTE;
  }

  public void setVIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
  }

  public FuncionesDto VIGENTE(String VIGENTE) {
    this.VIGENTE = VIGENTE;
    return this;
  }

 /**
   * Get IMPUTABLE
   * @return IMPUTABLE
  **/
  @JsonProperty("IMPUTABLE")
  public String getIMPUTABLE() {
    return IMPUTABLE;
  }

  public void setIMPUTABLE(String IMPUTABLE) {
    this.IMPUTABLE = IMPUTABLE;
  }

  public FuncionesDto IMPUTABLE(String IMPUTABLE) {
    this.IMPUTABLE = IMPUTABLE;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FuncionesDto {\n");
    
    sb.append("    FINALIDAD: ").append(toIndentedString(FINALIDAD)).append("\n");
    sb.append("    FUNCION: ").append(toIndentedString(FUNCION)).append("\n");
    sb.append("    CODIGO_FUNCION: ").append(toIndentedString(CODIGO_FUNCION)).append("\n");
    sb.append("    SUB_FUNCION: ").append(toIndentedString(SUB_FUNCION)).append("\n");
    sb.append("    DESC_FUNCION: ").append(toIndentedString(DESC_FUNCION)).append("\n");
    sb.append("    VIGENTE: ").append(toIndentedString(VIGENTE)).append("\n");
    sb.append("    IMPUTABLE: ").append(toIndentedString(IMPUTABLE)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public FuncionesDto() {
	initPROPERTY_MAP();
  }
  
  
  
  public FuncionesDto(String fINALIDAD, String fUNCION, String cODIGO_FUNCION, String sUB_FUNCION, String dESC_FUNCION,
		String vIGENTE, String iMPUTABLE) {
	super();
	initPROPERTY_MAP();
	FINALIDAD = fINALIDAD;
	FUNCION = fUNCION;
	CODIGO_FUNCION = cODIGO_FUNCION;
	SUB_FUNCION = sUB_FUNCION;
	DESC_FUNCION = dESC_FUNCION;
	VIGENTE = vIGENTE;
	IMPUTABLE = iMPUTABLE;
}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("FINALIDAD", "finalidad");
		super.PROPERTY_MAP.put("FUNCION", "id.funcion");
		super.PROPERTY_MAP.put("SUB_FUNCION", "sub_funcion");
		super.PROPERTY_MAP.put("DESC_FUNCION", "descFuncion");
		super.PROPERTY_MAP.put("VIGENTE", "vigente");
		super.PROPERTY_MAP.put("IMPUTABLE", "imputable");
	}
  
}

