package siafi.core.ppto.fpr.dto;


/**
  * Dto para Paginacion.
 **/

public class PaginacionDto  {
  

  private Integer PAGINA_ACTUAL = null;
  private Boolean PAGINA_ANTERIOR = null;
  private Boolean PAGINA_SIGUIENTE = null;
  private Integer TOTAL_PAGINAS = null;
  private Integer TOTAL_REGISTROS = null;
  private Integer TOTAL_REGISTROS_POR_PAGINA = null;
 /**
   * Get PAGINA_ACTUAL
   * @return PAGINA_ACTUAL
  **/

  public Integer getPAGINAACTUAL() {
    return PAGINA_ACTUAL;
  }

  public void setPAGINAACTUAL(Integer PAGINA_ACTUAL) {
    this.PAGINA_ACTUAL = PAGINA_ACTUAL;
  }

  public PaginacionDto PAGINA_ACTUAL(Integer PAGINA_ACTUAL) {
    this.PAGINA_ACTUAL = PAGINA_ACTUAL;
    return this;
  }

 /**
   * Get PAGINA_ANTERIOR
   * @return PAGINA_ANTERIOR
  **/

  public Boolean isPAGINAANTERIOR() {
    return PAGINA_ANTERIOR;
  }

  public void setPAGINAANTERIOR(Boolean PAGINA_ANTERIOR) {
    this.PAGINA_ANTERIOR = PAGINA_ANTERIOR;
  }

  public PaginacionDto PAGINA_ANTERIOR(Boolean PAGINA_ANTERIOR) {
    this.PAGINA_ANTERIOR = PAGINA_ANTERIOR;
    return this;
  }

 /**
   * Get PAGINA_SIGUIENTE
   * @return PAGINA_SIGUIENTE
  **/

  public Boolean isPAGINASIGUIENTE() {
    return PAGINA_SIGUIENTE;
  }

  public void setPAGINASIGUIENTE(Boolean PAGINA_SIGUIENTE) {
    this.PAGINA_SIGUIENTE = PAGINA_SIGUIENTE;
  }

  public PaginacionDto PAGINA_SIGUIENTE(Boolean PAGINA_SIGUIENTE) {
    this.PAGINA_SIGUIENTE = PAGINA_SIGUIENTE;
    return this;
  }

 /**
   * Get TOTAL_PAGINAS
   * @return TOTAL_PAGINAS
  **/

  public Integer getTOTALPAGINAS() {
    return TOTAL_PAGINAS;
  }

  public void setTOTALPAGINAS(Integer TOTAL_PAGINAS) {
    this.TOTAL_PAGINAS = TOTAL_PAGINAS;
  }

  public PaginacionDto TOTAL_PAGINAS(Integer TOTAL_PAGINAS) {
    this.TOTAL_PAGINAS = TOTAL_PAGINAS;
    return this;
  }

 /**
   * Get TOTAL_REGISTROS
   * @return TOTAL_REGISTROS
  **/

  public Integer getTOTALREGISTROS() {
    return TOTAL_REGISTROS;
  }

  public void setTOTALREGISTROS(Integer TOTAL_REGISTROS) {
    this.TOTAL_REGISTROS = TOTAL_REGISTROS;
  }

  public PaginacionDto TOTAL_REGISTROS(Integer TOTAL_REGISTROS) {
    this.TOTAL_REGISTROS = TOTAL_REGISTROS;
    return this;
  }

 /**
   * Get TOTAL_REGISTROS_POR_PAGINA
   * @return TOTAL_REGISTROS_POR_PAGINA
  **/
  public Integer getTOTALREGISTROSPORPAGINA() {
    return TOTAL_REGISTROS_POR_PAGINA;
  }

  public void setTOTALREGISTROSPORPAGINA(Integer TOTAL_REGISTROS_POR_PAGINA) {
    this.TOTAL_REGISTROS_POR_PAGINA = TOTAL_REGISTROS_POR_PAGINA;
  }

  public PaginacionDto TOTAL_REGISTROS_POR_PAGINA(Integer TOTAL_REGISTROS_POR_PAGINA) {
    this.TOTAL_REGISTROS_POR_PAGINA = TOTAL_REGISTROS_POR_PAGINA;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaginacionDto {\n");
    
    sb.append("    PAGINA_ACTUAL: ").append(toIndentedString(PAGINA_ACTUAL)).append("\n");
    sb.append("    PAGINA_ANTERIOR: ").append(toIndentedString(PAGINA_ANTERIOR)).append("\n");
    sb.append("    PAGINA_SIGUIENTE: ").append(toIndentedString(PAGINA_SIGUIENTE)).append("\n");
    sb.append("    TOTAL_PAGINAS: ").append(toIndentedString(TOTAL_PAGINAS)).append("\n");
    sb.append("    TOTAL_REGISTROS: ").append(toIndentedString(TOTAL_REGISTROS)).append("\n");
    sb.append("    TOTAL_REGISTROS_POR_PAGINA: ").append(toIndentedString(TOTAL_REGISTROS_POR_PAGINA)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

