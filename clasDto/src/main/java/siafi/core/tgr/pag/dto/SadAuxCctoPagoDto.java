package siafi.core.tgr.pag.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Dto para la tabla SAD_AUX_CCTO_PAGO
 **/

public class SadAuxCctoPagoDto extends AmbitosDto implements Serializable  {
  

  /**
	 * 
	 */
	private static final Long serialVersionUID = 1L;
	
  private Long CONCEPTO_PAGO = null;
  private String CONCEPTO_PAGO_DCTE = null;
  private String DESCRIPCION = null;
  private String TIPO_CONCEPTO = null;
  private Long DEDUCCION = null;
  private Long RETENCION = null;
  private Long PATRONAL = null;
  private String BEN_PAIS_ID = null;
  private String BEN_TIPO_ID = null;
  private String BEN_NRO_ID = null;
  private Long BEN_BANCO = null;
  private String BEN_CUENTA = null;
  private BigDecimal TECHO_MONETARIO = null; 
  private Map<String, String> AMBITOS = null;

 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get CONCEPTO_PAGO
   * @return CONCEPTO_PAGO
  **/
  @JsonProperty("CONCEPTO_PAGO")
  public Long getCONCEPTOPAGO() {
    return CONCEPTO_PAGO;
  }

  public void setCONCEPTOPAGO(Long CONCEPTO_PAGO) {
    this.CONCEPTO_PAGO = CONCEPTO_PAGO;
  }

  public SadAuxCctoPagoDto CONCEPTO_PAGO(Long CONCEPTO_PAGO) {
    this.CONCEPTO_PAGO = CONCEPTO_PAGO;
    return this;
  }

 /**
   * Get CONCEPTO_PAGO_DCTE
   * @return CONCEPTO_PAGO_DCTE
  **/
  @JsonProperty("CONCEPTO_PAGO_DCTE")
  public String getCONCEPTOPAGODCTE() {
    return CONCEPTO_PAGO_DCTE;
  }

  public void setCONCEPTOPAGODCTE(String CONCEPTO_PAGO_DCTE) {
    this.CONCEPTO_PAGO_DCTE = CONCEPTO_PAGO_DCTE;
  }

  public SadAuxCctoPagoDto CONCEPTO_PAGO_DCTE(String CONCEPTO_PAGO_DCTE) {
    this.CONCEPTO_PAGO_DCTE = CONCEPTO_PAGO_DCTE;
    return this;
  }

 /**
   * Get DESCRIPCION
   * @return DESCRIPCION
  **/
  @JsonProperty("DESCRIPCION")
  public String getDESCRIPCION() {
    return DESCRIPCION;
  }

  public void setDESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
  }

  public SadAuxCctoPagoDto DESCRIPCION(String DESCRIPCION) {
    this.DESCRIPCION = DESCRIPCION;
    return this;
  }

 /**
   * Get TIPO_CONCEPTO
   * @return TIPO_CONCEPTO
  **/
  @JsonProperty("TIPO_CONCEPTO")
  public String getTIPOCONCEPTO() {
    return TIPO_CONCEPTO;
  }

  public void setTIPOCONCEPTO(String TIPO_CONCEPTO) {
    this.TIPO_CONCEPTO = TIPO_CONCEPTO;
  }

  public SadAuxCctoPagoDto TIPO_CONCEPTO(String TIPO_CONCEPTO) {
    this.TIPO_CONCEPTO = TIPO_CONCEPTO;
    return this;
  }

 /**
   * Get DEDUCCION
   * @return DEDUCCION
  **/
  @JsonProperty("DEDUCCION")
  public Long getDEDUCCION() {
    return DEDUCCION;
  }

  public void setDEDUCCION(Long DEDUCCION) {
    this.DEDUCCION = DEDUCCION;
  }

  public SadAuxCctoPagoDto DEDUCCION(Long DEDUCCION) {
    this.DEDUCCION = DEDUCCION;
    return this;
  }

 /**
   * Get RETENCION
   * @return RETENCION
  **/
  @JsonProperty("RETENCION")
  public Long getRETENCION() {
    return RETENCION;
  }

  public void setRETENCION(Long RETENCION) {
    this.RETENCION = RETENCION;
  }

  public SadAuxCctoPagoDto RETENCION(Long RETENCION) {
    this.RETENCION = RETENCION;
    return this;
  }

 /**
   * Get PATRONAL
   * @return PATRONAL
  **/
  @JsonProperty("PATRONAL")
  public Long getPATRONAL() {
    return PATRONAL;
  }

  public void setPATRONAL(Long PATRONAL) {
    this.PATRONAL = PATRONAL;
  }

  public SadAuxCctoPagoDto PATRONAL(Long PATRONAL) {
    this.PATRONAL = PATRONAL;
    return this;
  }

 /**
   * Get BEN_PAIS_ID
   * @return BEN_PAIS_ID
  **/
  @JsonProperty("BEN_PAIS_ID")
  public String getBENPAISID() {
    return BEN_PAIS_ID;
  }

  public void setBENPAISID(String BEN_PAIS_ID) {
    this.BEN_PAIS_ID = BEN_PAIS_ID;
  }

  public SadAuxCctoPagoDto BEN_PAIS_ID(String BEN_PAIS_ID) {
    this.BEN_PAIS_ID = BEN_PAIS_ID;
    return this;
  }

 /**
   * Get BEN_TIPO_ID
   * @return BEN_TIPO_ID
  **/
  @JsonProperty("BEN_TIPO_ID")
  public String getBENTIPOID() {
    return BEN_TIPO_ID;
  }

  public void setBENTIPOID(String BEN_TIPO_ID) {
    this.BEN_TIPO_ID = BEN_TIPO_ID;
  }

  public SadAuxCctoPagoDto BEN_TIPO_ID(String BEN_TIPO_ID) {
    this.BEN_TIPO_ID = BEN_TIPO_ID;
    return this;
  }

 /**
   * Get BEN_NRO_ID
   * @return BEN_NRO_ID
  **/
  @JsonProperty("BEN_NRO_ID")
  public String getBENNROID() {
    return BEN_NRO_ID;
  }

  public void setBENNROID(String BEN_NRO_ID) {
    this.BEN_NRO_ID = BEN_NRO_ID;
  }

  public SadAuxCctoPagoDto BEN_NRO_ID(String BEN_NRO_ID) {
    this.BEN_NRO_ID = BEN_NRO_ID;
    return this;
  }

 /**
   * Get BEN_BANCO
   * @return BEN_BANCO
  **/
  @JsonProperty("BEN_BANCO")
  public Long getBENBANCO() {
    return BEN_BANCO;
  }

  public void setBENBANCO(Long BEN_BANCO) {
    this.BEN_BANCO = BEN_BANCO;
  }

  public SadAuxCctoPagoDto BEN_BANCO(Long BEN_BANCO) {
    this.BEN_BANCO = BEN_BANCO;
    return this;
  }

 /**
   * Get BEN_CUENTA
   * @return BEN_CUENTA
  **/
  @JsonProperty("BEN_CUENTA")
  public String getBENCUENTA() {
    return BEN_CUENTA;
  }

  public void setBENCUENTA(String BEN_CUENTA) {
    this.BEN_CUENTA = BEN_CUENTA;
  }

  public SadAuxCctoPagoDto BEN_CUENTA(String BEN_CUENTA) {
    this.BEN_CUENTA = BEN_CUENTA;
    return this;
  }

 /**
   * Get TECHO_MONETARIO
   * @return TECHO_MONETARIO
  **/
  @JsonProperty("TECHO_MONETARIO")
  public BigDecimal getTECHOMONETARIO() {
    return TECHO_MONETARIO;
  }

  public void setTECHOMONETARIO(BigDecimal TECHO_MONETARIO) {
    this.TECHO_MONETARIO = TECHO_MONETARIO;
  }

  public SadAuxCctoPagoDto TECHO_MONETARIO(BigDecimal TECHO_MONETARIO) {
    this.TECHO_MONETARIO = TECHO_MONETARIO;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public SadAuxCctoPagoDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public SadAuxCctoPagoDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }

  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public SadAuxCctoPagoDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SadAuxCctoPagoDto {\n");
    
    sb.append("    CONCEPTO_PAGO: ").append(toIndentedString(CONCEPTO_PAGO)).append("\n");
    sb.append("    CONCEPTO_PAGO_DCTE: ").append(toIndentedString(CONCEPTO_PAGO_DCTE)).append("\n");
    sb.append("    DESCRIPCION: ").append(toIndentedString(DESCRIPCION)).append("\n");
    sb.append("    TIPO_CONCEPTO: ").append(toIndentedString(TIPO_CONCEPTO)).append("\n");
    sb.append("    DEDUCCION: ").append(toIndentedString(DEDUCCION)).append("\n");
    sb.append("    RETENCION: ").append(toIndentedString(RETENCION)).append("\n");
    sb.append("    PATRONAL: ").append(toIndentedString(PATRONAL)).append("\n");
    sb.append("    BEN_PAIS_ID: ").append(toIndentedString(BEN_PAIS_ID)).append("\n");
    sb.append("    BEN_TIPO_ID: ").append(toIndentedString(BEN_TIPO_ID)).append("\n");
    sb.append("    BEN_NRO_ID: ").append(toIndentedString(BEN_NRO_ID)).append("\n");
    sb.append("    BEN_BANCO: ").append(toIndentedString(BEN_BANCO)).append("\n");
    sb.append("    BEN_CUENTA: ").append(toIndentedString(BEN_CUENTA)).append("\n");
    sb.append("    TECHO_MONETARIO: ").append(toIndentedString(TECHO_MONETARIO)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
  public SadAuxCctoPagoDto() {
	  initPROPERTY_MAP();
  }
  
  
  public SadAuxCctoPagoDto(Long cONCEPTO_PAGO, String cONCEPTO_PAGO_DCTE, String dESCRIPCION, String tIPO_CONCEPTO,
		  Long dEDUCCION, Long rETENCION, Long pATRONAL, String bEN_PAIS_ID, String bEN_TIPO_ID, String bEN_NRO_ID,
		  Long bEN_BANCO, String bEN_CUENTA, BigDecimal tECHO_MONETARIO) {
	super();
	
	initPROPERTY_MAP();
	CONCEPTO_PAGO = cONCEPTO_PAGO;
	CONCEPTO_PAGO_DCTE = cONCEPTO_PAGO_DCTE;
	DESCRIPCION = dESCRIPCION;
	TIPO_CONCEPTO = tIPO_CONCEPTO;
	DEDUCCION = dEDUCCION;
	RETENCION = rETENCION;
	PATRONAL = pATRONAL;
	BEN_PAIS_ID = bEN_PAIS_ID;
	BEN_TIPO_ID = bEN_TIPO_ID;
	BEN_NRO_ID = bEN_NRO_ID;
	BEN_BANCO = bEN_BANCO;
	BEN_CUENTA = bEN_CUENTA;
	TECHO_MONETARIO = tECHO_MONETARIO;
}

public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("CONCEPTO_PAGO", "conceptoPago");
		super.PROPERTY_MAP.put("CONCEPTO_PAGO_DCTE", "conceptoPagoDcte");
		super.PROPERTY_MAP.put("DESCRIPCION", "descripcion");
		
		super.PROPERTY_MAP.put("TIPO_CONCEPTO", "tipoConcepto");
		super.PROPERTY_MAP.put("DEDUCCION", "deduccion");
		super.PROPERTY_MAP.put("RETENCION", "retencion");
		
		super.PROPERTY_MAP.put("PATRONAL", "patronal");
		super.PROPERTY_MAP.put("BEN_PAIS_ID", "benPaisId");
		super.PROPERTY_MAP.put("BEN_TIPO_ID", "benTipoId");
		
		super.PROPERTY_MAP.put("BEN_NRO_ID", "benNroId");
		super.PROPERTY_MAP.put("BEN_BANCO", "benBanco");
		super.PROPERTY_MAP.put("BEN_CUENTA", "benCuenta");
		super.PROPERTY_MAP.put("TECHO_MONETARIO", "techoMonetario");
	}

}

