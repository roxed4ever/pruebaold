package siafi.ges.nomina.funcionarios.dto;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Paginacion  
 **/

public class PaginacionDto  {
  private String PAGINA_ACTUAL = null;
  private String PAGINA_ANTERIOR = null;
  private String PAGINA_SIGUIENTE = null;
  private String TOTAL_PAGINAS = null;
  private String TOTAL_REGISTROS = null;
  private String TOTAL_REGISTROS_POR_PAGINA = null;
 /**
   * Get PAGINA_ACTUAL
   * @return PAGINA_ACTUAL
  **/
  @JsonProperty("PAGINA_ACTUAL")
  public String getPAGINAACTUAL() {
    return PAGINA_ACTUAL;
  }

  public void setPAGINAACTUAL(String PAGINA_ACTUAL) {
    this.PAGINA_ACTUAL = PAGINA_ACTUAL;
  }

  public PaginacionDto PAGINA_ACTUAL(String PAGINA_ACTUAL) {
    this.PAGINA_ACTUAL = PAGINA_ACTUAL;
    return this;
  }

 /**
   * Get PAGINA_ANTERIOR
   * @return PAGINA_ANTERIOR
  **/
  @JsonProperty("PAGINA_ANTERIOR")
  public String getPAGINAANTERIOR() {
    return PAGINA_ANTERIOR;
  }

  public void setPAGINAANTERIOR(String PAGINA_ANTERIOR) {
    this.PAGINA_ANTERIOR = PAGINA_ANTERIOR;
  }

  public PaginacionDto PAGINA_ANTERIOR(String PAGINA_ANTERIOR) {
    this.PAGINA_ANTERIOR = PAGINA_ANTERIOR;
    return this;
  }

 /**
   * Get PAGINA_SIGUIENTE
   * @return PAGINA_SIGUIENTE
  **/
  @JsonProperty("PAGINA_SIGUIENTE")
  public String getPAGINASIGUIENTE() {
    return PAGINA_SIGUIENTE;
  }

  public void setPAGINASIGUIENTE(String PAGINA_SIGUIENTE) {
    this.PAGINA_SIGUIENTE = PAGINA_SIGUIENTE;
  }

  public PaginacionDto PAGINA_SIGUIENTE(String PAGINA_SIGUIENTE) {
    this.PAGINA_SIGUIENTE = PAGINA_SIGUIENTE;
    return this;
  }

 /**
   * Get TOTAL_PAGINAS
   * @return TOTAL_PAGINAS
  **/
  @JsonProperty("TOTAL_PAGINAS")
  public String getTOTALPAGINAS() {
    return TOTAL_PAGINAS;
  }

  public void setTOTALPAGINAS(String TOTAL_PAGINAS) {
    this.TOTAL_PAGINAS = TOTAL_PAGINAS;
  }

  public PaginacionDto TOTAL_PAGINAS(String TOTAL_PAGINAS) {
    this.TOTAL_PAGINAS = TOTAL_PAGINAS;
    return this;
  }

 /**
   * Get TOTAL_REGISTROS
   * @return TOTAL_REGISTROS
  **/
  @JsonProperty("TOTAL_REGISTROS")
  public String getTOTALREGISTROS() {
    return TOTAL_REGISTROS;
  }

  public void setTOTALREGISTROS(String TOTAL_REGISTROS) {
    this.TOTAL_REGISTROS = TOTAL_REGISTROS;
  }

  public PaginacionDto TOTAL_REGISTROS(String TOTAL_REGISTROS) {
    this.TOTAL_REGISTROS = TOTAL_REGISTROS;
    return this;
  }

 /**
   * Get TOTAL_REGISTROS_POR_PAGINA
   * @return TOTAL_REGISTROS_POR_PAGINA
  **/
  @JsonProperty("TOTAL_REGISTROS_POR_PAGINA")
  public String getTOTALREGISTROSPORPAGINA() {
    return TOTAL_REGISTROS_POR_PAGINA;
  }

  public void setTOTALREGISTROSPORPAGINA(String TOTAL_REGISTROS_POR_PAGINA) {
    this.TOTAL_REGISTROS_POR_PAGINA = TOTAL_REGISTROS_POR_PAGINA;
  }

  public PaginacionDto TOTAL_REGISTROS_POR_PAGINA(String TOTAL_REGISTROS_POR_PAGINA) {
    this.TOTAL_REGISTROS_POR_PAGINA = TOTAL_REGISTROS_POR_PAGINA;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaginacionDto {\n");
    
    sb.append("    PAGINA_ACTUAL: ").append(toIndentedString(PAGINA_ACTUAL)).append("\n");
    sb.append("    PAGINA_ANTERIOR: ").append(toIndentedString(PAGINA_ANTERIOR)).append("\n");
    sb.append("    PAGINA_SIGUIENTE: ").append(toIndentedString(PAGINA_SIGUIENTE)).append("\n");
    sb.append("    TOTAL_PAGINAS: ").append(toIndentedString(TOTAL_PAGINAS)).append("\n");
    sb.append("    TOTAL_REGISTROS: ").append(toIndentedString(TOTAL_REGISTROS)).append("\n");
    sb.append("    TOTAL_REGISTROS_POR_PAGINA: ").append(toIndentedString(TOTAL_REGISTROS_POR_PAGINA)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

