package siafi.ges.nomina.funcionarios.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import fwk.core.clas.dto.AmbitosDto;
import fwk.core.clas.dto.PaginacionDto;

/**
  * Informacion general del empleado, DTO
 **/

public class InformacionEmpleadoDto extends AmbitosDto implements Serializable  {
  private String NOMBRES = null;
  private String APELLIDOS = null;
  private String TIPO_ID = null;
  private String NUMERO_ID = null;
  private Long FUNCIONARIO = null;
  private String NAC_PAIS = null;
  private Long NAC_DEPARTAMENTO = null;
  private Long NAC_MUNICIPIO = null;
  private Long NAC_NACIONALIDAD = null;
  private Date NAC_FECHA = null;
  private String NAC_SEXO = null;
  private Long NAC_GRUPO_SANGUINEO = null;
  private String ESTADO_CIVIL = null;
  private Long BNC_BANCO = null;
  private String BNC_CUENTA = null;
  private String BNC_TIPO_CUENTA = null;
  private String DIRECCION = null;
  private String TELEFONO = null;
  private String CELULAR = null;
  private Long DECLARACION_JURADA = null;
  private String CORREO_PERSONAL = null;
  private String CORREO_TRABAJO = null;
  private Map<String, String> AMBITOS = null;

 /**
   *   
  **/
  private PaginacionDto PAGINACION = null;
 /**
   * Get NOMBRES
   * @return NOMBRES
  **/
  @JsonProperty("NOMBRES")
  public String getNOMBRES() {
    return NOMBRES;
  }

  public void setNOMBRES(String NOMBRES) {
    this.NOMBRES = NOMBRES;
  }

  public InformacionEmpleadoDto NOMBRES(String NOMBRES) {
    this.NOMBRES = NOMBRES;
    return this;
  }

 /**
   * Get APELLIDOS
   * @return APELLIDOS
  **/
  @JsonProperty("APELLIDOS")
  public String getAPELLIDOS() {
    return APELLIDOS;
  }

  public void setAPELLIDOS(String APELLIDOS) {
    this.APELLIDOS = APELLIDOS;
  }

  public InformacionEmpleadoDto APELLIDOS(String APELLIDOS) {
    this.APELLIDOS = APELLIDOS;
    return this;
  }

 /**
   * Get TIPO_ID
   * @return TIPO_ID
  **/
  @JsonProperty("TIPO_ID")
  public String getTIPOID() {
    return TIPO_ID;
  }

  public void setTIPOID(String TIPO_ID) {
    this.TIPO_ID = TIPO_ID;
  }

  public InformacionEmpleadoDto TIPO_ID(String TIPO_ID) {
    this.TIPO_ID = TIPO_ID;
    return this;
  }

 /**
   * Get NUMERO_ID
   * @return NUMERO_ID
  **/
  @JsonProperty("NUMERO_ID")
  public String getNUMEROID() {
    return NUMERO_ID;
  }

  public void setNUMEROID(String NUMERO_ID) {
    this.NUMERO_ID = NUMERO_ID;
  }

  public InformacionEmpleadoDto NUMERO_ID(String NUMERO_ID) {
    this.NUMERO_ID = NUMERO_ID;
    return this;
  }

 /**
   * Get FUNCIONARIO
   * @return FUNCIONARIO
  **/
  @JsonProperty("FUNCIONARIO")
  public Long getFUNCIONARIO() {
    return FUNCIONARIO;
  }

  public void setFUNCIONARIO(Long FUNCIONARIO) {
    this.FUNCIONARIO = FUNCIONARIO;
  }

  public InformacionEmpleadoDto FUNCIONARIO(Long FUNCIONARIO) {
    this.FUNCIONARIO = FUNCIONARIO;
    return this;
  }

 /**
   * Get NAC_PAIS
   * @return NAC_PAIS
  **/
  @JsonProperty("NAC_PAIS")
  public String getNACPAIS() {
    return NAC_PAIS;
  }

  public void setNACPAIS(String NAC_PAIS) {
    this.NAC_PAIS = NAC_PAIS;
  }

  public InformacionEmpleadoDto NAC_PAIS(String NAC_PAIS) {
    this.NAC_PAIS = NAC_PAIS;
    return this;
  }

 /**
   * Get NAC_DEPARTAMENTO
   * @return NAC_DEPARTAMENTO
  **/
  @JsonProperty("NAC_DEPARTAMENTO")
  public Long getNACDEPARTAMENTO() {
    return NAC_DEPARTAMENTO;
  }

  public void setNACDEPARTAMENTO(Long NAC_DEPARTAMENTO) {
    this.NAC_DEPARTAMENTO = NAC_DEPARTAMENTO;
  }

  public InformacionEmpleadoDto NAC_DEPARTAMENTO(Long NAC_DEPARTAMENTO) {
    this.NAC_DEPARTAMENTO = NAC_DEPARTAMENTO;
    return this;
  }

 /**
   * Get NAC_MUNICIPIO
   * @return NAC_MUNICIPIO
  **/
  @JsonProperty("NAC_MUNICIPIO")
  public Long getNACMUNICIPIO() {
    return NAC_MUNICIPIO;
  }

  public void setNACMUNICIPIO(Long NAC_MUNICIPIO) {
    this.NAC_MUNICIPIO = NAC_MUNICIPIO;
  }

  public InformacionEmpleadoDto NAC_MUNICIPIO(Long NAC_MUNICIPIO) {
    this.NAC_MUNICIPIO = NAC_MUNICIPIO;
    return this;
  }

 /**
   * Get NAC_NACIONALIDAD
   * @return NAC_NACIONALIDAD
  **/
  @JsonProperty("NAC_NACIONALIDAD")
  public Long getNACNACIONALIDAD() {
    return NAC_NACIONALIDAD;
  }

  public void setNACNACIONALIDAD(Long NAC_NACIONALIDAD) {
    this.NAC_NACIONALIDAD = NAC_NACIONALIDAD;
  }

  public InformacionEmpleadoDto NAC_NACIONALIDAD(Long NAC_NACIONALIDAD) {
    this.NAC_NACIONALIDAD = NAC_NACIONALIDAD;
    return this;
  }

 /**
   * Get NAC_FECHA
   * @return NAC_FECHA
  **/
  @JsonProperty("NAC_FECHA")
  public Date getNACFECHA() {
    return NAC_FECHA;
  }

  public void setNACFECHA(Date NAC_FECHA) {
    this.NAC_FECHA = NAC_FECHA;
  }

  public InformacionEmpleadoDto NAC_FECHA(Date NAC_FECHA) {
    this.NAC_FECHA = NAC_FECHA;
    return this;
  }

 /**
   * Get NAC_SEXO
   * @return NAC_SEXO
  **/
  @JsonProperty("NAC_SEXO")
  public String getNACSEXO() {
    return NAC_SEXO;
  }

  public void setNACSEXO(String NAC_SEXO) {
    this.NAC_SEXO = NAC_SEXO;
  }

  public InformacionEmpleadoDto NAC_SEXO(String NAC_SEXO) {
    this.NAC_SEXO = NAC_SEXO;
    return this;
  }

 /**
   * Get NAC_GRUPO_SANGUINEO
   * @return NAC_GRUPO_SANGUINEO
  **/
  @JsonProperty("NAC_GRUPO_SANGUINEO")
  public Long getNACGRUPOSANGUINEO() {
    return NAC_GRUPO_SANGUINEO;
  }

  public void setNACGRUPOSANGUINEO(Long NAC_GRUPO_SANGUINEO) {
    this.NAC_GRUPO_SANGUINEO = NAC_GRUPO_SANGUINEO;
  }

  public InformacionEmpleadoDto NAC_GRUPO_SANGUINEO(Long NAC_GRUPO_SANGUINEO) {
    this.NAC_GRUPO_SANGUINEO = NAC_GRUPO_SANGUINEO;
    return this;
  }

 /**
   * Get ESTADO_CIVIL
   * @return ESTADO_CIVIL
  **/
  @JsonProperty("ESTADO_CIVIL")
  public String getESTADOCIVIL() {
    return ESTADO_CIVIL;
  }

  public void setESTADOCIVIL(String ESTADO_CIVIL) {
    this.ESTADO_CIVIL = ESTADO_CIVIL;
  }

  public InformacionEmpleadoDto ESTADO_CIVIL(String ESTADO_CIVIL) {
    this.ESTADO_CIVIL = ESTADO_CIVIL;
    return this;
  }

 /**
   * Get BNC_BANCO
   * @return BNC_BANCO
  **/
  @JsonProperty("BNC_BANCO")
  public Long getBNCBANCO() {
    return BNC_BANCO;
  }

  public void setBNCBANCO(Long BNC_BANCO) {
    this.BNC_BANCO = BNC_BANCO;
  }

  public InformacionEmpleadoDto BNC_BANCO(Long BNC_BANCO) {
    this.BNC_BANCO = BNC_BANCO;
    return this;
  }

 /**
   * Get BNC_CUENTA
   * @return BNC_CUENTA
  **/
  @JsonProperty("BNC_CUENTA")
  public String getBNCCUENTA() {
    return BNC_CUENTA;
  }

  public void setBNCCUENTA(String BNC_CUENTA) {
    this.BNC_CUENTA = BNC_CUENTA;
  }

  public InformacionEmpleadoDto BNC_CUENTA(String BNC_CUENTA) {
    this.BNC_CUENTA = BNC_CUENTA;
    return this;
  }

 /**
   * Get BNC_TIPO_CUENTA
   * @return BNC_TIPO_CUENTA
  **/
  @JsonProperty("BNC_TIPO_CUENTA")
  public String getBNCTIPOCUENTA() {
    return BNC_TIPO_CUENTA;
  }

  public void setBNCTIPOCUENTA(String BNC_TIPO_CUENTA) {
    this.BNC_TIPO_CUENTA = BNC_TIPO_CUENTA;
  }

  public InformacionEmpleadoDto BNC_TIPO_CUENTA(String BNC_TIPO_CUENTA) {
    this.BNC_TIPO_CUENTA = BNC_TIPO_CUENTA;
    return this;
  }



 /**
   * Get DIRECCION
   * @return DIRECCION
  **/
  @JsonProperty("DIRECCION")
  public String getDireccion() {
    return DIRECCION;
  }

  public void setDireccion(String DIRECCION) {
    this.DIRECCION = DIRECCION;
  }

  public InformacionEmpleadoDto DIRECCION(String DIRECCION) {
    this.DIRECCION = DIRECCION;
    return this;
  }


 /**
   * Get TELEFONO
   * @return TELEFONO
  **/
  @JsonProperty("TELEFONO")
  public String getTELEFONO() {
    return TELEFONO;
  }

  public void setTELEFONO(String TELEFONO) {
    this.TELEFONO = TELEFONO;
  }

  public InformacionEmpleadoDto TELEFONO(String TELEFONO) {
    this.TELEFONO = TELEFONO;
    return this;
  }

 /**
   * Get CELULAR
   * @return CELULAR
  **/
  @JsonProperty("CELULAR")
  public String getCELULAR() {
    return CELULAR;
  }

  public void setCELULAR(String CELULAR) {
    this.CELULAR = CELULAR;
  }

  public InformacionEmpleadoDto CELULAR(String CELULAR) {
    this.CELULAR = CELULAR;
    return this;
  }

 /**
   * Get DECLARACION_JURADA
   * @return DECLARACION_JURADA
  **/
  @JsonProperty("DECLARACION_JURADA")
  public Long getDECLARACIONJURADA() {
    return DECLARACION_JURADA;
  }

  public void setDECLARACIONJURADA(Long DECLARACION_JURADA) {
    this.DECLARACION_JURADA = DECLARACION_JURADA;
  }

  public InformacionEmpleadoDto DECLARACION_JURADA(Long DECLARACION_JURADA) {
    this.DECLARACION_JURADA = DECLARACION_JURADA;
    return this;
  }

 /**
   * Get CORREO_PERSONAL
   * @return CORREO_PERSONAL
  **/
  @JsonProperty("CORREO_PERSONAL")
  public String getCORREOPERSONAL() {
    return CORREO_PERSONAL;
  }

  public void setCORREOPERSONAL(String CORREO_PERSONAL) {
    this.CORREO_PERSONAL = CORREO_PERSONAL;
  }

  public InformacionEmpleadoDto CORREO_PERSONAL(String CORREO_PERSONAL) {
    this.CORREO_PERSONAL = CORREO_PERSONAL;
    return this;
  }

 /**
   * Get CORREO_TRABAJO
   * @return CORREO_TRABAJO
  **/
  @JsonProperty("CORREO_TRABAJO")
  public String getCORREOTRABAJO() {
    return CORREO_TRABAJO;
  }

  public void setCORREOTRABAJO(String CORREO_TRABAJO) {
    this.CORREO_TRABAJO = CORREO_TRABAJO;
  }

  public InformacionEmpleadoDto CORREO_TRABAJO(String CORREO_TRABAJO) {
    this.CORREO_TRABAJO = CORREO_TRABAJO;
    return this;
  }

 /**
   * Get AMBITOS
   * @return AMBITOS
  **/
  @JsonProperty("AMBITOS")
  public Map<String, String> getAMBITOS() {
    return AMBITOS;
  }

  public void setAMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
  }

  public InformacionEmpleadoDto AMBITOS(Map<String, String> AMBITOS) {
    this.AMBITOS = AMBITOS;
    return this;
  }

  public InformacionEmpleadoDto putAMBITOSItem(String key, String AMBITOSItem) {
    this.AMBITOS.put(key, AMBITOSItem);
    return this;
  }

 /**
   * 
   * @return PAGINACION
  **/
  @JsonProperty("PAGINACION")
  public PaginacionDto getPAGINACION() {
	    return PAGINACION;
	  }


  public void setPAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
  }

  public InformacionEmpleadoDto PAGINACION(PaginacionDto PAGINACION) {
    this.PAGINACION = PAGINACION;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InformacionEmpleadoDto {\n");
    
    sb.append("    NOMBRES: ").append(toIndentedString(NOMBRES)).append("\n");
    sb.append("    APELLIDOS: ").append(toIndentedString(APELLIDOS)).append("\n");
    sb.append("    TIPO_ID: ").append(toIndentedString(TIPO_ID)).append("\n");
    sb.append("    NUMERO_ID: ").append(toIndentedString(NUMERO_ID)).append("\n");
    sb.append("    FUNCIONARIO: ").append(toIndentedString(FUNCIONARIO)).append("\n");
    sb.append("    NAC_PAIS: ").append(toIndentedString(NAC_PAIS)).append("\n");
    sb.append("    NAC_DEPARTAMENTO: ").append(toIndentedString(NAC_DEPARTAMENTO)).append("\n");
    sb.append("    NAC_MUNICIPIO: ").append(toIndentedString(NAC_MUNICIPIO)).append("\n");
    sb.append("    NAC_NACIONALIDAD: ").append(toIndentedString(NAC_NACIONALIDAD)).append("\n");
    sb.append("    NAC_FECHA: ").append(toIndentedString(NAC_FECHA)).append("\n");
    sb.append("    NAC_SEXO: ").append(toIndentedString(NAC_SEXO)).append("\n");
    sb.append("    NAC_GRUPO_SANGUINEO: ").append(toIndentedString(NAC_GRUPO_SANGUINEO)).append("\n");
    sb.append("    ESTADO_CIVIL: ").append(toIndentedString(ESTADO_CIVIL)).append("\n");
    sb.append("    BNC_BANCO: ").append(toIndentedString(BNC_BANCO)).append("\n");
    sb.append("    BNC_CUENTA: ").append(toIndentedString(BNC_CUENTA)).append("\n");
    sb.append("    BNC_TIPO_CUENTA: ").append(toIndentedString(BNC_TIPO_CUENTA)).append("\n");
    sb.append("    DIRECCION: ").append(toIndentedString(DIRECCION)).append("\n");
    sb.append("    TELEFONO: ").append(toIndentedString(TELEFONO)).append("\n");
    sb.append("    CELULAR: ").append(toIndentedString(CELULAR)).append("\n");
    sb.append("    DECLARACION_JURADA: ").append(toIndentedString(DECLARACION_JURADA)).append("\n");
    sb.append("    CORREO_PERSONAL: ").append(toIndentedString(CORREO_PERSONAL)).append("\n");
    sb.append("    CORREO_TRABAJO: ").append(toIndentedString(CORREO_TRABAJO)).append("\n");
    sb.append("    AMBITOS: ").append(toIndentedString(AMBITOS)).append("\n");
    sb.append("    PAGINACION: ").append(toIndentedString(PAGINACION)).append("\n");
    sb.append("}");
    return sb.toString();
  }
  
  public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("NOMBRES", "nombres");
		super.PROPERTY_MAP.put("APELLIDOS", "apellidos");
		super.PROPERTY_MAP.put("TIPO_ID", "tipoId");
		super.PROPERTY_MAP.put("NUMERO_ID", "numeroId");			
		super.PROPERTY_MAP.put("FUNCIONARIO", "funcionario");
		
		super.PROPERTY_MAP.put("NAC_PAIS", "nacPais");
		super.PROPERTY_MAP.put("NAC_DEPARTAMENTO", "nacDepartamento");
		super.PROPERTY_MAP.put("NAC_MUNICIPIO", "nacMunicipio");
		super.PROPERTY_MAP.put("NAC_NACIONALIDAD", "nacNacionalidad");
		super.PROPERTY_MAP.put("NAC_FECHA", "nacFecha");
		
		super.PROPERTY_MAP.put("NAC_SEXO", "nacSexo");
		super.PROPERTY_MAP.put("NAC_GRUPO_SANGUINEO", "nacGrupoSanguineo");
		
		super.PROPERTY_MAP.put("ESTADO_CIVIL", "estadoCivil");
		super.PROPERTY_MAP.put("BNC_BANCO", "bncBanco");
		super.PROPERTY_MAP.put("BNC_CUENTA", "bncCuenta");
		super.PROPERTY_MAP.put("BNC_TIPO_CUENTA", "bcnTipoCuenta");
		super.PROPERTY_MAP.put("DIRECCION", "dirBarrio"+" "+"dir_avenida_calle"+" "+"dir_edificio"+" "+"dir_numero"+" "+"dir_lugar");
		
		super.PROPERTY_MAP.put("TELEFONO", "telefono");
		super.PROPERTY_MAP.put("CELULAR", "celular");
		super.PROPERTY_MAP.put("DECLARACION_JURADA", "declaracionJurada");
		super.PROPERTY_MAP.put("CORREO_PERSONAL", "correoPersonal");
		super.PROPERTY_MAP.put("CORREO_TRABAJO", "correoTrabajo");
	}

  public InformacionEmpleadoDto() {
	  initPROPERTY_MAP(); 
  }
  
  public InformacionEmpleadoDto(Long fUNCIONARIO,
		String nAC_PAIS, Long nAC_DEPARTAMENTO, Long nAC_MUNICIPIO, Long nAC_NACIONALIDAD, Date nAC_FECHA,
		String nAC_SEXO, Long nAC_GRUPO_SANGUINEO, String eSTADO_CIVIL, Long bNC_BANCO, String bNC_CUENTA,
		String bNC_TIPO_CUENTA, String dIRECCION, String tELEFONO, String cELULAR, Long dECLARACION_JURADA,
		String cORREO_PERSONAL, String cORREO_TRABAJO) {
	super();
	initPROPERTY_MAP(); 
	FUNCIONARIO = fUNCIONARIO;
	NAC_PAIS = nAC_PAIS;
	NAC_DEPARTAMENTO = nAC_DEPARTAMENTO;
	NAC_MUNICIPIO = nAC_MUNICIPIO;
	NAC_NACIONALIDAD = nAC_NACIONALIDAD;
	NAC_FECHA = nAC_FECHA;
	NAC_SEXO = nAC_SEXO;
	NAC_GRUPO_SANGUINEO = nAC_GRUPO_SANGUINEO;
	ESTADO_CIVIL = eSTADO_CIVIL;
	BNC_BANCO = bNC_BANCO;
	BNC_CUENTA = bNC_CUENTA;
	BNC_TIPO_CUENTA = bNC_TIPO_CUENTA;
	DIRECCION = dIRECCION;
	TELEFONO = tELEFONO;
	CELULAR = cELULAR;
	DECLARACION_JURADA = dECLARACION_JURADA;
	CORREO_PERSONAL = cORREO_PERSONAL;
	CORREO_TRABAJO = cORREO_TRABAJO;
}
  
public InformacionEmpleadoDto(String nOMBRES, String aPELLIDOS, String tIPO_ID, String nUMERO_ID, Long fUNCIONARIO,
		String nAC_PAIS, Long nAC_DEPARTAMENTO, Long nAC_MUNICIPIO, Long nAC_NACIONALIDAD, Date nAC_FECHA,
		String nAC_SEXO, Long nAC_GRUPO_SANGUINEO, String eSTADO_CIVIL, Long bNC_BANCO, String bNC_CUENTA,
		String bNC_TIPO_CUENTA, String dIRECCION, String tELEFONO, String cORREO_PERSONAL,
		String cORREO_TRABAJO) {
	super();
	initPROPERTY_MAP(); 
	NOMBRES = nOMBRES;
	APELLIDOS = aPELLIDOS;
	TIPO_ID = tIPO_ID;
	NUMERO_ID = nUMERO_ID;
	FUNCIONARIO = fUNCIONARIO;
	NAC_PAIS = nAC_PAIS;
	NAC_DEPARTAMENTO = nAC_DEPARTAMENTO;
	NAC_MUNICIPIO = nAC_MUNICIPIO;
	NAC_NACIONALIDAD = nAC_NACIONALIDAD;
	NAC_FECHA = nAC_FECHA;
	NAC_SEXO = nAC_SEXO;
	NAC_GRUPO_SANGUINEO = nAC_GRUPO_SANGUINEO;
	ESTADO_CIVIL = eSTADO_CIVIL;
	BNC_BANCO = bNC_BANCO;
	BNC_CUENTA = bNC_CUENTA;
	BNC_TIPO_CUENTA = bNC_TIPO_CUENTA;
	DIRECCION = dIRECCION;
	TELEFONO = tELEFONO;
	CORREO_PERSONAL = cORREO_PERSONAL;
	CORREO_TRABAJO = cORREO_TRABAJO;
}



public InformacionEmpleadoDto(String nOMBRES, String aPELLIDOS, String tIPO_ID, String nUMERO_ID, Long fUNCIONARIO,
		String nAC_PAIS, Long nAC_DEPARTAMENTO, Long nAC_MUNICIPIO, Long nAC_NACIONALIDAD, Date nAC_FECHA,
		String nAC_SEXO, Long nAC_GRUPO_SANGUINEO, String dIRECCION, String tELEFONO, String cELULAR) {
	super();
	NOMBRES = nOMBRES;
	APELLIDOS = aPELLIDOS;
	TIPO_ID = tIPO_ID;
	NUMERO_ID = nUMERO_ID;
	FUNCIONARIO = fUNCIONARIO;
	NAC_PAIS = nAC_PAIS;
	NAC_DEPARTAMENTO = nAC_DEPARTAMENTO;
	NAC_MUNICIPIO = nAC_MUNICIPIO;
	NAC_NACIONALIDAD = nAC_NACIONALIDAD;
	NAC_FECHA = nAC_FECHA;
	NAC_SEXO = nAC_SEXO;
	NAC_GRUPO_SANGUINEO = nAC_GRUPO_SANGUINEO;
	DIRECCION = dIRECCION;
	TELEFONO = tELEFONO;
	CELULAR = cELULAR;
}

/**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

