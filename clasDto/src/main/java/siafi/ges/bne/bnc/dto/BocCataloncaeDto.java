package siafi.ges.bne.bnc.dto;

import java.io.Serializable;
import java.util.*;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-10-25T11:59:52.587-06:00")
public class BocCataloncaeDto extends AmbitosDto implements Serializable {

	private String CATALOGO = null;
	private Integer SEGMENTO = null;
	private Integer FAMILIA = null;
	private Integer CLASE = null;
	private Integer MATERIAL = null;
	private String DESC_CATALOGO = null;
	private String VIGENTE = null;
	private String ESTADO = null;

	public BocCataloncaeDto() {
		// TODO Auto-generated constructor stub
	}

	public BocCataloncaeDto(String cATALOGO, Integer sEGMENTO, Integer fAMILIA, Integer cLASE, Integer mATERIAL,
			String dESC_CATALOGO, String vIGENTE, String eSTADO) {
		super();
		CATALOGO = cATALOGO;
		SEGMENTO = sEGMENTO;
		FAMILIA = fAMILIA;
		CLASE = cLASE;
		MATERIAL = mATERIAL;
		DESC_CATALOGO = dESC_CATALOGO;
		VIGENTE = vIGENTE;
		ESTADO = eSTADO;
	}

	/**
	   * 
	   **/

	@JsonProperty("CATALOGO")
	public String getCATALOGO() {
		return CATALOGO;
	}

	public void setCATALOGO(String CATALOGO) {
		this.CATALOGO = CATALOGO;
	}

	/**
	 * 
	 **/

	@JsonProperty("SEGMENTO")
	public Integer getSEGMENTO() {
		return SEGMENTO;
	}

	public void setSEGMENTO(Integer SEGMENTO) {
		this.SEGMENTO = SEGMENTO;
	}

	/**
	 * 
	 **/

	@JsonProperty("FAMILIA")
	public Integer getFAMILIA() {
		return FAMILIA;
	}

	public void setFAMILIA(Integer FAMILIA) {
		this.FAMILIA = FAMILIA;
	}

	/**
	 * 
	 **/

	@JsonProperty("CLASE")
	public Integer getCLASE() {
		return CLASE;
	}

	public void setCLASE(Integer CLASE) {
		this.CLASE = CLASE;
	}

	/**
	 * 
	 **/

	@JsonProperty("MATERIAL")
	public Integer getMATERIAL() {
		return MATERIAL;
	}

	public void setMATERIAL(Integer MATERIAL) {
		this.MATERIAL = MATERIAL;
	}

	/**
	 * 
	 **/

	@JsonProperty("DESC_CATALOGO")
	public String getDESCCATALOGO() {
		return DESC_CATALOGO;
	}

	public void setDESCCATALOGO(String DESC_CATALOGO) {
		this.DESC_CATALOGO = DESC_CATALOGO;
	}

	/**
	 * 
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 * 
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class BocCataloncaeDto {\n");

		sb.append("    CATALOGO: ").append(StringUtil.toIndentedString(CATALOGO)).append("\n");
		sb.append("    SEGMENTO: ").append(StringUtil.toIndentedString(SEGMENTO)).append("\n");
		sb.append("    FAMILIA: ").append(StringUtil.toIndentedString(FAMILIA)).append("\n");
		sb.append("    CLASE: ").append(StringUtil.toIndentedString(CLASE)).append("\n");
		sb.append("    MATERIAL: ").append(StringUtil.toIndentedString(MATERIAL)).append("\n");
		sb.append("    DESC_CATALOGO: ").append(StringUtil.toIndentedString(DESC_CATALOGO)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
