package siafi.ges.bne.bnc.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonProperty;

import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;

/**
 * 
 **/

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-10-25T11:59:52.587-06:00")
public class BocCataloncaeObjetosDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String CATALOGO = null;
	private Double OBJETO_GESTION = null;
	private String OBJETO = null;
	private String ESTADO = null;

	
	public BocCataloncaeObjetosDto() {
		// TODO Auto-generated constructor stub
	}
	
	public BocCataloncaeObjetosDto(String cATALOGO, Double oBJETO_GESTION, String oBJETO, String eSTADO) {
		super();
		CATALOGO = cATALOGO;
		OBJETO_GESTION = oBJETO_GESTION;
		OBJETO = oBJETO;
		ESTADO = eSTADO;
	}

	/**
	 * 
	 **/

	@JsonProperty("CATALOGO")
	public String getCATALOGO() {
		return CATALOGO;
	}

	public void setCATALOGO(String CATALOGO) {
		this.CATALOGO = CATALOGO;
	}

	/**
	 * 
	 **/

	@JsonProperty("OBJETO_GESTION")
	public Double getOBJETOGESTION() {
		return OBJETO_GESTION;
	}

	public void setOBJETOGESTION(Double OBJETO_GESTION) {
		this.OBJETO_GESTION = OBJETO_GESTION;
	}

	/**
	 * 
	 **/

	@JsonProperty("OBJETO")
	public String getOBJETO() {
		return OBJETO;
	}

	public void setOBJETO(String OBJETO) {
		this.OBJETO = OBJETO;
	}

	/**
	 * 
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class BocCataloncaeObjetosDto {\n");

		sb.append("    CATALOGO: ").append(StringUtil.toIndentedString(CATALOGO)).append("\n");
		sb.append("    OBJETO_GESTION: ").append(StringUtil.toIndentedString(OBJETO_GESTION)).append("\n");
		sb.append("    OBJETO: ").append(StringUtil.toIndentedString(OBJETO)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");
		sb.append("}");
		return sb.toString();
	}
}
