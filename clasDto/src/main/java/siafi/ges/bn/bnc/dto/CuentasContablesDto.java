package siafi.ges.bn.bnc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.JsonProperty;
import clas.util.StringUtil;
import fwk.core.clas.dto.AmbitosDto;


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2019-09-10T19:00:07.822-06:00")
public class CuentasContablesDto extends AmbitosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer GESTION = null;
	private String CUENTA_CONTABLE = null;
	private String APROPIABLE = null;
	private String VIGENTE = null;
	private String GASTO = null;
	private String INGRESO = null;
	private String LEGALIZABLE = null;
	private String SIN_IMPUTACION = null;
	private String GLOSA_DEBITA_POR = null;
	private String GLOSA_ACREDITA_POR = null;
	private String GLOSA_SALDO = null;
	private String TIPO_SALDO = null;
	private String NIC_CUENTA_CONTABLE = null;
	private String NIC_ANTECESOR = null;
	private Integer NIC_SECUENCIA = null;
	private String CIERRE = null;
	private String ESTADO = null;

	public CuentasContablesDto() {
		initPROPERTY_MAP();
	}

	public CuentasContablesDto(Integer gESTION, String cUENTA_CONTABLE, String aPROPIABLE, String vIGENTE, String gASTO,
			String iNGRESO, String lEGALIZABLE, String sIN_IMPUTACION, String gLOSA_DEBITA_POR,
			String gLOSA_ACREDITA_POR, String gLOSA_SALDO, String tIPO_SALDO, String nIC_CUENTA_CONTABLE,
			String nIC_ANTECESOR, Integer nIC_SECUENCIA, String cIERRE, String eSTADO) {
		super();
		initPROPERTY_MAP();
		GESTION = gESTION;
		CUENTA_CONTABLE = cUENTA_CONTABLE;
		APROPIABLE = aPROPIABLE;
		VIGENTE = vIGENTE;
		GASTO = gASTO;
		INGRESO = iNGRESO;
		LEGALIZABLE = lEGALIZABLE;
		SIN_IMPUTACION = sIN_IMPUTACION;
		GLOSA_DEBITA_POR = gLOSA_DEBITA_POR;
		GLOSA_ACREDITA_POR = gLOSA_ACREDITA_POR;
		GLOSA_SALDO = gLOSA_SALDO;
		TIPO_SALDO = tIPO_SALDO;
		NIC_CUENTA_CONTABLE = nIC_CUENTA_CONTABLE;
		NIC_ANTECESOR = nIC_ANTECESOR;
		NIC_SECUENCIA = nIC_SECUENCIA;
		CIERRE = cIERRE;
		ESTADO = eSTADO;
	}

	/**
	   **/

	@JsonProperty("GESTION")
	public Integer getGESTION() {
		return GESTION;
	}

	public void setGESTION(Integer GESTION) {
		this.GESTION = GESTION;
	}

	/**
	 **/

	@JsonProperty("CUENTA_CONTABLE")
	public String getCUENTACONTABLE() {
		return CUENTA_CONTABLE;
	}

	public void setCUENTACONTABLE(String CUENTA_CONTABLE) {
		this.CUENTA_CONTABLE = CUENTA_CONTABLE;
	}

	/**
	 **/

	@JsonProperty("APROPIABLE")
	public String getAPROPIABLE() {
		return APROPIABLE;
	}

	public void setAPROPIABLE(String APROPIABLE) {
		this.APROPIABLE = APROPIABLE;
	}

	/**
	 **/

	@JsonProperty("VIGENTE")
	public String getVIGENTE() {
		return VIGENTE;
	}

	public void setVIGENTE(String VIGENTE) {
		this.VIGENTE = VIGENTE;
	}

	/**
	 **/

	@JsonProperty("GASTO")
	public String getGASTO() {
		return GASTO;
	}

	public void setGASTO(String GASTO) {
		this.GASTO = GASTO;
	}

	/**
	 **/

	@JsonProperty("INGRESO")
	public String getINGRESO() {
		return INGRESO;
	}

	public void setINGRESO(String INGRESO) {
		this.INGRESO = INGRESO;
	}

	/**
	 **/

	@JsonProperty("LEGALIZABLE")
	public String getLEGALIZABLE() {
		return LEGALIZABLE;
	}

	public void setLEGALIZABLE(String LEGALIZABLE) {
		this.LEGALIZABLE = LEGALIZABLE;
	}

	/**
	 **/

	@JsonProperty("SIN_IMPUTACION")
	public String getSINIMPUTACION() {
		return SIN_IMPUTACION;
	}

	public void setSINIMPUTACION(String SIN_IMPUTACION) {
		this.SIN_IMPUTACION = SIN_IMPUTACION;
	}

	/**
	 **/

	@JsonProperty("GLOSA_DEBITA_POR")
	public String getGLOSADEBITAPOR() {
		return GLOSA_DEBITA_POR;
	}

	public void setGLOSADEBITAPOR(String GLOSA_DEBITA_POR) {
		this.GLOSA_DEBITA_POR = GLOSA_DEBITA_POR;
	}

	/**
	 **/

	@JsonProperty("GLOSA_ACREDITA_POR")
	public String getGLOSAACREDITAPOR() {
		return GLOSA_ACREDITA_POR;
	}

	public void setGLOSAACREDITAPOR(String GLOSA_ACREDITA_POR) {
		this.GLOSA_ACREDITA_POR = GLOSA_ACREDITA_POR;
	}

	/**
	 **/

	@JsonProperty("GLOSA_SALDO")
	public String getGLOSASALDO() {
		return GLOSA_SALDO;
	}

	public void setGLOSASALDO(String GLOSA_SALDO) {
		this.GLOSA_SALDO = GLOSA_SALDO;
	}

	/**
	 **/

	@JsonProperty("TIPO_SALDO")
	public String getTIPOSALDO() {
		return TIPO_SALDO;
	}

	public void setTIPOSALDO(String TIPO_SALDO) {
		this.TIPO_SALDO = TIPO_SALDO;
	}

	/**
	 **/

	@JsonProperty("NIC_CUENTA_CONTABLE")
	public String getNICCUENTACONTABLE() {
		return NIC_CUENTA_CONTABLE;
	}

	public void setNICCUENTACONTABLE(String NIC_CUENTA_CONTABLE) {
		this.NIC_CUENTA_CONTABLE = NIC_CUENTA_CONTABLE;
	}

	/**
	 **/

	@JsonProperty("NIC_ANTECESOR")
	public String getNICANTECESOR() {
		return NIC_ANTECESOR;
	}

	public void setNICANTECESOR(String NIC_ANTECESOR) {
		this.NIC_ANTECESOR = NIC_ANTECESOR;
	}

	/**
	 **/

	@JsonProperty("NIC_SECUENCIA")
	public Integer getNICSECUENCIA() {
		return NIC_SECUENCIA;
	}

	public void setNICSECUENCIA(Integer NIC_SECUENCIA) {
		this.NIC_SECUENCIA = NIC_SECUENCIA;
	}

	/**
	 **/

	@JsonProperty("CIERRE")
	public String getCIERRE() {
		return CIERRE;
	}

	public void setCIERRE(String CIERRE) {
		this.CIERRE = CIERRE;
	}

	/**
	 **/

	@JsonProperty("ESTADO")
	public String getESTADO() {
		return ESTADO;
	}

	public void setESTADO(String ESTADO) {
		this.ESTADO = ESTADO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class CuentasContablesDto {\n");

		sb.append("    GESTION: ").append(StringUtil.toIndentedString(GESTION)).append("\n");
		sb.append("    CUENTA_CONTABLE: ").append(StringUtil.toIndentedString(CUENTA_CONTABLE)).append("\n");
		sb.append("    APROPIABLE: ").append(StringUtil.toIndentedString(APROPIABLE)).append("\n");
		sb.append("    VIGENTE: ").append(StringUtil.toIndentedString(VIGENTE)).append("\n");
		sb.append("    GASTO: ").append(StringUtil.toIndentedString(GASTO)).append("\n");
		sb.append("    INGRESO: ").append(StringUtil.toIndentedString(INGRESO)).append("\n");
		sb.append("    LEGALIZABLE: ").append(StringUtil.toIndentedString(LEGALIZABLE)).append("\n");
		sb.append("    SIN_IMPUTACION: ").append(StringUtil.toIndentedString(SIN_IMPUTACION)).append("\n");
		sb.append("    GLOSA_DEBITA_POR: ").append(StringUtil.toIndentedString(GLOSA_DEBITA_POR)).append("\n");
		sb.append("    GLOSA_ACREDITA_POR: ").append(StringUtil.toIndentedString(GLOSA_ACREDITA_POR)).append("\n");
		sb.append("    GLOSA_SALDO: ").append(StringUtil.toIndentedString(GLOSA_SALDO)).append("\n");
		sb.append("    TIPO_SALDO: ").append(StringUtil.toIndentedString(TIPO_SALDO)).append("\n");
		sb.append("    NIC_CUENTA_CONTABLE: ").append(StringUtil.toIndentedString(NIC_CUENTA_CONTABLE)).append("\n");
		sb.append("    NIC_ANTECESOR: ").append(StringUtil.toIndentedString(NIC_ANTECESOR)).append("\n");
		sb.append("    NIC_SECUENCIA: ").append(StringUtil.toIndentedString(NIC_SECUENCIA)).append("\n");
		sb.append("    CIERRE: ").append(StringUtil.toIndentedString(CIERRE)).append("\n");
		sb.append("    ESTADO: ").append(StringUtil.toIndentedString(ESTADO)).append("\n");

		sb.append("}");
		return sb.toString();
	}

	
	public void initPROPERTY_MAP() {
		super.PROPERTY_MAP = new HashMap<String, String>();
		super.PROPERTY_MAP.put("GESTION", "id.gestion");//
		super.PROPERTY_MAP.put("CUENTA_CONTABLE", "id.cuentaContable");//
		super.PROPERTY_MAP.put("APROPIABLE", "apropiable");//
		super.PROPERTY_MAP.put("VIGENTE", "vigente");//
		super.PROPERTY_MAP.put("GASTO", "gasto");//
		super.PROPERTY_MAP.put("INGRESO", "ingreso");//
		super.PROPERTY_MAP.put("LEGALIZABLE", "legalizable");//
		super.PROPERTY_MAP.put("SIN_IMPUTACION", "sinImputacion");//
		super.PROPERTY_MAP.put("GLOSA_DEBITA_POR", "glosaDebitaPor");//
		super.PROPERTY_MAP.put("GLOSA_ACREDITA_POR", "glosaAcreditaPor");//
		super.PROPERTY_MAP.put("GLOSA_SALDO", "glosaSaldo");////
		super.PROPERTY_MAP.put("TIPO_SALDO", "tipoSaldo");//
		super.PROPERTY_MAP.put("NIC_CUENTA_CONTABLE", "nicCuentaContable");
		super.PROPERTY_MAP.put("NIC_ANTECESOR", "nicAntecesor");
		super.PROPERTY_MAP.put("NIC_SECUENCIA", "nicSecuencia");
		super.PROPERTY_MAP.put("CIERRE", "cierre");
		super.PROPERTY_MAP.put("ESTADO", "apiEstado");

	}
}
