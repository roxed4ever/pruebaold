package siafi.ges.seg.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;

/**
  * Fecha contexto  
 **/

public class FechaContextoDto implements Serializable {
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private Date fechaContexto = null;
 /**
   * Get fechaContexto
   * @return fechaContexto
  **/
  @JsonProperty("fechaContexto")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  public Date getFechaContexto() {
    return fechaContexto;
  }

  public void setFechaContexto(Date fechaContexto) {
    this.fechaContexto = fechaContexto;
  }

  public FechaContextoDto fechaContexto(Date fechaContexto) {
    this.fechaContexto = fechaContexto;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FechaContextoDto {\n");
    
    sb.append("    fechaContexto: ").append(toIndentedString(fechaContexto)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

