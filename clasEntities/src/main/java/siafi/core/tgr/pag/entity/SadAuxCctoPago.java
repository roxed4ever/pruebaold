package siafi.core.tgr.pag.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the SAD_AUX_CCTO_PAGO database table.
 * 
 */
@Entity
@Table(name="SAD_AUX_CCTO_PAGO", schema="SIAFI")
@NamedQuery(name="SadAuxCctoPago.findAll", query="SELECT s FROM SadAuxCctoPago s")
public class SadAuxCctoPago implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@Column(name="CONCEPTO_PAGO")
	private Long conceptoPago;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="BEN_BANCO")
	private Long benBanco;

	@Column(name="BEN_CUENTA")
	private String benCuenta;

	@Column(name="BEN_NRO_ID")
	private String benNroId;

	@Column(name="BEN_PAIS_ID")
	private String benPaisId;

	@Column(name="BEN_TIPO_ID")
	private String benTipoId;

	@Column(name="CONCEPTO_PAGO_DCTE")
	private String conceptoPagoDcte;

	private Long deduccion;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private Long patronal;

	private Long retencion;

	@Column(name="TECHO_MONETARIO")
	private BigDecimal techoMonetario;

	@Column(name="TIPO_CONCEPTO")
	private String tipoConcepto;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public SadAuxCctoPago() {
	}

	public Long getConceptoPago() {
		return this.conceptoPago;
	}

	public void setConceptoPago(Long conceptoPago) {
		this.conceptoPago = conceptoPago;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Long getBenBanco() {
		return this.benBanco;
	}

	public void setBenBanco(Long benBanco) {
		this.benBanco = benBanco;
	}

	public String getBenCuenta() {
		return this.benCuenta;
	}

	public void setBenCuenta(String benCuenta) {
		this.benCuenta = benCuenta;
	}

	public String getBenNroId() {
		return this.benNroId;
	}

	public void setBenNroId(String benNroId) {
		this.benNroId = benNroId;
	}

	public String getBenPaisId() {
		return this.benPaisId;
	}

	public void setBenPaisId(String benPaisId) {
		this.benPaisId = benPaisId;
	}

	public String getBenTipoId() {
		return this.benTipoId;
	}

	public void setBenTipoId(String benTipoId) {
		this.benTipoId = benTipoId;
	}

	public String getConceptoPagoDcte() {
		return this.conceptoPagoDcte;
	}

	public void setConceptoPagoDcte(String conceptoPagoDcte) {
		this.conceptoPagoDcte = conceptoPagoDcte;
	}

	public Long getDeduccion() {
		return this.deduccion;
	}

	public void setDeduccion(Long deduccion) {
		this.deduccion = deduccion;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Long getPatronal() {
		return this.patronal;
	}

	public void setPatronal(Long patronal) {
		this.patronal = patronal;
	}

	public Long getRetencion() {
		return this.retencion;
	}

	public void setRetencion(Long retencion) {
		this.retencion = retencion;
	}

	public BigDecimal getTechoMonetario() {
		return this.techoMonetario;
	}

	public void setTechoMonetario(BigDecimal techoMonetario) {
		this.techoMonetario = techoMonetario;
	}

	public String getTipoConcepto() {
		return this.tipoConcepto;
	}

	public void setTipoConcepto(String tipoConcepto) {
		this.tipoConcepto = tipoConcepto;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}