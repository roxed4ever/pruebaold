package siafi.core.fpr.cg;

import java.io.Serializable;
import javax.persistence.*;



/**
 * The persistent class for the CG_ENTIDAD database table.
 * 
 */
@Entity
@Table(name="CG_ENTIDAD", schema="SAMI")
@NamedQuery(name="CgEntidad.findAll", query="SELECT c FROM CgEntidad c")
public class CgEntidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CgEntidadPK id;

	@Column(name="CODIGO_INSTITUCION")
	private String codigoInstitucion;

	@Column(name="CODIGO_UE")
	private Long codigoUe;

	private String cuentadancia;

	private String descripcion;

	private String geografico;

	@Column(name="ID_DEPARTAMENTO")
	private Long idDepartamento;

	@Column(name="ID_MUNICIPIO")
	private Long idMunicipio;

	@Column(name="IDENT_TRIBUTARIA")
	private String identTributaria;

	private String restrictiva;

	private String sector;

	public CgEntidad() {
	}

	public CgEntidadPK getId() {
		return this.id;
	}

	public void setId(CgEntidadPK id) {
		this.id = id;
	}

	public String getCodigoInstitucion() {
		return this.codigoInstitucion;
	}

	public void setCodigoInstitucion(String codigoInstitucion) {
		this.codigoInstitucion = codigoInstitucion;
	}

	public Long getCodigoUe() {
		return this.codigoUe;
	}

	public void setCodigoUe(Long codigoUe) {
		this.codigoUe = codigoUe;
	}

	public String getCuentadancia() {
		return this.cuentadancia;
	}

	public void setCuentadancia(String cuentadancia) {
		this.cuentadancia = cuentadancia;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getGeografico() {
		return this.geografico;
	}

	public void setGeografico(String geografico) {
		this.geografico = geografico;
	}

	public Long getIdDepartamento() {
		return this.idDepartamento;
	}

	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public Long getIdMunicipio() {
		return this.idMunicipio;
	}

	public void setIdMunicipio(Long idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getIdentTributaria() {
		return this.identTributaria;
	}

	public void setIdentTributaria(String identTributaria) {
		this.identTributaria = identTributaria;
	}

	public String getRestrictiva() {
		return this.restrictiva;
	}

	public void setRestrictiva(String restrictiva) {
		this.restrictiva = restrictiva;
	}

	public String getSector() {
		return this.sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

}