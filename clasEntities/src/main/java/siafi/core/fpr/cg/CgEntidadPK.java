package siafi.core.fpr.cg;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CG_ENTIDAD database table.
 * 
 */
@Embeddable
public class CgEntidadPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_ENTIDAD")
	private long idEntidad;

	@Column(name="ID_UNIDAD_EJECUTORA")
	private long idUnidadEjecutora;

	public CgEntidadPK() {
	}
	public long getIdEntidad() {
		return this.idEntidad;
	}
	public void setIdEntidad(long idEntidad) {
		this.idEntidad = idEntidad;
	}
	public long getIdUnidadEjecutora() {
		return this.idUnidadEjecutora;
	}
	public void setIdUnidadEjecutora(long idUnidadEjecutora) {
		this.idUnidadEjecutora = idUnidadEjecutora;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CgEntidadPK)) {
			return false;
		}
		CgEntidadPK castOther = (CgEntidadPK)other;
		return 
			(this.idEntidad == castOther.idEntidad)
			&& (this.idUnidadEjecutora == castOther.idUnidadEjecutora);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idEntidad ^ (this.idEntidad >>> 32)));
		hash = hash * prime + ((int) (this.idUnidadEjecutora ^ (this.idUnidadEjecutora >>> 32)));
		
		return hash;
	}
}