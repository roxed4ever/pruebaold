package siafi.core.tes.cuc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CUOTAS_DE_COMPROMISO database table.
 * 
 */
@Entity
@Table(name="CUOTAS_DE_COMPROMISO")
@NamedQuery(name="CuotasDeCompromiso.findAll", query="SELECT c FROM CuotasDeCompromiso c")
public class CuotasDeCompromiso implements Serializable {
	private static final long serialVersionUID = 1L;
	private CuotasDeCompromisoPK id;
	private Double ajustesPorDisminuciones;
	private String apiEstado;
	private String apiTransaccion;
	private Double cuotaAsignada;
	private Double cuotaEjecutada;
	private Double cuotaRedistribuida;
	private Double cuotaReservada;
	private Double cuotaSolicitada;
	private Double ejecucionRevertida;
	private Date fecCre;
	private Date fecMod;
	private String usuCre;
	private String usuMod;

	public CuotasDeCompromiso() {
	}


	@EmbeddedId
	public CuotasDeCompromisoPK getId() {
		return this.id;
	}

	public void setId(CuotasDeCompromisoPK id) {
		this.id = id;
	}


	@Column(name="AJUSTES_POR_DISMINUCIONES")
	public Double getAjustesPorDisminuciones() {
		return this.ajustesPorDisminuciones;
	}

	public void setAjustesPorDisminuciones(Double ajustesPorDisminuciones) {
		this.ajustesPorDisminuciones = ajustesPorDisminuciones;
	}


	@Column(name="API_ESTADO")
	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}


	@Column(name="API_TRANSACCION")
	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}


	@Column(name="CUOTA_ASIGNADA")
	public Double getCuotaAsignada() {
		return this.cuotaAsignada;
	}

	public void setCuotaAsignada(Double cuotaAsignada) {
		this.cuotaAsignada = cuotaAsignada;
	}


	@Column(name="CUOTA_EJECUTADA")
	public Double getCuotaEjecutada() {
		return this.cuotaEjecutada;
	}

	public void setCuotaEjecutada(Double cuotaEjecutada) {
		this.cuotaEjecutada = cuotaEjecutada;
	}


	@Column(name="CUOTA_REDISTRIBUIDA")
	public Double getCuotaRedistribuida() {
		return this.cuotaRedistribuida;
	}

	public void setCuotaRedistribuida(Double cuotaRedistribuida) {
		this.cuotaRedistribuida = cuotaRedistribuida;
	}


	@Column(name="CUOTA_RESERVADA")
	public Double getCuotaReservada() {
		return this.cuotaReservada;
	}

	public void setCuotaReservada(Double cuotaReservada) {
		this.cuotaReservada = cuotaReservada;
	}


	@Column(name="CUOTA_SOLICITADA")
	public Double getCuotaSolicitada() {
		return this.cuotaSolicitada;
	}

	public void setCuotaSolicitada(Double cuotaSolicitada) {
		this.cuotaSolicitada = cuotaSolicitada;
	}


	@Column(name="EJECUCION_REVERTIDA")
	public Double getEjecucionRevertida() {
		return this.ejecucionRevertida;
	}

	public void setEjecucionRevertida(Double ejecucionRevertida) {
		this.ejecucionRevertida = ejecucionRevertida;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}


	@Column(name="USU_CRE")
	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}


	@Column(name="USU_MOD")
	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}