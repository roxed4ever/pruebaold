package siafi.core.tes.cuc.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-12T18:18:44.366-0600")
@StaticMetamodel(CuotasDeCompromisoPK.class)
public class CuotasDeCompromisoPK_ {
	public static volatile SingularAttribute<CuotasDeCompromisoPK, Long> gestion;
	public static volatile SingularAttribute<CuotasDeCompromisoPK, Long> institucion;
	public static volatile SingularAttribute<CuotasDeCompromisoPK, Long> ga;
	public static volatile SingularAttribute<CuotasDeCompromisoPK, Long> ue;
	public static volatile SingularAttribute<CuotasDeCompromisoPK, Long> fuente;
	public static volatile SingularAttribute<CuotasDeCompromisoPK, Long> claseDeGasto;
	public static volatile SingularAttribute<CuotasDeCompromisoPK, Long> trimestre;
}
