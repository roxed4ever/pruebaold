package siafi.core.tes.cuc.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CUOTAS_DE_COMPROMISO database table.
 * 
 */
@Embeddable
public class CuotasDeCompromisoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private long gestion;
	private long institucion;
	private long ga;
	private long ue;
	private long trimestre;
	private long fuente;
	private long claseDeGasto;

	public CuotasDeCompromisoPK() {
	}

	@Column(insertable=false, updatable=false)
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}

	@Column(insertable=false, updatable=false)
	public long getInstitucion() {
		return this.institucion;
	}
	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}

	@Column(insertable=false, updatable=false)
	public long getGa() {
		return this.ga;
	}
	public void setGa(long ga) {
		this.ga = ga;
	}

	@Column(insertable=false, updatable=false)
	public long getUe() {
		return this.ue;
	}
	public void setUe(long ue) {
		this.ue = ue;
	}

	public long getTrimestre() {
		return this.trimestre;
	}
	public void setTrimestre(long trimestre) {
		this.trimestre = trimestre;
	}

	@Column(insertable=false, updatable=false)
	public long getFuente() {
		return this.fuente;
	}
	public void setFuente(long fuente) {
		this.fuente = fuente;
	}

	@Column(name="CLASE_DE_GASTO", insertable=false, updatable=false)
	public long getClaseDeGasto() {
		return this.claseDeGasto;
	}
	public void setClaseDeGasto(long claseDeGasto) {
		this.claseDeGasto = claseDeGasto;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CuotasDeCompromisoPK)) {
			return false;
		}
		CuotasDeCompromisoPK castOther = (CuotasDeCompromisoPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.institucion == castOther.institucion)
			&& (this.ga == castOther.ga)
			&& (this.ue == castOther.ue)
			&& (this.trimestre == castOther.trimestre)
			&& (this.fuente == castOther.fuente)
			&& (this.claseDeGasto == castOther.claseDeGasto);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.institucion ^ (this.institucion >>> 32)));
		hash = hash * prime + ((int) (this.ga ^ (this.ga >>> 32)));
		hash = hash * prime + ((int) (this.ue ^ (this.ue >>> 32)));
		hash = hash * prime + ((int) (this.trimestre ^ (this.trimestre >>> 32)));
		hash = hash * prime + ((int) (this.fuente ^ (this.fuente >>> 32)));
		hash = hash * prime + ((int) (this.claseDeGasto ^ (this.claseDeGasto >>> 32)));
		
		return hash;
	}
}