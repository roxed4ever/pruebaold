package siafi.core.tes.cuc.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-12T18:18:44.366-0600")
@StaticMetamodel(CuotasDeCompromiso.class)
public class CuotasDeCompromiso_ {
	public static volatile SingularAttribute<CuotasDeCompromiso, CuotasDeCompromisoPK> id;
	public static volatile SingularAttribute<CuotasDeCompromiso, Double> ajustesPorDisminuciones;
	public static volatile SingularAttribute<CuotasDeCompromiso, String> apiEstado;
	public static volatile SingularAttribute<CuotasDeCompromiso, String> apiTransaccion;
	public static volatile SingularAttribute<CuotasDeCompromiso, Double> cuotaAsignada;
	public static volatile SingularAttribute<CuotasDeCompromiso, Double> cuotaEjecutada;
	public static volatile SingularAttribute<CuotasDeCompromiso, Double> cuotaRedistribuida;
	public static volatile SingularAttribute<CuotasDeCompromiso, Double> cuotaReservada;
	public static volatile SingularAttribute<CuotasDeCompromiso, Double> cuotaSolicitada;
	public static volatile SingularAttribute<CuotasDeCompromiso, Double> ejecucionRevertida;
	public static volatile SingularAttribute<CuotasDeCompromiso, Date> fecCre;
	public static volatile SingularAttribute<CuotasDeCompromiso, Date> fecMod;
	public static volatile SingularAttribute<CuotasDeCompromiso, String> usuCre;
	public static volatile SingularAttribute<CuotasDeCompromiso, String> usuMod;
}
