package siafi.core.ppto.fpr.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the RUBROS_FUENTES database table.
 * 
 */
@Embeddable
public class RubrosFuentePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long gestion;

	@Column(insertable=false, updatable=false)
	private long fuente;

	@Column(insertable=false, updatable=false)
	private String rubro;

	public RubrosFuentePK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getFuente() {
		return this.fuente;
	}
	public void setFuente(long fuente) {
		this.fuente = fuente;
	}
	public String getRubro() {
		return this.rubro;
	}
	public void setRubro(String rubro) {
		this.rubro = rubro;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RubrosFuentePK)) {
			return false;
		}
		RubrosFuentePK castOther = (RubrosFuentePK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.fuente == castOther.fuente)
			&& this.rubro.equals(castOther.rubro);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.fuente ^ (this.fuente >>> 32)));
		hash = hash * prime + this.rubro.hashCode();
		
		return hash;
	}
}