package siafi.core.ppto.fpr.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the FUNCIONES database table.
 * 
 */
@Entity
@Table(name="FUNCIONES", schema="SIAFI")
@NamedQueries({
	@NamedQuery(name="Fuentes.findAll", query="SELECT f FROM Fuentes f"),
	@NamedQuery(name="FuentesXFuncionalidad", query="SELECT f FROM Fuentes f WHERE f.id.finalidad=:finalidad")	
})

public class Fuentes implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FuentesPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="CODIGO_FUNCION")
	private BigDecimal codigoFuncion;

	@Column(name="DESC_FUNCION")
	private String descFuncion;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private String imputable;

	@Column(name="SUB_FUNCION")
	private BigDecimal subFuncion;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public Fuentes() {
	}

	public FuentesPK getId() {
		return this.id;
	}

	public void setId(FuentesPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public BigDecimal getCodigoFuncion() {
		return this.codigoFuncion;
	}

	public void setCodigoFuncion(BigDecimal codigoFuncion) {
		this.codigoFuncion = codigoFuncion;
	}

	public String getDescFuncion() {
		return this.descFuncion;
	}

	public void setDescFuncion(String descFuncion) {
		this.descFuncion = descFuncion;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getImputable() {
		return this.imputable;
	}

	public void setImputable(String imputable) {
		this.imputable = imputable;
	}

	public BigDecimal getSubFuncion() {
		return this.subFuncion;
	}

	public void setSubFuncion(BigDecimal subFuncion) {
		this.subFuncion = subFuncion;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}