package siafi.core.ppto.fpr.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the FUNCIONES database table.
 * 
 */
@Embeddable
public class FuentesPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long finalidad;

	private long funcion;

	public FuentesPK() {
	}
	public long getFinalidad() {
		return this.finalidad;
	}
	public void setFinalidad(long finalidad) {
		this.finalidad = finalidad;
	}
	public long getFuncion() {
		return this.funcion;
	}
	public void setFuncion(long funcion) {
		this.funcion = funcion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FuentesPK)) {
			return false;
		}
		FuentesPK castOther = (FuentesPK)other;
		return 
			(this.finalidad == castOther.finalidad)
			&& (this.funcion == castOther.funcion);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.finalidad ^ (this.finalidad >>> 32)));
		hash = hash * prime + ((int) (this.funcion ^ (this.funcion >>> 32)));
		
		return hash;
	}
}