package siafi.core.ppto.fpr.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FINALIDADES database table.
 * 
 */
@Entity
@Table(name="FINALIDADES")
@NamedQuery(name="Finalidade.findAll", query="SELECT f FROM Finalidade f")
public class Finalidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long finalidad;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_FINALIDAD")
	private String descFinalidad;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public Finalidade() {
	}

	public long getFinalidad() {
		return this.finalidad;
	}

	public void setFinalidad(long finalidad) {
		this.finalidad = finalidad;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescFinalidad() {
		return this.descFinalidad;
	}

	public void setDescFinalidad(String descFinalidad) {
		this.descFinalidad = descFinalidad;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public Finalidade(long finalidad, String descFinalidad, String vigente) {
		super();
		this.finalidad = finalidad;
		this.descFinalidad = descFinalidad;
		this.vigente = vigente;
	}
	
	

}