package siafi.core.ppto.fpr.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the RUBROS_FUENTES database table.
 * 
 */
@Entity
@Table(name="RUBROS_FUENTES", schema="SIAFI")
@NamedQuery(name="RubrosFuente.findAll", query="SELECT r FROM RubrosFuente r")
public class RubrosFuente implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RubrosFuentePK id;

	@Column(name="ADM_CENTRAL")
	private String admCentral;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="EMP_LOCALES")
	private String empLocales;

	@Column(name="EMP_NACIONALES")
	private String empNacionales;

	private String excepcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="FIN_BANCARIAS")
	private String finBancarias;

	@Column(name="FIN_NO_BANCARIAS")
	private String finNoBancarias;

	@Column(name="GOB_LOCALES")
	private String gobLocales;

	@Column(name="INS_DESC")
	private String insDesc;

	@Column(name="SEG_SOCIAL")
	private String segSocial;

	private String universidades;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public RubrosFuente() {
	}

	public RubrosFuentePK getId() {
		return this.id;
	}

	public void setId(RubrosFuentePK id) {
		this.id = id;
	}

	public String getAdmCentral() {
		return this.admCentral;
	}

	public void setAdmCentral(String admCentral) {
		this.admCentral = admCentral;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getEmpLocales() {
		return this.empLocales;
	}

	public void setEmpLocales(String empLocales) {
		this.empLocales = empLocales;
	}

	public String getEmpNacionales() {
		return this.empNacionales;
	}

	public void setEmpNacionales(String empNacionales) {
		this.empNacionales = empNacionales;
	}

	public String getExcepcion() {
		return this.excepcion;
	}

	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getFinBancarias() {
		return this.finBancarias;
	}

	public void setFinBancarias(String finBancarias) {
		this.finBancarias = finBancarias;
	}

	public String getFinNoBancarias() {
		return this.finNoBancarias;
	}

	public void setFinNoBancarias(String finNoBancarias) {
		this.finNoBancarias = finNoBancarias;
	}

	public String getGobLocales() {
		return this.gobLocales;
	}

	public void setGobLocales(String gobLocales) {
		this.gobLocales = gobLocales;
	}

	public String getInsDesc() {
		return this.insDesc;
	}

	public void setInsDesc(String insDesc) {
		this.insDesc = insDesc;
	}

	public String getSegSocial() {
		return this.segSocial;
	}

	public void setSegSocial(String segSocial) {
		this.segSocial = segSocial;
	}

	public String getUniversidades() {
		return this.universidades;
	}

	public void setUniversidades(String universidades) {
		this.universidades = universidades;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}