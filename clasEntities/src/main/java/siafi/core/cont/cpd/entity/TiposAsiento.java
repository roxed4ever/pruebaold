package siafi.core.cont.cpd.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TIPOS_ASIENTOS database table.
 * 
 */
@Entity
@Table(name="TIPOS_ASIENTOS", schema="SIAFI")
@NamedQuery(name="TiposAsiento.findAll", query="SELECT t FROM TiposAsiento t")
public class TiposAsiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="TIPO_ASIENTO")
	private String tipoAsiento;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String apropiable;

	@Column(name="CODIGO_ANTERIOR")
	private String codigoAnterior;

	private String contabilizacion;

	@Column(name="DESC_TIPO_ASIENTO")
	private String descTipoAsiento;

	@Column(name="EVOLUCION_PATRIMONIO")
	private String evolucionPatrimonio;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="FLUJO_EFECTIVO")
	private String flujoEfectivo;

	@Column(name="NIC_ANTECESOR")
	private String nicAntecesor;

	@Column(name="NIC_OPERACION")
	private String nicOperacion;

	@Column(name="NIC_SECUENCIA")
	private Long nicSecuencia;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public TiposAsiento() {
	}

	public String getTipoAsiento() {
		return this.tipoAsiento;
	}

	public void setTipoAsiento(String tipoAsiento) {
		this.tipoAsiento = tipoAsiento;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getApropiable() {
		return this.apropiable;
	}

	public void setApropiable(String apropiable) {
		this.apropiable = apropiable;
	}

	public String getCodigoAnterior() {
		return this.codigoAnterior;
	}

	public void setCodigoAnterior(String codigoAnterior) {
		this.codigoAnterior = codigoAnterior;
	}

	public String getContabilizacion() {
		return this.contabilizacion;
	}

	public void setContabilizacion(String contabilizacion) {
		this.contabilizacion = contabilizacion;
	}

	public String getDescTipoAsiento() {
		return this.descTipoAsiento;
	}

	public void setDescTipoAsiento(String descTipoAsiento) {
		this.descTipoAsiento = descTipoAsiento;
	}

	public String getEvolucionPatrimonio() {
		return this.evolucionPatrimonio;
	}

	public void setEvolucionPatrimonio(String evolucionPatrimonio) {
		this.evolucionPatrimonio = evolucionPatrimonio;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getFlujoEfectivo() {
		return this.flujoEfectivo;
	}

	public void setFlujoEfectivo(String flujoEfectivo) {
		this.flujoEfectivo = flujoEfectivo;
	}

	public String getNicAntecesor() {
		return this.nicAntecesor;
	}

	public void setNicAntecesor(String nicAntecesor) {
		this.nicAntecesor = nicAntecesor;
	}

	public String getNicOperacion() {
		return this.nicOperacion;
	}

	public void setNicOperacion(String nicOperacion) {
		this.nicOperacion = nicOperacion;
	}

	public Long getNicSecuencia() {
		return this.nicSecuencia;
	}

	public void setNicSecuencia(Long nicSecuencia) {
		this.nicSecuencia = nicSecuencia;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}