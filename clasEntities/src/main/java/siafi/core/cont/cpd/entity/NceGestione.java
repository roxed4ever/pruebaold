package siafi.core.cont.cpd.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the NCE_GESTIONES database table.
 * 
 */
@Entity
@Table(name="NCE_GESTIONES", schema="SIAFI")
@NamedQuery(name="NceGestione.findAll", query="SELECT n FROM NceGestione n")
public class NceGestione implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private NceGestionePK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TABLA")
	private String apiTabla;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String contabiliza;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CIERRE")
	private Date fechaCierre;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DESDE")
	private Date fechaDesde;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_HASTA")
	private Date fechaHasta;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public NceGestione() {
	}

	public NceGestionePK getId() {
		return this.id;
	}

	public void setId(NceGestionePK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTabla() {
		return this.apiTabla;
	}

	public void setApiTabla(String apiTabla) {
		this.apiTabla = apiTabla;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getContabiliza() {
		return this.contabiliza;
	}

	public void setContabiliza(String contabiliza) {
		this.contabiliza = contabiliza;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Date getFechaCierre() {
		return this.fechaCierre;
	}

	public void setFechaCierre(Date fechaCierre) {
		this.fechaCierre = fechaCierre;
	}

	public Date getFechaDesde() {
		return this.fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return this.fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}