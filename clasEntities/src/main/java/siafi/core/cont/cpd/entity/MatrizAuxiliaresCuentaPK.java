package siafi.core.cont.cpd.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the MATRIZ_AUXILIARES_CUENTA database table.
 * 
 */
@Embeddable
public class MatrizAuxiliaresCuentaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long gestion;

	@Column(insertable=false, updatable=false)
	private long institucion;

	@Column(name="CUENTA_CONTABLE", insertable=false, updatable=false)
	private String cuentaContable;

	@Column(insertable=false, updatable=false)
	private String auxiliar;

	public MatrizAuxiliaresCuentaPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getInstitucion() {
		return this.institucion;
	}
	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}
	public String getCuentaContable() {
		return this.cuentaContable;
	}
	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}
	public String getAuxiliar() {
		return this.auxiliar;
	}
	public void setAuxiliar(String auxiliar) {
		this.auxiliar = auxiliar;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MatrizAuxiliaresCuentaPK)) {
			return false;
		}
		MatrizAuxiliaresCuentaPK castOther = (MatrizAuxiliaresCuentaPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.institucion == castOther.institucion)
			&& this.cuentaContable.equals(castOther.cuentaContable)
			&& this.auxiliar.equals(castOther.auxiliar);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.institucion ^ (this.institucion >>> 32)));
		hash = hash * prime + this.cuentaContable.hashCode();
		hash = hash * prime + this.auxiliar.hashCode();
		
		return hash;
	}
}