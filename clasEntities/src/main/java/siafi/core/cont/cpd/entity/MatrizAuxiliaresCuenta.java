package siafi.core.cont.cpd.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the MATRIZ_AUXILIARES_CUENTA database table.
 * 
 */
@Entity
@Table(name="MATRIZ_AUXILIARES_CUENTA", schema="SIAFI")
@NamedQuery(name="MatrizAuxiliares.findAll", query="SELECT m FROM MatrizAuxiliaresCuenta m")
public class MatrizAuxiliaresCuenta implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private MatrizAuxiliaresCuentaPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="AUX_PRINCIPAL")
	private String auxPrincipal;

	@Column(name="FEC_CRE")
	private Date fecCre;

	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public MatrizAuxiliaresCuenta() {
	}

	public MatrizAuxiliaresCuentaPK getId() {
		return this.id;
	}

	public void setId(MatrizAuxiliaresCuentaPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getAuxPrincipal() {
		return this.auxPrincipal;
	}

	public void setAuxPrincipal(String auxPrincipal) {
		this.auxPrincipal = auxPrincipal;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}