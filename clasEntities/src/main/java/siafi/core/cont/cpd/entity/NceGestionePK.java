package siafi.core.cont.cpd.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the NCE_GESTIONES database table.
 * 
 */
@Embeddable
public class NceGestionePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long entecontable;

	private long gestion;

	public NceGestionePK() {
	}
	public long getEntecontable() {
		return this.entecontable;
	}
	public void setEntecontable(long entecontable) {
		this.entecontable = entecontable;
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof NceGestionePK)) {
			return false;
		}
		NceGestionePK castOther = (NceGestionePK)other;
		return 
			(this.entecontable == castOther.entecontable)
			&& (this.gestion == castOther.gestion);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.entecontable ^ (this.entecontable >>> 32)));
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		
		return hash;
	}
}