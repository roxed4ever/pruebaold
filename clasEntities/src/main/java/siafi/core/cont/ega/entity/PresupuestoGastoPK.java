package siafi.core.cont.ega.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PRESUPUESTO_GASTOS database table.
 * 
 */
@Embeddable
public class PresupuestoGastoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private long gestion;
	private long institucion;
	private long ga;
	private long ue;
	private long programa;
	private long subPrograma;
	private long proyecto;
	private long actividadObra;
	private long fuente;
	private long organismo;
	private String objeto;
	private long trfBeneficiario;

	public PresupuestoGastoPK() {
	}
	
	

	public PresupuestoGastoPK(long gestion, long institucion, long ga, long ue, long programa, long subPrograma,
			long proyecto, long actividadObra, long fuente, long organismo, String objeto, long trfBeneficiario) {
		super();
		this.gestion = gestion;
		this.institucion = institucion;
		this.ga = ga;
		this.ue = ue;
		this.programa = programa;
		this.subPrograma = subPrograma;
		this.proyecto = proyecto;
		this.actividadObra = actividadObra;
		this.fuente = fuente;
		this.organismo = organismo;
		this.objeto = objeto;
		this.trfBeneficiario = trfBeneficiario;
	}



	@Column(insertable=false, updatable=false)
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}

	public long getInstitucion() {
		return this.institucion;
	}
	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}

	public long getGa() {
		return this.ga;
	}
	public void setGa(long ga) {
		this.ga = ga;
	}

	public long getUe() {
		return this.ue;
	}
	public void setUe(long ue) {
		this.ue = ue;
	}

	public long getPrograma() {
		return this.programa;
	}
	public void setPrograma(long programa) {
		this.programa = programa;
	}

	@Column(name="SUB_PROGRAMA")
	public long getSubPrograma() {
		return this.subPrograma;
	}
	public void setSubPrograma(long subPrograma) {
		this.subPrograma = subPrograma;
	}

	public long getProyecto() {
		return this.proyecto;
	}
	public void setProyecto(long proyecto) {
		this.proyecto = proyecto;
	}

	@Column(name="ACTIVIDAD_OBRA")
	public long getActividadObra() {
		return this.actividadObra;
	}
	public void setActividadObra(long actividadObra) {
		this.actividadObra = actividadObra;
	}

	@Column(insertable=false, updatable=false)
	public long getFuente() {
		return this.fuente;
	}
	public void setFuente(long fuente) {
		this.fuente = fuente;
	}

	@Column(insertable=false, updatable=false)
	public long getOrganismo() {
		return this.organismo;
	}
	public void setOrganismo(long organismo) {
		this.organismo = organismo;
	}

	@Column(insertable=false, updatable=false)
	public String getObjeto() {
		return this.objeto;
	}
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	@Column(name="TRF_BENEFICIARIO")
	public long getTrfBeneficiario() {
		return this.trfBeneficiario;
	}
	public void setTrfBeneficiario(long trfBeneficiario) {
		this.trfBeneficiario = trfBeneficiario;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PresupuestoGastoPK)) {
			return false;
		}
		PresupuestoGastoPK castOther = (PresupuestoGastoPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.institucion == castOther.institucion)
			&& (this.ga == castOther.ga)
			&& (this.ue == castOther.ue)
			&& (this.programa == castOther.programa)
			&& (this.subPrograma == castOther.subPrograma)
			&& (this.proyecto == castOther.proyecto)
			&& (this.actividadObra == castOther.actividadObra)
			&& (this.fuente == castOther.fuente)
			&& (this.organismo == castOther.organismo)
			&& this.objeto.equals(castOther.objeto)
			&& (this.trfBeneficiario == castOther.trfBeneficiario);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.institucion ^ (this.institucion >>> 32)));
		hash = hash * prime + ((int) (this.ga ^ (this.ga >>> 32)));
		hash = hash * prime + ((int) (this.ue ^ (this.ue >>> 32)));
		hash = hash * prime + ((int) (this.programa ^ (this.programa >>> 32)));
		hash = hash * prime + ((int) (this.subPrograma ^ (this.subPrograma >>> 32)));
		hash = hash * prime + ((int) (this.proyecto ^ (this.proyecto >>> 32)));
		hash = hash * prime + ((int) (this.actividadObra ^ (this.actividadObra >>> 32)));
		hash = hash * prime + ((int) (this.fuente ^ (this.fuente >>> 32)));
		hash = hash * prime + ((int) (this.organismo ^ (this.organismo >>> 32)));
		hash = hash * prime + this.objeto.hashCode();
		hash = hash * prime + ((int) (this.trfBeneficiario ^ (this.trfBeneficiario >>> 32)));
		
		return hash;
	}
}