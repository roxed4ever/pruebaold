package siafi.core.cont.ega.entity;


import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-03T19:08:56.724-0600")
@StaticMetamodel(PresupuestoGasto.class)
public class PresupuestoGasto_ {
	public static volatile SingularAttribute<PresupuestoGasto, PresupuestoGastoPK> id;
	public static volatile SingularAttribute<PresupuestoGasto, String> apiEstado;
	public static volatile SingularAttribute<PresupuestoGasto, String> apiTransaccion;
	public static volatile SingularAttribute<PresupuestoGasto, Double> aumentosSolicitados;
	public static volatile SingularAttribute<PresupuestoGasto, Double> compromisoAprobado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> compromisoVerificado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> congelamientoSolicitado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> devengadoAprobado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> devengadoFirmado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> devengadoVerificado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> disminucionesSolicitadas;
	public static volatile SingularAttribute<PresupuestoGasto, Date> fecCre;
	public static volatile SingularAttribute<PresupuestoGasto, Date> fecMod;
	public static volatile SingularAttribute<PresupuestoGasto, Double> pagoAprobado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> pptoInicial;
	public static volatile SingularAttribute<PresupuestoGasto, Double> precompromisoAprobado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> precompromisoVerificado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> secuencialFteEspec;
	public static volatile SingularAttribute<PresupuestoGasto, String> usuCre;
	public static volatile SingularAttribute<PresupuestoGasto, String> usuMod;
	public static volatile SingularAttribute<PresupuestoGasto, Double> aumentos;
	public static volatile SingularAttribute<PresupuestoGasto, Double> compromiso;
	public static volatile SingularAttribute<PresupuestoGasto, Double> congelamiento;
	public static volatile SingularAttribute<PresupuestoGasto, Double> devengado;
	public static volatile SingularAttribute<PresupuestoGasto, Double> disminuciones;
	public static volatile SingularAttribute<PresupuestoGasto, Double> pago;
	public static volatile SingularAttribute<PresupuestoGasto, Double> precompromiso;
}
