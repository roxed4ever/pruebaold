package siafi.core.cont.ega.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the BIP_FTE_ESPECIFICA database table.
 * 
 */
@Embeddable
public class BipFteEspecificaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private String bip;
	private String nroConvenio;
	private long fuente;
	private long organismo;

	public BipFteEspecificaPK() {
	}

	
	public BipFteEspecificaPK(String bip, String nroConvenio, long fuente, long organismo) {
		super();
		this.bip = bip;
		this.nroConvenio = nroConvenio;
		this.fuente = fuente;
		this.organismo = organismo;
	}


	@Column(insertable=false, updatable=false)
	public String getBip() {
		return this.bip;
	}
	public void setBip(String bip) {
		this.bip = bip;
	}

	@Column(name="NRO_CONVENIO", insertable=false, updatable=false)
	public String getNroConvenio() {
		return this.nroConvenio;
	}
	public void setNroConvenio(String nroConvenio) {
		this.nroConvenio = nroConvenio;
	}

	@Column(insertable=false, updatable=false)
	public long getFuente() {
		return this.fuente;
	}
	public void setFuente(long fuente) {
		this.fuente = fuente;
	}

	@Column(insertable=false, updatable=false)
	public long getOrganismo() {
		return this.organismo;
	}
	public void setOrganismo(long organismo) {
		this.organismo = organismo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof BipFteEspecificaPK)) {
			return false;
		}
		BipFteEspecificaPK castOther = (BipFteEspecificaPK)other;
		return 
			this.bip.equals(castOther.bip)
			&& this.nroConvenio.equals(castOther.nroConvenio)
			&& (this.fuente == castOther.fuente)
			&& (this.organismo == castOther.organismo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.bip.hashCode();
		hash = hash * prime + this.nroConvenio.hashCode();
		hash = hash * prime + ((int) (this.fuente ^ (this.fuente >>> 32)));
		hash = hash * prime + ((int) (this.organismo ^ (this.organismo >>> 32)));
		
		return hash;
	}
}