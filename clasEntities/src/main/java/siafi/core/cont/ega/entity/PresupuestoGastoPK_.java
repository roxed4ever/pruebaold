package siafi.core.cont.ega.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-03T19:08:56.724-0600")
@StaticMetamodel(PresupuestoGastoPK.class)
public class PresupuestoGastoPK_ {
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> gestion;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> subPrograma;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> actividadObra;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> fuente;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> organismo;
	public static volatile SingularAttribute<PresupuestoGastoPK, String> objeto;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> trfBeneficiario;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> institucion;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> ga;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> ue;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> programa;
	public static volatile SingularAttribute<PresupuestoGastoPK, Long> proyecto;
}
