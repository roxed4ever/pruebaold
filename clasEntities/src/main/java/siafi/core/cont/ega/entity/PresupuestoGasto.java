package siafi.core.cont.ega.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the PRESUPUESTO_GASTOS database table.
 * 
 */
@Entity
@Table(name = "PRESUPUESTO_GASTOS")
@NamedQuery(name = "PresupuestoGasto.findAll", query = "SELECT p FROM PresupuestoGasto p")
public class PresupuestoGasto implements Serializable {
	private static final long serialVersionUID = 1L;
	private PresupuestoGastoPK id;
	private String apiEstado;
	private String apiTransaccion;
	private Double aumentos;
	private Double aumentosSolicitados;
	private Double compromiso;
	private Double compromisoAprobado;
	private Double compromisoVerificado;
	private Double congelamiento;
	private Double congelamientoSolicitado;
	private Double devengado;
	private Double devengadoAprobado;
	private Double devengadoFirmado;
	private Double devengadoVerificado;
	private Double disminuciones;
	private Double disminucionesSolicitadas;
	private Date fecCre;
	private Date fecMod;
	private Double pago;
	private Double pagoAprobado;
	private Double pptoInicial;
	private Double precompromiso;
	private Double precompromisoAprobado;
	private Double precompromisoVerificado;
	private Double secuencialFteEspec;
	private String usuCre;
	private String usuMod;

	public PresupuestoGasto() {
	}

	public PresupuestoGasto(long gestion, long institucion, long ga, long ue, long programa, long subPrograma,
			long proyecto, long actividadObra, long fuente, long organismo, String objeto, long trfBeneficiario,
			String apiEstado, String apiTransaccion, Double aumentos, Double aumentosSolicitados,
			Double compromiso, Double compromisoAprobado, Double compromisoVerificado,
			Double congelamiento, Double congelamientoSolicitado, Double devengado,
			Double devengadoAprobado, Double devengadoFirmado, Double devengadoVerificado,
			Double disminuciones, Double disminucionesSolicitadas, Date fecCre, Date fecMod, Double pago,
			Double pagoAprobado, Double pptoInicial, Double precompromiso, Double precompromisoAprobado,
			Double precompromisoVerificado, Double secuencialFteEspec, String usuCre, String usuMod) {
		super();
		this.id = new PresupuestoGastoPK(gestion, institucion, ga, ue, programa, subPrograma, proyecto, actividadObra,
				fuente, organismo, objeto, trfBeneficiario);
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.aumentos = aumentos;
		this.aumentosSolicitados = aumentosSolicitados;
		this.compromiso = compromiso;
		this.compromisoAprobado = compromisoAprobado;
		this.compromisoVerificado = compromisoVerificado;
		this.congelamiento = congelamiento;
		this.congelamientoSolicitado = congelamientoSolicitado;
		this.devengado = devengado;
		this.devengadoAprobado = devengadoAprobado;
		this.devengadoFirmado = devengadoFirmado;
		this.devengadoVerificado = devengadoVerificado;
		this.disminuciones = disminuciones;
		this.disminucionesSolicitadas = disminucionesSolicitadas;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.pago = pago;
		this.pagoAprobado = pagoAprobado;
		this.pptoInicial = pptoInicial;
		this.precompromiso = precompromiso;
		this.precompromisoAprobado = precompromisoAprobado;
		this.precompromisoVerificado = precompromisoVerificado;
		this.secuencialFteEspec = secuencialFteEspec;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
	}

	@EmbeddedId
	public PresupuestoGastoPK getId() {
		return this.id;
	}

	public void setId(PresupuestoGastoPK id) {
		this.id = id;
	}

	@Column(name = "API_ESTADO")
	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	@Column(name = "API_TRANSACCION")
	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Double getAumentos() {
		return this.aumentos;
	}

	public void setAumentos(Double aumentos) {
		this.aumentos = aumentos;
	}

	@Column(name = "AUMENTOS_SOLICITADOS")
	public Double getAumentosSolicitados() {
		return this.aumentosSolicitados;
	}

	public void setAumentosSolicitados(Double aumentosSolicitados) {
		this.aumentosSolicitados = aumentosSolicitados;
	}

	public Double getCompromiso() {
		return this.compromiso;
	}

	public void setCompromiso(Double compromiso) {
		this.compromiso = compromiso;
	}

	@Column(name = "COMPROMISO_APROBADO")
	public Double getCompromisoAprobado() {
		return this.compromisoAprobado;
	}

	public void setCompromisoAprobado(Double compromisoAprobado) {
		this.compromisoAprobado = compromisoAprobado;
	}

	@Column(name = "COMPROMISO_VERIFICADO")
	public Double getCompromisoVerificado() {
		return this.compromisoVerificado;
	}

	public void setCompromisoVerificado(Double compromisoVerificado) {
		this.compromisoVerificado = compromisoVerificado;
	}

	public Double getCongelamiento() {
		return this.congelamiento;
	}

	public void setCongelamiento(Double congelamiento) {
		this.congelamiento = congelamiento;
	}

	@Column(name = "CONGELAMIENTO_SOLICITADO")
	public Double getCongelamientoSolicitado() {
		return this.congelamientoSolicitado;
	}

	public void setCongelamientoSolicitado(Double congelamientoSolicitado) {
		this.congelamientoSolicitado = congelamientoSolicitado;
	}

	public Double getDevengado() {
		return this.devengado;
	}

	public void setDevengado(Double devengado) {
		this.devengado = devengado;
	}

	@Column(name = "DEVENGADO_APROBADO")
	public Double getDevengadoAprobado() {
		return this.devengadoAprobado;
	}

	public void setDevengadoAprobado(Double devengadoAprobado) {
		this.devengadoAprobado = devengadoAprobado;
	}

	@Column(name = "DEVENGADO_FIRMADO")
	public Double getDevengadoFirmado() {
		return this.devengadoFirmado;
	}

	public void setDevengadoFirmado(Double devengadoFirmado) {
		this.devengadoFirmado = devengadoFirmado;
	}

	@Column(name = "DEVENGADO_VERIFICADO")
	public Double getDevengadoVerificado() {
		return this.devengadoVerificado;
	}

	public void setDevengadoVerificado(Double devengadoVerificado) {
		this.devengadoVerificado = devengadoVerificado;
	}

	public Double getDisminuciones() {
		return this.disminuciones;
	}

	public void setDisminuciones(Double disminuciones) {
		this.disminuciones = disminuciones;
	}

	@Column(name = "DISMINUCIONES_SOLICITADAS")
	public Double getDisminucionesSolicitadas() {
		return this.disminucionesSolicitadas;
	}

	public void setDisminucionesSolicitadas(Double disminucionesSolicitadas) {
		this.disminucionesSolicitadas = disminucionesSolicitadas;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FEC_CRE")
	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FEC_MOD")
	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getPago() {
		return this.pago;
	}

	public void setPago(Double pago) {
		this.pago = pago;
	}

	@Column(name = "PAGO_APROBADO")
	public Double getPagoAprobado() {
		return this.pagoAprobado;
	}

	public void setPagoAprobado(Double pagoAprobado) {
		this.pagoAprobado = pagoAprobado;
	}

	@Column(name = "PPTO_INICIAL")
	public Double getPptoInicial() {
		return this.pptoInicial;
	}

	public void setPptoInicial(Double pptoInicial) {
		this.pptoInicial = pptoInicial;
	}

	public Double getPrecompromiso() {
		return this.precompromiso;
	}

	public void setPrecompromiso(Double precompromiso) {
		this.precompromiso = precompromiso;
	}

	@Column(name = "PRECOMPROMISO_APROBADO")
	public Double getPrecompromisoAprobado() {
		return this.precompromisoAprobado;
	}

	public void setPrecompromisoAprobado(Double precompromisoAprobado) {
		this.precompromisoAprobado = precompromisoAprobado;
	}

	@Column(name = "PRECOMPROMISO_VERIFICADO")
	public Double getPrecompromisoVerificado() {
		return this.precompromisoVerificado;
	}

	public void setPrecompromisoVerificado(Double precompromisoVerificado) {
		this.precompromisoVerificado = precompromisoVerificado;
	}

	@Column(name = "SECUENCIAL_FTE_ESPEC")
	public Double getSecuencialFteEspec() {
		return this.secuencialFteEspec;
	}

	public void setSecuencialFteEspec(Double secuencialFteEspec) {
		this.secuencialFteEspec = secuencialFteEspec;
	}

	@Column(name = "USU_CRE")
	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	@Column(name = "USU_MOD")
	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}