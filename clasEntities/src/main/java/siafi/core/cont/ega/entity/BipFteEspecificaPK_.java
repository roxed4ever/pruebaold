package siafi.core.cont.ega.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-03T19:08:56.724-0600")
@StaticMetamodel(BipFteEspecificaPK.class)
public class BipFteEspecificaPK_ {
	public static volatile SingularAttribute<BipFteEspecificaPK, String> bip;
	public static volatile SingularAttribute<BipFteEspecificaPK, String> nroConvenio;
	public static volatile SingularAttribute<BipFteEspecificaPK, Long> fuente;
	public static volatile SingularAttribute<BipFteEspecificaPK, Long> organismo;
}
