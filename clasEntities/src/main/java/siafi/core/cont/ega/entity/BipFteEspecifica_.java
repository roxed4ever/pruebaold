package siafi.core.cont.ega.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-03T19:08:56.724-0600")
@StaticMetamodel(BipFteEspecifica.class)
public class BipFteEspecifica_ {
	public static volatile SingularAttribute<BipFteEspecifica, BipFteEspecificaPK> id;
	public static volatile SingularAttribute<BipFteEspecifica, String> apiEstado;
	public static volatile SingularAttribute<BipFteEspecifica, String> apiTransaccion;
	public static volatile SingularAttribute<BipFteEspecifica, Date> fecCre;
	public static volatile SingularAttribute<BipFteEspecifica, Date> fecMod;
	public static volatile SingularAttribute<BipFteEspecifica, String> usuCre;
	public static volatile SingularAttribute<BipFteEspecifica, String> usuMod;
	public static volatile SingularAttribute<BipFteEspecifica, Double> secuencial;
	public static volatile SingularAttribute<BipFteEspecifica, String> vigente;
}
