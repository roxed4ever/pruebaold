package siafi.core.cont.ega.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BIP_FTE_ESPECIFICA database table.
 * 
 */
@Entity
@Table(name="BIP_FTE_ESPECIFICA")
@NamedQuery(name="BipFteEspecifica.findAll", query="SELECT b FROM BipFteEspecifica b")
public class BipFteEspecifica implements Serializable {
	private static final long serialVersionUID = 1L;
	private BipFteEspecificaPK id;
	private String apiEstado;
	private String apiTransaccion;
	private Date fecCre;
	private Date fecMod;
	private Double secuencial;
	private String usuCre;
	private String usuMod;
	private String vigente;

	public BipFteEspecifica() {
	}

	

	public BipFteEspecifica(BipFteEspecificaPK id, String apiEstado, String apiTransaccion, Date fecCre, Date fecMod,
			Double secuencial, String usuCre, String usuMod, String vigente) {
		super();
		this.id = id;
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.secuencial = secuencial;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
	}

	public BipFteEspecifica(String bip, String nroConvenio, long fuente, long organismo, String apiEstado, String apiTransaccion, Date fecCre, Date fecMod,
			Double secuencial, String usuCre, String usuMod, String vigente) {
		super();
		this.id = new BipFteEspecificaPK(bip, nroConvenio, fuente, organismo);
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.secuencial = secuencial;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
	}


	@EmbeddedId
	public BipFteEspecificaPK getId() {
		return this.id;
	}

	public void setId(BipFteEspecificaPK id) {
		this.id = id;
	}


	@Column(name="API_ESTADO")
	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}


	@Column(name="API_TRANSACCION")
	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}


	public Double getSecuencial() {
		return this.secuencial;
	}

	public void setSecuencial(Double secuencial) {
		this.secuencial = secuencial;
	}


	@Column(name="USU_CRE")
	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}


	@Column(name="USU_MOD")
	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}


	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}