package siafi.core.cont.ing.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the EIN_EXCEPCIONES database table.
 * 
 */
@Entity
@Table(name="EIN_EXCEPCIONES", schema="SIAFI")
@NamedQuery(name="EinExcepcione.findAll", query="SELECT e FROM EinExcepcione e")
public class EinExcepcione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String co0;

	private String co1;

	private String co2;

	private String co3;

	private String co4;

	private String co5;

	private String co6;

	private String co7;

	private String co8;

	private String co9;

	@Column(name="DIFERENCIAL_CAMBIARIO")
	private BigDecimal diferencialCambiario;

	@Id	
	private String excepcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private BigDecimal ga;

	private BigDecimal gestion;

	private BigDecimal importe;

	@Column(name="IMPORTE_ME")
	private BigDecimal importeMe;

	private BigDecimal institucion;

	private String moneda;

	@Column(name="NRO_DEVENGADO")
	private BigDecimal nroDevengado;

	@Column(name="NRO_PERCIBIDO")
	private BigDecimal nroPercibido;

	@Column(name="NRO_SECUENCIA")
	private BigDecimal nroSecuencia;

	private String observaciones;

	private BigDecimal secuencia;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public EinExcepcione() {
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getCo0() {
		return this.co0;
	}

	public void setCo0(String co0) {
		this.co0 = co0;
	}

	public String getCo1() {
		return this.co1;
	}

	public void setCo1(String co1) {
		this.co1 = co1;
	}

	public String getCo2() {
		return this.co2;
	}

	public void setCo2(String co2) {
		this.co2 = co2;
	}

	public String getCo3() {
		return this.co3;
	}

	public void setCo3(String co3) {
		this.co3 = co3;
	}

	public String getCo4() {
		return this.co4;
	}

	public void setCo4(String co4) {
		this.co4 = co4;
	}

	public String getCo5() {
		return this.co5;
	}

	public void setCo5(String co5) {
		this.co5 = co5;
	}

	public String getCo6() {
		return this.co6;
	}

	public void setCo6(String co6) {
		this.co6 = co6;
	}

	public String getCo7() {
		return this.co7;
	}

	public void setCo7(String co7) {
		this.co7 = co7;
	}

	public String getCo8() {
		return this.co8;
	}

	public void setCo8(String co8) {
		this.co8 = co8;
	}

	public String getCo9() {
		return this.co9;
	}

	public void setCo9(String co9) {
		this.co9 = co9;
	}

	public BigDecimal getDiferencialCambiario() {
		return this.diferencialCambiario;
	}

	public void setDiferencialCambiario(BigDecimal diferencialCambiario) {
		this.diferencialCambiario = diferencialCambiario;
	}

	public String getExcepcion() {
		return this.excepcion;
	}

	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public BigDecimal getGa() {
		return this.ga;
	}

	public void setGa(BigDecimal ga) {
		this.ga = ga;
	}

	public BigDecimal getGestion() {
		return this.gestion;
	}

	public void setGestion(BigDecimal gestion) {
		this.gestion = gestion;
	}

	public BigDecimal getImporte() {
		return this.importe;
	}

	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	public BigDecimal getImporteMe() {
		return this.importeMe;
	}

	public void setImporteMe(BigDecimal importeMe) {
		this.importeMe = importeMe;
	}

	public BigDecimal getInstitucion() {
		return this.institucion;
	}

	public void setInstitucion(BigDecimal institucion) {
		this.institucion = institucion;
	}

	public String getMoneda() {
		return this.moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public BigDecimal getNroDevengado() {
		return this.nroDevengado;
	}

	public void setNroDevengado(BigDecimal nroDevengado) {
		this.nroDevengado = nroDevengado;
	}

	public BigDecimal getNroPercibido() {
		return this.nroPercibido;
	}

	public void setNroPercibido(BigDecimal nroPercibido) {
		this.nroPercibido = nroPercibido;
	}

	public BigDecimal getNroSecuencia() {
		return this.nroSecuencia;
	}

	public void setNroSecuencia(BigDecimal nroSecuencia) {
		this.nroSecuencia = nroSecuencia;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public BigDecimal getSecuencia() {
		return this.secuencia;
	}

	public void setSecuencia(BigDecimal secuencia) {
		this.secuencia = secuencia;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}