package siafi.ges.nomina.funcionarios.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the SAD_AUX_DOCENTE database table.
 * 
 */
@Entity
@Table(name="SAD_AUX_DOCENTE", schema="SIAFI")
@NamedQuery(name="SadAuxDocente.findAll", query="SELECT s FROM SadAuxDocente s")
public class SadAuxDocente implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SadAuxDocentePK id;

	@Column(name="ANIOS_SERVICIO")
	private Long aniosServicio;

	private String apellidos;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private Long banco;

	@Column(name="CBA_CODIGO_ERROR")
	private Long cbaCodigoError;

	@Column(name="COD_BAJA_SIARH")
	private String codBajaSiarh;

	@Column(name="CORREO_PERSONAL")
	private String correoPersonal;

	@Column(name="CORREO_TRABAJO")
	private String correoTrabajo;

	private String cuenta;

	@Column(name="DEPTO_DOMICILIO")
	private Long deptoDomicilio;

	@Column(name="DIAS_SERVICIO")
	private Long diasServicio;

	private String direccion;

	@Column(name="ESTADO_CIVIL")
	private String estadoCivil;

	@Column(name="ESTADO_CUENTA")
	private String estadoCuenta;

	@Column(name="ESTADO_DCTE_INPREMA")
	private String estadoDcteInprema;

	@Column(name="ESTADO_DCTE_RNP")
	private String estadoDcteRnp;

	@Column(name="ESTADO_DCTE_SEDUC")
	private String estadoDcteSeduc;

	@Column(name="ESTADO_DOCENTE")
	private String estadoDocente;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_NACIMIENTO")
	private Date fechaNacimiento;

	@Column(name="FICHA_FNP")
	private String fichaFnp;

	private Long funcionario;

	private String generacion;

	@Column(name="GRADO_PROFESIONAL")
	private String gradoProfesional;

	private String inprema;

	@Column(name="MEDIO_PAGO")
	private String medioPago;

	@Column(name="MESES_SERVICIO")
	private Long mesesServicio;

	private String moneda;

	@Column(name="MUNI_DOMICILIO")
	private Long muniDomicilio;

	@Column(name="NAC_DEPARTAMENTO")
	private Long nacDepartamento;

	@Column(name="NAC_GRUPO_SANGUINEO")
	private Long nacGrupoSanguineo;

	@Column(name="NAC_MUNICIPIO")
	private Long nacMunicipio;

	@Column(name="NAC_NACIONALIDAD")
	private Long nacNacionalidad;

	@Column(name="NAC_PAIS")
	private String nacPais;

	private String nombres;

	@Column(name="NOMBRES_APELLIDOS")
	private String nombresApellidos;

	private String observaciones;

	private String origen;

	private String sexo;

	@Column(name="SIGLA_APLICACION")
	private String siglaAplicacion;

	private String telefonos;

	@Column(name="TIPO_CUENTA")
	private String tipoCuenta;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public SadAuxDocente() {
	}

	public SadAuxDocentePK getId() {
		return this.id;
	}

	public void setId(SadAuxDocentePK id) {
		this.id = id;
	}

	public Long getAniosServicio() {
		return this.aniosServicio;
	}

	public void setAniosServicio(Long aniosServicio) {
		this.aniosServicio = aniosServicio;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Long getBanco() {
		return this.banco;
	}

	public void setBanco(Long banco) {
		this.banco = banco;
	}

	public Long getCbaCodigoError() {
		return this.cbaCodigoError;
	}

	public void setCbaCodigoError(Long cbaCodigoError) {
		this.cbaCodigoError = cbaCodigoError;
	}

	public String getCodBajaSiarh() {
		return this.codBajaSiarh;
	}

	public void setCodBajaSiarh(String codBajaSiarh) {
		this.codBajaSiarh = codBajaSiarh;
	}

	public String getCorreoPersonal() {
		return this.correoPersonal;
	}

	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	public String getCorreoTrabajo() {
		return this.correoTrabajo;
	}

	public void setCorreoTrabajo(String correoTrabajo) {
		this.correoTrabajo = correoTrabajo;
	}

	public String getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public Long getDeptoDomicilio() {
		return this.deptoDomicilio;
	}

	public void setDeptoDomicilio(Long deptoDomicilio) {
		this.deptoDomicilio = deptoDomicilio;
	}

	public Long getDiasServicio() {
		return this.diasServicio;
	}

	public void setDiasServicio(Long diasServicio) {
		this.diasServicio = diasServicio;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getEstadoCuenta() {
		return this.estadoCuenta;
	}

	public void setEstadoCuenta(String estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}

	public String getEstadoDcteInprema() {
		return this.estadoDcteInprema;
	}

	public void setEstadoDcteInprema(String estadoDcteInprema) {
		this.estadoDcteInprema = estadoDcteInprema;
	}

	public String getEstadoDcteRnp() {
		return this.estadoDcteRnp;
	}

	public void setEstadoDcteRnp(String estadoDcteRnp) {
		this.estadoDcteRnp = estadoDcteRnp;
	}

	public String getEstadoDcteSeduc() {
		return this.estadoDcteSeduc;
	}

	public void setEstadoDcteSeduc(String estadoDcteSeduc) {
		this.estadoDcteSeduc = estadoDcteSeduc;
	}

	public String getEstadoDocente() {
		return this.estadoDocente;
	}

	public void setEstadoDocente(String estadoDocente) {
		this.estadoDocente = estadoDocente;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFichaFnp() {
		return this.fichaFnp;
	}

	public void setFichaFnp(String fichaFnp) {
		this.fichaFnp = fichaFnp;
	}

	public Long getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Long funcionario) {
		this.funcionario = funcionario;
	}

	public String getGeneracion() {
		return this.generacion;
	}

	public void setGeneracion(String generacion) {
		this.generacion = generacion;
	}

	public String getGradoProfesional() {
		return this.gradoProfesional;
	}

	public void setGradoProfesional(String gradoProfesional) {
		this.gradoProfesional = gradoProfesional;
	}

	public String getInprema() {
		return this.inprema;
	}

	public void setInprema(String inprema) {
		this.inprema = inprema;
	}

	public String getMedioPago() {
		return this.medioPago;
	}

	public void setMedioPago(String medioPago) {
		this.medioPago = medioPago;
	}

	public Long getMesesServicio() {
		return this.mesesServicio;
	}

	public void setMesesServicio(Long mesesServicio) {
		this.mesesServicio = mesesServicio;
	}

	public String getMoneda() {
		return this.moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public Long getMuniDomicilio() {
		return this.muniDomicilio;
	}

	public void setMuniDomicilio(Long muniDomicilio) {
		this.muniDomicilio = muniDomicilio;
	}

	public Long getNacDepartamento() {
		return this.nacDepartamento;
	}

	public void setNacDepartamento(Long nacDepartamento) {
		this.nacDepartamento = nacDepartamento;
	}

	public Long getNacGrupoSanguineo() {
		return this.nacGrupoSanguineo;
	}

	public void setNacGrupoSanguineo(Long nacGrupoSanguineo) {
		this.nacGrupoSanguineo = nacGrupoSanguineo;
	}

	public Long getNacMunicipio() {
		return this.nacMunicipio;
	}

	public void setNacMunicipio(Long nacMunicipio) {
		this.nacMunicipio = nacMunicipio;
	}

	public Long getNacNacionalidad() {
		return this.nacNacionalidad;
	}

	public void setNacNacionalidad(Long nacNacionalidad) {
		this.nacNacionalidad = nacNacionalidad;
	}

	public String getNacPais() {
		return this.nacPais;
	}

	public void setNacPais(String nacPais) {
		this.nacPais = nacPais;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNombresApellidos() {
		return this.nombresApellidos;
	}

	public void setNombresApellidos(String nombresApellidos) {
		this.nombresApellidos = nombresApellidos;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getOrigen() {
		return this.origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getSiglaAplicacion() {
		return this.siglaAplicacion;
	}

	public void setSiglaAplicacion(String siglaAplicacion) {
		this.siglaAplicacion = siglaAplicacion;
	}

	public String getTelefonos() {
		return this.telefonos;
	}

	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}

	public String getTipoCuenta() {
		return this.tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}