package siafi.ges.nomina.funcionarios.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FICHA_PERSONALES database table.
 * 
 */
@Entity
@Table(name="FICHA_PERSONALES",schema="SIAFI")
@NamedQuery(name="FichaPersonale.findAll", query="SELECT f FROM FichaPersonale f")
public class FichaPersonale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long funcionario;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="BNC_BANCO")
	private Long bncBanco;

	@Column(name="BNC_CBA_CODIGO_ERROR")
	private Long bncCbaCodigoError;

	@Column(name="BNC_CUENTA")
	private String bncCuenta;

	@Column(name="BNC_ESTADO_CUENTA")
	private String bncEstadoCuenta;

	@Column(name="BNC_MONEDA")
	private String bncMoneda;

	@Column(name="BNC_PLAZA")
	private Long bncPlaza;

	@Column(name="BNC_TIPO_CUENTA")
	private String bncTipoCuenta;

	private String celular;

	@Column(name="CORREO_PERSONAL")
	private String correoPersonal;

	@Column(name="CORREO_TRABAJO")
	private String correoTrabajo;

	@Column(name="DECLARACION_JURADA")
	private Long declaracionJurada;

	@Column(name="DESC_BENEFICIARIO_CTA")
	private String descBeneficiarioCta;

	@Column(name="DIR_AVENIDA_CALLE")
	private String dirAvenidaCalle;

	@Column(name="DIR_BARRIO")
	private String dirBarrio;

	@Column(name="DIR_EDIFICIO")
	private String dirEdificio;

	@Column(name="DIR_LUGAR")
	private String dirLugar;

	@Column(name="DIR_NUMERO")
	private Long dirNumero;

	@Column(name="ESTADO_CIVIL")
	private String estadoCivil;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="NAC_DEPARTAMENTO")
	private Long nacDepartamento;

	@Temporal(TemporalType.DATE)
	@Column(name="NAC_FECHA")
	private Date nacFecha;

	@Column(name="NAC_GRUPO_SANGUINEO")
	private Long nacGrupoSanguineo;

	@Column(name="NAC_MUNICIPIO")
	private Long nacMunicipio;

	@Column(name="NAC_NACIONALIDAD")
	private Long nacNacionalidad;

	@Column(name="NAC_PAIS")
	private String nacPais;

	@Column(name="NAC_SEXO")
	private String nacSexo;

	private String representante;

	private String telefono;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public FichaPersonale() {
	}

	public long getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(long funcionario) {
		this.funcionario = funcionario;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Long getBncBanco() {
		return this.bncBanco;
	}

	public void setBncBanco(Long bncBanco) {
		this.bncBanco = bncBanco;
	}

	public Long getBncCbaCodigoError() {
		return this.bncCbaCodigoError;
	}

	public void setBncCbaCodigoError(Long bncCbaCodigoError) {
		this.bncCbaCodigoError = bncCbaCodigoError;
	}

	public String getBncCuenta() {
		return this.bncCuenta;
	}

	public void setBncCuenta(String bncCuenta) {
		this.bncCuenta = bncCuenta;
	}

	public String getBncEstadoCuenta() {
		return this.bncEstadoCuenta;
	}

	public void setBncEstadoCuenta(String bncEstadoCuenta) {
		this.bncEstadoCuenta = bncEstadoCuenta;
	}

	public String getBncMoneda() {
		return this.bncMoneda;
	}

	public void setBncMoneda(String bncMoneda) {
		this.bncMoneda = bncMoneda;
	}

	public Long getBncPlaza() {
		return this.bncPlaza;
	}

	public void setBncPlaza(Long bncPlaza) {
		this.bncPlaza = bncPlaza;
	}

	public String getBncTipoCuenta() {
		return this.bncTipoCuenta;
	}

	public void setBncTipoCuenta(String bncTipoCuenta) {
		this.bncTipoCuenta = bncTipoCuenta;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCorreoPersonal() {
		return this.correoPersonal;
	}

	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	public String getCorreoTrabajo() {
		return this.correoTrabajo;
	}

	public void setCorreoTrabajo(String correoTrabajo) {
		this.correoTrabajo = correoTrabajo;
	}

	public Long getDeclaracionJurada() {
		return this.declaracionJurada;
	}

	public void setDeclaracionJurada(Long declaracionJurada) {
		this.declaracionJurada = declaracionJurada;
	}

	public String getDescBeneficiarioCta() {
		return this.descBeneficiarioCta;
	}

	public void setDescBeneficiarioCta(String descBeneficiarioCta) {
		this.descBeneficiarioCta = descBeneficiarioCta;
	}

	public String getDirAvenidaCalle() {
		return this.dirAvenidaCalle;
	}

	public void setDirAvenidaCalle(String dirAvenidaCalle) {
		this.dirAvenidaCalle = dirAvenidaCalle;
	}

	public String getDirBarrio() {
		return this.dirBarrio;
	}

	public void setDirBarrio(String dirBarrio) {
		this.dirBarrio = dirBarrio;
	}

	public String getDirEdificio() {
		return this.dirEdificio;
	}

	public void setDirEdificio(String dirEdificio) {
		this.dirEdificio = dirEdificio;
	}

	public String getDirLugar() {
		return this.dirLugar;
	}

	public void setDirLugar(String dirLugar) {
		this.dirLugar = dirLugar;
	}

	public Long getDirNumero() {
		return this.dirNumero;
	}

	public void setDirNumero(Long dirNumero) {
		this.dirNumero = dirNumero;
	}

	public String getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Long getNacDepartamento() {
		return this.nacDepartamento;
	}

	public void setNacDepartamento(Long nacDepartamento) {
		this.nacDepartamento = nacDepartamento;
	}

	public Date getNacFecha() {
		return this.nacFecha;
	}

	public void setNacFecha(Date nacFecha) {
		this.nacFecha = nacFecha;
	}

	public Long getNacGrupoSanguineo() {
		return this.nacGrupoSanguineo;
	}

	public void setNacGrupoSanguineo(Long nacGrupoSanguineo) {
		this.nacGrupoSanguineo = nacGrupoSanguineo;
	}

	public Long getNacMunicipio() {
		return this.nacMunicipio;
	}

	public void setNacMunicipio(Long nacMunicipio) {
		this.nacMunicipio = nacMunicipio;
	}

	public Long getNacNacionalidad() {
		return this.nacNacionalidad;
	}

	public void setNacNacionalidad(Long nacNacionalidad) {
		this.nacNacionalidad = nacNacionalidad;
	}

	public String getNacPais() {
		return this.nacPais;
	}

	public void setNacPais(String nacPais) {
		this.nacPais = nacPais;
	}

	public String getNacSexo() {
		return this.nacSexo;
	}

	public void setNacSexo(String nacSexo) {
		this.nacSexo = nacSexo;
	}

	public String getRepresentante() {
		return this.representante;
	}

	public void setRepresentante(String representante) {
		this.representante = representante;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}