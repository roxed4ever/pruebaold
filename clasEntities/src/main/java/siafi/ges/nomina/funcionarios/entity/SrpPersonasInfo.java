package siafi.ges.nomina.funcionarios.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the SRP_PERSONAS_INFO database table.
 * 
 */
@Entity
@Table(name="SRP_PERSONAS_INFO", schema="SIREP")
@NamedQuery(name="SrpPersonasInfo.findAll", query="SELECT s FROM SrpPersonasInfo s")
public class SrpPersonasInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PERSONA")
	private long idPersona;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String celular;

	@Column(name="DEPARTAMENTO_NACIMIENTO")
	private Long departamentoNacimiento;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_NACIMIENTO")
	private Date fechaNacimiento;

	private String genero;

	@Column(name="GRUPO_SANGUINEO")
	private String grupoSanguineo;

	@Column(name="LUGAR_NACIMIENTO")
	private String lugarNacimiento;

	@Column(name="LUGAR_RESIDENCIA")
	private String lugarResidencia;

	@Column(name="MUNICIPIO_NACIMIENTO")
	private Long municipioNacimiento;

	private String nacionalidad;

	@Column(name="NRO_DOCUMENTO")
	private Long nroDocumento;

	private String profesion;

	private String telefono;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	//bi-directional one-to-one association to SrpPersona
	@OneToOne
	@JoinColumn(name="ID_PERSONA")
	private SrpPersona srpPersona;

	public SrpPersonasInfo() {
	}

	public long getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(long idPersona) {
		this.idPersona = idPersona;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Long getDepartamentoNacimiento() {
		return this.departamentoNacimiento;
	}

	public void setDepartamentoNacimiento(Long departamentoNacimiento) {
		this.departamentoNacimiento = departamentoNacimiento;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getGenero() {
		return this.genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getGrupoSanguineo() {
		return this.grupoSanguineo;
	}

	public void setGrupoSanguineo(String grupoSanguineo) {
		this.grupoSanguineo = grupoSanguineo;
	}

	public String getLugarNacimiento() {
		return this.lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public String getLugarResidencia() {
		return this.lugarResidencia;
	}

	public void setLugarResidencia(String lugarResidencia) {
		this.lugarResidencia = lugarResidencia;
	}

	public Long getMunicipioNacimiento() {
		return this.municipioNacimiento;
	}

	public void setMunicipioNacimiento(Long municipioNacimiento) {
		this.municipioNacimiento = municipioNacimiento;
	}

	public String getNacionalidad() {
		return this.nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public Long getNroDocumento() {
		return this.nroDocumento;
	}

	public void setNroDocumento(Long nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getProfesion() {
		return this.profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public SrpPersona getSrpPersona() {
		return this.srpPersona;
	}

	public void setSrpPersona(SrpPersona srpPersona) {
		this.srpPersona = srpPersona;
	}

}