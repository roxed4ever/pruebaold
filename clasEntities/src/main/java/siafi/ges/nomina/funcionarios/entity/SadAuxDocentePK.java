package siafi.ges.nomina.funcionarios.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the SAD_AUX_DOCENTE database table.
 * 
 */
@Embeddable
public class SadAuxDocentePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PAIS_ID")
	private String paisId;

	@Column(name="TIPO_ID", insertable=false, updatable=false)
	private String tipoId;

	@Column(name="NRO_ID")
	private String nroId;

	public SadAuxDocentePK() {
	}
	public String getPaisId() {
		return this.paisId;
	}
	public void setPaisId(String paisId) {
		this.paisId = paisId;
	}
	public String getTipoId() {
		return this.tipoId;
	}
	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}
	public String getNroId() {
		return this.nroId;
	}
	public void setNroId(String nroId) {
		this.nroId = nroId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SadAuxDocentePK)) {
			return false;
		}
		SadAuxDocentePK castOther = (SadAuxDocentePK)other;
		return 
			this.paisId.equals(castOther.paisId)
			&& this.tipoId.equals(castOther.tipoId)
			&& this.nroId.equals(castOther.nroId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.paisId.hashCode();
		hash = hash * prime + this.tipoId.hashCode();
		hash = hash * prime + this.nroId.hashCode();
		
		return hash;
	}
}