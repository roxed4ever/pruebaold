package siafi.ges.bn.bnc.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-10T19:38:26.880-0600")
@StaticMetamodel(CuentasContablePK.class)
public class CuentasContablePK_ {
	public static volatile SingularAttribute<CuentasContablePK, String> cuentaContable;
	public static volatile SingularAttribute<CuentasContablePK, Long> gestion;
}
