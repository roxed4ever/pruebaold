package siafi.ges.bn.bnc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CUENTAS_CONTABLES database table.
 * 
 */
@Entity
@Table(name="CUENTAS_CONTABLES")
@NamedQuery(name="CuentasContable.findAll", query="SELECT c FROM CuentasContable c")
public class CuentasContable implements Serializable {
	private static final long serialVersionUID = 1L;
	private CuentasContablePK id;
	private String apiEstado;
	private String apiTransaccion;
	private String apropiable;
	private String cierre;
	private String descCuentaContable;
	private Date fecCre;
	private Date fecMod;
	private String gasto;
	private String glosaAcreditaPor;
	private String glosaDebitaPor;
	private String glosaSaldo;
	private String ingreso;
	private String legalizable;
	private String nicAntecesor;
	private String nicCuentaContable;
	private Double nicSecuencia;
	private String sinImputacion;
	private String tipoSaldo;
	private String usuCre;
	private String usuMod;
	private String vigente;

	public CuentasContable() {
	}


	@EmbeddedId
	public CuentasContablePK getId() {
		return this.id;
	}

	public void setId(CuentasContablePK id) {
		this.id = id;
	}


	@Column(name="API_ESTADO")
	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}


	@Column(name="API_TRANSACCION")
	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}


	public String getApropiable() {
		return this.apropiable;
	}

	public void setApropiable(String apropiable) {
		this.apropiable = apropiable;
	}


	public String getCierre() {
		return this.cierre;
	}

	public void setCierre(String cierre) {
		this.cierre = cierre;
	}


	@Column(name="DESC_CUENTA_CONTABLE")
	public String getDescCuentaContable() {
		return this.descCuentaContable;
	}

	public void setDescCuentaContable(String descCuentaContable) {
		this.descCuentaContable = descCuentaContable;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}


	public String getGasto() {
		return this.gasto;
	}

	public void setGasto(String gasto) {
		this.gasto = gasto;
	}


	@Column(name="GLOSA_ACREDITA_POR")
	public String getGlosaAcreditaPor() {
		return this.glosaAcreditaPor;
	}

	public void setGlosaAcreditaPor(String glosaAcreditaPor) {
		this.glosaAcreditaPor = glosaAcreditaPor;
	}


	@Column(name="GLOSA_DEBITA_POR")
	public String getGlosaDebitaPor() {
		return this.glosaDebitaPor;
	}

	public void setGlosaDebitaPor(String glosaDebitaPor) {
		this.glosaDebitaPor = glosaDebitaPor;
	}


	@Column(name="GLOSA_SALDO")
	public String getGlosaSaldo() {
		return this.glosaSaldo;
	}

	public void setGlosaSaldo(String glosaSaldo) {
		this.glosaSaldo = glosaSaldo;
	}


	public String getIngreso() {
		return this.ingreso;
	}

	public void setIngreso(String ingreso) {
		this.ingreso = ingreso;
	}


	public String getLegalizable() {
		return this.legalizable;
	}

	public void setLegalizable(String legalizable) {
		this.legalizable = legalizable;
	}


	@Column(name="NIC_ANTECESOR")
	public String getNicAntecesor() {
		return this.nicAntecesor;
	}

	public void setNicAntecesor(String nicAntecesor) {
		this.nicAntecesor = nicAntecesor;
	}


	@Column(name="NIC_CUENTA_CONTABLE")
	public String getNicCuentaContable() {
		return this.nicCuentaContable;
	}

	public void setNicCuentaContable(String nicCuentaContable) {
		this.nicCuentaContable = nicCuentaContable;
	}


	@Column(name="NIC_SECUENCIA")
	public Double getNicSecuencia() {
		return this.nicSecuencia;
	}

	public void setNicSecuencia(Double nicSecuencia) {
		this.nicSecuencia = nicSecuencia;
	}


	@Column(name="SIN_IMPUTACION")
	public String getSinImputacion() {
		return this.sinImputacion;
	}

	public void setSinImputacion(String sinImputacion) {
		this.sinImputacion = sinImputacion;
	}


	@Column(name="TIPO_SALDO")
	public String getTipoSaldo() {
		return this.tipoSaldo;
	}

	public void setTipoSaldo(String tipoSaldo) {
		this.tipoSaldo = tipoSaldo;
	}


	@Column(name="USU_CRE")
	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}


	@Column(name="USU_MOD")
	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}


	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}