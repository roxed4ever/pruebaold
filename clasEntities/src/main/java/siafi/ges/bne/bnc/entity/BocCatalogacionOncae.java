package siafi.ges.bne.bnc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BOC_CATALOGACION_ONCAE database table.
 * 
 */
@Entity
@Table(name="BOC_CATALOGACION_ONCAE")
@NamedQuery(name="BocCatalogacionOncae.findAll", query="SELECT b FROM BocCatalogacionOncae b")
public class BocCatalogacionOncae implements Serializable {
	private static final long serialVersionUID = 1L;
	private String catalogo;
	private String apiEstado;
	private String apiTransaccion;
	private Double clase;
	private String descCatalogo;
	private Double familia;
	private Date fecCre;
	private Date fecMod;
	private Double material;
	private Double segmento;
	private String usuCre;
	private String usuMod;
	private String vigente;
	private List<BocCatoncaeObjeto> bocCatoncaeObjetos;

	public BocCatalogacionOncae() {
	}


	@Id
	public String getCatalogo() {
		return this.catalogo;
	}

	public void setCatalogo(String catalogo) {
		this.catalogo = catalogo;
	}


	@Column(name="API_ESTADO")
	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}


	@Column(name="API_TRANSACCION")
	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}


	public Double getClase() {
		return this.clase;
	}

	public void setClase(Double clase) {
		this.clase = clase;
	}


	@Column(name="DESC_CATALOGO")
	public String getDescCatalogo() {
		return this.descCatalogo;
	}

	public void setDescCatalogo(String descCatalogo) {
		this.descCatalogo = descCatalogo;
	}


	public Double getFamilia() {
		return this.familia;
	}

	public void setFamilia(Double familia) {
		this.familia = familia;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}


	public Double getMaterial() {
		return this.material;
	}

	public void setMaterial(Double material) {
		this.material = material;
	}


	public Double getSegmento() {
		return this.segmento;
	}

	public void setSegmento(Double segmento) {
		this.segmento = segmento;
	}


	@Column(name="USU_CRE")
	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}


	@Column(name="USU_MOD")
	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}


	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}


	//bi-directional many-to-one association to BocCatoncaeObjeto
	@OneToMany(mappedBy="bocCatalogacionOncae")
	public List<BocCatoncaeObjeto> getBocCatoncaeObjetos() {
		return this.bocCatoncaeObjetos;
	}

	public void setBocCatoncaeObjetos(List<BocCatoncaeObjeto> bocCatoncaeObjetos) {
		this.bocCatoncaeObjetos = bocCatoncaeObjetos;
	}

	public BocCatoncaeObjeto addBocCatoncaeObjeto(BocCatoncaeObjeto bocCatoncaeObjeto) {
		getBocCatoncaeObjetos().add(bocCatoncaeObjeto);
		bocCatoncaeObjeto.setBocCatalogacionOncae(this);

		return bocCatoncaeObjeto;
	}

	public BocCatoncaeObjeto removeBocCatoncaeObjeto(BocCatoncaeObjeto bocCatoncaeObjeto) {
		getBocCatoncaeObjetos().remove(bocCatoncaeObjeto);
		bocCatoncaeObjeto.setBocCatalogacionOncae(null);

		return bocCatoncaeObjeto;
	}

}