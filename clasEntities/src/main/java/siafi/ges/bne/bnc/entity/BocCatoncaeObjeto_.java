package siafi.ges.bne.bnc.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-10-25T11:17:11.987-0600")
@StaticMetamodel(BocCatoncaeObjeto.class)
public class BocCatoncaeObjeto_ {
	public static volatile SingularAttribute<BocCatoncaeObjeto, BocCatoncaeObjetoPK> id;
	public static volatile SingularAttribute<BocCatoncaeObjeto, String> apiEstado;
	public static volatile SingularAttribute<BocCatoncaeObjeto, String> apiTransaccion;
	public static volatile SingularAttribute<BocCatoncaeObjeto, Date> fecCre;
	public static volatile SingularAttribute<BocCatoncaeObjeto, Date> fecMod;
	public static volatile SingularAttribute<BocCatoncaeObjeto, String> usuCre;
	public static volatile SingularAttribute<BocCatoncaeObjeto, String> usuMod;
	public static volatile SingularAttribute<BocCatoncaeObjeto, BocCatalogacionOncae> bocCatalogacionOncae;
	public static volatile SingularAttribute<BocCatoncaeObjeto, String> objeto;
}
