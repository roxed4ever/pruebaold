package siafi.ges.bne.bnc.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-10-25T11:17:11.987-0600")
@StaticMetamodel(BocCatoncaeObjetoPK.class)
public class BocCatoncaeObjetoPK_ {
	public static volatile SingularAttribute<BocCatoncaeObjetoPK, String> catalogo;
	public static volatile SingularAttribute<BocCatoncaeObjetoPK, Long> objetoGestion;
}
