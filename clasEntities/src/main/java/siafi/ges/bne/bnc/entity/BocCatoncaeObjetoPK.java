package siafi.ges.bne.bnc.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the BOC_CATONCAE_OBJETOS database table.
 * 
 */
@Embeddable
public class BocCatoncaeObjetoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private String catalogo;
	private long objetoGestion;

	public BocCatoncaeObjetoPK() {
	}

	@Column(insertable=false, updatable=false)
	public String getCatalogo() {
		return this.catalogo;
	}
	public void setCatalogo(String catalogo) {
		this.catalogo = catalogo;
	}

	@Column(name="OBJETO_GESTION", insertable=false, updatable=false)
	public long getObjetoGestion() {
		return this.objetoGestion;
	}
	public void setObjetoGestion(long objetoGestion) {
		this.objetoGestion = objetoGestion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof BocCatoncaeObjetoPK)) {
			return false;
		}
		BocCatoncaeObjetoPK castOther = (BocCatoncaeObjetoPK)other;
		return 
			this.catalogo.equals(castOther.catalogo)
			&& (this.objetoGestion == castOther.objetoGestion);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.catalogo.hashCode();
		hash = hash * prime + ((int) (this.objetoGestion ^ (this.objetoGestion >>> 32)));
		
		return hash;
	}
}