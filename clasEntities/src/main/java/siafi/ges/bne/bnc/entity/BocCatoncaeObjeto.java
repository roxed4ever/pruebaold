package siafi.ges.bne.bnc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BOC_CATONCAE_OBJETOS database table.
 * 
 */
@Entity
@Table(name="BOC_CATONCAE_OBJETOS")
@NamedQuery(name="BocCatoncaeObjeto.findAll", query="SELECT b FROM BocCatoncaeObjeto b")
public class BocCatoncaeObjeto implements Serializable {
	private static final long serialVersionUID = 1L;
	private BocCatoncaeObjetoPK id;
	private String apiEstado;
	private String apiTransaccion;
	private Date fecCre;
	private Date fecMod;
	private String objeto;
	private String usuCre;
	private String usuMod;
	private BocCatalogacionOncae bocCatalogacionOncae;

	public BocCatoncaeObjeto() {
	}


	@EmbeddedId
	public BocCatoncaeObjetoPK getId() {
		return this.id;
	}

	public void setId(BocCatoncaeObjetoPK id) {
		this.id = id;
	}


	@Column(name="API_ESTADO")
	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}


	@Column(name="API_TRANSACCION")
	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}


	public String getObjeto() {
		return this.objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}


	@Column(name="USU_CRE")
	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}


	@Column(name="USU_MOD")
	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}


	//bi-directional many-to-one association to BocCatalogacionOncae
	@ManyToOne
	@JoinColumn(name="CATALOGO")
	public BocCatalogacionOncae getBocCatalogacionOncae() {
		return this.bocCatalogacionOncae;
	}

	public void setBocCatalogacionOncae(BocCatalogacionOncae bocCatalogacionOncae) {
		this.bocCatalogacionOncae = bocCatalogacionOncae;
	}

}