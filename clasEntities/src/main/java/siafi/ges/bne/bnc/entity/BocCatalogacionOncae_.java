package siafi.ges.bne.bnc.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-10-25T11:17:11.971-0600")
@StaticMetamodel(BocCatalogacionOncae.class)
public class BocCatalogacionOncae_ {
	public static volatile SingularAttribute<BocCatalogacionOncae, String> catalogo;
	public static volatile SingularAttribute<BocCatalogacionOncae, String> apiEstado;
	public static volatile SingularAttribute<BocCatalogacionOncae, String> apiTransaccion;
	public static volatile SingularAttribute<BocCatalogacionOncae, String> descCatalogo;
	public static volatile SingularAttribute<BocCatalogacionOncae, Date> fecCre;
	public static volatile SingularAttribute<BocCatalogacionOncae, Date> fecMod;
	public static volatile SingularAttribute<BocCatalogacionOncae, String> usuCre;
	public static volatile SingularAttribute<BocCatalogacionOncae, String> usuMod;
	public static volatile ListAttribute<BocCatalogacionOncae, BocCatoncaeObjeto> bocCatoncaeObjetos;
	public static volatile SingularAttribute<BocCatalogacionOncae, BigDecimal> clase;
	public static volatile SingularAttribute<BocCatalogacionOncae, BigDecimal> familia;
	public static volatile SingularAttribute<BocCatalogacionOncae, BigDecimal> material;
	public static volatile SingularAttribute<BocCatalogacionOncae, BigDecimal> segmento;
	public static volatile SingularAttribute<BocCatalogacionOncae, String> vigente;
}
