package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the INSTITUCIONES database table.
 * 
 */
@Entity
@Table(name="INSTITUCIONES")
@NamedQuery(name="Institucione.findAll", query="SELECT i FROM Instituciones i")
public class Instituciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long institucion;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="BASE_LEGAL")
	private String baseLegal;

	@Column(name="CODIGO_ANTERIOR")
	private Double codigoAnterior;

	@Column(name="DESC_INSTITUCION")
	private String descInstitucion;

	@Column(name="DETALLA_PPTO")
	private String detallaPpto;

	@Column(name="EJECUTA_EGRESOS")
	private String ejecutaEgresos;

	@Column(name="EJECUTA_INGRESOS")
	private String ejecutaIngresos;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="FORMULA_PPTO")
	private String formulaPpto;

	private String gabinete;

	@Column(name="OPERA_CUT")
	private String operaCut;

	private Double rtn;

	@Column(name="SIGLA_INSTITUCION")
	private String siglaInstitucion;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	@Column(name="VIGENTE")
	private String vigente;

	public Instituciones() {
	}

	public long getInstitucion() {
		return this.institucion;
	}

	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getBaseLegal() {
		return this.baseLegal;
	}

	public void setBaseLegal(String baseLegal) {
		this.baseLegal = baseLegal;
	}

	public Double getCodigoAnterior() {
		return this.codigoAnterior;
	}

	public void setCodigoAnterior(Double codigoAnterior) {
		this.codigoAnterior = codigoAnterior;
	}

	public String getDescInstitucion() {
		return this.descInstitucion;
	}

	public void setDescInstitucion(String descInstitucion) {
		this.descInstitucion = descInstitucion;
	}

	public String getDetallaPpto() {
		return this.detallaPpto;
	}

	public void setDetallaPpto(String detallaPpto) {
		this.detallaPpto = detallaPpto;
	}

	public String getEjecutaEgresos() {
		return this.ejecutaEgresos;
	}

	public void setEjecutaEgresos(String ejecutaEgresos) {
		this.ejecutaEgresos = ejecutaEgresos;
	}

	public String getEjecutaIngresos() {
		return this.ejecutaIngresos;
	}

	public void setEjecutaIngresos(String ejecutaIngresos) {
		this.ejecutaIngresos = ejecutaIngresos;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getFormulaPpto() {
		return this.formulaPpto;
	}

	public void setFormulaPpto(String formulaPpto) {
		this.formulaPpto = formulaPpto;
	}

	public String getGabinete() {
		return this.gabinete;
	}

	public void setGabinete(String gabinete) {
		this.gabinete = gabinete;
	}

	public String getOperaCut() {
		return this.operaCut;
	}

	public void setOperaCut(String operaCut) {
		this.operaCut = operaCut;
	}

	public Double getRtn() {
		return this.rtn;
	}

	public void setRtn(Double rtn) {
		this.rtn = rtn;
	}

	public String getSiglaInstitucion() {
		return this.siglaInstitucion;
	}

	public void setSiglaInstitucion(String siglaInstitucion) {
		this.siglaInstitucion = siglaInstitucion;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}