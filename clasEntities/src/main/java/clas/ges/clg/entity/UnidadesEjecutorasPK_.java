package clas.ges.clg.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.808-0600")
@StaticMetamodel(UnidadesEjecutorasPK.class)
public class UnidadesEjecutorasPK_ {
	public static volatile SingularAttribute<UnidadesEjecutorasPK, Long> gestion;
	public static volatile SingularAttribute<UnidadesEjecutorasPK, Long> institucion;
	public static volatile SingularAttribute<UnidadesEjecutorasPK, Long> ue;
}
