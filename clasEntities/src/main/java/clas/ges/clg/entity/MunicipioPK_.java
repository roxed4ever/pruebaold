package clas.ges.clg.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.802-0600")
@StaticMetamodel(MunicipioPK.class)
public class MunicipioPK_ {
	public static volatile SingularAttribute<MunicipioPK, Long> departamento;
	public static volatile SingularAttribute<MunicipioPK, Long> municipio;
}
