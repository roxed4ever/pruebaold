package clas.ges.clg.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-30T17:28:34.365-0600")
@StaticMetamodel(AldeaPK.class)
public class AldeaPK_ {
	public static volatile SingularAttribute<AldeaPK, Long> departamento;
	public static volatile SingularAttribute<AldeaPK, Long> municipio;
	public static volatile SingularAttribute<AldeaPK, Long> aldea;
}
