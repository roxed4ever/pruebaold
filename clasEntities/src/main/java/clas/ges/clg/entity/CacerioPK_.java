package clas.ges.clg.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-30T17:28:34.370-0600")
@StaticMetamodel(CacerioPK.class)
public class CacerioPK_ {
	public static volatile SingularAttribute<CacerioPK, Long> departamento;
	public static volatile SingularAttribute<CacerioPK, Long> municipio;
	public static volatile SingularAttribute<CacerioPK, Long> aldea;
	public static volatile SingularAttribute<CacerioPK, Long> cacerio;
}
