package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.786-0600")
@StaticMetamodel(GabinetesSectoriales.class)
public class GabinetesSectoriales_ {
	public static volatile SingularAttribute<GabinetesSectoriales, GabinetesSectorialesPK> id;
	public static volatile SingularAttribute<GabinetesSectoriales, String> apiEstado;
	public static volatile SingularAttribute<GabinetesSectoriales, String> apiTransaccion;
	public static volatile SingularAttribute<GabinetesSectoriales, String> descGabinete;
	public static volatile SingularAttribute<GabinetesSectoriales, Date> fecCre;
	public static volatile SingularAttribute<GabinetesSectoriales, Date> fecMod;
	public static volatile SingularAttribute<GabinetesSectoriales, Double> institucion;
	public static volatile SingularAttribute<GabinetesSectoriales, String> usuCre;
	public static volatile SingularAttribute<GabinetesSectoriales, String> usuMod;
	public static volatile SingularAttribute<GabinetesSectoriales, String> vigente;
}
