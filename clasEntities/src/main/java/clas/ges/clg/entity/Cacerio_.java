package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-30T17:28:34.366-0600")
@StaticMetamodel(Cacerio.class)
public class Cacerio_ {
	public static volatile SingularAttribute<Cacerio, CacerioPK> id;
	public static volatile SingularAttribute<Cacerio, String> apiEstado;
	public static volatile SingularAttribute<Cacerio, String> apiTransaccion;
	public static volatile SingularAttribute<Cacerio, String> descCacerio;
	public static volatile SingularAttribute<Cacerio, Date> fecCre;
	public static volatile SingularAttribute<Cacerio, Date> fecMod;
	public static volatile SingularAttribute<Cacerio, String> usuCre;
	public static volatile SingularAttribute<Cacerio, String> usuMod;
	public static volatile SingularAttribute<Cacerio, String> vigente;
}
