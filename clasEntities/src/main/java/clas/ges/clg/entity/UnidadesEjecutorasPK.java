package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the UNIDADES_EJECUTORAS database table.
 * 
 */
@Embeddable
public class UnidadesEjecutorasPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	@Column(insertable=false, updatable=false)
	private long institucion;

	private long ue;

	public UnidadesEjecutorasPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getInstitucion() {
		return this.institucion;
	}
	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}
	public long getUe() {
		return this.ue;
	}
	public void setUe(long ue) {
		this.ue = ue;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UnidadesEjecutorasPK)) {
			return false;
		}
		UnidadesEjecutorasPK castOther = (UnidadesEjecutorasPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.institucion == castOther.institucion)
			&& (this.ue == castOther.ue);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.institucion ^ (this.institucion >>> 32)));
		hash = hash * prime + ((int) (this.ue ^ (this.ue >>> 32)));
		
		return hash;
	}
}