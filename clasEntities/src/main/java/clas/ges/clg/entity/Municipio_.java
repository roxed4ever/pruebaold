package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.801-0600")
@StaticMetamodel(Municipio.class)
public class Municipio_ {
	public static volatile SingularAttribute<Municipio, MunicipioPK> id;
	public static volatile SingularAttribute<Municipio, String> apiEstado;
	public static volatile SingularAttribute<Municipio, String> apiTransaccion;
	public static volatile SingularAttribute<Municipio, String> descMunicipio;
	public static volatile SingularAttribute<Municipio, Date> fecCre;
	public static volatile SingularAttribute<Municipio, Date> fecMod;
	public static volatile SingularAttribute<Municipio, String> siglaMunicipio;
	public static volatile SingularAttribute<Municipio, String> usuCre;
	public static volatile SingularAttribute<Municipio, String> usuMod;
	public static volatile SingularAttribute<Municipio, String> vigente;
}
