package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.804-0600")
@StaticMetamodel(Regiones.class)
public class Regiones_ {
	public static volatile SingularAttribute<Regiones, String> region;
	public static volatile SingularAttribute<Regiones, String> apiEstado;
	public static volatile SingularAttribute<Regiones, String> apiTransaccion;
	public static volatile SingularAttribute<Regiones, String> descRegion;
	public static volatile SingularAttribute<Regiones, Date> fecCre;
	public static volatile SingularAttribute<Regiones, Date> fecMod;
	public static volatile SingularAttribute<Regiones, String> usuCre;
	public static volatile SingularAttribute<Regiones, String> usuMod;
	public static volatile SingularAttribute<Regiones, String> vigente;
}
