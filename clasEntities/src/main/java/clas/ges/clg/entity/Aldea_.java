package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-30T17:28:34.361-0600")
@StaticMetamodel(Aldea.class)
public class Aldea_ {
	public static volatile SingularAttribute<Aldea, AldeaPK> id;
	public static volatile SingularAttribute<Aldea, String> apiEstado;
	public static volatile SingularAttribute<Aldea, String> apiTransaccion;
	public static volatile SingularAttribute<Aldea, String> descAldea;
	public static volatile SingularAttribute<Aldea, Date> fecCre;
	public static volatile SingularAttribute<Aldea, Date> fecMod;
	public static volatile SingularAttribute<Aldea, String> usuCre;
	public static volatile SingularAttribute<Aldea, String> usuMod;
	public static volatile SingularAttribute<Aldea, String> vigente;
}
