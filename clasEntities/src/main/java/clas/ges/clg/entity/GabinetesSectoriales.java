package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the GABINETES_SECTORIALES database table.
 * 
 */
@Entity
@Table(name="GABINETES_SECTORIALES")
@NamedQuery(name="GabinetesSectoriale.findAll", query="SELECT g FROM GabinetesSectoriales g")
public class GabinetesSectoriales implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private GabinetesSectorialesPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_GABINETE")
	private String descGabinete;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="INSTITUCION")
	private Double institucion;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	@Column(name="VIGENTE")
	private String vigente;

	public GabinetesSectoriales() {
	}

	public GabinetesSectorialesPK getId() {
		return this.id;
	}

	public void setId(GabinetesSectorialesPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescGabinete() {
		return this.descGabinete;
	}

	public void setDescGabinete(String descGabinete) {
		this.descGabinete = descGabinete;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getInstitucion() {
		return this.institucion;
	}

	public void setInstitucion(Double institucion) {
		this.institucion = institucion;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}