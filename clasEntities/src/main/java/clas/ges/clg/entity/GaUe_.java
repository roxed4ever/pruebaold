package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.790-0600")
@StaticMetamodel(GaUe.class)
public class GaUe_ {
	public static volatile SingularAttribute<GaUe, GaUePK> id;
	public static volatile SingularAttribute<GaUe, String> apiEstado;
	public static volatile SingularAttribute<GaUe, String> apiTransaccion;
	public static volatile SingularAttribute<GaUe, Double> etapaDocumento;
	public static volatile SingularAttribute<GaUe, Date> fecCre;
	public static volatile SingularAttribute<GaUe, Date> fecMod;
	public static volatile SingularAttribute<GaUe, String> usuCre;
	public static volatile SingularAttribute<GaUe, String> usuMod;
	public static volatile SingularAttribute<GaUe, String> vigente;
}
