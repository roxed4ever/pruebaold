package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the MUNICIPIOS database table.
 * 
 */
@Embeddable
public class MunicipioPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long departamento;

	private long municipio;

	public MunicipioPK() {
	}
	public long getDepartamento() {
		return this.departamento;
	}
	public void setDepartamento(long departamento) {
		this.departamento = departamento;
	}
	public long getMunicipio() {
		return this.municipio;
	}
	public void setMunicipio(long municipio) {
		this.municipio = municipio;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MunicipioPK)) {
			return false;
		}
		MunicipioPK castOther = (MunicipioPK)other;
		return 
			(this.departamento == castOther.departamento)
			&& (this.municipio == castOther.municipio);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.departamento ^ (this.departamento >>> 32)));
		hash = hash * prime + ((int) (this.municipio ^ (this.municipio >>> 32)));
		
		return hash;
	}
}