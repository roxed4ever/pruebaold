package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.798-0600")
@StaticMetamodel(Instituciones.class)
public class Instituciones_ {
	public static volatile SingularAttribute<Instituciones, Long> institucion;
	public static volatile SingularAttribute<Instituciones, String> apiEstado;
	public static volatile SingularAttribute<Instituciones, String> apiTransaccion;
	public static volatile SingularAttribute<Instituciones, String> baseLegal;
	public static volatile SingularAttribute<Instituciones, Double> codigoAnterior;
	public static volatile SingularAttribute<Instituciones, String> descInstitucion;
	public static volatile SingularAttribute<Instituciones, String> detallaPpto;
	public static volatile SingularAttribute<Instituciones, String> ejecutaEgresos;
	public static volatile SingularAttribute<Instituciones, String> ejecutaIngresos;
	public static volatile SingularAttribute<Instituciones, Date> fecCre;
	public static volatile SingularAttribute<Instituciones, Date> fecMod;
	public static volatile SingularAttribute<Instituciones, String> formulaPpto;
	public static volatile SingularAttribute<Instituciones, String> gabinete;
	public static volatile SingularAttribute<Instituciones, String> operaCut;
	public static volatile SingularAttribute<Instituciones, Double> rtn;
	public static volatile SingularAttribute<Instituciones, String> siglaInstitucion;
	public static volatile SingularAttribute<Instituciones, String> usuCre;
	public static volatile SingularAttribute<Instituciones, String> usuMod;
	public static volatile SingularAttribute<Instituciones, String> vigente;
}
