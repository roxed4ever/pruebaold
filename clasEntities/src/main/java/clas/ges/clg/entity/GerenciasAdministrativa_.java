package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.795-0600")
@StaticMetamodel(GerenciasAdministrativa.class)
public class GerenciasAdministrativa_ {
	public static volatile SingularAttribute<GerenciasAdministrativa, GerenciasAdministrativaPK> id;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> apiEstado;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> apiTransaccion;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> baseLegal;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> descGa;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> ejecutaEgresos;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> ejecutaIngresos;
	public static volatile SingularAttribute<GerenciasAdministrativa, Double> etapaDocumento;
	public static volatile SingularAttribute<GerenciasAdministrativa, Date> fecCre;
	public static volatile SingularAttribute<GerenciasAdministrativa, Date> fecMod;
	public static volatile SingularAttribute<GerenciasAdministrativa, Double> rtn;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> tipoGa;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> usuCre;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> usuMod;
	public static volatile SingularAttribute<GerenciasAdministrativa, String> vigente;
}
