package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.783-0600")
@StaticMetamodel(Departamento.class)
public class Departamento_ {
	public static volatile SingularAttribute<Departamento, Long> departamento;
	public static volatile SingularAttribute<Departamento, String> apiEstado;
	public static volatile SingularAttribute<Departamento, String> apiTransaccion;
	public static volatile SingularAttribute<Departamento, String> descDepartamento;
	public static volatile SingularAttribute<Departamento, Date> fecCre;
	public static volatile SingularAttribute<Departamento, Date> fecMod;
	public static volatile SingularAttribute<Departamento, String> usuCre;
	public static volatile SingularAttribute<Departamento, String> usuMod;
	public static volatile SingularAttribute<Departamento, String> vigente;
}
