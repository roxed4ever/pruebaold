package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CACERIOS database table.
 * 
 */
@Embeddable
public class CacerioPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long departamento;

	@Column(insertable=false, updatable=false)
	private long municipio;

	private long aldea;

	private long cacerio;

	public CacerioPK() {
	}
	public long getDepartamento() {
		return this.departamento;
	}
	public void setDepartamento(long departamento) {
		this.departamento = departamento;
	}
	public long getMunicipio() {
		return this.municipio;
	}
	public void setMunicipio(long municipio) {
		this.municipio = municipio;
	}
	public long getAldea() {
		return this.aldea;
	}
	public void setAldea(long aldea) {
		this.aldea = aldea;
	}
	public long getCacerio() {
		return this.cacerio;
	}
	public void setCacerio(long cacerio) {
		this.cacerio = cacerio;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CacerioPK)) {
			return false;
		}
		CacerioPK castOther = (CacerioPK)other;
		return 
			(this.departamento == castOther.departamento)
			&& (this.municipio == castOther.municipio)
			&& (this.aldea == castOther.aldea)
			&& (this.cacerio == castOther.cacerio);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.departamento ^ (this.departamento >>> 32)));
		hash = hash * prime + ((int) (this.municipio ^ (this.municipio >>> 32)));
		hash = hash * prime + ((int) (this.aldea ^ (this.aldea >>> 32)));
		hash = hash * prime + ((int) (this.cacerio ^ (this.cacerio >>> 32)));
		
		return hash;
	}
}