package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the MUNICIPIOS database table.
 * 
 */
@Entity
@Table(name="MUNICIPIOS")
@NamedQuery(name="Municipio.findAll", query="SELECT m FROM Municipio m")
public class Municipio implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MunicipioPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_MUNICIPIO")
	private String descMunicipio;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="SIGLA_MUNICIPIO")
	private String siglaMunicipio;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	@Column(name="VIGENTE")
	private String vigente;

	public Municipio() {
	}

	public MunicipioPK getId() {
		return this.id;
	}

	public void setId(MunicipioPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescMunicipio() {
		return this.descMunicipio;
	}

	public void setDescMunicipio(String descMunicipio) {
		this.descMunicipio = descMunicipio;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getSiglaMunicipio() {
		return this.siglaMunicipio;
	}

	public void setSiglaMunicipio(String siglaMunicipio) {
		this.siglaMunicipio = siglaMunicipio;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}