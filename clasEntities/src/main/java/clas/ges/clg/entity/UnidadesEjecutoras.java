package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the UNIDADES_EJECUTORAS database table.
 * 
 */
@Entity
@Table(name="UNIDADES_EJECUTORAS")
@NamedQuery(name="UnidadesEjecutora.findAll", query="SELECT u FROM UnidadesEjecutoras u")
public class UnidadesEjecutoras implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UnidadesEjecutorasPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="BASE_LEGAL")
	private String baseLegal;

	@Column(name="DESC_UE")
	private String descUe;

	@Column(name="EJECUTA_EGRESOS")
	private String ejecutaEgresos;

	@Column(name="EJECUTA_INGRESOS")
	private String ejecutaIngresos;

	@Column(name="ETAPA_DOCUMENTO")
	private Double etapaDocumento;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private Double rtn;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	@Column(name="VIGENTE")
	private String vigente;

	public UnidadesEjecutoras() {
	}

	public UnidadesEjecutorasPK getId() {
		return this.id;
	}

	public void setId(UnidadesEjecutorasPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getBaseLegal() {
		return this.baseLegal;
	}

	public void setBaseLegal(String baseLegal) {
		this.baseLegal = baseLegal;
	}

	public String getDescUe() {
		return this.descUe;
	}

	public void setDescUe(String descUe) {
		this.descUe = descUe;
	}

	public String getEjecutaEgresos() {
		return this.ejecutaEgresos;
	}

	public void setEjecutaEgresos(String ejecutaEgresos) {
		this.ejecutaEgresos = ejecutaEgresos;
	}

	public String getEjecutaIngresos() {
		return this.ejecutaIngresos;
	}

	public void setEjecutaIngresos(String ejecutaIngresos) {
		this.ejecutaIngresos = ejecutaIngresos;
	}

	public Double getEtapaDocumento() {
		return this.etapaDocumento;
	}

	public void setEtapaDocumento(Double etapaDocumento) {
		this.etapaDocumento = etapaDocumento;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getRtn() {
		return this.rtn;
	}

	public void setRtn(Double rtn) {
		this.rtn = rtn;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}