package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T14:49:23.570-0600")
@StaticMetamodel(Cuenca.class)
public class Cuenca_ {
	public static volatile SingularAttribute<Cuenca, String> cuenca;
	public static volatile SingularAttribute<Cuenca, String> apiEstado;
	public static volatile SingularAttribute<Cuenca, String> apiTransaccion;
	public static volatile SingularAttribute<Cuenca, String> descCuenca;
	public static volatile SingularAttribute<Cuenca, Date> fecCre;
	public static volatile SingularAttribute<Cuenca, Date> fecMod;
	public static volatile SingularAttribute<Cuenca, String> usuCre;
	public static volatile SingularAttribute<Cuenca, String> usuMod;
	public static volatile SingularAttribute<Cuenca, String> vigente;
}
