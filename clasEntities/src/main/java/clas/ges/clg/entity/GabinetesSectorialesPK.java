package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the GABINETES_SECTORIALES database table.
 * 
 */
@Embeddable
public class GabinetesSectorialesPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	private long gabinete;

	public GabinetesSectorialesPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getGabinete() {
		return this.gabinete;
	}
	public void setGabinete(long gabinete) {
		this.gabinete = gabinete;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof GabinetesSectorialesPK)) {
			return false;
		}
		GabinetesSectorialesPK castOther = (GabinetesSectorialesPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.gabinete == castOther.gabinete);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.gabinete ^ (this.gabinete >>> 32)));
		
		return hash;
	}
}