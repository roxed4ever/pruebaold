package clas.ges.clg.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ALDEAS database table.
 * 
 */
@Embeddable
public class AldeaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long departamento;

	@Column(insertable=false, updatable=false)
	private long municipio;

	private long aldea;

	public AldeaPK() {
	}
	public long getDepartamento() {
		return this.departamento;
	}
	public void setDepartamento(long departamento) {
		this.departamento = departamento;
	}
	public long getMunicipio() {
		return this.municipio;
	}
	public void setMunicipio(long municipio) {
		this.municipio = municipio;
	}
	public long getAldea() {
		return this.aldea;
	}
	public void setAldea(long aldea) {
		this.aldea = aldea;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AldeaPK)) {
			return false;
		}
		AldeaPK castOther = (AldeaPK)other;
		return 
			(this.departamento == castOther.departamento)
			&& (this.municipio == castOther.municipio)
			&& (this.aldea == castOther.aldea);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.departamento ^ (this.departamento >>> 32)));
		hash = hash * prime + ((int) (this.municipio ^ (this.municipio >>> 32)));
		hash = hash * prime + ((int) (this.aldea ^ (this.aldea >>> 32)));
		
		return hash;
	}
}