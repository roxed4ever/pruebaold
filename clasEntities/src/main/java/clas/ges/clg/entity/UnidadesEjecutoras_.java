package clas.ges.clg.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:02:52.806-0600")
@StaticMetamodel(UnidadesEjecutoras.class)
public class UnidadesEjecutoras_ {
	public static volatile SingularAttribute<UnidadesEjecutoras, UnidadesEjecutorasPK> id;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> apiEstado;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> apiTransaccion;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> baseLegal;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> descUe;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> ejecutaEgresos;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> ejecutaIngresos;
	public static volatile SingularAttribute<UnidadesEjecutoras, Double> etapaDocumento;
	public static volatile SingularAttribute<UnidadesEjecutoras, Date> fecCre;
	public static volatile SingularAttribute<UnidadesEjecutoras, Date> fecMod;
	public static volatile SingularAttribute<UnidadesEjecutoras, Double> rtn;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> usuCre;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> usuMod;
	public static volatile SingularAttribute<UnidadesEjecutoras, String> vigente;
}
