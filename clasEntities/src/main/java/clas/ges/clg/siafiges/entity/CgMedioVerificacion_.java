package clas.ges.clg.siafiges.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-12T10:44:12.211-0600")
@StaticMetamodel(CgMedioVerificacion.class)
public class CgMedioVerificacion_ {
	public static volatile SingularAttribute<CgMedioVerificacion, CgMedioVerificacionPK> id;
	public static volatile SingularAttribute<CgMedioVerificacion, String> descripcion;
	public static volatile SingularAttribute<CgMedioVerificacion, Date> fechaCrea;
	public static volatile SingularAttribute<CgMedioVerificacion, Date> fechaModifica;
	public static volatile SingularAttribute<CgMedioVerificacion, String> nombre;
	public static volatile SingularAttribute<CgMedioVerificacion, String> usuarioCrea;
	public static volatile SingularAttribute<CgMedioVerificacion, String> usuarioModifica;
	public static volatile SingularAttribute<CgMedioVerificacion, String> vigente;
}
