package clas.ges.clg.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CG_CLASIFICACION_POBLACION database table.
 * 
 */
@Entity
@Table(name="CG_CLASIFICACION_POBLACION", schema = "SIAFI")
@NamedQuery(name="CgClasificacionPoblacion.findAll", query="SELECT c FROM CgClasificacionPoblacion c")
public class CgClasificacionPoblacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CgClasificacionPoblacionPK id;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREA")
	private Date fechaCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICA")
	private Date fechaModifica;

	private String nombre;

	@Column(name="USUARIO_CREA")
	private String usuarioCrea;

	@Column(name="USUARIO_MODIFICA")
	private String usuarioModifica;

	private String vigente;

	public CgClasificacionPoblacion() {
	}

	public CgClasificacionPoblacionPK getId() {
		return this.id;
	}

	public void setId(CgClasificacionPoblacionPK id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaCrea() {
		return this.fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuarioCrea() {
		return this.usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}