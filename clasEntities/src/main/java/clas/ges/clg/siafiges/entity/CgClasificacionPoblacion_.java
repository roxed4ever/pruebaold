package clas.ges.clg.siafiges.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-12T10:44:12.195-0600")
@StaticMetamodel(CgClasificacionPoblacion.class)
public class CgClasificacionPoblacion_ {
	public static volatile SingularAttribute<CgClasificacionPoblacion, CgClasificacionPoblacionPK> id;
	public static volatile SingularAttribute<CgClasificacionPoblacion, String> descripcion;
	public static volatile SingularAttribute<CgClasificacionPoblacion, Date> fechaCrea;
	public static volatile SingularAttribute<CgClasificacionPoblacion, Date> fechaModifica;
	public static volatile SingularAttribute<CgClasificacionPoblacion, String> nombre;
	public static volatile SingularAttribute<CgClasificacionPoblacion, String> usuarioCrea;
	public static volatile SingularAttribute<CgClasificacionPoblacion, String> usuarioModifica;
	public static volatile SingularAttribute<CgClasificacionPoblacion, String> vigente;
}
