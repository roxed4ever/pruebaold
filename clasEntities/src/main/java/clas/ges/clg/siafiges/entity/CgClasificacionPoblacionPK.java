package clas.ges.clg.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CG_CLASIFICACION_POBLACION database table.
 * 
 */
@Embeddable
public class CgClasificacionPoblacionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	private long clasificacion;

	public CgClasificacionPoblacionPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getClasificacion() {
		return this.clasificacion;
	}
	public void setClasificacion(long clasificacion) {
		this.clasificacion = clasificacion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CgClasificacionPoblacionPK)) {
			return false;
		}
		CgClasificacionPoblacionPK castOther = (CgClasificacionPoblacionPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.clasificacion == castOther.clasificacion);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.clasificacion ^ (this.clasificacion >>> 32)));
		
		return hash;
	}
}