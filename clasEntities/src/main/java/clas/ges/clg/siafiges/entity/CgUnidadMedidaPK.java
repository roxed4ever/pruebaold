package clas.ges.clg.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CG_UNIDAD_MEDIDA database table.
 * 
 */
@Embeddable
public class CgUnidadMedidaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	@Column(name="UNIDAD_MEDIDA")
	private long unidadMedida;

	public CgUnidadMedidaPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getUnidadMedida() {
		return this.unidadMedida;
	}
	public void setUnidadMedida(long unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CgUnidadMedidaPK)) {
			return false;
		}
		CgUnidadMedidaPK castOther = (CgUnidadMedidaPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.unidadMedida == castOther.unidadMedida);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.unidadMedida ^ (this.unidadMedida >>> 32)));
		
		return hash;
	}
}