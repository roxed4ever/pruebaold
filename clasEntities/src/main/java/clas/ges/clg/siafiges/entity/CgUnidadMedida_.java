package clas.ges.clg.siafiges.entity;


import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-12T10:44:12.602-0600")
@StaticMetamodel(CgUnidadMedida.class)
public class CgUnidadMedida_ {
	public static volatile SingularAttribute<CgUnidadMedida, CgUnidadMedidaPK> id;
	public static volatile SingularAttribute<CgUnidadMedida, Date> fechaCrea;
	public static volatile SingularAttribute<CgUnidadMedida, Date> fechaModifica;
	public static volatile SingularAttribute<CgUnidadMedida, String> nombre;
	public static volatile SingularAttribute<CgUnidadMedida, String> simbolo;
	public static volatile SingularAttribute<CgUnidadMedida, Double> tipoUnidad;
	public static volatile SingularAttribute<CgUnidadMedida, String> usuarioCrea;
	public static volatile SingularAttribute<CgUnidadMedida, String> usuarioModifica;
	public static volatile SingularAttribute<CgUnidadMedida, String> vigente;
}
