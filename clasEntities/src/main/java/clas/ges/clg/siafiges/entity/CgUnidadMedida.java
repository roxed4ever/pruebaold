package clas.ges.clg.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CG_UNIDAD_MEDIDA database table.
 * 
 */
@Entity
@Table(name="CG_UNIDAD_MEDIDA", schema = "SIAFI")
@NamedQuery(name="CgUnidadMedida.findAll", query="SELECT c FROM CgUnidadMedida c")
public class CgUnidadMedida implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CgUnidadMedidaPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREA")
	private Date fechaCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICA")
	private Date fechaModifica;

	private String nombre;

	private String simbolo;

	@Column(name="TIPO_UNIDAD")
	private Double tipoUnidad;

	@Column(name="USUARIO_CREA")
	private String usuarioCrea;

	@Column(name="USUARIO_MODIFICA")
	private String usuarioModifica;

	private String vigente;

	public CgUnidadMedida() {
	}

	public CgUnidadMedidaPK getId() {
		return this.id;
	}

	public void setId(CgUnidadMedidaPK id) {
		this.id = id;
	}

	public Date getFechaCrea() {
		return this.fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSimbolo() {
		return this.simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	public Double getTipoUnidad() {
		return this.tipoUnidad;
	}

	public void setTipoUnidad(Double tipoUnidad) {
		this.tipoUnidad = tipoUnidad;
	}

	public String getUsuarioCrea() {
		return this.usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}