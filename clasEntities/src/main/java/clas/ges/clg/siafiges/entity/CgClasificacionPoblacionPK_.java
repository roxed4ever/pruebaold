package clas.ges.clg.siafiges.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-12T10:44:12.211-0600")
@StaticMetamodel(CgClasificacionPoblacionPK.class)
public class CgClasificacionPoblacionPK_ {
	public static volatile SingularAttribute<CgClasificacionPoblacionPK, Long> gestion;
	public static volatile SingularAttribute<CgClasificacionPoblacionPK, Long> clasificacion;
}
