package clas.ges.clg.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CG_MEDIO_VERIFICACION database table.
 * 
 */
@Embeddable
public class CgMedioVerificacionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	@Column(name="MEDIO_VERIFICACION")
	private long medioVerificacion;

	public CgMedioVerificacionPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getMedioVerificacion() {
		return this.medioVerificacion;
	}
	public void setMedioVerificacion(long medioVerificacion) {
		this.medioVerificacion = medioVerificacion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CgMedioVerificacionPK)) {
			return false;
		}
		CgMedioVerificacionPK castOther = (CgMedioVerificacionPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.medioVerificacion == castOther.medioVerificacion);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.medioVerificacion ^ (this.medioVerificacion >>> 32)));
		
		return hash;
	}
}