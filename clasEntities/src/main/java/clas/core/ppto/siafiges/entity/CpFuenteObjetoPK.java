package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CP_FUENTE_OBJETO database table.
 * 
 */
@Embeddable
public class CpFuenteObjetoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	private long fuente;

	@Column(name="OBJETO_GASTO")
	private long objetoGasto;

	@Column(name="SECTOR_PUBLICO")
	private long sectorPublico;

	public CpFuenteObjetoPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getFuente() {
		return this.fuente;
	}
	public void setFuente(long fuente) {
		this.fuente = fuente;
	}
	public long getObjetoGasto() {
		return this.objetoGasto;
	}
	public void setObjetoGasto(long objetoGasto) {
		this.objetoGasto = objetoGasto;
	}
	public long getSectorPublico() {
		return this.sectorPublico;
	}
	public void setSectorPublico(long sectorPublico) {
		this.sectorPublico = sectorPublico;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CpFuenteObjetoPK)) {
			return false;
		}
		CpFuenteObjetoPK castOther = (CpFuenteObjetoPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.fuente == castOther.fuente)
			&& (this.objetoGasto == castOther.objetoGasto)
			&& (this.sectorPublico == castOther.sectorPublico);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.fuente ^ (this.fuente >>> 32)));
		hash = hash * prime + ((int) (this.objetoGasto ^ (this.objetoGasto >>> 32)));
		hash = hash * prime + ((int) (this.sectorPublico ^ (this.sectorPublico >>> 32)));
		
		return hash;
	}
}