package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CP_ECONOMICO database table.
 * 
 */
@Embeddable
public class CpEconomicoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	private long economico;

	public CpEconomicoPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getEconomico() {
		return this.economico;
	}
	public void setEconomico(long economico) {
		this.economico = economico;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CpEconomicoPK)) {
			return false;
		}
		CpEconomicoPK castOther = (CpEconomicoPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.economico == castOther.economico);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.economico ^ (this.economico >>> 32)));
		
		return hash;
	}
}