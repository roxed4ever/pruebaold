package clas.core.ppto.siafiges.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-11T11:18:01.634-0600")
@StaticMetamodel(CpFuenteObjetoPK.class)
public class CpFuenteObjetoPK_ {
	public static volatile SingularAttribute<CpFuenteObjetoPK, Long> gestion;
	public static volatile SingularAttribute<CpFuenteObjetoPK, Long> fuente;
	public static volatile SingularAttribute<CpFuenteObjetoPK, Long> objetoGasto;
	public static volatile SingularAttribute<CpFuenteObjetoPK, Long> sectorPublico;
}
