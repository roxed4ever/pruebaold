package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CP_MATRIZ_ECONOMICA database table.
 * 
 */
@Entity
@Table(name="CP_MATRIZ_ECONOMICA", schema = "SIAFI")
@NamedQuery(name="CpMatrizEconomica.findAll", query="SELECT c FROM CpMatrizEconomica c")
public class CpMatrizEconomica implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CpMatrizEconomicaPK id;

	private Double economico;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREA")
	private Date fechaCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICA")
	private Date fechaModifica;

	@Column(name="OBJETO_GASTO_FIN")
	private Double objetoGastoFin;

	@Column(name="OBJETO_GASTO_INI")
	private Double objetoGastoIni;

	@Column(name="RUBRO_FIN")
	private Double rubroFin;

	@Column(name="RUBRO_INI")
	private Double rubroIni;

	@Column(name="SECTOR_PUBLICO")
	private Double sectorPublico;

	@Column(name="TIENE_PROYECTO")
	private String tieneProyecto;

	@Column(name="TIPO_ADMINISTRACION")
	private String tipoAdministracion;

	@Column(name="TIPO_PROYECTO")
	private Double tipoProyecto;

	@Column(name="USUARIO_CREA")
	private String usuarioCrea;

	@Column(name="USUARIO_MODIFICA")
	private String usuarioModifica;

	private String vigente;

	public CpMatrizEconomica() {
	}

	public CpMatrizEconomicaPK getId() {
		return this.id;
	}

	public void setId(CpMatrizEconomicaPK id) {
		this.id = id;
	}

	public Double getEconomico() {
		return this.economico;
	}

	public void setEconomico(Double economico) {
		this.economico = economico;
	}

	public Date getFechaCrea() {
		return this.fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public Double getObjetoGastoFin() {
		return this.objetoGastoFin;
	}

	public void setObjetoGastoFin(Double objetoGastoFin) {
		this.objetoGastoFin = objetoGastoFin;
	}

	public Double getObjetoGastoIni() {
		return this.objetoGastoIni;
	}

	public void setObjetoGastoIni(Double objetoGastoIni) {
		this.objetoGastoIni = objetoGastoIni;
	}

	public Double getRubroFin() {
		return this.rubroFin;
	}

	public void setRubroFin(Double rubroFin) {
		this.rubroFin = rubroFin;
	}

	public Double getRubroIni() {
		return this.rubroIni;
	}

	public void setRubroIni(Double rubroIni) {
		this.rubroIni = rubroIni;
	}

	public Double getSectorPublico() {
		return this.sectorPublico;
	}

	public void setSectorPublico(Double sectorPublico) {
		this.sectorPublico = sectorPublico;
	}

	public String getTieneProyecto() {
		return this.tieneProyecto;
	}

	public void setTieneProyecto(String tieneProyecto) {
		this.tieneProyecto = tieneProyecto;
	}

	public String getTipoAdministracion() {
		return this.tipoAdministracion;
	}

	public void setTipoAdministracion(String tipoAdministracion) {
		this.tipoAdministracion = tipoAdministracion;
	}

	public Double getTipoProyecto() {
		return this.tipoProyecto;
	}

	public void setTipoProyecto(Double tipoProyecto) {
		this.tipoProyecto = tipoProyecto;
	}

	public String getUsuarioCrea() {
		return this.usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}