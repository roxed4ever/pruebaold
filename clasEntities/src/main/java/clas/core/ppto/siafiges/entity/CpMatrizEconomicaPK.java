package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CP_MATRIZ_ECONOMICA database table.
 * 
 */
@Embeddable
public class CpMatrizEconomicaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long gestion;

	@Column(name="GASTO_INGRESO")
	private String gastoIngreso;

	private long secuencia;

	public CpMatrizEconomicaPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public String getGastoIngreso() {
		return this.gastoIngreso;
	}
	public void setGastoIngreso(String gastoIngreso) {
		this.gastoIngreso = gastoIngreso;
	}
	public long getSecuencia() {
		return this.secuencia;
	}
	public void setSecuencia(long secuencia) {
		this.secuencia = secuencia;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CpMatrizEconomicaPK)) {
			return false;
		}
		CpMatrizEconomicaPK castOther = (CpMatrizEconomicaPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& this.gastoIngreso.equals(castOther.gastoIngreso)
			&& (this.secuencia == castOther.secuencia);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + this.gastoIngreso.hashCode();
		hash = hash * prime + ((int) (this.secuencia ^ (this.secuencia >>> 32)));
		
		return hash;
	}
}