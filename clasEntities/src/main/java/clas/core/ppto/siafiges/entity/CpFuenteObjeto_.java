package clas.core.ppto.siafiges.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-11T11:18:01.633-0600")
@StaticMetamodel(CpFuenteObjeto.class)
public class CpFuenteObjeto_ {
	public static volatile SingularAttribute<CpFuenteObjeto, CpFuenteObjetoPK> id;
	public static volatile SingularAttribute<CpFuenteObjeto, String> excepcion;
	public static volatile SingularAttribute<CpFuenteObjeto, Date> fechaCrea;
	public static volatile SingularAttribute<CpFuenteObjeto, Date> fechaModifica;
	public static volatile SingularAttribute<CpFuenteObjeto, String> usuarioCrea;
	public static volatile SingularAttribute<CpFuenteObjeto, String> usuarioModifica;
	public static volatile SingularAttribute<CpFuenteObjeto, String> vigente;
}
