package clas.core.ppto.siafiges.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-11T11:18:01.639-0600")
@StaticMetamodel(CpMatrizEconomicaPK.class)
public class CpMatrizEconomicaPK_ {
	public static volatile SingularAttribute<CpMatrizEconomicaPK, Long> gestion;
	public static volatile SingularAttribute<CpMatrizEconomicaPK, String> gastoIngreso;
	public static volatile SingularAttribute<CpMatrizEconomicaPK, Long> secuencia;
}
