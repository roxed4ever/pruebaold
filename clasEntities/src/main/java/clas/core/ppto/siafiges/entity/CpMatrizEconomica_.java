package clas.core.ppto.siafiges.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-11T11:18:01.636-0600")
@StaticMetamodel(CpMatrizEconomica.class)
public class CpMatrizEconomica_ {
	public static volatile SingularAttribute<CpMatrizEconomica, CpMatrizEconomicaPK> id;
	public static volatile SingularAttribute<CpMatrizEconomica, Double> economico;
	public static volatile SingularAttribute<CpMatrizEconomica, Date> fechaCrea;
	public static volatile SingularAttribute<CpMatrizEconomica, Date> fechaModifica;
	public static volatile SingularAttribute<CpMatrizEconomica, Double> objetoGastoFin;
	public static volatile SingularAttribute<CpMatrizEconomica, Double> objetoGastoIni;
	public static volatile SingularAttribute<CpMatrizEconomica, Double> rubroFin;
	public static volatile SingularAttribute<CpMatrizEconomica, Double> rubroIni;
	public static volatile SingularAttribute<CpMatrizEconomica, Double> sectorPublico;
	public static volatile SingularAttribute<CpMatrizEconomica, String> tieneProyecto;
	public static volatile SingularAttribute<CpMatrizEconomica, String> tipoAdministracion;
	public static volatile SingularAttribute<CpMatrizEconomica, Double> tipoProyecto;
	public static volatile SingularAttribute<CpMatrizEconomica, String> usuarioCrea;
	public static volatile SingularAttribute<CpMatrizEconomica, String> usuarioModifica;
	public static volatile SingularAttribute<CpMatrizEconomica, String> vigente;
}
