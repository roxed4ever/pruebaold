package clas.core.ppto.siafiges.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-11T11:18:01.640-0600")
@StaticMetamodel(CpPolitica.class)
public class CpPolitica_ {
	public static volatile SingularAttribute<CpPolitica, CpPoliticaPK> id;
	public static volatile SingularAttribute<CpPolitica, String> descripcion;
	public static volatile SingularAttribute<CpPolitica, Date> fechaCrea;
	public static volatile SingularAttribute<CpPolitica, Date> fechaModifica;
	public static volatile SingularAttribute<CpPolitica, String> imputable;
	public static volatile SingularAttribute<CpPolitica, Double> nivel;
	public static volatile SingularAttribute<CpPolitica, String> usuarioCrea;
	public static volatile SingularAttribute<CpPolitica, String> usuarioModifica;
	public static volatile SingularAttribute<CpPolitica, String> vigente;
}
