package clas.core.ppto.siafiges.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-11T11:18:01.625-0600")
@StaticMetamodel(CpEconomico.class)
public class CpEconomico_ {
	public static volatile SingularAttribute<CpEconomico, CpEconomicoPK> id;
	public static volatile SingularAttribute<CpEconomico, Date> fechaCrea;
	public static volatile SingularAttribute<CpEconomico, Date> fechaModifica;
	public static volatile SingularAttribute<CpEconomico, String> gastoIngreso;
	public static volatile SingularAttribute<CpEconomico, String> imputable;
	public static volatile SingularAttribute<CpEconomico, String> nombre;
	public static volatile SingularAttribute<CpEconomico, String> usuarioCrea;
	public static volatile SingularAttribute<CpEconomico, String> usuarioModifica;
	public static volatile SingularAttribute<CpEconomico, String> vigente;
}
