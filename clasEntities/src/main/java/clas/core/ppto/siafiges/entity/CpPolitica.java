package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CP_POLITICA database table.
 * 
 */
@Entity
@Table(name="CP_POLITICA", schema = "SIAFI")
@NamedQuery(name="CpPolitica.findAll", query="SELECT c FROM CpPolitica c")
public class CpPolitica implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CpPoliticaPK id;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREA")
	private Date fechaCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICA")
	private Date fechaModifica;

	private String imputable;

	private Double nivel;

	@Column(name="USUARIO_CREA")
	private String usuarioCrea;

	@Column(name="USUARIO_MODIFICA")
	private String usuarioModifica;

	private String vigente;

	public CpPolitica() {
	}

	public CpPoliticaPK getId() {
		return this.id;
	}

	public void setId(CpPoliticaPK id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFechaCrea() {
		return this.fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public String getImputable() {
		return this.imputable;
	}

	public void setImputable(String imputable) {
		this.imputable = imputable;
	}

	public Double getNivel() {
		return this.nivel;
	}

	public void setNivel(Double nivel) {
		this.nivel = nivel;
	}

	public String getUsuarioCrea() {
		return this.usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}