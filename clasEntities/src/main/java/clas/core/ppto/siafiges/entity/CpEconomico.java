package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CP_ECONOMICO database table.
 * 
 */
@Entity
@Table(name="CP_ECONOMICO", schema = "SIAFI")
@NamedQuery(name="CpEconomico.findAll", query="SELECT c FROM CpEconomico c")
public class CpEconomico implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CpEconomicoPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREA")
	private Date fechaCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICA")
	private Date fechaModifica;

	@Column(name="GASTO_INGRESO")
	private String gastoIngreso;

	private String imputable;

	private String nombre;

	@Column(name="USUARIO_CREA")
	private String usuarioCrea;

	@Column(name="USUARIO_MODIFICA")
	private String usuarioModifica;

	private String vigente;

	public CpEconomico() {
	}

	public CpEconomicoPK getId() {
		return this.id;
	}

	public void setId(CpEconomicoPK id) {
		this.id = id;
	}

	public Date getFechaCrea() {
		return this.fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public String getGastoIngreso() {
		return this.gastoIngreso;
	}

	public void setGastoIngreso(String gastoIngreso) {
		this.gastoIngreso = gastoIngreso;
	}

	public String getImputable() {
		return this.imputable;
	}

	public void setImputable(String imputable) {
		this.imputable = imputable;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuarioCrea() {
		return this.usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}