package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CP_POLITICA database table.
 * 
 */
@Embeddable
public class CpPoliticaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	private long politica;

	public CpPoliticaPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getPolitica() {
		return this.politica;
	}
	public void setPolitica(long politica) {
		this.politica = politica;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CpPoliticaPK)) {
			return false;
		}
		CpPoliticaPK castOther = (CpPoliticaPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.politica == castOther.politica);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.politica ^ (this.politica >>> 32)));
		
		return hash;
	}
}