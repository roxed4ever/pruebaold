package clas.core.ppto.siafiges.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CP_FUENTE_OBJETO database table.
 * 
 */
@Entity
@Table(name="CP_FUENTE_OBJETO", schema = "SIAFI")
@NamedQuery(name="CpFuenteObjeto.findAll", query="SELECT c FROM CpFuenteObjeto c")
public class CpFuenteObjeto implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CpFuenteObjetoPK id;

	private String excepcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CREA")
	private Date fechaCrea;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_MODIFICA")
	private Date fechaModifica;

	@Column(name="USUARIO_CREA")
	private String usuarioCrea;

	@Column(name="USUARIO_MODIFICA")
	private String usuarioModifica;

	private String vigente;

	public CpFuenteObjeto() {
	}

	public CpFuenteObjetoPK getId() {
		return this.id;
	}

	public void setId(CpFuenteObjetoPK id) {
		this.id = id;
	}

	public String getExcepcion() {
		return this.excepcion;
	}

	public void setExcepcion(String excepcion) {
		this.excepcion = excepcion;
	}

	public Date getFechaCrea() {
		return this.fechaCrea;
	}

	public void setFechaCrea(Date fechaCrea) {
		this.fechaCrea = fechaCrea;
	}

	public Date getFechaModifica() {
		return this.fechaModifica;
	}

	public void setFechaModifica(Date fechaModifica) {
		this.fechaModifica = fechaModifica;
	}

	public String getUsuarioCrea() {
		return this.usuarioCrea;
	}

	public void setUsuarioCrea(String usuarioCrea) {
		this.usuarioCrea = usuarioCrea;
	}

	public String getUsuarioModifica() {
		return this.usuarioModifica;
	}

	public void setUsuarioModifica(String usuarioModifica) {
		this.usuarioModifica = usuarioModifica;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}