package clas.core.ppto.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.528-0600")
@StaticMetamodel(SubPrograma.class)
public class SubPrograma_ {
	public static volatile SingularAttribute<SubPrograma, SubProgramaPK> id;
	public static volatile SingularAttribute<SubPrograma, String> apiEstado;
	public static volatile SingularAttribute<SubPrograma, String> apiTransaccion;
	public static volatile SingularAttribute<SubPrograma, String> descSubPrograma;
	public static volatile SingularAttribute<SubPrograma, BigDecimal> etapaDocumento;
	public static volatile SingularAttribute<SubPrograma, Date> fecCre;
	public static volatile SingularAttribute<SubPrograma, Date> fecMod;
	public static volatile SingularAttribute<SubPrograma, String> sinProduccion;
	public static volatile SingularAttribute<SubPrograma, String> usuCre;
	public static volatile SingularAttribute<SubPrograma, String> usuMod;
	public static volatile SingularAttribute<SubPrograma, String> vigente;
	public static volatile ListAttribute<SubPrograma, Proyecto> proyectos;
}
