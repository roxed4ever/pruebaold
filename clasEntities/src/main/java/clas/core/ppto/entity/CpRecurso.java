package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the CP_RECURSO database table.
 * 
 */
@Entity
@Table(name="CP_RECURSO", schema="SAMI")
@NamedQuery(name="CpRecurso.findAll", query="SELECT c FROM CpRecurso c")
public class CpRecurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_RECURSO")
	private Long idRecurso;

	private String descripcion;

	private Long ejercicio;

	private String imputable;

	private Long nivel;

	@Column(name="POR_FUNCIONAMIENTO")
	private Long porFuncionamiento;

	@Column(name="POR_INVERSION")
	private Long porInversion;

	private String recurso;

	private String restrictiva;

	@Column(name="TIPO_SERVICIO")
	private Long tipoServicio;

	//bi-directional many-to-one association to CpRecurso
	@ManyToOne
	@JoinColumn(name="ID_RECURSO_PADRE")
	private CpRecurso cpRecurso;

	//bi-directional many-to-one association to CpRecurso
	@OneToMany(mappedBy="cpRecurso")
	private List<CpRecurso> cpRecursos;

	public CpRecurso() {
	}

	public long getIdRecurso() {
		return this.idRecurso;
	}

	public void setIdRecurso(long idRecurso) {
		this.idRecurso = idRecurso;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getEjercicio() {
		return this.ejercicio;
	}

	public void setEjercicio(Long ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getImputable() {
		return this.imputable;
	}

	public void setImputable(String imputable) {
		this.imputable = imputable;
	}

	public Long getNivel() {
		return this.nivel;
	}

	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}

	public Long getPorFuncionamiento() {
		return this.porFuncionamiento;
	}

	public void setPorFuncionamiento(Long porFuncionamiento) {
		this.porFuncionamiento = porFuncionamiento;
	}

	public Long getPorInversion() {
		return this.porInversion;
	}

	public void setPorInversion(Long porInversion) {
		this.porInversion = porInversion;
	}

	public String getRecurso() {
		return this.recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public String getRestrictiva() {
		return this.restrictiva;
	}

	public void setRestrictiva(String restrictiva) {
		this.restrictiva = restrictiva;
	}

	public Long getTipoServicio() {
		return this.tipoServicio;
	}

	public void setTipoServicio(Long tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	public CpRecurso getCpRecurso() {
		return this.cpRecurso;
	}

	public void setCpRecurso(CpRecurso cpRecurso) {
		this.cpRecurso = cpRecurso;
	}

	public List<CpRecurso> getCpRecursos() {
		return this.cpRecursos;
	}

	public void setCpRecursos(List<CpRecurso> cpRecursos) {
		this.cpRecursos = cpRecursos;
	}

	public CpRecurso addCpRecurso(CpRecurso cpRecurso) {
		getCpRecursos().add(cpRecurso);
		cpRecurso.setCpRecurso(this);

		return cpRecurso;
	}

	public CpRecurso removeCpRecurso(CpRecurso cpRecurso) {
		getCpRecursos().remove(cpRecurso);
		cpRecurso.setCpRecurso(null);

		return cpRecurso;
	}
	
	

}