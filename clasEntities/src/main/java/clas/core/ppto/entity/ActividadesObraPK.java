package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ACTIVIDADES_OBRAS database table.
 * 
 */
@Embeddable
public class ActividadesObraPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long gestion;

	@Column(insertable=false, updatable=false)
	private long institucion;

	@Column(insertable=false, updatable=false)
	private long programa;

	@Column(name="SUB_PROGRAMA", insertable=false, updatable=false)
	private long subPrograma;

	@Column(insertable=false, updatable=false)
	private long proyecto;

	@Column(name="ACTIVIDAD_OBRA")
	private long actividadObra;

	public ActividadesObraPK() {
	}
	
	
	public ActividadesObraPK(long gestion, long institucion, long programa, long subPrograma, long proyecto,
			long actividadObra) {
		super();
		this.gestion = gestion;
		this.institucion = institucion;
		this.programa = programa;
		this.subPrograma = subPrograma;
		this.proyecto = proyecto;
		this.actividadObra = actividadObra;
	}


	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getInstitucion() {
		return this.institucion;
	}
	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}
	public long getPrograma() {
		return this.programa;
	}
	public void setPrograma(long programa) {
		this.programa = programa;
	}
	public long getSubPrograma() {
		return this.subPrograma;
	}
	public void setSubPrograma(long subPrograma) {
		this.subPrograma = subPrograma;
	}
	public long getProyecto() {
		return this.proyecto;
	}
	public void setProyecto(long proyecto) {
		this.proyecto = proyecto;
	}
	public long getActividadObra() {
		return this.actividadObra;
	}
	public void setActividadObra(long actividadObra) {
		this.actividadObra = actividadObra;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ActividadesObraPK)) {
			return false;
		}
		ActividadesObraPK castOther = (ActividadesObraPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.institucion == castOther.institucion)
			&& (this.programa == castOther.programa)
			&& (this.subPrograma == castOther.subPrograma)
			&& (this.proyecto == castOther.proyecto)
			&& (this.actividadObra == castOther.actividadObra);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.institucion ^ (this.institucion >>> 32)));
		hash = hash * prime + ((int) (this.programa ^ (this.programa >>> 32)));
		hash = hash * prime + ((int) (this.subPrograma ^ (this.subPrograma >>> 32)));
		hash = hash * prime + ((int) (this.proyecto ^ (this.proyecto >>> 32)));
		hash = hash * prime + ((int) (this.actividadObra ^ (this.actividadObra >>> 32)));
		
		return hash;
	}
}