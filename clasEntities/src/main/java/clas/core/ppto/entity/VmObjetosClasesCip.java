package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the VM_OBJETOS_CLASES_CIP database table.
 * 
 */
@Entity
@Table(name="VM_OBJETOS_CLASES_CIP")
@NamedQuery(name="VmObjetosClasesCip.findAll", query="SELECT v FROM VmObjetosClasesCip v")
public class VmObjetosClasesCip implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private VmObjetosClasesCipPK vmId;
	
	@Column(name="CLASE_DE_GASTO")
	private Double claseDeGasto;

	@Column(name="DESC_CLASE_DE_GASTO")
	private String descClaseDeGasto;


	public VmObjetosClasesCip() {
	}

	public Double getClaseDeGasto() {
		return this.claseDeGasto;
	}

	public void setClaseDeGasto(Double claseDeGasto) {
		this.claseDeGasto = claseDeGasto;
	}

	public String getDescClaseDeGasto() {
		return this.descClaseDeGasto;
	}

	public void setDescClaseDeGasto(String descClaseDeGasto) {
		this.descClaseDeGasto = descClaseDeGasto;
	}

	public VmObjetosClasesCipPK getVmId() {
		return vmId;
	}

	public void setVmId(VmObjetosClasesCipPK vmId) {
		this.vmId = vmId;
	}

}