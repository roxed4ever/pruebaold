package clas.core.ppto.entity;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.528-0600")
@StaticMetamodel(ObjetosDelGasto.class)
public class ObjetosDelGasto_ {
	
	public static volatile SingularAttribute<ObjetosDelGasto, ObjetosDelGastoPK> id;
	public static volatile SingularAttribute<ObjetosDelGasto, String> apiEstado;
	public static volatile SingularAttribute<ObjetosDelGasto, String> apiTransaccion;
	public static volatile SingularAttribute<ObjetosDelGasto, String> descObjeto;
	public static volatile SingularAttribute<ObjetosDelGasto, Date> fecCre;
	public static volatile SingularAttribute<ObjetosDelGasto, Date> fecMod;
	public static volatile SingularAttribute<ObjetosDelGasto, Double> grupoObjeto;
	public static volatile SingularAttribute<ObjetosDelGasto, String> imputable;
	public static volatile SingularAttribute<ObjetosDelGasto, String> instExcepcionErp;
	public static volatile SingularAttribute<ObjetosDelGasto, String> instExcepcionMpr;
	public static volatile SingularAttribute<ObjetosDelGasto, String> noAumentaErp;
	public static volatile SingularAttribute<ObjetosDelGasto, String> noAumentaMpr;
	public static volatile SingularAttribute<ObjetosDelGasto, String> noDisminuyeErp;
	public static volatile SingularAttribute<ObjetosDelGasto, String> noDisminuyeMpr;
	public static volatile SingularAttribute<ObjetosDelGasto, Double> partidaObjeto;
	public static volatile SingularAttribute<ObjetosDelGasto, Double> subGrupoObjeto;
	public static volatile SingularAttribute<ObjetosDelGasto, Double> subPartidaObjeto;
	public static volatile SingularAttribute<ObjetosDelGasto, String> trasladoIntegroMpr;
	public static volatile SingularAttribute<ObjetosDelGasto, String> usuCre;
	public static volatile SingularAttribute<ObjetosDelGasto, String> usuMod;
	public static volatile SingularAttribute<ObjetosDelGasto, String> vigente;
	public static volatile SingularAttribute<ObjetosDelGasto, VmObjetosClasesCip> vmObjetosClasesCipBean;

}
