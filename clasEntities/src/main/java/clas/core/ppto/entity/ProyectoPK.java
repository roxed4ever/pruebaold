package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PROYECTOS database table.
 * 
 */
@Embeddable
public class ProyectoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long gestion;

	@Column(insertable=false, updatable=false)
	private long institucion;

	@Column(insertable=false, updatable=false)
	private long programa;

	@Column(name="SUB_PROGRAMA", insertable=false, updatable=false)
	private long subPrograma;
	
	private long proyecto;

	public ProyectoPK() {
	}
	
	
	public ProyectoPK(long gestion, long institucion, long programa, long subPrograma, long proyecto) {
		super();
		this.gestion = gestion;
		this.institucion = institucion;
		this.programa = programa;
		this.subPrograma = subPrograma;
		this.proyecto = proyecto;
	}


	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getInstitucion() {
		return this.institucion;
	}
	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}
	public long getPrograma() {
		return this.programa;
	}
	public void setPrograma(long programa) {
		this.programa = programa;
	}
	public long getSubPrograma() {
		return this.subPrograma;
	}
	public void setSubPrograma(long subPrograma) {
		this.subPrograma = subPrograma;
	}
	public long getProyecto() {
		return this.proyecto;
	}
	public void setProyecto(long proyecto) {
		this.proyecto = proyecto;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ProyectoPK)) {
			return false;
		}
		ProyectoPK castOther = (ProyectoPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.institucion == castOther.institucion)
			&& (this.programa == castOther.programa)
			&& (this.subPrograma == castOther.subPrograma)
			&& (this.proyecto == castOther.proyecto);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.institucion ^ (this.institucion >>> 32)));
		hash = hash * prime + ((int) (this.programa ^ (this.programa >>> 32)));
		hash = hash * prime + ((int) (this.subPrograma ^ (this.subPrograma >>> 32)));
		hash = hash * prime + ((int) (this.proyecto ^ (this.proyecto >>> 32)));
		
		return hash;
	}
}