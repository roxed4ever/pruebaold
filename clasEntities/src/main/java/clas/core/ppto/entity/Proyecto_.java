package clas.core.ppto.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.528-0600")
@StaticMetamodel(Proyecto.class)
public class Proyecto_ {
	public static volatile SingularAttribute<Proyecto, ProyectoPK> id;
	public static volatile SingularAttribute<Proyecto, String> apiEstado;
	public static volatile SingularAttribute<Proyecto, String> apiTransaccion;
	public static volatile SingularAttribute<Proyecto, String> bip;
	public static volatile SingularAttribute<Proyecto, String> descProyecto;
	public static volatile SingularAttribute<Proyecto, BigDecimal> etapaDocumento;
	public static volatile SingularAttribute<Proyecto, Date> fecCre;
	public static volatile SingularAttribute<Proyecto, Date> fecMod;
	public static volatile SingularAttribute<Proyecto, BigDecimal> finalidad;
	public static volatile SingularAttribute<Proyecto, BigDecimal> funcion;
	public static volatile SingularAttribute<Proyecto, String> sinProduccion;
	public static volatile SingularAttribute<Proyecto, BigDecimal> ubigeoDep;
	public static volatile SingularAttribute<Proyecto, BigDecimal> ubigeoMun;
	public static volatile SingularAttribute<Proyecto, String> usuCre;
	public static volatile SingularAttribute<Proyecto, String> usuMod;
	public static volatile SingularAttribute<Proyecto, String> vigente;
	public static volatile ListAttribute<Proyecto, ActividadesObra> actividadesObras;
	public static volatile SingularAttribute<Proyecto, SubPrograma> subProgramaBean;
}
