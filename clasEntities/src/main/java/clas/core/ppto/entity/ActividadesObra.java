package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the ACTIVIDADES_OBRAS database table.
 * 
 */
@Entity
@Table(name = "ACTIVIDADES_OBRAS")
@NamedQuery(name = "ActividadesObra.findAll", query = "SELECT a FROM ActividadesObra a")

public class ActividadesObra implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ActividadesObraPK id;

	@Column(name = "API_ESTADO")
	private String apiEstado;

	@Column(name = "API_TRANSACCION")
	private String apiTransaccion;

	private Double componente;

	@Column(name = "DESC_ACTIVIDAD_OBRA")
	private String descActividadObra;

	@Column(name = "ETAPA_DOCUMENTO")
	private Double etapaDocumento;

	@Temporal(TemporalType.DATE)
	@Column(name = "FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name = "FEC_MOD")
	private Date fecMod;

	private Double indicador;

	private Double meta;

	private Double objetivo;

	@Column(name = "SIN_PRODUCCION")
	private String sinProduccion;

	@Column(name = "TIPO_ACTIVIDAD")
	private String tipoActividad;

	@Column(name = "TIPO_ADMINISTRACION")
	private String tipoAdministracion;

	@Column(name = "TIPO_AOB")
	private String tipoAob;

	@Column(name = "UBIGEO_DEP")
	private Double ubigeoDep;

	@Column(name = "UBIGEO_MUN")
	private Double ubigeoMun;

	@Column(name = "USU_CRE")
	private String usuCre;

	@Column(name = "USU_MOD")
	private String usuMod;

	private String vigente;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumns({
			@JoinColumn(name = "GESTION", referencedColumnName = "GESTION", insertable = false, updatable = false),
			@JoinColumn(name = "INSTITUCION", referencedColumnName = "INSTITUCION", insertable = false, updatable = false),
			@JoinColumn(name = "PROGRAMA", referencedColumnName = "PROGRAMA", insertable = false, updatable = false),
			@JoinColumn(name = "SUB_PROGRAMA", referencedColumnName = "SUB_PROGRAMA", insertable = false, updatable = false),
			@JoinColumn(name = "PROYECTO", referencedColumnName = "PROYECTO", insertable = false, updatable = false) })
	private Proyecto proyectoBean;

	public ActividadesObra() {
	}

	
	public ActividadesObra(ActividadesObraPK id, String apiEstado, String apiTransaccion, Double componente,
			String descActividadObra, Double etapaDocumento, Date fecCre, Date fecMod, Double indicador, Double meta,
			Double objetivo, String sinProduccion, String tipoActividad, String tipoAdministracion, String tipoAob,
			Double ubigeoDep, Double ubigeoMun, String usuCre, String usuMod, String vigente, Proyecto proyectoBean) {
		super();
		this.id = id;
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.componente = componente;
		this.descActividadObra = descActividadObra;
		this.etapaDocumento = etapaDocumento;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.indicador = indicador;
		this.meta = meta;
		this.objetivo = objetivo;
		this.sinProduccion = sinProduccion;
		this.tipoActividad = tipoActividad;
		this.tipoAdministracion = tipoAdministracion;
		this.tipoAob = tipoAob;
		this.ubigeoDep = ubigeoDep;
		this.ubigeoMun = ubigeoMun;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
		this.proyectoBean = proyectoBean;
	}

	
	public ActividadesObra(long gestion, long institucion, long programa, long subPrograma, long proyecto,
			long actividadObra, String apiEstado, String apiTransaccion, Double componente,
			String descActividadObra, Double etapaDocumento, Date fecCre, Date fecMod, Double indicador, Double meta,
			Double objetivo, String sinProduccion, String tipoActividad, String tipoAdministracion, String tipoAob,
			Double ubigeoDep, Double ubigeoMun, String usuCre, String usuMod, String vigente, Proyecto proyectoBean) {
		super();
		this.id = new ActividadesObraPK(gestion, institucion, programa, subPrograma, proyecto, actividadObra);
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.componente = componente;
		this.descActividadObra = descActividadObra;
		this.etapaDocumento = etapaDocumento;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.indicador = indicador;
		this.meta = meta;
		this.objetivo = objetivo;
		this.sinProduccion = sinProduccion;
		this.tipoActividad = tipoActividad;
		this.tipoAdministracion = tipoAdministracion;
		this.tipoAob = tipoAob;
		this.ubigeoDep = ubigeoDep;
		this.ubigeoMun = ubigeoMun;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
		this.proyectoBean = proyectoBean;
	}

	public ActividadesObraPK getId() {
		return this.id;
	}

	public void setId(ActividadesObraPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Double getComponente() {
		return this.componente;
	}

	public void setComponente(Double componente) {
		this.componente = componente;
	}

	public String getDescActividadObra() {
		return this.descActividadObra;
	}

	public void setDescActividadObra(String descActividadObra) {
		this.descActividadObra = descActividadObra;
	}

	public Double getEtapaDocumento() {
		return this.etapaDocumento;
	}

	public void setEtapaDocumento(Double etapaDocumento) {
		this.etapaDocumento = etapaDocumento;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getIndicador() {
		return this.indicador;
	}

	public void setIndicador(Double indicador) {
		this.indicador = indicador;
	}

	public Double getMeta() {
		return this.meta;
	}

	public void setMeta(Double meta) {
		this.meta = meta;
	}

	public Double getObjetivo() {
		return this.objetivo;
	}

	public void setObjetivo(Double objetivo) {
		this.objetivo = objetivo;
	}

	public String getSinProduccion() {
		return this.sinProduccion;
	}

	public void setSinProduccion(String sinProduccion) {
		this.sinProduccion = sinProduccion;
	}

	public String getTipoActividad() {
		return this.tipoActividad;
	}

	public void setTipoActividad(String tipoActividad) {
		this.tipoActividad = tipoActividad;
	}

	public String getTipoAdministracion() {
		return this.tipoAdministracion;
	}

	public void setTipoAdministracion(String tipoAdministracion) {
		this.tipoAdministracion = tipoAdministracion;
	}

	public String getTipoAob() {
		return this.tipoAob;
	}

	public void setTipoAob(String tipoAob) {
		this.tipoAob = tipoAob;
	}

	public Double getUbigeoDep() {
		return this.ubigeoDep;
	}

	public void setUbigeoDep(Double ubigeoDep) {
		this.ubigeoDep = ubigeoDep;
	}

	public Double getUbigeoMun() {
		return this.ubigeoMun;
	}

	public void setUbigeoMun(Double ubigeoMun) {
		this.ubigeoMun = ubigeoMun;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public Proyecto getProyectoBean() {
		return this.proyectoBean;
	}

	public void setProyectoBean(Proyecto proyectoBean) {
		this.proyectoBean = proyectoBean;
	}

}