package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PROGRAMAS database table.
 * 
 */
@Embeddable
public class ProgramaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	@Column(insertable=false, updatable=false)
	private long institucion;

	private long programa;

	public ProgramaPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getInstitucion() {
		return this.institucion;
	}
	public void setInstitucion(long institucion) {
		this.institucion = institucion;
	}
	public long getPrograma() {
		return this.programa;
	}
	public void setPrograma(long programa) {
		this.programa = programa;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ProgramaPK)) {
			return false;
		}
		ProgramaPK castOther = (ProgramaPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.institucion == castOther.institucion)
			&& (this.programa == castOther.programa);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.institucion ^ (this.institucion >>> 32)));
		hash = hash * prime + ((int) (this.programa ^ (this.programa >>> 32)));
		
		return hash;
	}
}