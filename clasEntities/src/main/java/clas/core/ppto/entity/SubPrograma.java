package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the SUB_PROGRAMAS database table.
 * 
 */
@Entity
@Table(name="SUB_PROGRAMAS")
@NamedQuery(name="SubPrograma.findAll", query="SELECT s FROM SubPrograma s")
public class SubPrograma implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SubProgramaPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_SUB_PROGRAMA")
	private String descSubPrograma;

	@Column(name="ETAPA_DOCUMENTO")
	private Double etapaDocumento;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="SIN_PRODUCCION")
	private String sinProduccion;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="id.gestion", referencedColumnName="GESTION", insertable = false, updatable = false),
		@JoinColumn(name="id.institucion", referencedColumnName="INSTITUCION", insertable = false, updatable = false),
		@JoinColumn(name="id.programa", referencedColumnName="PROGRAMA", insertable = false, updatable = false)
		})
	private Programa programaBean;

	public SubPrograma() {
	}

	
	public SubPrograma(long gestion, long institucion, long programa, long subPrograma, String apiEstado, String apiTransaccion, String descSubPrograma,
			Double etapaDocumento, Date fecCre, Date fecMod, String sinProduccion, String usuCre, String usuMod,
			String vigente) {
		super();
		this.id = new SubProgramaPK(gestion,institucion,programa,subPrograma);
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.descSubPrograma = descSubPrograma;
		this.etapaDocumento = etapaDocumento;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.sinProduccion = sinProduccion;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
	}


	public SubProgramaPK getId() {
		return this.id;
	}

	public void setId(SubProgramaPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescSubPrograma() {
		return this.descSubPrograma;
	}

	public void setDescSubPrograma(String descSubPrograma) {
		this.descSubPrograma = descSubPrograma;
	}

	public Double getEtapaDocumento() {
		return this.etapaDocumento;
	}

	public void setEtapaDocumento(Double etapaDocumento) {
		this.etapaDocumento = etapaDocumento;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getSinProduccion() {
		return this.sinProduccion;
	}

	public void setSinProduccion(String sinProduccion) {
		this.sinProduccion = sinProduccion;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public Proyecto addProyecto(Proyecto proyecto) {
		proyecto.setSubProgramaBean(this);

		return proyecto;
	}

	public Proyecto removeProyecto(Proyecto proyecto) {
		proyecto.setSubProgramaBean(null);

		return proyecto;
	}

	public Programa getProgramaBean() {
		return this.programaBean;
	}

	public void setProgramaBean(Programa programaBean) {
		this.programaBean = programaBean;
	}

}