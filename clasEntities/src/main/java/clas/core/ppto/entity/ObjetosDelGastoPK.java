package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the OBJETOS_DEL_GASTO database table.
 * 
 */
@Embeddable
public class ObjetosDelGastoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	private String objeto;

	public ObjetosDelGastoPK() {
	}
	
	
	public ObjetosDelGastoPK(long gestion, String objeto) {
		super();
		this.gestion = gestion;
		this.objeto = objeto;
	}


	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public String getObjeto() {
		return this.objeto;
	}
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ObjetosDelGastoPK)) {
			return false;
		}
		ObjetosDelGastoPK castOther = (ObjetosDelGastoPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& this.objeto.equals(castOther.objeto);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + this.objeto.hashCode();
		
		return hash;
	}
}