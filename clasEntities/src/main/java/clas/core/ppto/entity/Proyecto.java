package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PROYECTOS database table.
 * 
 */
@Entity
@Table(name="PROYECTOS")
@NamedQuery(name="Proyecto.findAll", query="SELECT p FROM Proyecto p")
@DiscriminatorValue("PROYECTOS")
public class Proyecto implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProyectoPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String bip;

	@Column(name="DESC_PROYECTO")
	private String descProyecto;

	@Column(name="ETAPA_DOCUMENTO")
	private Double etapaDocumento;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private Double finalidad;

	private Double funcion;

	@Column(name="SIN_PRODUCCION")
	private String sinProduccion;

	@Column(name="UBIGEO_DEP")
	private Double ubigeoDep;

	@Column(name="UBIGEO_MUN")
	private Double ubigeoMun;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;


	//bi-directional many-to-one association to SubPrograma
	@ManyToOne(optional = true)
	@JoinColumns({
		@JoinColumn (name="GESTION", referencedColumnName="GESTION", insertable = false, updatable = false),
		@JoinColumn (name="INSTITUCION", referencedColumnName="INSTITUCION", insertable = false, updatable = false),
		@JoinColumn (name="PROGRAMA", referencedColumnName="PROGRAMA", insertable = false, updatable = false),
		@JoinColumn (name="SUB_PROGRAMA", referencedColumnName="SUB_PROGRAMA", insertable = false, updatable = false)
		}) 
	private SubPrograma subProgramaBean;

	public Proyecto() {
	}
	
	public Proyecto(long gestion, long institucion, long programa, long subPrograma, long proyecto, 
			String apiEstado, String apiTransaccion, String bip, String descProyecto,
			Double etapaDocumento, Date fecCre, Date fecMod, Double finalidad, Double funcion, String sinProduccion,
			Double ubigeoDep, Double ubigeoMun, String usuCre, String usuMod, String vigente) {
		super();
		this.id = new ProyectoPK(gestion, institucion,programa, subPrograma, proyecto);
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.bip = bip;
		this.descProyecto = descProyecto;
		this.etapaDocumento = etapaDocumento;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.finalidad = finalidad;
		this.funcion = funcion;
		this.sinProduccion = sinProduccion;
		this.ubigeoDep = ubigeoDep;
		this.ubigeoMun = ubigeoMun;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
	}

	public Proyecto(ProyectoPK id, String apiEstado, String apiTransaccion, String bip, String descProyecto,
			Double etapaDocumento, Date fecCre, Date fecMod, Double finalidad, Double funcion, String sinProduccion,
			Double ubigeoDep, Double ubigeoMun, String usuCre, String usuMod, String vigente,
			SubPrograma subProgramaBean) {
		super();
		this.id = id;
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.bip = bip;
		this.descProyecto = descProyecto;
		this.etapaDocumento = etapaDocumento;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.finalidad = finalidad;
		this.funcion = funcion;
		this.sinProduccion = sinProduccion;
		this.ubigeoDep = ubigeoDep;
		this.ubigeoMun = ubigeoMun;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
		this.subProgramaBean = subProgramaBean;
	}



	public ProyectoPK getId() {
		return this.id;
	}

	public void setId(ProyectoPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getBip() {
		return this.bip;
	}

	public void setBip(String bip) {
		this.bip = bip;
	}

	public String getDescProyecto() {
		return this.descProyecto;
	}

	public void setDescProyecto(String descProyecto) {
		this.descProyecto = descProyecto;
	}

	public Double getEtapaDocumento() {
		return this.etapaDocumento;
	}

	public void setEtapaDocumento(Double etapaDocumento) {
		this.etapaDocumento = etapaDocumento;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getFinalidad() {
		return this.finalidad;
	}

	public void setFinalidad(Double finalidad) {
		this.finalidad = finalidad;
	}

	public Double getFuncion() {
		return this.funcion;
	}

	public void setFuncion(Double funcion) {
		this.funcion = funcion;
	}

	public String getSinProduccion() {
		return this.sinProduccion;
	}

	public void setSinProduccion(String sinProduccion) {
		this.sinProduccion = sinProduccion;
	}

	public Double getUbigeoDep() {
		return this.ubigeoDep;
	}

	public void setUbigeoDep(Double ubigeoDep) {
		this.ubigeoDep = ubigeoDep;
	}

	public Double getUbigeoMun() {
		return this.ubigeoMun;
	}

	public void setUbigeoMun(Double ubigeoMun) {
		this.ubigeoMun = ubigeoMun;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public ActividadesObra addActividadesObra(ActividadesObra actividadesObra) {
		actividadesObra.setProyectoBean(this);

		return actividadesObra;
	}

	public ActividadesObra removeActividadesObra(ActividadesObra actividadesObra) {
		actividadesObra.setProyectoBean(null);

		return actividadesObra;
	}

	public SubPrograma getSubProgramaBean() {
		return this.subProgramaBean;
	}

	public void setSubProgramaBean(SubPrograma subProgramaBean) {
		this.subProgramaBean = subProgramaBean;
	}

}