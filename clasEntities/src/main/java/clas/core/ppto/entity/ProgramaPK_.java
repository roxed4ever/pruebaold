package clas.core.ppto.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.528-0600")
@StaticMetamodel(ProgramaPK.class)
public class ProgramaPK_ {
	public static volatile SingularAttribute<ProgramaPK, Long> gestion;
	public static volatile SingularAttribute<ProgramaPK, Long> institucion;
	public static volatile SingularAttribute<ProgramaPK, Long> programa;
}
