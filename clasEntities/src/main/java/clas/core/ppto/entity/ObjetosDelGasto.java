package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the OBJETOS_DEL_GASTO database table.
 * 
 */
@Entity
@Table(name="OBJETOS_DEL_GASTO")
@NamedQuery(name="ObjetosDelGasto.findAll", query="SELECT o FROM ObjetosDelGasto o")
public class ObjetosDelGasto implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ObjetosDelGastoPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_OBJETO")
	private String descObjeto;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="GRUPO_OBJETO")
	private Double grupoObjeto;

	private String imputable;

	@Column(name="INST_EXCEPCION_ERP")
	private String instExcepcionErp;

	@Column(name="INST_EXCEPCION_MPR")
	private String instExcepcionMpr;

	@Column(name="NO_AUMENTA_ERP")
	private String noAumentaErp;

	@Column(name="NO_AUMENTA_MPR")
	private String noAumentaMpr;

	@Column(name="NO_DISMINUYE_ERP")
	private String noDisminuyeErp;

	@Column(name="NO_DISMINUYE_MPR")
	private String noDisminuyeMpr;

	@Column(name="PARTIDA_OBJETO")
	private Double partidaObjeto;

	@Column(name="SUB_GRUPO_OBJETO")
	private Double subGrupoObjeto;

	@Column(name="SUB_PARTIDA_OBJETO")
	private Double subPartidaObjeto;

	@Column(name="TRASLADO_INTEGRO_MPR")
	private String trasladoIntegroMpr;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;
	
	
	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "GESTION", referencedColumnName = "GESTION", insertable = false, updatable = false),
			@JoinColumn(name = "GRUPO_OBJETO", referencedColumnName = "GRUPO_OBJETO", insertable = false, updatable = false),
			@JoinColumn(name = "OBJETO", referencedColumnName = "OBJETO", insertable = false, updatable = false)})
	private VmObjetosClasesCip vmObjetosClasesCipBean;
	
	
	public ObjetosDelGasto() {
	}

	
	
	public ObjetosDelGasto(long gestion, String objeto, String apiEstado, String apiTransaccion, String descObjeto,
			Date fecCre, Date fecMod, Double grupoObjeto, String imputable, String instExcepcionErp,
			String instExcepcionMpr, String noAumentaErp, String noAumentaMpr, String noDisminuyeErp,
			String noDisminuyeMpr, Double partidaObjeto, Double subGrupoObjeto, Double subPartidaObjeto,
			String trasladoIntegroMpr, String usuCre, String usuMod, String vigente,
			VmObjetosClasesCip vmObjetosClasesCipBean) {
		super();
		this.id = new ObjetosDelGastoPK(gestion, descObjeto);
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.descObjeto = descObjeto;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.grupoObjeto = grupoObjeto;
		this.imputable = imputable;
		this.instExcepcionErp = instExcepcionErp;
		this.instExcepcionMpr = instExcepcionMpr;
		this.noAumentaErp = noAumentaErp;
		this.noAumentaMpr = noAumentaMpr;
		this.noDisminuyeErp = noDisminuyeErp;
		this.noDisminuyeMpr = noDisminuyeMpr;
		this.partidaObjeto = partidaObjeto;
		this.subGrupoObjeto = subGrupoObjeto;
		this.subPartidaObjeto = subPartidaObjeto;
		this.trasladoIntegroMpr = trasladoIntegroMpr;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
		this.vmObjetosClasesCipBean = vmObjetosClasesCipBean;
	}



	public ObjetosDelGastoPK getId() {
		return this.id;
	}

	public void setId(ObjetosDelGastoPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescObjeto() {
		return this.descObjeto;
	}

	public void setDescObjeto(String descObjeto) {
		this.descObjeto = descObjeto;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getGrupoObjeto() {
		return this.grupoObjeto;
	}

	public void setGrupoObjeto(Double grupoObjeto) {
		this.grupoObjeto = grupoObjeto;
	}

	public String getImputable() {
		return this.imputable;
	}

	public void setImputable(String imputable) {
		this.imputable = imputable;
	}

	public String getInstExcepcionErp() {
		return this.instExcepcionErp;
	}

	public void setInstExcepcionErp(String instExcepcionErp) {
		this.instExcepcionErp = instExcepcionErp;
	}

	public String getInstExcepcionMpr() {
		return this.instExcepcionMpr;
	}

	public void setInstExcepcionMpr(String instExcepcionMpr) {
		this.instExcepcionMpr = instExcepcionMpr;
	}

	public String getNoAumentaErp() {
		return this.noAumentaErp;
	}

	public void setNoAumentaErp(String noAumentaErp) {
		this.noAumentaErp = noAumentaErp;
	}

	public String getNoAumentaMpr() {
		return this.noAumentaMpr;
	}

	public void setNoAumentaMpr(String noAumentaMpr) {
		this.noAumentaMpr = noAumentaMpr;
	}

	public String getNoDisminuyeErp() {
		return this.noDisminuyeErp;
	}

	public void setNoDisminuyeErp(String noDisminuyeErp) {
		this.noDisminuyeErp = noDisminuyeErp;
	}

	public String getNoDisminuyeMpr() {
		return this.noDisminuyeMpr;
	}

	public void setNoDisminuyeMpr(String noDisminuyeMpr) {
		this.noDisminuyeMpr = noDisminuyeMpr;
	}

	public Double getPartidaObjeto() {
		return this.partidaObjeto;
	}

	public void setPartidaObjeto(Double partidaObjeto) {
		this.partidaObjeto = partidaObjeto;
	}

	public Double getSubGrupoObjeto() {
		return this.subGrupoObjeto;
	}

	public void setSubGrupoObjeto(Double subGrupoObjeto) {
		this.subGrupoObjeto = subGrupoObjeto;
	}

	public Double getSubPartidaObjeto() {
		return this.subPartidaObjeto;
	}

	public void setSubPartidaObjeto(Double subPartidaObjeto) {
		this.subPartidaObjeto = subPartidaObjeto;
	}

	public String getTrasladoIntegroMpr() {
		return this.trasladoIntegroMpr;
	}

	public void setTrasladoIntegroMpr(String trasladoIntegroMpr) {
		this.trasladoIntegroMpr = trasladoIntegroMpr;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

	public VmObjetosClasesCip getVmObjetosClasesCipBean() {
		return vmObjetosClasesCipBean;
	}

	public void setVmObjetosClasesCipBean(VmObjetosClasesCip vmObjetosClasesCipBean) {
		this.vmObjetosClasesCipBean = vmObjetosClasesCipBean;
	}

}