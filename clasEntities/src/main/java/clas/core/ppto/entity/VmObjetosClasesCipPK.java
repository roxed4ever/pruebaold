package clas.core.ppto.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class VmObjetosClasesCipPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="GESTION", insertable=false, updatable=false, table = "VM_OBJETOS_CLASES_CIP")
	private Double gestionVm;

	@Column(name="GRUPO_OBJETO", insertable=false, updatable=false, table = "VM_OBJETOS_CLASES_CIP")
	private String grupoObjetoVm;

	@Column(name="OBJETO", insertable=false, updatable=false, table = "VM_OBJETOS_CLASES_CIP")
	private String objetoVm;
	
	public VmObjetosClasesCipPK() {
		// TODO Auto-generated constructor stub
	}

	public Double getGestionVm() {
		return gestionVm;
	}

	public void setGestionVm(Double gestionVm) {
		this.gestionVm = gestionVm;
	}

	public String getGrupoObjetoVm() {
		return grupoObjetoVm;
	}

	public void setGrupoObjetoVm(String grupoObjetoVm) {
		this.grupoObjetoVm = grupoObjetoVm;
	}

	public String getObjetoVm() {
		return objetoVm;
	}

	public void setObjetoVm(String objetoVm) {
		this.objetoVm = objetoVm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gestionVm == null) ? 0 : gestionVm.hashCode());
		result = prime * result + ((grupoObjetoVm == null) ? 0 : grupoObjetoVm.hashCode());
		result = prime * result + ((objetoVm == null) ? 0 : objetoVm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VmObjetosClasesCipPK other = (VmObjetosClasesCipPK) obj;
		if (gestionVm == null) {
			if (other.gestionVm != null)
				return false;
		} else if (!gestionVm.equals(other.gestionVm))
			return false;
		if (grupoObjetoVm == null) {
			if (other.grupoObjetoVm != null)
				return false;
		} else if (!grupoObjetoVm.equals(other.grupoObjetoVm))
			return false;
		if (objetoVm == null) {
			if (other.objetoVm != null)
				return false;
		} else if (!objetoVm.equals(other.objetoVm))
			return false;
		return true;
	}

	
}
