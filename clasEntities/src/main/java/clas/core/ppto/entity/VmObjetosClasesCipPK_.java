package clas.core.ppto.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:04:25.731-0600")
@StaticMetamodel(VmObjetosClasesCipPK.class)
public class VmObjetosClasesCipPK_ {
	public static volatile SingularAttribute<VmObjetosClasesCipPK, Double> gestionVm;
	public static volatile SingularAttribute<VmObjetosClasesCipPK, String> grupoObjetoVm;
	public static volatile SingularAttribute<VmObjetosClasesCipPK, String> objetoVm;
}
