package clas.core.ppto.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PROGRAMAS database table.
 * 
 */
@Entity
@Table(name="PROGRAMAS")
@NamedQuery(name="Programa.findAll", query="SELECT p FROM Programa p")
public class Programa implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProgramaPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_PROGRAMA")
	private String descPrograma;

	@Column(name="ETAPA_DOCUMENTO")
	private Double etapaDocumento;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private Double finalidad;

	private Double funcion;

	@Column(name="SIN_PRODUCCION")
	private String sinProduccion;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;


	public Programa() {
	}

	public Programa(ProgramaPK id, String apiEstado, String apiTransaccion, String descPrograma, Double etapaDocumento,
			Date fecCre, Date fecMod, Double finalidad, Double funcion, String sinProduccion, String usuCre,
			String usuMod, String vigente) {
		super();
		this.id = id;
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.descPrograma = descPrograma;
		this.etapaDocumento = etapaDocumento;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.finalidad = finalidad;
		this.funcion = funcion;
		this.sinProduccion = sinProduccion;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
		this.vigente = vigente;
	}



	public ProgramaPK getId() {
		return this.id;
	}

	public void setId(ProgramaPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescPrograma() {
		return this.descPrograma;
	}

	public void setDescPrograma(String descPrograma) {
		this.descPrograma = descPrograma;
	}

	public Double getEtapaDocumento() {
		return this.etapaDocumento;
	}

	public void setEtapaDocumento(Double etapaDocumento) {
		this.etapaDocumento = etapaDocumento;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getFinalidad() {
		return this.finalidad;
	}

	public void setFinalidad(Double finalidad) {
		this.finalidad = finalidad;
	}

	public Double getFuncion() {
		return this.funcion;
	}

	public void setFuncion(Double funcion) {
		this.funcion = funcion;
	}

	public String getSinProduccion() {
		return this.sinProduccion;
	}

	public void setSinProduccion(String sinProduccion) {
		this.sinProduccion = sinProduccion;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}