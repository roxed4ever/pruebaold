package clas.core.ppto.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.528-0600")
@StaticMetamodel(SubProgramaPK.class)
public class SubProgramaPK_ {
	public static volatile SingularAttribute<SubProgramaPK, Long> gestion;
	public static volatile SingularAttribute<SubProgramaPK, Long> institucion;
	public static volatile SingularAttribute<SubProgramaPK, Long> programa;
	public static volatile SingularAttribute<SubProgramaPK, Long> subPrograma;
}
