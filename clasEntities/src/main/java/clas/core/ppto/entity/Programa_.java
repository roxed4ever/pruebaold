package clas.core.ppto.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.528-0600")
@StaticMetamodel(Programa.class)
public class Programa_ {

	public static volatile SingularAttribute<Programa, ProgramaPK> id;
	public static volatile SingularAttribute<Programa, String> apiEstado;
	public static volatile SingularAttribute<Programa, String> apiTransaccion;
	public static volatile SingularAttribute<Programa, String> descPrograma;
	public static volatile SingularAttribute<Programa, Double> etapaDocumento;
	public static volatile SingularAttribute<Programa, Date> fecCre;
	public static volatile SingularAttribute<Programa, Date> fecMod;
	public static volatile SingularAttribute<Programa, Double> finalidad;
	public static volatile SingularAttribute<Programa, Double> funcion;
	public static volatile SingularAttribute<Programa, String> sinProduccion;
	public static volatile SingularAttribute<Programa, Double> ubigeoDep;
	public static volatile SingularAttribute<Programa, Double> ubigeoMun;
	public static volatile SingularAttribute<Programa, String> usuCre;
	public static volatile SingularAttribute<Programa, String> usuMod;
	public static volatile SingularAttribute<Programa, String> vigente;

}
