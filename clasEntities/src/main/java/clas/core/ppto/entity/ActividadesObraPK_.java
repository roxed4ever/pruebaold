package clas.core.ppto.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.512-0600")
@StaticMetamodel(ActividadesObraPK.class)
public class ActividadesObraPK_ {
	public static volatile SingularAttribute<ActividadesObraPK, Long> gestion;
	public static volatile SingularAttribute<ActividadesObraPK, Long> institucion;
	public static volatile SingularAttribute<ActividadesObraPK, Long> programa;
	public static volatile SingularAttribute<ActividadesObraPK, Long> subPrograma;
	public static volatile SingularAttribute<ActividadesObraPK, Long> proyecto;
	public static volatile SingularAttribute<ActividadesObraPK, Long> actividadObra;
}
