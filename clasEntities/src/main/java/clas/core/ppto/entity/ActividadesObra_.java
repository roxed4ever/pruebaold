package clas.core.ppto.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.496-0600")
@StaticMetamodel(ActividadesObra.class)
public class ActividadesObra_ {
	public static volatile SingularAttribute<ActividadesObra, ActividadesObraPK> id;
	public static volatile SingularAttribute<ActividadesObra, String> apiEstado;
	public static volatile SingularAttribute<ActividadesObra, String> apiTransaccion;
	public static volatile SingularAttribute<ActividadesObra, Double> componente;
	public static volatile SingularAttribute<ActividadesObra, String> descActividadObra;
	public static volatile SingularAttribute<ActividadesObra, Double> etapaDocumento;
	public static volatile SingularAttribute<ActividadesObra, Date> fecCre;
	public static volatile SingularAttribute<ActividadesObra, Date> fecMod;
	public static volatile SingularAttribute<ActividadesObra, Double> indicador;
	public static volatile SingularAttribute<ActividadesObra, Double> meta;
	public static volatile SingularAttribute<ActividadesObra, Double> objetivo;
	public static volatile SingularAttribute<ActividadesObra, String> sinProduccion;
	public static volatile SingularAttribute<ActividadesObra, String> tipoActividad;
	public static volatile SingularAttribute<ActividadesObra, String> tipoAdministracion;
	public static volatile SingularAttribute<ActividadesObra, String> tipoAob;
	public static volatile SingularAttribute<ActividadesObra, Double> ubigeoDep;
	public static volatile SingularAttribute<ActividadesObra, Double> ubigeoMun;
	public static volatile SingularAttribute<ActividadesObra, String> usuCre;
	public static volatile SingularAttribute<ActividadesObra, String> usuMod;
	public static volatile SingularAttribute<ActividadesObra, String> vigente;
	public static volatile SingularAttribute<ActividadesObra, Proyecto> proyectoBean;
}
