package clas.core.ppto.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-29T11:23:36.528-0600")
@StaticMetamodel(ProyectoPK.class)
public class ProyectoPK_ {
	public static volatile SingularAttribute<ProyectoPK, Long> gestion;
	public static volatile SingularAttribute<ProyectoPK, Long> institucion;
	public static volatile SingularAttribute<ProyectoPK, Long> programa;
	public static volatile SingularAttribute<ProyectoPK, Long> subPrograma;
	public static volatile SingularAttribute<ProyectoPK, Long> proyecto;
}
