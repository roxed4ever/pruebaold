package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CLASES_DE_GASTO_SIP_DET database table.
 * 
 */
@Embeddable
public class ClasesDeGastoSipDetPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private long gestion;
	private long claseDeGasto;
	private String cuenta;

	public ClasesDeGastoSipDetPK() {
	}

	public ClasesDeGastoSipDetPK(long gestion, long claseDeGasto, String cuenta) {
		super();
		this.gestion = gestion;
		this.claseDeGasto = claseDeGasto;
		this.cuenta = cuenta;
	}

	@Column(insertable=false, updatable=false)
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}

	@Column(name="CLASE_DE_GASTO", insertable=false, updatable=false)
	public long getClaseDeGasto() {
		return this.claseDeGasto;
	}
	public void setClaseDeGasto(long claseDeGasto) {
		this.claseDeGasto = claseDeGasto;
	}

	@Column(insertable=false, updatable=false)
	public String getCuenta() {
		return this.cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ClasesDeGastoSipDetPK)) {
			return false;
		}
		ClasesDeGastoSipDetPK castOther = (ClasesDeGastoSipDetPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.claseDeGasto == castOther.claseDeGasto)
			&& this.cuenta.equals(castOther.cuenta);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.claseDeGasto ^ (this.claseDeGasto >>> 32)));
		hash = hash * prime + this.cuenta.hashCode();
		
		return hash;
	}
}