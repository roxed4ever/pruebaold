package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.991-0600")
@StaticMetamodel(FuentesFinanciamiento.class)
public class FuentesFinanciamiento_ {
	public static volatile SingularAttribute<FuentesFinanciamiento, Long> fuente;
	public static volatile SingularAttribute<FuentesFinanciamiento, String> apiEstado;
	public static volatile SingularAttribute<FuentesFinanciamiento, String> apiTransaccion;
	public static volatile SingularAttribute<FuentesFinanciamiento, String> descFuente;
	public static volatile SingularAttribute<FuentesFinanciamiento, Date> fecCre;
	public static volatile SingularAttribute<FuentesFinanciamiento, Date> fecMod;
	public static volatile SingularAttribute<FuentesFinanciamiento, Double> grupoFuente;
	public static volatile SingularAttribute<FuentesFinanciamiento, Double> subGrupoFuente;
	public static volatile SingularAttribute<FuentesFinanciamiento, String> usuCre;
	public static volatile SingularAttribute<FuentesFinanciamiento, String> usuMod;
	public static volatile SingularAttribute<FuentesFinanciamiento, String> vigente;
}
