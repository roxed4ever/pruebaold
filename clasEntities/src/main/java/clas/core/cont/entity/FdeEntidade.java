package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FDE_ENTIDADES database table.
 * 
 */
@Entity
@Table(name="FDE_ENTIDADES")
@NamedQuery(name="FdeEntidade.findAll", query="SELECT f FROM FdeEntidade f")
public class FdeEntidade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long entidad;

	@Column(name="API_CLIENTE")
	private String apiCliente;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String asignable;

	@Column(name="DESC_ENTIDAD")
	private String descEntidad;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private String naturaleza;

	@Column(name="NEW_ASIGNABLE")
	private String newAsignable;

	@Column(name="NEW_DESC_ENTIDAD")
	private String newDescEntidad;

	@Column(name="NEW_NATURALEZA")
	private String newNaturaleza;

	@Column(name="NEW_SECTOR")
	private Double newSector;

	@Column(name="NEW_SIGLA_ENTIDAD")
	private String newSiglaEntidad;

	@Column(name="NEW_VIGENTE")
	private String newVigente;

	private Double sector;

	@Column(name="SIGLA_ENTIDAD")
	private String siglaEntidad;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public FdeEntidade() {
	}

	public long getEntidad() {
		return this.entidad;
	}

	public void setEntidad(long entidad) {
		this.entidad = entidad;
	}

	public String getApiCliente() {
		return this.apiCliente;
	}

	public void setApiCliente(String apiCliente) {
		this.apiCliente = apiCliente;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getAsignable() {
		return this.asignable;
	}

	public void setAsignable(String asignable) {
		this.asignable = asignable;
	}

	public String getDescEntidad() {
		return this.descEntidad;
	}

	public void setDescEntidad(String descEntidad) {
		this.descEntidad = descEntidad;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getNaturaleza() {
		return this.naturaleza;
	}

	public void setNaturaleza(String naturaleza) {
		this.naturaleza = naturaleza;
	}

	public String getNewAsignable() {
		return this.newAsignable;
	}

	public void setNewAsignable(String newAsignable) {
		this.newAsignable = newAsignable;
	}

	public String getNewDescEntidad() {
		return this.newDescEntidad;
	}

	public void setNewDescEntidad(String newDescEntidad) {
		this.newDescEntidad = newDescEntidad;
	}

	public String getNewNaturaleza() {
		return this.newNaturaleza;
	}

	public void setNewNaturaleza(String newNaturaleza) {
		this.newNaturaleza = newNaturaleza;
	}

	public Double getNewSector() {
		return this.newSector;
	}

	public void setNewSector(Double newSector) {
		this.newSector = newSector;
	}

	public String getNewSiglaEntidad() {
		return this.newSiglaEntidad;
	}

	public void setNewSiglaEntidad(String newSiglaEntidad) {
		this.newSiglaEntidad = newSiglaEntidad;
	}

	public String getNewVigente() {
		return this.newVigente;
	}

	public void setNewVigente(String newVigente) {
		this.newVigente = newVigente;
	}

	public Double getSector() {
		return this.sector;
	}

	public void setSector(Double sector) {
		this.sector = sector;
	}

	public String getSiglaEntidad() {
		return this.siglaEntidad;
	}

	public void setSiglaEntidad(String siglaEntidad) {
		this.siglaEntidad = siglaEntidad;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}