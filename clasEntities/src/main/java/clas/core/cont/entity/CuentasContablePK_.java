package clas.core.cont.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.983-0600")
@StaticMetamodel(CuentasContablePK.class)
public class CuentasContablePK_ {
	public static volatile SingularAttribute<CuentasContablePK, Long> gestion;
	public static volatile SingularAttribute<CuentasContablePK, String> cuentaContable;
}
