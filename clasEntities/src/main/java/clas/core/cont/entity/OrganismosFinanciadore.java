package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ORGANISMOS_FINANCIADORES database table.
 * 
 */
@Entity
@Table(name="ORGANISMOS_FINANCIADORES")
@NamedQuery(name="OrganismosFinanciadore.findAll", query="SELECT o FROM OrganismosFinanciadore o")
public class OrganismosFinanciadore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long organismo;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_ORGANISMO")
	private String descOrganismo;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public OrganismosFinanciadore() {
	}

	public long getOrganismo() {
		return this.organismo;
	}

	public void setOrganismo(long organismo) {
		this.organismo = organismo;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescOrganismo() {
		return this.descOrganismo;
	}

	public void setDescOrganismo(String descOrganismo) {
		this.descOrganismo = descOrganismo;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}