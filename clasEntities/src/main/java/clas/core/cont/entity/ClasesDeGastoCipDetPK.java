package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CLASES_DE_GASTO_CIP_DET database table.
 * 
 */
@Embeddable
public class ClasesDeGastoCipDetPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private long gestion;
	private long claseDeGasto;
	private String objeto;

	public ClasesDeGastoCipDetPK() {
	}

	
	public ClasesDeGastoCipDetPK(long gestion, long claseDeGasto, String objeto) {
		super();
		this.gestion = gestion;
		this.claseDeGasto = claseDeGasto;
		this.objeto = objeto;
	}


	@Column(insertable=false, updatable=false)
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}

	@Column(name="CLASE_DE_GASTO", insertable=false, updatable=false)
	public long getClaseDeGasto() {
		return this.claseDeGasto;
	}
	public void setClaseDeGasto(long claseDeGasto) {
		this.claseDeGasto = claseDeGasto;
	}

	public String getObjeto() {
		return this.objeto;
	}
	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ClasesDeGastoCipDetPK)) {
			return false;
		}
		ClasesDeGastoCipDetPK castOther = (ClasesDeGastoCipDetPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.claseDeGasto == castOther.claseDeGasto)
			&& this.objeto.equals(castOther.objeto);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.claseDeGasto ^ (this.claseDeGasto >>> 32)));
		hash = hash * prime + this.objeto.hashCode();
		
		return hash;
	}
}