package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.967-0600")
@StaticMetamodel(ClasesDeGastoSip.class)
public class ClasesDeGastoSip_ {
	public static volatile SingularAttribute<ClasesDeGastoSip, ClasesDeGastoSipPK> id;
	public static volatile SingularAttribute<ClasesDeGastoSip, String> apiEstado;
	public static volatile SingularAttribute<ClasesDeGastoSip, String> apiTransaccion;
	public static volatile SingularAttribute<ClasesDeGastoSip, String> descClaseDeGasto;
	public static volatile SingularAttribute<ClasesDeGastoSip, Date> fecCre;
	public static volatile SingularAttribute<ClasesDeGastoSip, Date> fecMod;
	public static volatile SingularAttribute<ClasesDeGastoSip, String> usuCre;
	public static volatile SingularAttribute<ClasesDeGastoSip, String> usuMod;
}
