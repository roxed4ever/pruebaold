package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-04T18:03:35.163-0600")
@StaticMetamodel(ClasesDeGastoSipDet.class)
public class ClasesDeGastoSipDet_ {
	public static volatile SingularAttribute<ClasesDeGastoSipDet, ClasesDeGastoSipDetPK> id;
	public static volatile SingularAttribute<ClasesDeGastoSipDet, String> apiEstado;
	public static volatile SingularAttribute<ClasesDeGastoSipDet, String> apiTransaccion;
	public static volatile SingularAttribute<ClasesDeGastoSipDet, Date> fecCre;
	public static volatile SingularAttribute<ClasesDeGastoSipDet, Date> fecMod;
	public static volatile SingularAttribute<ClasesDeGastoSipDet, String> usuCre;
	public static volatile SingularAttribute<ClasesDeGastoSipDet, String> usuMod;
}
