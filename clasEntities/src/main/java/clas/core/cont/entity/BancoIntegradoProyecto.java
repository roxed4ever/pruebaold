package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the BANCO_INTEGRADO_PROYECTOS database table.
 * 
 */
@Entity
@Table(name="BANCO_INTEGRADO_PROYECTOS")
@NamedQuery(name="BancoIntegradoProyecto.findAll", query="SELECT b FROM BancoIntegradoProyecto b")
public class BancoIntegradoProyecto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String bip;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="AREA_SECTORIAL_IP")
	private Double areaSectorialIp;

	@Column(name="BIP_SNIPH")
	private Double bipSniph;

	@Column(name="DESC_BIP")
	private String descBip;

	@Column(name="ETAPA_BIP")
	private String etapaBip;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="NRO_CONVENIO")
	private String nroConvenio;

	private String principal;

	@Column(name="SECTOR_IP")
	private Double sectorIp;

	@Column(name="SUB_SECTOR_IP")
	private Double subSectorIp;

	@Column(name="TIPO_BIP")
	private String tipoBip;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public BancoIntegradoProyecto() {
	}

	public String getBip() {
		return this.bip;
	}

	public void setBip(String bip) {
		this.bip = bip;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Double getAreaSectorialIp() {
		return this.areaSectorialIp;
	}

	public void setAreaSectorialIp(Double areaSectorialIp) {
		this.areaSectorialIp = areaSectorialIp;
	}

	public Double getBipSniph() {
		return this.bipSniph;
	}

	public void setBipSniph(Double bipSniph) {
		this.bipSniph = bipSniph;
	}

	public String getDescBip() {
		return this.descBip;
	}

	public void setDescBip(String descBip) {
		this.descBip = descBip;
	}

	public String getEtapaBip() {
		return this.etapaBip;
	}

	public void setEtapaBip(String etapaBip) {
		this.etapaBip = etapaBip;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getNroConvenio() {
		return this.nroConvenio;
	}

	public void setNroConvenio(String nroConvenio) {
		this.nroConvenio = nroConvenio;
	}

	public String getPrincipal() {
		return this.principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public Double getSectorIp() {
		return this.sectorIp;
	}

	public void setSectorIp(Double sectorIp) {
		this.sectorIp = sectorIp;
	}

	public Double getSubSectorIp() {
		return this.subSectorIp;
	}

	public void setSubSectorIp(Double subSectorIp) {
		this.subSectorIp = subSectorIp;
	}

	public String getTipoBip() {
		return this.tipoBip;
	}

	public void setTipoBip(String tipoBip) {
		this.tipoBip = tipoBip;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}