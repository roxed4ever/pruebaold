package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.951-0600")
@StaticMetamodel(BancoIntegradoProyecto.class)
public class BancoIntegradoProyecto_ {
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> bip;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> apiEstado;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> apiTransaccion;
	public static volatile SingularAttribute<BancoIntegradoProyecto, Double> areaSectorialIp;
	public static volatile SingularAttribute<BancoIntegradoProyecto, Double> bipSniph;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> descBip;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> etapaBip;
	public static volatile SingularAttribute<BancoIntegradoProyecto, Date> fecCre;
	public static volatile SingularAttribute<BancoIntegradoProyecto, Date> fecMod;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> nroConvenio;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> principal;
	public static volatile SingularAttribute<BancoIntegradoProyecto, Double> sectorIp;
	public static volatile SingularAttribute<BancoIntegradoProyecto, Double> subSectorIp;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> tipoBip;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> usuCre;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> usuMod;
	public static volatile SingularAttribute<BancoIntegradoProyecto, String> vigente;
}
