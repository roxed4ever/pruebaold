package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CLASES_DE_GASTO_SIP_DET database table.
 * 
 */
@Entity
@Table(name="CLASES_DE_GASTO_SIP_DET")
@NamedQuery(name="ClasesDeGastoSipDet.findAll", query="SELECT c FROM ClasesDeGastoSipDet c")
public class ClasesDeGastoSipDet implements Serializable {
	private static final long serialVersionUID = 1L;
	private ClasesDeGastoSipDetPK id;
	private String apiEstado;
	private String apiTransaccion;
	private Date fecCre;
	private Date fecMod;
	private String usuCre;
	private String usuMod;

	public ClasesDeGastoSipDet() {
	}


	public ClasesDeGastoSipDet(ClasesDeGastoSipDetPK id, String apiEstado, String apiTransaccion, Date fecCre,
			Date fecMod, String usuCre, String usuMod) {
		super();
		this.id = id;
		this.apiEstado = apiEstado;
		this.apiTransaccion = apiTransaccion;
		this.fecCre = fecCre;
		this.fecMod = fecMod;
		this.usuCre = usuCre;
		this.usuMod = usuMod;
	}


	@EmbeddedId
	public ClasesDeGastoSipDetPK getId() {
		return this.id;
	}

	public void setId(ClasesDeGastoSipDetPK id) {
		this.id = id;
	}


	@Column(name="API_ESTADO")
	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}


	@Column(name="API_TRANSACCION")
	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}


	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}


	@Column(name="USU_CRE")
	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}


	@Column(name="USU_MOD")
	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}