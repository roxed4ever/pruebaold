package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FUENTES_FINANCIAMIENTO database table.
 * 
 */
@Entity
@Table(name="FUENTES_FINANCIAMIENTO")
@NamedQuery(name="FuentesFinanciamiento.findAll", query="SELECT f FROM FuentesFinanciamiento f")
public class FuentesFinanciamiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long fuente;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_FUENTE")
	private String descFuente;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="GRUPO_FUENTE")
	private Double grupoFuente;

	@Column(name="SUB_GRUPO_FUENTE")
	private Double subGrupoFuente;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public FuentesFinanciamiento() {
	}

	public long getFuente() {
		return this.fuente;
	}

	public void setFuente(long fuente) {
		this.fuente = fuente;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescFuente() {
		return this.descFuente;
	}

	public void setDescFuente(String descFuente) {
		this.descFuente = descFuente;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getGrupoFuente() {
		return this.grupoFuente;
	}

	public void setGrupoFuente(Double grupoFuente) {
		this.grupoFuente = grupoFuente;
	}

	public Double getSubGrupoFuente() {
		return this.subGrupoFuente;
	}

	public void setSubGrupoFuente(Double subGrupoFuente) {
		this.subGrupoFuente = subGrupoFuente;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}