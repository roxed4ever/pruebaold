package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.976-0600")
@StaticMetamodel(Convenio.class)
public class Convenio_ {
	public static volatile SingularAttribute<Convenio, String> nroConvenio;
	public static volatile SingularAttribute<Convenio, String> apiEstado;
	public static volatile SingularAttribute<Convenio, String> apiTransaccion;
	public static volatile SingularAttribute<Convenio, String> descConvenio;
	public static volatile SingularAttribute<Convenio, Date> fecCre;
	public static volatile SingularAttribute<Convenio, Date> fecMod;
	public static volatile SingularAttribute<Convenio, String> fondosNacionales;
	public static volatile SingularAttribute<Convenio, String> notaPrioridad;
	public static volatile SingularAttribute<Convenio, String> usuCre;
	public static volatile SingularAttribute<Convenio, String> usuMod;
}
