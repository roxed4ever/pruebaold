package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.955-0600")
@StaticMetamodel(ClasesDeGastoCip.class)
public class ClasesDeGastoCip_ {
	public static volatile SingularAttribute<ClasesDeGastoCip, ClasesDeGastoCipPK> id;
	public static volatile SingularAttribute<ClasesDeGastoCip, String> apiEstado;
	public static volatile SingularAttribute<ClasesDeGastoCip, String> apiTransaccion;
	public static volatile SingularAttribute<ClasesDeGastoCip, String> descClaseDeGasto;
	public static volatile SingularAttribute<ClasesDeGastoCip, Date> fecCre;
	public static volatile SingularAttribute<ClasesDeGastoCip, Date> fecMod;
	public static volatile SingularAttribute<ClasesDeGastoCip, String> usuCre;
	public static volatile SingularAttribute<ClasesDeGastoCip, String> usuMod;
}
