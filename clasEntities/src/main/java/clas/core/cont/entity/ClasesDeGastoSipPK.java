package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CLASES_DE_GASTO_SIP database table.
 * 
 */
@Embeddable
public class ClasesDeGastoSipPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	@Column(name="CLASE_DE_GASTO")
	private long claseDeGasto;

	public ClasesDeGastoSipPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getClaseDeGasto() {
		return this.claseDeGasto;
	}
	public void setClaseDeGasto(long claseDeGasto) {
		this.claseDeGasto = claseDeGasto;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ClasesDeGastoSipPK)) {
			return false;
		}
		ClasesDeGastoSipPK castOther = (ClasesDeGastoSipPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.claseDeGasto == castOther.claseDeGasto);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.claseDeGasto ^ (this.claseDeGasto >>> 32)));
		
		return hash;
	}
}