package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CUENTAS_CONTABLES database table.
 * 
 */
@Embeddable
public class CuentasContablePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long gestion;

	@Column(name="CUENTA_CONTABLE")
	private String cuentaContable;

	public CuentasContablePK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public String getCuentaContable() {
		return this.cuentaContable;
	}
	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CuentasContablePK)) {
			return false;
		}
		CuentasContablePK castOther = (CuentasContablePK)other;
		return 
			(this.gestion == castOther.gestion)
			&& this.cuentaContable.equals(castOther.cuentaContable);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + this.cuentaContable.hashCode();
		
		return hash;
	}
}