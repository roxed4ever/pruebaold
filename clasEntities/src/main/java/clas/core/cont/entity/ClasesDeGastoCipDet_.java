package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-04T18:03:35.149-0600")
@StaticMetamodel(ClasesDeGastoCipDet.class)
public class ClasesDeGastoCipDet_ {
	public static volatile SingularAttribute<ClasesDeGastoCipDet, ClasesDeGastoCipDetPK> id;
	public static volatile SingularAttribute<ClasesDeGastoCipDet, String> apiEstado;
	public static volatile SingularAttribute<ClasesDeGastoCipDet, String> apiTransaccion;
	public static volatile SingularAttribute<ClasesDeGastoCipDet, Date> fecCre;
	public static volatile SingularAttribute<ClasesDeGastoCipDet, Date> fecMod;
	public static volatile SingularAttribute<ClasesDeGastoCipDet, String> usuCre;
	public static volatile SingularAttribute<ClasesDeGastoCipDet, String> usuMod;
}
