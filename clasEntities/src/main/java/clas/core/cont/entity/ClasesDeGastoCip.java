package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
//import java.util.List;


/**
 * The persistent class for the CLASES_DE_GASTO_CIP database table.
 * 
 */
@Entity
@Table(name="CLASES_DE_GASTO_CIP")
@NamedQuery(name="ClasesDeGastoCip.findAll", query="SELECT c FROM ClasesDeGastoCip c")
public class ClasesDeGastoCip implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ClasesDeGastoCipPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_CLASE_DE_GASTO")
	private String descClaseDeGasto;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	//bi-directional many-to-one association to ClasesDeGastoCipDet
	//@OneToMany(mappedBy="clasesDeGastoCip")
	//private List<ClasesDeGastoCipDet> clasesDeGastoCipDets;

	public ClasesDeGastoCip() {
	}

	public ClasesDeGastoCipPK getId() {
		return this.id;
	}

	public void setId(ClasesDeGastoCipPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescClaseDeGasto() {
		return this.descClaseDeGasto;
	}

	public void setDescClaseDeGasto(String descClaseDeGasto) {
		this.descClaseDeGasto = descClaseDeGasto;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	/*public List<ClasesDeGastoCipDet> getClasesDeGastoCipDets() {
		return this.clasesDeGastoCipDets;
	}

	public void setClasesDeGastoCipDets(List<ClasesDeGastoCipDet> clasesDeGastoCipDets) {
		this.clasesDeGastoCipDets = clasesDeGastoCipDets;
	}

	public ClasesDeGastoCipDet addClasesDeGastoCipDet(ClasesDeGastoCipDet clasesDeGastoCipDet) {
		getClasesDeGastoCipDets().add(clasesDeGastoCipDet);
		clasesDeGastoCipDet.setClasesDeGastoCip(this);

		return clasesDeGastoCipDet;
	}

	public ClasesDeGastoCipDet removeClasesDeGastoCipDet(ClasesDeGastoCipDet clasesDeGastoCipDet) {
		getClasesDeGastoCipDets().remove(clasesDeGastoCipDet);
		clasesDeGastoCipDet.setClasesDeGastoCip(null);

		return clasesDeGastoCipDet;
	}
*/
}