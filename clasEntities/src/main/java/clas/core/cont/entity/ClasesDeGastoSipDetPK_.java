package clas.core.cont.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-04T18:03:35.165-0600")
@StaticMetamodel(ClasesDeGastoSipDetPK.class)
public class ClasesDeGastoSipDetPK_ {
	public static volatile SingularAttribute<ClasesDeGastoSipDetPK, Long> gestion;
	public static volatile SingularAttribute<ClasesDeGastoSipDetPK, Long> claseDeGasto;
	public static volatile SingularAttribute<ClasesDeGastoSipDetPK, String> cuenta;
}
