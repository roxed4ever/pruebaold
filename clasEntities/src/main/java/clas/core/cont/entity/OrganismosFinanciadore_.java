package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.993-0600")
@StaticMetamodel(OrganismosFinanciadore.class)
public class OrganismosFinanciadore_ {
	public static volatile SingularAttribute<OrganismosFinanciadore, Long> organismo;
	public static volatile SingularAttribute<OrganismosFinanciadore, String> apiEstado;
	public static volatile SingularAttribute<OrganismosFinanciadore, String> apiTransaccion;
	public static volatile SingularAttribute<OrganismosFinanciadore, String> descOrganismo;
	public static volatile SingularAttribute<OrganismosFinanciadore, Date> fecCre;
	public static volatile SingularAttribute<OrganismosFinanciadore, Date> fecMod;
	public static volatile SingularAttribute<OrganismosFinanciadore, String> usuCre;
	public static volatile SingularAttribute<OrganismosFinanciadore, String> usuMod;
	public static volatile SingularAttribute<OrganismosFinanciadore, String> vigente;
}
