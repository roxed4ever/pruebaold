package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.987-0600")
@StaticMetamodel(FdeEntidade.class)
public class FdeEntidade_ {
	public static volatile SingularAttribute<FdeEntidade, Long> entidad;
	public static volatile SingularAttribute<FdeEntidade, String> apiCliente;
	public static volatile SingularAttribute<FdeEntidade, String> apiEstado;
	public static volatile SingularAttribute<FdeEntidade, String> apiTransaccion;
	public static volatile SingularAttribute<FdeEntidade, String> asignable;
	public static volatile SingularAttribute<FdeEntidade, String> descEntidad;
	public static volatile SingularAttribute<FdeEntidade, Date> fecCre;
	public static volatile SingularAttribute<FdeEntidade, Date> fecMod;
	public static volatile SingularAttribute<FdeEntidade, String> naturaleza;
	public static volatile SingularAttribute<FdeEntidade, String> newAsignable;
	public static volatile SingularAttribute<FdeEntidade, String> newDescEntidad;
	public static volatile SingularAttribute<FdeEntidade, String> newNaturaleza;
	public static volatile SingularAttribute<FdeEntidade, Double> newSector;
	public static volatile SingularAttribute<FdeEntidade, String> newSiglaEntidad;
	public static volatile SingularAttribute<FdeEntidade, String> newVigente;
	public static volatile SingularAttribute<FdeEntidade, Double> sector;
	public static volatile SingularAttribute<FdeEntidade, String> siglaEntidad;
	public static volatile SingularAttribute<FdeEntidade, String> usuCre;
	public static volatile SingularAttribute<FdeEntidade, String> usuMod;
	public static volatile SingularAttribute<FdeEntidade, String> vigente;
}
