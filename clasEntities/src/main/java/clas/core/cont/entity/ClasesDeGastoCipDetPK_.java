package clas.core.cont.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-04T18:03:35.161-0600")
@StaticMetamodel(ClasesDeGastoCipDetPK.class)
public class ClasesDeGastoCipDetPK_ {
	public static volatile SingularAttribute<ClasesDeGastoCipDetPK, Long> gestion;
	public static volatile SingularAttribute<ClasesDeGastoCipDetPK, Long> claseDeGasto;
	public static volatile SingularAttribute<ClasesDeGastoCipDetPK, String> objeto;
}
