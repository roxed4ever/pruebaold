package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
//import java.util.List;


/**
 * The persistent class for the CLASES_DE_GASTO_SIP database table.
 * 
 */
@Entity
@Table(name="CLASES_DE_GASTO_SIP")
@NamedQuery(name="ClasesDeGastoSip.findAll", query="SELECT c FROM ClasesDeGastoSip c")
public class ClasesDeGastoSip implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ClasesDeGastoSipPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="DESC_CLASE_DE_GASTO")
	private String descClaseDeGasto;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	//bi-directional many-to-one association to ClasesDeGastoSipDet
	//@OneToMany(mappedBy="clasesDeGastoSip")
	//private List<ClasesDeGastoSipDet> clasesDeGastoSipDets;

	public ClasesDeGastoSip() {
	}

	public ClasesDeGastoSipPK getId() {
		return this.id;
	}

	public void setId(ClasesDeGastoSipPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getDescClaseDeGasto() {
		return this.descClaseDeGasto;
	}

	public void setDescClaseDeGasto(String descClaseDeGasto) {
		this.descClaseDeGasto = descClaseDeGasto;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	/*public List<ClasesDeGastoSipDet> getClasesDeGastoSipDets() {
		return this.clasesDeGastoSipDets;
	}

	public void setClasesDeGastoSipDets(List<ClasesDeGastoSipDet> clasesDeGastoSipDets) {
		this.clasesDeGastoSipDets = clasesDeGastoSipDets;
	}

	public ClasesDeGastoSipDet addClasesDeGastoSipDet(ClasesDeGastoSipDet clasesDeGastoSipDet) {
		getClasesDeGastoSipDets().add(clasesDeGastoSipDet);
		clasesDeGastoSipDet.setClasesDeGastoSip(this);

		return clasesDeGastoSipDet;
	}

	public ClasesDeGastoSipDet removeClasesDeGastoSipDet(ClasesDeGastoSipDet clasesDeGastoSipDet) {
		getClasesDeGastoSipDets().remove(clasesDeGastoSipDet);
		clasesDeGastoSipDet.setClasesDeGastoSip(null);

		return clasesDeGastoSipDet;
	}*/

}