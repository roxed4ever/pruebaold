package clas.core.cont.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CUENTAS_CONTABLES database table.
 * 
 */
@Entity
@Table(name="CUENTAS_CONTABLES")
@NamedQuery(name="CuentasContable.findAll", query="SELECT c FROM CuentasContable c")
public class CuentasContable implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CuentasContablePK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String apropiable;

	private String cierre;

	@Column(name="DESC_CUENTA_CONTABLE")
	private String descCuentaContable;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private String gasto;

	@Column(name="GLOSA_ACREDITA_POR")
	private String glosaAcreditaPor;

	@Column(name="GLOSA_DEBITA_POR")
	private String glosaDebitaPor;

	@Column(name="GLOSA_SALDO")
	private String glosaSaldo;

	private String ingreso;

	private String legalizable;

	@Column(name="NIC_ANTECESOR")
	private String nicAntecesor;

	@Column(name="NIC_CUENTA_CONTABLE")
	private String nicCuentaContable;

	@Column(name="NIC_SECUENCIA")
	private Double nicSecuencia;

	@Column(name="SIN_IMPUTACION")
	private String sinImputacion;

	@Column(name="TIPO_SALDO")
	private String tipoSaldo;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public CuentasContable() {
	}

	public CuentasContablePK getId() {
		return this.id;
	}

	public void setId(CuentasContablePK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getApropiable() {
		return this.apropiable;
	}

	public void setApropiable(String apropiable) {
		this.apropiable = apropiable;
	}

	public String getCierre() {
		return this.cierre;
	}

	public void setCierre(String cierre) {
		this.cierre = cierre;
	}

	public String getDescCuentaContable() {
		return this.descCuentaContable;
	}

	public void setDescCuentaContable(String descCuentaContable) {
		this.descCuentaContable = descCuentaContable;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getGasto() {
		return this.gasto;
	}

	public void setGasto(String gasto) {
		this.gasto = gasto;
	}

	public String getGlosaAcreditaPor() {
		return this.glosaAcreditaPor;
	}

	public void setGlosaAcreditaPor(String glosaAcreditaPor) {
		this.glosaAcreditaPor = glosaAcreditaPor;
	}

	public String getGlosaDebitaPor() {
		return this.glosaDebitaPor;
	}

	public void setGlosaDebitaPor(String glosaDebitaPor) {
		this.glosaDebitaPor = glosaDebitaPor;
	}

	public String getGlosaSaldo() {
		return this.glosaSaldo;
	}

	public void setGlosaSaldo(String glosaSaldo) {
		this.glosaSaldo = glosaSaldo;
	}

	public String getIngreso() {
		return this.ingreso;
	}

	public void setIngreso(String ingreso) {
		this.ingreso = ingreso;
	}

	public String getLegalizable() {
		return this.legalizable;
	}

	public void setLegalizable(String legalizable) {
		this.legalizable = legalizable;
	}

	public String getNicAntecesor() {
		return this.nicAntecesor;
	}

	public void setNicAntecesor(String nicAntecesor) {
		this.nicAntecesor = nicAntecesor;
	}

	public String getNicCuentaContable() {
		return this.nicCuentaContable;
	}

	public void setNicCuentaContable(String nicCuentaContable) {
		this.nicCuentaContable = nicCuentaContable;
	}

	public Double getNicSecuencia() {
		return this.nicSecuencia;
	}

	public void setNicSecuencia(Double nicSecuencia) {
		this.nicSecuencia = nicSecuencia;
	}

	public String getSinImputacion() {
		return this.sinImputacion;
	}

	public void setSinImputacion(String sinImputacion) {
		this.sinImputacion = sinImputacion;
	}

	public String getTipoSaldo() {
		return this.tipoSaldo;
	}

	public void setTipoSaldo(String tipoSaldo) {
		this.tipoSaldo = tipoSaldo;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}