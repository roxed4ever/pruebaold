package clas.core.cont.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-19T15:21:56.979-0600")
@StaticMetamodel(CuentasContable.class)
public class CuentasContable_ {
	public static volatile SingularAttribute<CuentasContable, CuentasContablePK> id;
	public static volatile SingularAttribute<CuentasContable, String> apiEstado;
	public static volatile SingularAttribute<CuentasContable, String> apiTransaccion;
	public static volatile SingularAttribute<CuentasContable, String> apropiable;
	public static volatile SingularAttribute<CuentasContable, String> cierre;
	public static volatile SingularAttribute<CuentasContable, String> descCuentaContable;
	public static volatile SingularAttribute<CuentasContable, Date> fecCre;
	public static volatile SingularAttribute<CuentasContable, Date> fecMod;
	public static volatile SingularAttribute<CuentasContable, String> gasto;
	public static volatile SingularAttribute<CuentasContable, String> glosaAcreditaPor;
	public static volatile SingularAttribute<CuentasContable, String> glosaDebitaPor;
	public static volatile SingularAttribute<CuentasContable, String> glosaSaldo;
	public static volatile SingularAttribute<CuentasContable, String> ingreso;
	public static volatile SingularAttribute<CuentasContable, String> legalizable;
	public static volatile SingularAttribute<CuentasContable, String> nicAntecesor;
	public static volatile SingularAttribute<CuentasContable, String> nicCuentaContable;
	public static volatile SingularAttribute<CuentasContable, Double> nicSecuencia;
	public static volatile SingularAttribute<CuentasContable, String> sinImputacion;
	public static volatile SingularAttribute<CuentasContable, String> tipoSaldo;
	public static volatile SingularAttribute<CuentasContable, String> usuCre;
	public static volatile SingularAttribute<CuentasContable, String> usuMod;
	public static volatile SingularAttribute<CuentasContable, String> vigente;
}
