package clas.core.tes.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.246-0600")
@StaticMetamodel(CbaCodigosRefExternoPK.class)
public class CbaCodigosRefExternoPK_ {
	public static volatile SingularAttribute<CbaCodigosRefExternoPK, Long> banco;
	public static volatile SingularAttribute<CbaCodigosRefExternoPK, Long> codigoReferencia;
	public static volatile SingularAttribute<CbaCodigosRefExternoPK, String> codigoRefInterno;
}
