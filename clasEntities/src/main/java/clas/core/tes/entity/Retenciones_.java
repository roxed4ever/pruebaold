package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.264-0600")
@StaticMetamodel(Retenciones.class)
public class Retenciones_ {
	public static volatile SingularAttribute<Retenciones, RetencionesPK> id;
	public static volatile SingularAttribute<Retenciones, String> apiEstado;
	public static volatile SingularAttribute<Retenciones, String> apiTransaccion;
	public static volatile SingularAttribute<Retenciones, String> bip;
	public static volatile SingularAttribute<Retenciones, String> codigoSigade;
	public static volatile SingularAttribute<Retenciones, String> cuentaContable;
	public static volatile SingularAttribute<Retenciones, String> descripcion;
	public static volatile SingularAttribute<Retenciones, Date> fecCre;
	public static volatile SingularAttribute<Retenciones, Date> fecMod;
	public static volatile SingularAttribute<Retenciones, Double> fuente;
	public static volatile SingularAttribute<Retenciones, Double> ga;
	public static volatile SingularAttribute<Retenciones, Double> institucion;
	public static volatile SingularAttribute<Retenciones, String> nroConvenio;
	public static volatile SingularAttribute<Retenciones, Double> organismo;
	public static volatile SingularAttribute<Retenciones, String> rubro;
	public static volatile SingularAttribute<Retenciones, Double> subTipo;
	public static volatile SingularAttribute<Retenciones, Double> tipo;
	public static volatile SingularAttribute<Retenciones, String> tipoFormulario;
	public static volatile SingularAttribute<Retenciones, Double> tramo;
	public static volatile SingularAttribute<Retenciones, Double> trfBenef;
	public static volatile SingularAttribute<Retenciones, String> usuCre;
	public static volatile SingularAttribute<Retenciones, String> usuMod;
}
