package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DEDUCCIONES database table.
 * 
 */
@Entity
@Table(name="DEDUCCIONES", schema = "SIAFI")
@NamedQuery(name="Deducciones.findAll", query="SELECT d FROM Deducciones d")
public class Deducciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long deduccion;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="BASE_CALCULO")
	private String baseCalculo;

	@Column(name="BASE_LEY")
	private String baseLey;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="MODO_CALCULO")
	private String modoCalculo;

	@Column(name="MONTO_FIJO")
	private Double montoFijo;

	@Column(name="MONTO_PORC")
	private Double montoPorc;

	private String neteable;

	private String prioridad;

	@Column(name="SUB_TIPO")
	private Double subTipo;

	private Double tipo;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	private String vigente;

	public Deducciones() {
	}

	public long getDeduccion() {
		return this.deduccion;
	}

	public void setDeduccion(long deduccion) {
		this.deduccion = deduccion;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getBaseCalculo() {
		return this.baseCalculo;
	}

	public void setBaseCalculo(String baseCalculo) {
		this.baseCalculo = baseCalculo;
	}

	public String getBaseLey() {
		return this.baseLey;
	}

	public void setBaseLey(String baseLey) {
		this.baseLey = baseLey;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getModoCalculo() {
		return this.modoCalculo;
	}

	public void setModoCalculo(String modoCalculo) {
		this.modoCalculo = modoCalculo;
	}

	public Double getMontoFijo() {
		return this.montoFijo;
	}

	public void setMontoFijo(Double montoFijo) {
		this.montoFijo = montoFijo;
	}

	public Double getMontoPorc() {
		return this.montoPorc;
	}

	public void setMontoPorc(Double montoPorc) {
		this.montoPorc = montoPorc;
	}

	public String getNeteable() {
		return this.neteable;
	}

	public void setNeteable(String neteable) {
		this.neteable = neteable;
	}

	public String getPrioridad() {
		return this.prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public Double getSubTipo() {
		return this.subTipo;
	}

	public void setSubTipo(Double subTipo) {
		this.subTipo = subTipo;
	}

	public Double getTipo() {
		return this.tipo;
	}

	public void setTipo(Double tipo) {
		this.tipo = tipo;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public String getVigente() {
		return this.vigente;
	}

	public void setVigente(String vigente) {
		this.vigente = vigente;
	}

}