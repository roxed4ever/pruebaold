package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-08-30T11:52:38.870-0600")
@StaticMetamodel(Moneda.class)
public class Moneda_ {
	public static volatile SingularAttribute<Moneda, String> moneda;
	public static volatile SingularAttribute<Moneda, String> apiEstado;
	public static volatile SingularAttribute<Moneda, String> apiTransaccion;
	public static volatile SingularAttribute<Moneda, String> descMoneda;
	public static volatile SingularAttribute<Moneda, Date> fecCre;
	public static volatile SingularAttribute<Moneda, Date> fecMod;
	public static volatile SingularAttribute<Moneda, String> usuCre;
	public static volatile SingularAttribute<Moneda, String> usuMod;
}
