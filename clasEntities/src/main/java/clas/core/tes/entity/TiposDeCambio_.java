package clas.core.tes.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.277-0600")
@StaticMetamodel(TiposDeCambio.class)
public class TiposDeCambio_ {
	public static volatile SingularAttribute<TiposDeCambio, TiposDeCambioPK> id;
	public static volatile SingularAttribute<TiposDeCambio, String> apiEstado;
	public static volatile SingularAttribute<TiposDeCambio, String> apiTransaccion;
	public static volatile SingularAttribute<TiposDeCambio, Date> fecCre;
	public static volatile SingularAttribute<TiposDeCambio, Date> fecMod;
	public static volatile SingularAttribute<TiposDeCambio, BigDecimal> montoCompra;
	public static volatile SingularAttribute<TiposDeCambio, BigDecimal> montoVenta;
	public static volatile SingularAttribute<TiposDeCambio, String> usuCre;
	public static volatile SingularAttribute<TiposDeCambio, String> usuMod;
	public static volatile SingularAttribute<TiposDeCambio, Moneda> monedaBean;
}
