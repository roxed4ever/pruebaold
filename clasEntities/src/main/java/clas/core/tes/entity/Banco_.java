package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.231-0600")
@StaticMetamodel(Banco.class)
public class Banco_ {
	public static volatile SingularAttribute<Banco, Long> banco;
	public static volatile SingularAttribute<Banco, String> apiEstado;
	public static volatile SingularAttribute<Banco, String> apiTransaccion;
	public static volatile SingularAttribute<Banco, Long> benBanco;
	public static volatile SingularAttribute<Banco, String> benCuenta;
	public static volatile SingularAttribute<Banco, String> benNroId;
	public static volatile SingularAttribute<Banco, String> benPaisId;
	public static volatile SingularAttribute<Banco, String> benTipoId;
	public static volatile SingularAttribute<Banco, String> conectadoSiafi;
	public static volatile SingularAttribute<Banco, String> cuentaEncaje;
	public static volatile SingularAttribute<Banco, String> descBanco;
	public static volatile SingularAttribute<Banco, Date> fecCre;
	public static volatile SingularAttribute<Banco, Date> fecMod;
	public static volatile SingularAttribute<Banco, String> habilitadoEnvios;
	public static volatile SingularAttribute<Banco, String> recipienteId;
	public static volatile SingularAttribute<Banco, String> siglaBanco;
	public static volatile SingularAttribute<Banco, String> tipoBanco;
	public static volatile SingularAttribute<Banco, String> usuCre;
	public static volatile SingularAttribute<Banco, String> usuMod;
}
