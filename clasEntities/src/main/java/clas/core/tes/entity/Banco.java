package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the BANCOS database table.
 * 
 */
@Entity
@Table(name="BANCOS", schema = "SIAFI")
@NamedQuery(name="Banco.findAll", query="SELECT b FROM Banco b")
public class Banco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long banco;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="BEN_BANCO")
	private Long benBanco;

	@Column(name="BEN_CUENTA")
	private String benCuenta;

	@Column(name="BEN_NRO_ID")
	private String benNroId;

	@Column(name="BEN_PAIS_ID")
	private String benPaisId;

	@Column(name="BEN_TIPO_ID")
	private String benTipoId;

	@Column(name="CONECTADO_SIAFI")
	private String conectadoSiafi;

	@Column(name="CUENTA_ENCAJE")
	private String cuentaEncaje;

	@Column(name="DESC_BANCO")
	private String descBanco;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="HABILITADO_ENVIOS")
	private String habilitadoEnvios;

	@Column(name="RECIPIENTE_ID")
	private String recipienteId;

	@Column(name="SIGLA_BANCO")
	private String siglaBanco;

	@Column(name="TIPO_BANCO")
	private String tipoBanco;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public Banco() {
	}

	public Long getBanco() {
		return this.banco;
	}

	public void setBanco(Long banco) {
		this.banco = banco;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Long getBenBanco() {
		return this.benBanco;
	}

	public void setBenBanco(Long benBanco) {
		this.benBanco = benBanco;
	}

	public String getBenCuenta() {
		return this.benCuenta;
	}

	public void setBenCuenta(String benCuenta) {
		this.benCuenta = benCuenta;
	}

	public String getBenNroId() {
		return this.benNroId;
	}

	public void setBenNroId(String benNroId) {
		this.benNroId = benNroId;
	}

	public String getBenPaisId() {
		return this.benPaisId;
	}

	public void setBenPaisId(String benPaisId) {
		this.benPaisId = benPaisId;
	}

	public String getBenTipoId() {
		return this.benTipoId;
	}

	public void setBenTipoId(String benTipoId) {
		this.benTipoId = benTipoId;
	}

	public String getConectadoSiafi() {
		return this.conectadoSiafi;
	}

	public void setConectadoSiafi(String conectadoSiafi) {
		this.conectadoSiafi = conectadoSiafi;
	}

	public String getCuentaEncaje() {
		return this.cuentaEncaje;
	}

	public void setCuentaEncaje(String cuentaEncaje) {
		this.cuentaEncaje = cuentaEncaje;
	}

	public String getDescBanco() {
		return this.descBanco;
	}

	public void setDescBanco(String descBanco) {
		this.descBanco = descBanco;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getHabilitadoEnvios() {
		return this.habilitadoEnvios;
	}

	public void setHabilitadoEnvios(String habilitadoEnvios) {
		this.habilitadoEnvios = habilitadoEnvios;
	}

	public String getRecipienteId() {
		return this.recipienteId;
	}

	public void setRecipienteId(String recipienteId) {
		this.recipienteId = recipienteId;
	}

	public String getSiglaBanco() {
		return this.siglaBanco;
	}

	public void setSiglaBanco(String siglaBanco) {
		this.siglaBanco = siglaBanco;
	}

	public String getTipoBanco() {
		return this.tipoBanco;
	}

	public void setTipoBanco(String tipoBanco) {
		this.tipoBanco = tipoBanco;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}