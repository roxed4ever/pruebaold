package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CBA_CODIGOS_REF_INTERNO database table.
 * 
 */
@Entity
@Table(name="CBA_CODIGOS_REF_INTERNO", schema = "SIAFI")
@NamedQuery(name="CbaCodigosRefInterno.findAll", query="SELECT c FROM CbaCodigosRefInterno c")
public class CbaCodigosRefInterno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODIGO_REFERENCIA")
	private String codigoReferencia;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="CONTRA_CODIGO")
	private String contraCodigo;

	@Column(name="DESC_CODIGO_REFERENCIA")
	private String descCodigoReferencia;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="GRUPO_CONCILIACION")
	private Double grupoConciliacion;

	@Column(name="REGISTRA_SOLO_LIBRETAS")
	private String registraSoloLibretas;

	@Column(name="SIGLA_CODIGO_REFERENCIA")
	private String siglaCodigoReferencia;

	@Column(name="TIPO_CODIGO")
	private String tipoCodigo;

	@Column(name="TIPO_CONCILIACION")
	private Double tipoConciliacion;

	@Column(name="TIPO_REGISTRO")
	private String tipoRegistro;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public CbaCodigosRefInterno() {
	}

	public String getCodigoReferencia() {
		return this.codigoReferencia;
	}

	public void setCodigoReferencia(String codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getContraCodigo() {
		return this.contraCodigo;
	}

	public void setContraCodigo(String contraCodigo) {
		this.contraCodigo = contraCodigo;
	}

	public String getDescCodigoReferencia() {
		return this.descCodigoReferencia;
	}

	public void setDescCodigoReferencia(String descCodigoReferencia) {
		this.descCodigoReferencia = descCodigoReferencia;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getGrupoConciliacion() {
		return this.grupoConciliacion;
	}

	public void setGrupoConciliacion(Double grupoConciliacion) {
		this.grupoConciliacion = grupoConciliacion;
	}

	public String getRegistraSoloLibretas() {
		return this.registraSoloLibretas;
	}

	public void setRegistraSoloLibretas(String registraSoloLibretas) {
		this.registraSoloLibretas = registraSoloLibretas;
	}

	public String getSiglaCodigoReferencia() {
		return this.siglaCodigoReferencia;
	}

	public void setSiglaCodigoReferencia(String siglaCodigoReferencia) {
		this.siglaCodigoReferencia = siglaCodigoReferencia;
	}

	public String getTipoCodigo() {
		return this.tipoCodigo;
	}

	public void setTipoCodigo(String tipoCodigo) {
		this.tipoCodigo = tipoCodigo;
	}

	public Double getTipoConciliacion() {
		return this.tipoConciliacion;
	}

	public void setTipoConciliacion(Double tipoConciliacion) {
		this.tipoConciliacion = tipoConciliacion;
	}

	public String getTipoRegistro() {
		return this.tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}