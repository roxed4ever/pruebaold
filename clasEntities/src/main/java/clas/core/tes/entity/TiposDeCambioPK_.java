package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.283-0600")
@StaticMetamodel(TiposDeCambioPK.class)
public class TiposDeCambioPK_ {
	public static volatile SingularAttribute<TiposDeCambioPK, Date> fecha;
	public static volatile SingularAttribute<TiposDeCambioPK, String> moneda;
}
