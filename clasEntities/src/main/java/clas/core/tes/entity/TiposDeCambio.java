package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the TIPOS_DE_CAMBIO database table.
 * 
 */
@Entity
@Table(name="TIPOS_DE_CAMBIO", schema = "SIAFI")
@NamedQuery(name="TiposDeCambio.findAll", query="SELECT t FROM TiposDeCambio t")
public class TiposDeCambio implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TiposDeCambioPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="MONTO_COMPRA")
	private BigDecimal montoCompra;

	@Column(name="MONTO_VENTA")
	private BigDecimal montoVenta;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	//bi-directional many-to-one association to Moneda
	@ManyToOne
	@JoinColumn(name="MONEDA")
	private Moneda monedaBean;

	public TiposDeCambio() {
	}

	public TiposDeCambioPK getId() {
		return this.id;
	}

	public void setId(TiposDeCambioPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public BigDecimal getMontoCompra() {
		return this.montoCompra;
	}

	public void setMontoCompra(BigDecimal montoCompra) {
		this.montoCompra = montoCompra;
	}

	public BigDecimal getMontoVenta() {
		return this.montoVenta;
	}

	public void setMontoVenta(BigDecimal montoVenta) {
		this.montoVenta = montoVenta;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public Moneda getMonedaBean() {
		return this.monedaBean;
	}

	public void setMonedaBean(Moneda monedaBean) {
		this.monedaBean = monedaBean;
	}

}