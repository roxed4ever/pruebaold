package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CBA_CODIGOS_REF_EXTERNO database table.
 * 
 */
@Entity
@Table(name="CBA_CODIGOS_REF_EXTERNO", schema = "SIAFI")
@NamedQuery(name="CbaCodigosRefExterno.findAll", query="SELECT c FROM CbaCodigosRefExterno c")
public class CbaCodigosRefExterno implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CbaCodigosRefExternoPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	@Column(name="CONTRA_CODIGO")
	private Double contraCodigo;

	@Column(name="DESC_CODIGO_REFERENCIA")
	private String descCodigoReferencia;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	@Column(name="SIGLA_CODIGO_REFERENCIA")
	private String siglaCodigoReferencia;

	@Column(name="TIPO_REGISTRO")
	private String tipoRegistro;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public CbaCodigosRefExterno() {
	}

	public CbaCodigosRefExternoPK getId() {
		return this.id;
	}

	public void setId(CbaCodigosRefExternoPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public Double getContraCodigo() {
		return this.contraCodigo;
	}

	public void setContraCodigo(Double contraCodigo) {
		this.contraCodigo = contraCodigo;
	}

	public String getDescCodigoReferencia() {
		return this.descCodigoReferencia;
	}

	public void setDescCodigoReferencia(String descCodigoReferencia) {
		this.descCodigoReferencia = descCodigoReferencia;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public String getSiglaCodigoReferencia() {
		return this.siglaCodigoReferencia;
	}

	public void setSiglaCodigoReferencia(String siglaCodigoReferencia) {
		this.siglaCodigoReferencia = siglaCodigoReferencia;
	}

	public String getTipoRegistro() {
		return this.tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}