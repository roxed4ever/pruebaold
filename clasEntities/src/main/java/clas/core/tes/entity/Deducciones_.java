package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.260-0600")
@StaticMetamodel(Deducciones.class)
public class Deducciones_ {
	public static volatile SingularAttribute<Deducciones, Long> deduccion;
	public static volatile SingularAttribute<Deducciones, String> apiEstado;
	public static volatile SingularAttribute<Deducciones, String> apiTransaccion;
	public static volatile SingularAttribute<Deducciones, String> baseCalculo;
	public static volatile SingularAttribute<Deducciones, String> baseLey;
	public static volatile SingularAttribute<Deducciones, String> descripcion;
	public static volatile SingularAttribute<Deducciones, Date> fecCre;
	public static volatile SingularAttribute<Deducciones, Date> fecMod;
	public static volatile SingularAttribute<Deducciones, String> modoCalculo;
	public static volatile SingularAttribute<Deducciones, Double> montoFijo;
	public static volatile SingularAttribute<Deducciones, Double> montoPorc;
	public static volatile SingularAttribute<Deducciones, String> neteable;
	public static volatile SingularAttribute<Deducciones, String> prioridad;
	public static volatile SingularAttribute<Deducciones, Double> subTipo;
	public static volatile SingularAttribute<Deducciones, Double> tipo;
	public static volatile SingularAttribute<Deducciones, String> usuCre;
	public static volatile SingularAttribute<Deducciones, String> usuMod;
	public static volatile SingularAttribute<Deducciones, String> vigente;
}
