package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.250-0600")
@StaticMetamodel(CbaCodigosRefInterno.class)
public class CbaCodigosRefInterno_ {
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> codigoReferencia;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> apiEstado;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> apiTransaccion;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> contraCodigo;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> descCodigoReferencia;
	public static volatile SingularAttribute<CbaCodigosRefInterno, Date> fecCre;
	public static volatile SingularAttribute<CbaCodigosRefInterno, Date> fecMod;
	public static volatile SingularAttribute<CbaCodigosRefInterno, Double> grupoConciliacion;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> registraSoloLibretas;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> siglaCodigoReferencia;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> tipoCodigo;
	public static volatile SingularAttribute<CbaCodigosRefInterno, Double> tipoConciliacion;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> tipoRegistro;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> usuCre;
	public static volatile SingularAttribute<CbaCodigosRefInterno, String> usuMod;
}
