package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TIPOS_DE_CAMBIO database table.
 * 
 */
@Embeddable
public class TiposDeCambioPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	private java.util.Date fecha;

	@Column(insertable=false, updatable=false)
	private String moneda;

	public TiposDeCambioPK() {
	}
	public java.util.Date getFecha() {
		return this.fecha;
	}
	public void setFecha(java.util.Date fecha) {
		this.fecha = fecha;
	}
	public String getMoneda() {
		return this.moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TiposDeCambioPK)) {
			return false;
		}
		TiposDeCambioPK castOther = (TiposDeCambioPK)other;
		return 
			this.fecha.equals(castOther.fecha)
			&& this.moneda.equals(castOther.moneda);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.fecha.hashCode();
		hash = hash * prime + this.moneda.hashCode();
		
		return hash;
	}
}