package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.237-0600")
@StaticMetamodel(CbaCodigosRefExterno.class)
public class CbaCodigosRefExterno_ {
	public static volatile SingularAttribute<CbaCodigosRefExterno, CbaCodigosRefExternoPK> id;
	public static volatile SingularAttribute<CbaCodigosRefExterno, String> apiEstado;
	public static volatile SingularAttribute<CbaCodigosRefExterno, String> apiTransaccion;
	public static volatile SingularAttribute<CbaCodigosRefExterno, Double> contraCodigo;
	public static volatile SingularAttribute<CbaCodigosRefExterno, String> descCodigoReferencia;
	public static volatile SingularAttribute<CbaCodigosRefExterno, Date> fecCre;
	public static volatile SingularAttribute<CbaCodigosRefExterno, Date> fecMod;
	public static volatile SingularAttribute<CbaCodigosRefExterno, String> siglaCodigoReferencia;
	public static volatile SingularAttribute<CbaCodigosRefExterno, String> tipoRegistro;
	public static volatile SingularAttribute<CbaCodigosRefExterno, String> usuCre;
	public static volatile SingularAttribute<CbaCodigosRefExterno, String> usuMod;
}
