package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CBA_CODIGOS_REF_EXTERNO database table.
 * 
 */
@Embeddable
public class CbaCodigosRefExternoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long banco;

	@Column(name="CODIGO_REFERENCIA")
	private long codigoReferencia;

	@Column(name="CODIGO_REF_INTERNO")
	private String codigoRefInterno;

	public CbaCodigosRefExternoPK() {
	}
	public long getBanco() {
		return this.banco;
	}
	public void setBanco(long banco) {
		this.banco = banco;
	}
	public long getCodigoReferencia() {
		return this.codigoReferencia;
	}
	public void setCodigoReferencia(long codigoReferencia) {
		this.codigoReferencia = codigoReferencia;
	}
	public String getCodigoRefInterno() {
		return this.codigoRefInterno;
	}
	public void setCodigoRefInterno(String codigoRefInterno) {
		this.codigoRefInterno = codigoRefInterno;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CbaCodigosRefExternoPK)) {
			return false;
		}
		CbaCodigosRefExternoPK castOther = (CbaCodigosRefExternoPK)other;
		return 
			(this.banco == castOther.banco)
			&& (this.codigoReferencia == castOther.codigoReferencia)
			&& this.codigoRefInterno.equals(castOther.codigoRefInterno);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.banco ^ (this.banco >>> 32)));
		hash = hash * prime + ((int) (this.codigoReferencia ^ (this.codigoReferencia >>> 32)));
		hash = hash * prime + this.codigoRefInterno.hashCode();
		
		return hash;
	}
}