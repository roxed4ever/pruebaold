package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the RETENCIONES database table.
 * 
 */
@Entity
@Table(name="RETENCIONES", schema = "SIAFI")
@NamedQuery(name="Retenciones.findAll", query="SELECT r FROM Retenciones r")
public class Retenciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RetencionesPK id;

	@Column(name="API_ESTADO")
	private String apiEstado;

	@Column(name="API_TRANSACCION")
	private String apiTransaccion;

	private String bip;

	@Column(name="CODIGO_SIGADE")
	private String codigoSigade;

	@Column(name="CUENTA_CONTABLE")
	private String cuentaContable;

	private String descripcion;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_CRE")
	private Date fecCre;

	@Temporal(TemporalType.DATE)
	@Column(name="FEC_MOD")
	private Date fecMod;

	private Double fuente;

	private Double ga;

	private Double institucion;

	@Column(name="NRO_CONVENIO")
	private String nroConvenio;

	private Double organismo;

	private String rubro;

	@Column(name="SUB_TIPO")
	private Double subTipo;

	private Double tipo;

	@Column(name="TIPO_FORMULARIO")
	private String tipoFormulario;

	private Double tramo;

	@Column(name="TRF_BENEF")
	private Double trfBenef;

	@Column(name="USU_CRE")
	private String usuCre;

	@Column(name="USU_MOD")
	private String usuMod;

	public Retenciones() {
	}

	public RetencionesPK getId() {
		return this.id;
	}

	public void setId(RetencionesPK id) {
		this.id = id;
	}

	public String getApiEstado() {
		return this.apiEstado;
	}

	public void setApiEstado(String apiEstado) {
		this.apiEstado = apiEstado;
	}

	public String getApiTransaccion() {
		return this.apiTransaccion;
	}

	public void setApiTransaccion(String apiTransaccion) {
		this.apiTransaccion = apiTransaccion;
	}

	public String getBip() {
		return this.bip;
	}

	public void setBip(String bip) {
		this.bip = bip;
	}

	public String getCodigoSigade() {
		return this.codigoSigade;
	}

	public void setCodigoSigade(String codigoSigade) {
		this.codigoSigade = codigoSigade;
	}

	public String getCuentaContable() {
		return this.cuentaContable;
	}

	public void setCuentaContable(String cuentaContable) {
		this.cuentaContable = cuentaContable;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecCre() {
		return this.fecCre;
	}

	public void setFecCre(Date fecCre) {
		this.fecCre = fecCre;
	}

	public Date getFecMod() {
		return this.fecMod;
	}

	public void setFecMod(Date fecMod) {
		this.fecMod = fecMod;
	}

	public Double getFuente() {
		return this.fuente;
	}

	public void setFuente(Double fuente) {
		this.fuente = fuente;
	}

	public Double getGa() {
		return this.ga;
	}

	public void setGa(Double ga) {
		this.ga = ga;
	}

	public Double getInstitucion() {
		return this.institucion;
	}

	public void setInstitucion(Double institucion) {
		this.institucion = institucion;
	}

	public String getNroConvenio() {
		return this.nroConvenio;
	}

	public void setNroConvenio(String nroConvenio) {
		this.nroConvenio = nroConvenio;
	}

	public Double getOrganismo() {
		return this.organismo;
	}

	public void setOrganismo(Double organismo) {
		this.organismo = organismo;
	}

	public String getRubro() {
		return this.rubro;
	}

	public void setRubro(String rubro) {
		this.rubro = rubro;
	}

	public Double getSubTipo() {
		return this.subTipo;
	}

	public void setSubTipo(Double subTipo) {
		this.subTipo = subTipo;
	}

	public Double getTipo() {
		return this.tipo;
	}

	public void setTipo(Double tipo) {
		this.tipo = tipo;
	}

	public String getTipoFormulario() {
		return this.tipoFormulario;
	}

	public void setTipoFormulario(String tipoFormulario) {
		this.tipoFormulario = tipoFormulario;
	}

	public Double getTramo() {
		return this.tramo;
	}

	public void setTramo(Double tramo) {
		this.tramo = tramo;
	}

	public Double getTrfBenef() {
		return this.trfBenef;
	}

	public void setTrfBenef(Double trfBenef) {
		this.trfBenef = trfBenef;
	}

	public String getUsuCre() {
		return this.usuCre;
	}

	public void setUsuCre(String usuCre) {
		this.usuCre = usuCre;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}