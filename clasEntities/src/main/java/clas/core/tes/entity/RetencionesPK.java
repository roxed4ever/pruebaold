package clas.core.tes.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the RETENCIONES database table.
 * 
 */
@Embeddable
public class RetencionesPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(insertable=false, updatable=false)
	private long gestion;

	private long retencion;

	public RetencionesPK() {
	}
	public long getGestion() {
		return this.gestion;
	}
	public void setGestion(long gestion) {
		this.gestion = gestion;
	}
	public long getRetencion() {
		return this.retencion;
	}
	public void setRetencion(long retencion) {
		this.retencion = retencion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof RetencionesPK)) {
			return false;
		}
		RetencionesPK castOther = (RetencionesPK)other;
		return 
			(this.gestion == castOther.gestion)
			&& (this.retencion == castOther.retencion);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.gestion ^ (this.gestion >>> 32)));
		hash = hash * prime + ((int) (this.retencion ^ (this.retencion >>> 32)));
		
		return hash;
	}
}