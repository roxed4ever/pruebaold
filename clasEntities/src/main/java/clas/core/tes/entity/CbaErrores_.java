package clas.core.tes.entity;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2019-09-20T15:05:39.255-0600")
@StaticMetamodel(CbaErrores.class)
public class CbaErrores_ {
	public static volatile SingularAttribute<CbaErrores, Long> error;
	public static volatile SingularAttribute<CbaErrores, String> apiEstado;
	public static volatile SingularAttribute<CbaErrores, String> apiTransaccion;
	public static volatile SingularAttribute<CbaErrores, String> descError;
	public static volatile SingularAttribute<CbaErrores, Date> fecCre;
	public static volatile SingularAttribute<CbaErrores, Date> fecMod;
	public static volatile SingularAttribute<CbaErrores, String> siglaError;
	public static volatile SingularAttribute<CbaErrores, String> tipoError;
	public static volatile SingularAttribute<CbaErrores, String> usuCre;
	public static volatile SingularAttribute<CbaErrores, String> usuMod;
}
