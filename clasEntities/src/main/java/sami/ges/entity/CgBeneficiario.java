package sami.ges.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the CG_BENEFICIARIO database table.
 * 
 */
@Entity
@Table(name="CG_BENEFICIARIO", schema="SAMI")
@NamedQuery(name="CgBeneficiario.findAll", query="SELECT c FROM CgBeneficiario c")
public class CgBeneficiario implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CgBeneficiarioPK id;

	private String apellido;

	@Column(name="COD_BANCO")
	private Long codBanco;

	private String codigo;

	@Column(name="CORREO_ELECTRONICO")
	private String correoElectronico;

	@Column(name="CUENTA_BANCARIA")
	private String cuentaBancaria;

	@Column(name="DEPARTAMENTO_ADMINISTRATIVO")
	private String departamentoAdministrativo;

	private String direccion;

	@Column(name="ES_GENERICO")
	private String esGenerico;

	private String estado;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_ESTADO")
	private Date fechaEstado;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_INGRESO")
	private Date fechaIngreso;

	@Column(name="ID_EMPLEADO_EXTERNO")
	private Long idEmpleadoExterno;

	@Column(name="ID_TIPO_BENEFICIARIO")
	private Long idTipoBeneficiario;

	@Column(name="ID_TIPO_CUENTA")
	private Long idTipoCuenta;

	private String ihss;

	@Column(name="NO_PLACA")
	private String noPlaca;

	private String nombre;

	@Column(name="NUMERO_IDENTIDAD")
	private String numeroIdentidad;

	private String puesto;

	private String rtn;

	private String situacion;

	private String telefono;

	public CgBeneficiario() {
	}

	public CgBeneficiarioPK getId() {
		return this.id;
	}

	public void setId(CgBeneficiarioPK id) {
		this.id = id;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Long getCodBanco() {
		return this.codBanco;
	}

	public void setCodBanco(Long codBanco) {
		this.codBanco = codBanco;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCorreoElectronico() {
		return this.correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getCuentaBancaria() {
		return this.cuentaBancaria;
	}

	public void setCuentaBancaria(String cuentaBancaria) {
		this.cuentaBancaria = cuentaBancaria;
	}

	public String getDepartamentoAdministrativo() {
		return this.departamentoAdministrativo;
	}

	public void setDepartamentoAdministrativo(String departamentoAdministrativo) {
		this.departamentoAdministrativo = departamentoAdministrativo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEsGenerico() {
		return this.esGenerico;
	}

	public void setEsGenerico(String esGenerico) {
		this.esGenerico = esGenerico;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaEstado() {
		return this.fechaEstado;
	}

	public void setFechaEstado(Date fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Long getIdEmpleadoExterno() {
		return this.idEmpleadoExterno;
	}

	public void setIdEmpleadoExterno(Long idEmpleadoExterno) {
		this.idEmpleadoExterno = idEmpleadoExterno;
	}

	public Long getIdTipoBeneficiario() {
		return this.idTipoBeneficiario;
	}

	public void setIdTipoBeneficiario(Long idTipoBeneficiario) {
		this.idTipoBeneficiario = idTipoBeneficiario;
	}

	public Long getIdTipoCuenta() {
		return this.idTipoCuenta;
	}

	public void setIdTipoCuenta(Long idTipoCuenta) {
		this.idTipoCuenta = idTipoCuenta;
	}

	public String getIhss() {
		return this.ihss;
	}

	public void setIhss(String ihss) {
		this.ihss = ihss;
	}

	public String getNoPlaca() {
		return this.noPlaca;
	}

	public void setNoPlaca(String noPlaca) {
		this.noPlaca = noPlaca;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroIdentidad() {
		return this.numeroIdentidad;
	}

	public void setNumeroIdentidad(String numeroIdentidad) {
		this.numeroIdentidad = numeroIdentidad;
	}

	public String getPuesto() {
		return this.puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getRtn() {
		return this.rtn;
	}

	public void setRtn(String rtn) {
		this.rtn = rtn;
	}

	public String getSituacion() {
		return this.situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public CgBeneficiario(long idBeneficiario, long idEntidad, long idUnidad, String apellido, Long codBanco, String codigo, String correoElectronico,
			String cuentaBancaria, String departamentoAdministrativo, String direccion, String esGenerico,
			String estado, Date fechaEstado, Date fechaIngreso, Long idEmpleadoExterno, Long idTipoBeneficiario,
			Long idTipoCuenta, String ihss, String noPlaca, String nombre, String numeroIdentidad, String puesto,
			String rtn, String situacion, String telefono) {
		super();
		CgBeneficiarioPK pk  = new CgBeneficiarioPK(idBeneficiario, idEntidad, idUnidad);
		this.id = pk;
		this.apellido = apellido;
		this.codBanco = codBanco;
		this.codigo = codigo;
		this.correoElectronico = correoElectronico;
		this.cuentaBancaria = cuentaBancaria;
		this.departamentoAdministrativo = departamentoAdministrativo;
		this.direccion = direccion;
		this.esGenerico = esGenerico;
		this.estado = estado;
		this.fechaEstado = fechaEstado;
		this.fechaIngreso = fechaIngreso;
		this.idEmpleadoExterno = idEmpleadoExterno;
		this.idTipoBeneficiario = idTipoBeneficiario;
		this.idTipoCuenta = idTipoCuenta;
		this.ihss = ihss;
		this.noPlaca = noPlaca;
		this.nombre = nombre;
		this.numeroIdentidad = numeroIdentidad;
		this.puesto = puesto;
		this.rtn = rtn;
		this.situacion = situacion;
		this.telefono = telefono;
	}
	
	

}