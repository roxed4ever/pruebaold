package sami.core.fpr.cc.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CC_CLASE_REGISTRO database table.
 * 
 */
@Entity
@Table(name="CC_CLASE_REGISTRO", schema="SAMI")
@NamedQuery(name="CcClaseRegistro.findAll", query="SELECT c FROM CcClaseRegistro c")
public class CcClaseRegistro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CLASE_REGISTRO")
	private String claseRegistro;

	@Column(name="CLASE_REGISTRO_PAGO")
	private String claseRegistroPago;

	private String descripcion;

	@Column(name="ID_NATURALEZA")
	private Long idNaturaleza;

	@Column(name="ORIGEN_TRANSACCION")
	private String origenTransaccion;

	private String restrictiva;

	public CcClaseRegistro() {
	}

	public String getClaseRegistro() {
		return this.claseRegistro;
	}

	public void setClaseRegistro(String claseRegistro) {
		this.claseRegistro = claseRegistro;
	}

	public String getClaseRegistroPago() {
		return this.claseRegistroPago;
	}

	public void setClaseRegistroPago(String claseRegistroPago) {
		this.claseRegistroPago = claseRegistroPago;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdNaturaleza() {
		return this.idNaturaleza;
	}

	public void setIdNaturaleza(Long idNaturaleza) {
		this.idNaturaleza = idNaturaleza;
	}

	public String getOrigenTransaccion() {
		return this.origenTransaccion;
	}

	public void setOrigenTransaccion(String origenTransaccion) {
		this.origenTransaccion = origenTransaccion;
	}

	public String getRestrictiva() {
		return this.restrictiva;
	}

	public void setRestrictiva(String restrictiva) {
		this.restrictiva = restrictiva;
	}

}