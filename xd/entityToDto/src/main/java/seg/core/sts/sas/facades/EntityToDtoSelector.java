package seg.core.sts.sas.facades;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.inject.Named;

import seg.core.sts.sas.routeBuilder.EntityToDtoQueueRouteBuilder;


@Named
public class EntityToDtoSelector {
	
	
	
	 private static int invoked=0;
	 
	public String seleccionarRuta(String tipo) {
		

		if(invoked==1) {
			invoked=0;
			return null;
		}	
		
		String destino =null;
		
		//SE DEBE IMPLEMENTAR GENERACION DINAMICA		
		if(tipo.equalsIgnoreCase("sasUsuario")  ) {
			destino = "jms:queue:SasUsuarioToDto";
		}else if( tipo.equalsIgnoreCase("SasAccesoPerfilDefaultDto") ) {
			destino = "jms:queue:SasAccesoPerfilDefaultDto";
		}else if( tipo.equalsIgnoreCase("SasPerfilDto") ) {
			destino = "jms:queue:SasPerfilDto";
		}
		
		
		
		
		invoked++;
		
		return destino;
		
	}
	
	
	

}
