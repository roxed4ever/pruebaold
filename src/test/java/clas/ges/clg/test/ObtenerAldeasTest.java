package clas.ges.clg.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;
import clas.core.tes.dto.MonedasDto;
import clas.core.tes.entity.Moneda;
import clas.ges.clg.dto.AldeaDto;
import clas.ges.clg.entity.Aldea;
import clas.ges.clg.service.ClgAldeaService;


@RunWith(Arquillian.class)
public class ObtenerAldeasTest {
	
	@Inject
	private ClgAldeaService serviceFacade;

	@Deployment
	public static Archive<?> createFileTest(){
		Archive<?> archive = ShrinkWrap.create(WebArchive.class	, "obtenerAldeasTest.war")
				.addPackage(Aldea.class.getPackage())
				.addPackage(AldeaDto.class.getPackage())
				.addAsResource("META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
		
		return archive;
		
	}
	
	
	@Test
	@InSequence(1)
	public void testObtenerClasificador() {
		
		AldeaDto aldeaDto = new AldeaDto();
		/*Service call*/
		List<Aldea> lista = serviceFacade.obtenerListaAldeas(aldeaDto);
		assertNotNull(lista);
		
	}
	
}
