package clas.ges.clg.service;

import java.util.List;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import clas.ges.clg.dto.AldeaDto;
import clas.ges.clg.entity.Aldea;
import clas.ges.clg.entity.AldeaPK_;
import clas.ges.clg.entity.Aldea_;
import fwk.core.clas.facade.AbstractFacade;

@Named
public class ClgAldeaService extends AbstractFacade<Aldea>{
 
	@PersistenceContext
	private EntityManager em;

	public ClgAldeaService() {
		super(Aldea.class);
	}
	
	private AldeaDto aldeaDto;

	@Override
    protected EntityManager getEntityManager() {
        return em;
    }
	
	@Override
	protected CriteriaQuery<Aldea> queryPreparation(CriteriaBuilder cb, CriteriaQuery<Aldea> cq, Root<Aldea> prog) {
		
		CriteriaQuery<Aldea> select = cq.select(prog);
		return select;
	}

	@Override
	public  Predicate predicateCreation(CriteriaBuilder cb, CriteriaQuery<Aldea> cq, Root<Aldea> root, Predicate predParam) { 
		if(aldeaDto!=null) {
			if(aldeaDto!=null && aldeaDto.getDEPARTAMENTO()!=null && aldeaDto.getDEPARTAMENTO() > 0)
				predParam = cb.and(predParam, cb.equal(root.get(Aldea_.id).get(AldeaPK_.departamento), aldeaDto.getDEPARTAMENTO()));
			
			if(aldeaDto!=null && aldeaDto.getMUNICIPIO()!=null && aldeaDto.getMUNICIPIO() > 0)
				predParam = cb.and(predParam, cb.equal(root.get(Aldea_.id).get(AldeaPK_.municipio), aldeaDto.getMUNICIPIO()));
			
			if(aldeaDto!=null && aldeaDto.getALDEA()!=null && aldeaDto.getALDEA() > 0)
				predParam = cb.and(predParam, cb.equal(root.get(Aldea_.id).get(AldeaPK_.aldea), aldeaDto.getALDEA()));
		}
		return predParam;
	}

	public List<Aldea> obtenerListaAldeas(AldeaDto aldeaDto) {
		this.aldeaDto = aldeaDto;
		List<Aldea> listaAldeas = getCustomQuery(aldeaDto.getAmbitosDto(),
				aldeaDto.getPAGINACION(), aldeaDto.getPROPERTY_MAP());
		return listaAldeas;
	}

}
