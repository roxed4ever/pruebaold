package clas.ges.clg.routeBuilder;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;

import clas.ges.clg.service.ClgAldeaService;

import javax.jms.ConnectionFactory;

@ApplicationScoped
@ContextName("clas.core.tes.ObtenerMonedasService.RB")
public class ObtenerAldeasRouteBuilder extends RouteBuilder {

	@Resource(mappedName = "java:jboss/DefaultJMSConnectionFactory")
	private ConnectionFactory connectionFactory;


	@Override
	public void configure() throws Exception {

		from("jms:queue:ClgRequestQueue?selector=ServiceType='obtenerAldeas'").log("leyendo mensaje de jms: ${body}")
				.to("direct:ObtenerAldeasRb");

		from("direct:ObtenerAldeasRb").log("leyendo mensaje de jms: ${body}")
				.bean(ClgAldeaService.class, "obtenerListaAldeas(${body})")
				.setHeader("tipoEntidad", constant("ClgAldeaList")).setExchangePattern(ExchangePattern.InOut)
				.to("jms:queue:EntityToDtoQueue").to("mock:resultado");
	}

}
